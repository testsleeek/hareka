package org.kareha.hareka.client.parser;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.field.PositionMemory;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class PositionMemoryParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final byte[] data = in.readByteArray();

		final FieldEntity entity = session.getMirrors().getSelfMirror().getFieldEntity();
		if (entity == null) {
			return;
		}
		final PositionMemory positionMemory = new PositionMemory(data, entity.getLocalId());
		session.getServer().addPositionMemory(positionMemory);
		session.getMirrors().getSystemMirror().handlePositionMemoryAdded(positionMemory);
	}

}
