package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class SetMarkPacket extends Packet {

	public SetMarkPacket() {

	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.SET_MARK;
	}

}
