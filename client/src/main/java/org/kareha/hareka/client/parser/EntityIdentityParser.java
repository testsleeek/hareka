package org.kareha.hareka.client.parser;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class EntityIdentityParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final LocalEntityId chatEntityId = LocalEntityId.readFrom(in);
		final LocalEntityId fieldEntityId = LocalEntityId.readFrom(in);

		session.getServer().addEntityIdentity(chatEntityId, fieldEntityId);
	}

}
