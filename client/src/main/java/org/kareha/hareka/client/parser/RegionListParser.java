package org.kareha.hareka.client.parser;

import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.field.Region;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class RegionListParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int size = in.readCompactUInt();
		final List<Region> regions = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			regions.add(Region.readFrom(in));
		}

		session.getMirrors().getFieldMirror().getField().setRegions(regions);
	}

}
