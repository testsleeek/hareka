package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;
import org.kareha.hareka.user.Role;

public final class DefineRolePacket extends Packet {

	public DefineRolePacket(final Role role) {
		out.write(role);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.DEFINE_ROLE;
	}

}
