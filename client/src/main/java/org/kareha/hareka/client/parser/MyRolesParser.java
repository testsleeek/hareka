package org.kareha.hareka.client.parser;

import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class MyRolesParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int size = in.readCompactUInt();
		final List<String> roles = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			roles.add(in.readString());
		}

		session.getMirrors().getAdminMirror().handleMyRoles(roles);
	}

}
