package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class IssueRoleTokenPacket extends Packet {

	public IssueRoleTokenPacket(final String roleId) {
		out.writeString(roleId);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.ISSUE_ROLE_TOKEN;
	}

}
