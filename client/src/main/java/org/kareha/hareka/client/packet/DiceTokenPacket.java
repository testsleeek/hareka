package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.DiceRoll;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class DiceTokenPacket extends Packet {

	public DiceTokenPacket(final DiceRoll diceRoll) {
		out.writeCompactUInt(diceRoll.getId());
		out.writeByteArray(diceRoll.getToken());
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.DICE_TOKEN;
	}

}
