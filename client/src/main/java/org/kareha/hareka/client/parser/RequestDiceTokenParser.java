package org.kareha.hareka.client.parser;

import java.util.logging.Logger;

import org.kareha.hareka.client.DiceRoll;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.packet.DiceTokenPacket;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class RequestDiceTokenParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(RequestDiceTokenParser.class.getName());

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int id = in.readCompactUInt();

		final DiceRoll diceRoll = session.getDiceRollTable().accept(id);
		if (diceRoll == null) {
			// TODO report error
			logger.warning("A nonexistent dice roll: id=" + id);
			return;
		}
		session.write(new DiceTokenPacket(diceRoll));
	}

}
