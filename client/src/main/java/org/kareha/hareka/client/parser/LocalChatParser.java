package org.kareha.hareka.client.parser;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class LocalChatParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final LocalEntityId id = LocalEntityId.readFrom(in);
		final String name = in.readString();
		final String content = in.readString();

		final ChatEntity entity = session.getMirrors().getEntityMirror().updateChatEntity(id, name);
		session.getMirrors().getChatMirror().getLocalChatSession().receiveChat(entity, content);
	}

}
