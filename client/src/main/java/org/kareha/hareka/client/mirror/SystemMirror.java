package org.kareha.hareka.client.mirror;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.kareha.hareka.LocalSessionId;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.field.PositionMemory;
import org.kareha.hareka.client.packet.RequestEchoPacket;
import org.kareha.hareka.wait.WaitTable;

public class SystemMirror implements AutoCloseable {

	public interface Listener {

		void versionChecked(boolean matched);

		void exitting(String message);

		void informed(String message);

		void messageArrived(String message);

		void textArrived(String text);

		void peerKeyArrived(PublicKey key);

		void tokenRequested(int handlerId, String message);

		void generatePow(int handlerId, BigInteger target, byte[] data, String message);

		void resourceServer(String host, int port, boolean connectSecurely, LocalSessionId sessionKey);

		void positionMemoryAdded(PositionMemory positionMemory);

	}

	public interface KeyPairProvider {

		KeyPair getKeyPair(int version);

	}

	private final List<Listener> listeners = new CopyOnWriteArrayList<>();
	private final WaitTable waitTable;
	private final ScheduledExecutorService pool;
	private final Runnable keepAliveTask;
	private volatile KeyPairProvider keyPairProvider;
	@GuardedBy("this")
	private Long requestEchoTime;
	@GuardedBy("this")
	private Integer minWaitDelay;
	@GuardedBy("this")
	private boolean echoStarted;

	private SystemMirror(final Session session, final WaitTable waitTable) {
		if (session == null || waitTable == null) {
			throw new IllegalArgumentException("null");
		}
		this.waitTable = waitTable;
		pool = Executors.newScheduledThreadPool(1);
		keepAliveTask = () -> {
			synchronized (this) {
				requestEchoTime = System.currentTimeMillis();
				session.write(new RequestEchoPacket());
			}
		};
	}

	public static SystemMirror newInstance(final Session session, final WaitTable waitTable) {
		return new SystemMirror(session, waitTable);
	}

	public void addListener(final Listener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(final Listener listener) {
		return listeners.remove(listener);
	}

	public Collection<Listener> getListeners() {
		return new ArrayList<>(listeners);
	}

	public synchronized void startEcho() {
		if (echoStarted) {
			return;
		}
		pool.scheduleAtFixedRate(keepAliveTask, 4, 8, TimeUnit.SECONDS);
		echoStarted = true;
	}

	@Override
	public void close() {
		pool.shutdownNow();
	}

	public void handleCheckVersion(final boolean matched) {
		for (final Listener listener : listeners) {
			listener.versionChecked(matched);
		}
	}

	public void handleExit(final String message) {
		for (final Listener listener : listeners) {
			listener.exitting(message);
		}
	}

	public void handleInform(final String message) {
		for (final Listener listener : listeners) {
			listener.informed(message);
		}
	}

	public void handleMessage(final String message) {
		for (final Listener listener : listeners) {
			listener.messageArrived(message);
		}
	}

	public void handleText(final String text) {
		for (final Listener listener : listeners) {
			listener.textArrived(text);
		}
	}

	public void handlePeerKeyArrived(final PublicKey key) {
		for (final Listener listener : listeners) {
			listener.peerKeyArrived(key);
		}
	}

	public void handleRequestToken(final int handlerId, final String message) {
		for (final Listener listener : listeners) {
			listener.tokenRequested(handlerId, message);
		}
	}

	public void handleRequestPow(final int handlerId, final BigInteger target, final byte[] data,
			final String message) {
		for (final Listener listener : listeners) {
			listener.generatePow(handlerId, target, data, message);
		}
	}

	public void setKeyPairProvider(final KeyPairProvider v) {
		keyPairProvider = v;
	}

	public KeyPair getKeyPair(final int version) {
		final KeyPairProvider kpp = keyPairProvider;
		if (kpp == null) {
			return null;
		}
		return kpp.getKeyPair(version);
	}

	public void handleResourceServer(final String host, final int port, final boolean connectSecurely,
			final LocalSessionId sessionKey) {
		for (final Listener listener : listeners) {
			listener.resourceServer(host, port, connectSecurely, sessionKey);
		}
	}

	public synchronized void handleEcho() {
		if (requestEchoTime == null) {
			return;
		}
		final int delay = (int) (System.currentTimeMillis() - requestEchoTime);
		final int tmp = delay * 7 / 8;
		final int d;
		if (tmp < 16) {
			d = 0;
		} else {
			d = tmp;
		}
		if (minWaitDelay == null) {
			minWaitDelay = d;
		} else if (d < minWaitDelay) {
			minWaitDelay = d;
		}
		waitTable.setDelay(minWaitDelay);
		requestEchoTime = null;
	}

	public void handlePositionMemoryAdded(final PositionMemory positionMemory) {
		for (final Listener listener : listeners) {
			listener.positionMemoryAdded(positionMemory);
		}
	}

}
