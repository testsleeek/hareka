package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class RemoveSpecialTilePacket extends Packet {

	public RemoveSpecialTilePacket() {

	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.REMOVE_SPECIAL_TILE;
	}

}
