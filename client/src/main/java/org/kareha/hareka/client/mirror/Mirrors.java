package org.kareha.hareka.client.mirror;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.wait.WaitTable;

public class Mirrors implements AutoCloseable {

	private final SystemMirror systemMirror;
	private final GroupMirror groupMirror;
	private final UserMirror userMirror;
	private final EntityMirror entityMirror;
	private final SelfMirror selfMirror;
	private final ChatMirror chatMirror;
	private final FieldMirror fieldMirror;
	private final AdminMirror adminMirror;
	private final AbilityMirror abilityMirror;
	private final InventoryMirror inventoryMirror;

	public Mirrors(final Session session) {
		final WaitTable waitTable = new WaitTable();
		systemMirror = SystemMirror.newInstance(session, waitTable);
		groupMirror = new GroupMirror();
		userMirror = new UserMirror();
		entityMirror = new EntityMirror(session);
		selfMirror = new SelfMirror(session, waitTable);
		chatMirror = new ChatMirror(session);
		fieldMirror = new FieldMirror();
		adminMirror = new AdminMirror();
		abilityMirror = new AbilityMirror(session, waitTable);
		inventoryMirror = new InventoryMirror(session, waitTable);
	}

	public SystemMirror getSystemMirror() {
		return systemMirror;
	}

	public GroupMirror getGroupMirror() {
		return groupMirror;
	}

	public UserMirror getUserMirror() {
		return userMirror;
	}

	public EntityMirror getEntityMirror() {
		return entityMirror;
	}

	public SelfMirror getSelfMirror() {
		return selfMirror;
	}

	public ChatMirror getChatMirror() {
		return chatMirror;
	}

	public FieldMirror getFieldMirror() {
		return fieldMirror;
	}

	public AdminMirror getAdminMirror() {
		return adminMirror;
	}

	public AbilityMirror getAbilityMirror() {
		return abilityMirror;
	}

	public InventoryMirror getInventoryMirror() {
		return inventoryMirror;
	}

	@Override
	public void close() {
		systemMirror.close();
	}

}
