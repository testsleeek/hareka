package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;
import org.kareha.hareka.user.UserRegistrationMode;

public final class SetUserRegistrationModePacket extends Packet {

	public SetUserRegistrationModePacket(final UserRegistrationMode mode) {
		out.write(mode);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.SET_USER_REGISTRATION_MODE;
	}

}
