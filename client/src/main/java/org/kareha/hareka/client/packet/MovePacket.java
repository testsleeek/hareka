package org.kareha.hareka.client.packet;

import org.kareha.hareka.field.Vector;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;
import org.kareha.hareka.wait.Wait;

public final class MovePacket extends Packet {

	public MovePacket(final Wait wait, final Vector position) {
		out.writeCompactULong(wait.getId());
		out.write(position);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.MOVE;
	}

}
