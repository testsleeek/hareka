package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class RequestRoleListPacket extends Packet {

	public RequestRoleListPacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.REQUEST_ROLE_LIST;
	}

}
