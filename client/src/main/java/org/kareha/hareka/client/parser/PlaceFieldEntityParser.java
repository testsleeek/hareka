package org.kareha.hareka.client.parser;

import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class PlaceFieldEntityParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(PlaceFieldEntityParser.class.getName());

	@Override
	public void handle(final PacketInput in, final Session session) {
		final LocalEntityId id = LocalEntityId.readFrom(in);
		final Placement placement = Placement.readFrom(in);
		final int motionWait = in.readCompactUInt();

		final FieldEntity entity = session.getMirrors().getEntityMirror().getFieldEntity(id);
		if (entity == null) {
			logger.warning("A nonexistent field entity placed: id=" + id + " placement=" + placement + " motionWait="
					+ motionWait);
			return;
		}
		entity.place(placement, motionWait);
	}

}
