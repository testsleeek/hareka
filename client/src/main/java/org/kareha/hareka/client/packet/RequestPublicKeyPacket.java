package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class RequestPublicKeyPacket extends Packet {

	public RequestPublicKeyPacket(final int handlerId, final int version, final byte[] nonce) {
		out.writeCompactUInt(handlerId);
		out.writeCompactUInt(version);
		out.writeByteArray(nonce);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.REQUEST_PUBLIC_KEY;
	}

}
