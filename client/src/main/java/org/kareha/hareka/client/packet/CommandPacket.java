package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class CommandPacket extends Packet {

	public CommandPacket(final String[] args) {
		out.writeCompactUInt(args.length);
		for (int i = 0; i < args.length; i++) {
			out.writeString(args[i]);
		}
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.COMMAND;
	}

}
