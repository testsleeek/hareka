package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class DeleteRoleTokenPacket extends Packet {

	public DeleteRoleTokenPacket(final long roleTokenId) {
		out.writeCompactULong(roleTokenId);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.DELETE_ROLE_TOKEN;
	}

}
