package org.kareha.hareka.client.parser;

import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.mirror.UserMirror;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class CharactersParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int count = in.readCompactUInt();
		final List<UserMirror.CharacterEntry> entries = new ArrayList<>();
		for (int i = 0; i < count; i++) {
			final LocalEntityId id = LocalEntityId.readFrom(in);
			final String name = in.readString();
			final String shape = in.readString();
			entries.add(new UserMirror.CharacterEntry(id, name, shape));
		}

		session.getMirrors().getUserMirror().handleLoadCharacters(session, entries);
	}

}
