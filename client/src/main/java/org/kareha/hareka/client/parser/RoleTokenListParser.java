package org.kareha.hareka.client.parser;

import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.LocalKeyId;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.user.RoleToken;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class RoleTokenListParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int size = in.readCompactUInt();
		final List<RoleToken> roleTokens = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			final long id = in.readCompactULong();
			final long issuedDate = in.readCompactLong();
			final LocalKeyId issuer = LocalKeyId.readFrom(in);
			final String roleId = in.readString();
			final RoleToken token = new RoleToken(id, issuedDate, issuer, roleId);
			roleTokens.add(token);
		}

		session.getMirrors().getAdminMirror().handleRoleTokenList(roleTokens);
	}

}
