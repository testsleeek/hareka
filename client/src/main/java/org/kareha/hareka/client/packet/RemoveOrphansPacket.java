package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class RemoveOrphansPacket extends Packet {

	public RemoveOrphansPacket() {

	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.REMOVE_ORPHANS;
	}

}
