package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class GetPositionMemoryPacket extends Packet {

	public GetPositionMemoryPacket() {

	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.GET_POSITION_MEMORY;
	}

}
