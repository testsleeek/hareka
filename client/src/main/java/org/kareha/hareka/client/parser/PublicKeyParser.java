package org.kareha.hareka.client.parser;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.kareha.hareka.Constants;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.key.KeyGrabber;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class PublicKeyParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(PublicKeyParser.class.getName());

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int handlerId = in.readCompactUInt();
		final int version = in.readCompactUInt();
		Key key = null;
		try {
			key = in.readKey();
		} catch (final NoSuchAlgorithmException | InvalidKeySpecException e) {
			logger.log(Level.SEVERE, "Invalid key", e);
		}
		final String signatureAlgorithm = in.readString();
		final byte[] signature = in.readByteArray();

		final KeyGrabber.Entry entry = session.getKeyGrabber().get(handlerId);
		if (entry == null) {
			// TODO handle possible attack
			return;
		}

		if (key == null) {
			// have been informed above
			return;
		}

		if (!Arrays.asList(Constants.getAcceptablePublicKeyAlgorithms()).contains(key.getAlgorithm())) {
			logger.severe("Algorithm not acceptable: " + key.getAlgorithm());
			return;
		}
		if (!Arrays.asList(Constants.getAcceptablePublicKeyFormats()).contains(key.getFormat())) {
			logger.severe("Format not acceptable: " + key.getFormat());
			return;
		}

		if (!(key instanceof PublicKey)) {
			logger.severe("Invalid key"); // TODO make informative
			return;
		}
		final PublicKey publicKey = (PublicKey) key;

		if (!entry.verify(publicKey, signatureAlgorithm, signature)) {
			logger.warning("Invalid signature"); // TODO make informative
			return;
		}
		entry.handle(version, publicKey);
	}

}
