package org.kareha.hareka.client.parser;

import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.field.ClientField;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.field.StatBar;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class AddFieldEntityParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(AddFieldEntityParser.class.getName());

	@Override
	public void handle(final PacketInput in, final Session session) {
		final LocalEntityId id = LocalEntityId.readFrom(in);
		final Placement placement = Placement.readFrom(in);
		final String shape = in.readString();
		final boolean healthAlive = in.readBoolean();
		final int healthBar = in.readByte();
		final boolean magicAlive = in.readBoolean();
		final int magicBar = in.readByte();
		final long count = in.readCompactULong();

		final ClientField field = session.getMirrors().getFieldMirror().getField();
		final FieldEntity entity = new FieldEntity(id, field, placement, shape, count);
		entity.setHealthBar(new StatBar(healthAlive, healthBar));
		entity.setMagicBar(new StatBar(magicAlive, magicBar));
		if (!session.getMirrors().getEntityMirror().addFieldEntity(entity)) {
			logger.warning("An already existent field entity has been added: " + id);
		}
	}

}
