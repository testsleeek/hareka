package org.kareha.hareka.client.packet;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class DropItemPacket extends Packet {

	public DropItemPacket(final LocalEntityId itemId, final long count) {
		out.write(itemId);
		out.writeCompactULong(count);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.DROP_ITEM;
	}

}
