package org.kareha.hareka.client;

import java.io.File;
import java.io.IOException;
import java.security.Key;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.Version;
import org.kareha.hareka.client.server.Servers;
import org.kareha.hareka.key.KeyId;
import org.kareha.hareka.key.KeyIdPersistentFormat;
import org.kareha.hareka.key.KeyPersistentFormat;
import org.kareha.hareka.key.KeyStack;
import org.kareha.hareka.key.KeyStatic;
import org.kareha.hareka.key.SimpleKeyStore;
import org.kareha.hareka.persistent.FastPersistentHashTable;
import org.kareha.hareka.persistent.PersistentHashTable;

public class Context {

	private final Version version;
	private final File dataDirectory;
	private final KeyStack keyStack;
	private final SimpleKeyStore simpleKeyStore;
	private final ConnectionSettings connectionSettings;
	private final KeyStatic serverStatic;
	private final PersistentHashTable<Key, KeyId> keyIndex;
	private final Servers servers;

	// version can be null for fake
	// dataDirectory can be null for fake
	public Context(final Version version, final File dataDirectory) throws IOException, JAXBException {
		this.version = version;
		this.dataDirectory = dataDirectory;
		keyStack = new KeyStack(new File(dataDirectory, "KeyStack.xml"));
		simpleKeyStore = new SimpleKeyStore(new File(dataDirectory, "KeyStore"));
		connectionSettings = new ConnectionSettings(new File(dataDirectory, "ConnectionSettings.xml"));
		serverStatic = new KeyStatic(new File(dataDirectory, "ServerStatic.xml"));
		keyIndex = new FastPersistentHashTable<>(new File(dataDirectory, "KeyIndex.dat"), new KeyPersistentFormat(),
				new KeyIdPersistentFormat());
		servers = new Servers(new File(dataDirectory, "server"), serverStatic, keyIndex);
	}

	public Version getVersion() {
		return version;
	}

	public File getDataDirectory() {
		return dataDirectory;
	}

	public KeyStack getKeyStack() {
		return keyStack;
	}

	public SimpleKeyStore getSimpleKeyStore() {
		return simpleKeyStore;
	}

	public ConnectionSettings getConnectionSettings() {
		return connectionSettings;
	}

	public Servers getServers() {
		return servers;
	}

	public void save() throws JAXBException {
		serverStatic.save();
	}

}
