package org.kareha.hareka.client.parser;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class FieldParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int viewSize = in.readCompactUInt();

		session.getMirrors().getSelfMirror().setFieldEntity(null);
		session.getMirrors().getFieldMirror().newField(viewSize);
	}

}
