package org.kareha.hareka.client.packet;

import java.util.Collection;

import org.kareha.hareka.field.Region;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class SetRegionListPacket extends Packet {

	public SetRegionListPacket(final Collection<Region> regions) {
		out.writeCompactUInt(regions.size());
		for (final Region region : regions) {
			out.write(region);
		}
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.SET_REGION_LIST;
	}

}
