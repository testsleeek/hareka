package org.kareha.hareka.client.parser;

import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class TilesParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int size = in.readCompactUInt();
		final List<TilePiece> list = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			final TilePiece tilePiece = TilePiece.readFrom(in);
			list.add(tilePiece);
		}

		session.getMirrors().getFieldMirror().getField().setTiles(list);
	}

}
