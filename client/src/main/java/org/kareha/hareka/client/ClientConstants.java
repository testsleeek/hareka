package org.kareha.hareka.client;

import java.io.File;

import org.kareha.hareka.annotation.SynchronizedWith;

public final class ClientConstants {

	private ClientConstants() {
		throw new AssertionError();
	}

	@SynchronizedWith("org.kareha.hareka.updater.UpdaterConstants")
	public static final String DATA_DIRECTORY_PATH = System.getProperty("user.dir") + File.separator + "hareka";

	public static final String OFFICIAL_SERVER_NAME = "Kareha's Lair";
	public static final String OFFICIAL_SERVER_HOST = "hareka.kareha.org";
	public static final int OFFICIAL_SERVER_PORT = 2468;
	public static final String OFFICIAL_SITE = "https://hareka.kareha.org/";
	public static final String OFFICIAL_SOURCE_CODE = "https://gitlab.com/tyatsumi/hareka";
	public static final String OFFICIAL_DOCUMENTS = "https://tyatsumi.gitlab.io/hareka/";

}
