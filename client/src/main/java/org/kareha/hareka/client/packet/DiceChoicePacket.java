package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.DiceRoll;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class DiceChoicePacket extends Packet {

	public DiceChoicePacket(final DiceRoll diceRoll) {
		out.writeCompactUInt(diceRoll.getId());
		final int[] choice = diceRoll.getChoice();
		out.writeCompactUInt(choice.length);
		for (int i = 0; i < choice.length; i++) {
			out.writeCompactUInt(choice[i]);
		}
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.DICE_CHOICE;
	}

}
