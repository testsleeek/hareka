package org.kareha.hareka.client.parser;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class EffectParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final String effectId = in.readString();
		final Vector origin = Vector.readFrom(in);
		final Vector target = Vector.readFrom(in);

		session.getMirrors().getFieldMirror().handleAddEffect(effectId, origin, target);
	}

}
