package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class TokenPacket extends Packet {

	public TokenPacket(final int handlerId, final byte[] token) {
		out.writeCompactUInt(handlerId);
		out.writeByteArray(token);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.TOKEN;
	}

}
