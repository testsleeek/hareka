package org.kareha.hareka.client.packet;

import org.kareha.hareka.field.TilePattern;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class DrawRingPacket extends Packet {

	public DrawRingPacket(final Vector center, final int size, final TilePattern tilePattern, final boolean force) {
		out.write(center);
		out.writeCompactUInt(size);
		out.write(tilePattern);
		out.writeBoolean(force);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.DRAW_RING;
	}

}
