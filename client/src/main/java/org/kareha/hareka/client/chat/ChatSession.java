package org.kareha.hareka.client.chat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.kareha.hareka.annotation.GuardedBy;

public abstract class ChatSession {

	public static class Message {

		private final ChatEntity speaker;
		private final String content;

		public Message(final ChatEntity speaker, final String content) {
			this.speaker = speaker;
			this.content = content;
		}

		public ChatEntity getSpeaker() {
			return speaker;
		}

		public String getContent() {
			return content;
		}

	}

	public interface Listener {

		void chatReceived(Message message);

	}

	protected final List<Listener> listeners = new CopyOnWriteArrayList<>();
	@GuardedBy("this")
	protected final List<Message> messages = new ArrayList<>();

	public void addListener(final Listener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(final Listener listener) {
		return listeners.remove(listener);
	}

	public Collection<Listener> getListeners() {
		return new ArrayList<>(listeners);
	}

	public synchronized int getMessagesSize() {
		return messages.size();
	}

	public synchronized Message getMessage(final int index) {
		return messages.get(index);
	}

	public synchronized Collection<Message> getMessages() {
		if (messages.isEmpty()) {
			return Collections.emptyList();
		}
		return new ArrayList<>(messages);
	}

	public abstract void sendChat(String content);

	public void receiveChat(final ChatEntity speaker, final String content) {
		final Message message = new Message(speaker, content);
		synchronized (this) {
			messages.add(message);
		}
		for (final Listener listener : listeners) {
			listener.chatReceived(message);
		}
	}

}
