package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class TeleportToPositionMemoryPacket extends Packet {

	public TeleportToPositionMemoryPacket(final byte[] positionMemory) {
		out.writeByteArray(positionMemory);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.TELEPORT_TO_POSITION_MEMORY;
	}

}
