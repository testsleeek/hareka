package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class ReduceToBackgroundTilesPacket extends Packet {

	public ReduceToBackgroundTilesPacket() {

	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.REDUCE_TO_BACKGROUND_TILES;
	}

}
