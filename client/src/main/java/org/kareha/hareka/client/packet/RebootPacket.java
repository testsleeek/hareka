package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class RebootPacket extends Packet {

	public RebootPacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.REBOOT;
	}

}
