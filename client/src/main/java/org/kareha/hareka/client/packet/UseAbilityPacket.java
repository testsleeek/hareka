package org.kareha.hareka.client.packet;

import java.util.logging.Logger;

import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.game.TargetType;
import org.kareha.hareka.game.AbilityType;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;
import org.kareha.hareka.wait.Wait;

public final class UseAbilityPacket extends Packet {

	private static final Logger logger = Logger.getLogger(UseAbilityPacket.class.getName());

	public UseAbilityPacket(final Wait wait, final AbilityType abilityType) {
		if (abilityType.getTargetType() != TargetType.NULL) {
			logger.severe("Invalid ability type=" + abilityType);
		}
		out.writeCompactULong(wait.getId());
		out.write(abilityType);
	}

	public UseAbilityPacket(final Wait wait, final AbilityType abilityType, final FieldEntity target) {
		if (abilityType.getTargetType() != TargetType.FIELD_ENTITY) {
			logger.severe("Invalid ability type=" + abilityType);
		}
		out.writeCompactULong(wait.getId());
		out.write(abilityType);
		out.write(target.getLocalId());
	}

	public UseAbilityPacket(final Wait wait, final AbilityType abilityType, final Vector target) {
		if (abilityType.getTargetType() != TargetType.TILE) {
			logger.severe("Invalid ability type=" + abilityType);
		}
		out.writeLong(wait.getId());
		out.write(abilityType);
		out.write(target);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.USE_ABILITY;
	}

}
