package org.kareha.hareka.client.parser;

import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.field.StatBar;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class HealthBarParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(HealthBarParser.class.getName());

	@Override
	public void handle(final PacketInput in, final Session session) {
		final LocalEntityId localId = LocalEntityId.readFrom(in);
		final boolean alive = in.readBoolean();
		final int bar = in.readByte();

		final FieldEntity fieldEntity = session.getMirrors().getEntityMirror().getFieldEntity(localId);
		if (fieldEntity == null) {
			logger.warning("Unknown field entity: " + localId);
			return;
		}
		fieldEntity.setHealthBar(new StatBar(alive, bar));
	}

}
