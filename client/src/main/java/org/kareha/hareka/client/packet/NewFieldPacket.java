package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class NewFieldPacket extends Packet {

	public NewFieldPacket(final int size) {
		out.writeCompactUInt(size);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.NEW_FIELD;
	}

}
