package org.kareha.hareka.client.parser;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class ExitParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final String message = in.readString();

		session.getMirrors().getSystemMirror().handleExit(message);
		session.close();
	}

}
