package org.kareha.hareka.client.packet;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class LoginCharacterPacket extends Packet {

	public LoginCharacterPacket(final int requestId, final LocalEntityId id) {
		out.writeCompactUInt(requestId);
		out.write(id);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.LOGIN_CHARACTER;
	}

}
