package org.kareha.hareka.client.packet;

import org.kareha.hareka.field.TilePattern;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class DrawLinePacket extends Packet {

	public DrawLinePacket(final Vector a, final Vector b, final TilePattern tilePattern, final boolean force) {
		out.write(a);
		out.write(b);
		out.write(tilePattern);
		out.writeBoolean(force);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.DRAW_LINE;
	}

}
