package org.kareha.hareka.client;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class ConnectionSettings {

	@Private
	static final Logger logger = Logger.getLogger(ConnectionSettings.class.getName());

	private static final String DEFAULT_PATH = "org/kareha/hareka/client/ConnectionSettings.xml";

	private final File file;
	@GuardedBy("this")
	@Private
	final List<Entry> entries = new ArrayList<>();
	@GuardedBy("this")
	@Private
	int index = -1;

	public ConnectionSettings(final File file) throws JAXBException {
		this.file = file;
		if (!load()) {
			setupDefault();
			helperSave();
		}
	}

	private synchronized void setupDefault() throws JAXBException {
		final Adapted adapted;
		try (final InputStream in = ConnectionSettings.class.getClassLoader().getResourceAsStream(DEFAULT_PATH)) {
			if (in == null) {
				throw new IOException("Resource not found");
			}
			adapted = JaxbUtil.unmarshal(in, JAXBContext.newInstance(Adapted.class));
		} catch (final IOException e) {
			throw new JAXBException(e);
		}
		helperLoad(adapted);
	}

	@XmlJavaTypeAdapter(ConnectionSettings.Entry.Adapter.class)
	public static class Entry {

		@Private
		final String name;
		@Private
		final String host;
		@Private
		final int port;
		@Private
		final String site;

		public Entry(final String name, final String host, final int port, final String site) {
			if (name == null || host == null || site == null) {
				throw new IllegalArgumentException("null");
			}
			this.name = name;
			this.host = host;
			this.port = port;
			this.site = site;
		}

		@Override
		public String toString() {
			return name;
		}

		@XmlType(name = "connectionSettingsEntry")
		@XmlAccessorType(XmlAccessType.NONE)
		private static class Adapted {

			@XmlElement
			private String name;
			@XmlElement
			private String host;
			@XmlElement
			private int port;
			@XmlElement
			private String site;

			@SuppressWarnings("unused")
			private Adapted() {
				// used by JAXB
			}

			@Private
			Adapted(final Entry v) {
				name = v.name;
				host = v.host;
				port = v.port;
				site = v.site;
			}

			@Private
			Entry unmarshal() {
				if (name == null || host == null) {
					logger.warning("host or name is null. It is changed to \"\".");
				}
				return new Entry(name != null ? name : "", host != null ? host : "", port,
						site != null ? site : ClientConstants.OFFICIAL_SITE);
			}

		}

		@Private
		Adapted marshal() {
			return new Adapted(this);
		}

		private static class Adapter extends XmlAdapter<Adapted, Entry> {

			@Override
			public Adapted marshal(final Entry v) throws Exception {
				if (v == null) {
					return null;
				}
				return v.marshal();
			}

			@Override
			public Entry unmarshal(final Adapted v) throws Exception {
				if (v == null) {
					return null;
				}
				return v.unmarshal();
			}

		}

		public String getName() {
			return name;
		}

		public String getHost() {
			return host;
		}

		public int getPort() {
			return port;
		}

		public String getSite() {
			return site;
		}

		public String getClient() {
			return site + "hareka.jar";
		}

		public String getUpdater() {
			return site + "harekaupd.jar";
		}

	}

	@XmlRootElement(name = "connectionSettings")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement(name = "entry")
		@Private
		List<Entry> entries;
		@XmlElement
		@Private
		Integer index;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final ConnectionSettings v) {
			synchronized (v) {
				entries = new ArrayList<>(v.entries);
				index = v.index;
			}
		}

	}

	@GuardedBy("this")
	private void helperLoad(final Adapted adapted) {
		entries.clear();
		if (adapted.entries != null) {
			entries.addAll(adapted.entries);
		}
		if (adapted.index == null || adapted.index < -1 || adapted.index >= entries.size()) {
			logger.warning("index=" + adapted.index + " while size=" + entries.size() + ". index is changed to -1.");
			index = -1;
		} else {
			index = adapted.index;
		}
	}

	private synchronized boolean load() throws JAXBException {
		if (!file.isFile()) {
			return false;
		}
		final Adapted adapted = JaxbUtil.unmarshal(file, Adapted.class);
		helperLoad(adapted);
		return true;
	}

	private synchronized void helperSave() throws JAXBException {
		JaxbUtil.marshal(new Adapted(this), file);
	}

	public void save() throws JAXBException {
		helperSave();
	}

	public synchronized int size() {
		return entries.size();
	}

	public synchronized Entry getEntry(final int v) {
		return entries.get(v);
	}

	public synchronized void setEntry(final int index, final Entry entry) {
		entries.set(index, entry);
	}

	public synchronized Collection<Entry> getEntries() {
		return new ArrayList<>(entries);
	}

	public synchronized void addEntry(final Entry value) {
		entries.add(value);
	}

	public synchronized void addEntry(final int index, final Entry value) {
		entries.add(index, value);
	}

	public synchronized Entry removeEntry(final int index) {
		return entries.remove(index);
	}

	public synchronized int getSelectedIndex() {
		return index;
	}

	public synchronized void setSelectedIndex(final int v) {
		if (v < -1 || v >= entries.size()) {
			throw new IllegalArgumentException("Out of bounds: index=" + v + " while size=" + entries.size());
		}
		index = v;
	}

	public synchronized Entry getSelectedEntry() {
		if (index < 0 || index >= entries.size()) {
			return null;
		}
		return entries.get(index);
	}

}
