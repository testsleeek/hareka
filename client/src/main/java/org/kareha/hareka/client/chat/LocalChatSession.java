package org.kareha.hareka.client.chat;

import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.client.Session;

@ThreadSafe
public class LocalChatSession extends ChatSession {

	private final Session session;

	public LocalChatSession(final Session session) {
		this.session = session;
	}

	@Override
	public void sendChat(final String content) {
		if (session != null) {
			session.writeLocalChat(content);
		}
	}

}
