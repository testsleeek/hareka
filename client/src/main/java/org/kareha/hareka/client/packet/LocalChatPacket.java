package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class LocalChatPacket extends Packet {

	public LocalChatPacket(final String content) {
		out.writeString(content);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.LOCAL_CHAT;
	}

}
