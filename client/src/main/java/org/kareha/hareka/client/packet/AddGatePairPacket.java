package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class AddGatePairPacket extends Packet {

	public AddGatePairPacket() {

	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.ADD_GATE_PAIR;
	}

}
