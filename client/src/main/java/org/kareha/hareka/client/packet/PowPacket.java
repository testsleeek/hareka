package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class PowPacket extends Packet {

	public PowPacket(final int handlerId, final byte[] nonce) {
		out.writeCompactUInt(handlerId);
		out.writeByteArray(nonce);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.POW;
	}

}
