package org.kareha.hareka.client.chat;

import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.client.Session;

@ThreadSafe
public class PrivateChatSession extends ChatSession {

	private final ChatEntity peer;
	private final Session session;

	public PrivateChatSession(final ChatEntity peer, final Session session) {
		this.peer = peer;
		this.session = session;
	}

	public ChatEntity getPeer() {
		return peer;
	}

	@Override
	public void sendChat(final String content) {
		if (session != null) {
			session.writePrivateChat(peer, content);
		}
	}

}
