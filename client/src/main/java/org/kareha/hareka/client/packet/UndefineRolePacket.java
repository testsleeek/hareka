package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class UndefineRolePacket extends Packet {

	public UndefineRolePacket(final String roleId) {
		out.writeString(roleId);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.UNDEFINE_ROLE;
	}

}
