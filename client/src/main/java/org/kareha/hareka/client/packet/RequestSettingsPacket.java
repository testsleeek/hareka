package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class RequestSettingsPacket extends Packet {

	public RequestSettingsPacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.REQUEST_SETTINGS;
	}

}
