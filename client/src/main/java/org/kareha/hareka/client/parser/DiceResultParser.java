package org.kareha.hareka.client.parser;

import java.util.logging.Logger;

import org.kareha.hareka.client.DiceRoll;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class DiceResultParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(DiceResultParser.class.getName());

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int id = in.readCompactUInt();
		final int value = in.readCompactUInt();
		final byte[] secret = in.readByteArray();

		final DiceRoll diceRoll = session.getDiceRollTable().getDiceRoll(id);
		if (diceRoll == null) {
			// TODO report error
			logger.warning("A nonexistent dice roll result: id=" + id);
			return;
		}
		diceRoll.setValue(value);
		diceRoll.setSecret(secret);
		if (!diceRoll.checkHash()) {
			// TODO report cheat
			logger.warning("A dice roll cheat: id=" + id);
			return;
		}
	}

}
