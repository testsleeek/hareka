package org.kareha.hareka.client.mirror;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.packet.MovePacket;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.util.Name;
import org.kareha.hareka.wait.Wait;
import org.kareha.hareka.wait.WaitTable;
import org.kareha.hareka.wait.WaitType;

public class SelfMirror {

	public interface Listener {

		void selfFieldEntitySet(FieldEntity fieldEntity);

		void selfNameSet(Name name);

		void selfRefreshed();

	}

	private final List<Listener> listeners = new CopyOnWriteArrayList<>();
	private final Session session;
	@GuardedBy("this")
	private ChatEntity chatEntity;
	@GuardedBy("this")
	private FieldEntity fieldEntity;
	private final WaitTable waitTable;
	@GuardedBy("this")
	private Name name = new Name("");
	@GuardedBy("this")
	private boolean waitTeleport;

	public SelfMirror(final Session session, final WaitTable waitTable) {
		this.session = session;
		this.waitTable = waitTable;
	}

	public void addListener(final Listener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(final Listener listener) {
		return listeners.remove(listener);
	}

	public Collection<Listener> getListeners() {
		return new ArrayList<>(listeners);
	}

	public synchronized ChatEntity getChatEntity() {
		return chatEntity;
	}

	public synchronized void setChatEntity(final ChatEntity entity) {
		chatEntity = entity;
	}

	public synchronized FieldEntity getFieldEntity() {
		return fieldEntity;
	}

	public void setFieldEntity(final FieldEntity entity) {
		synchronized (this) {
			fieldEntity = entity;
		}
		for (final Listener listener : listeners) {
			listener.selfFieldEntitySet(entity);
		}
	}

	public WaitTable getWaitTable() {
		return waitTable;
	}

	public boolean move(final Vector position) {
		final Wait wait = waitTable.get(WaitType.MOTION);
		if (wait == null) {
			return false;
		}
		session.write(new MovePacket(wait, position));
		return true;
	}

	public void handleSetName(final Name name) {
		synchronized (this) {
			this.name = name;
		}
		for (final Listener listener : listeners) {
			listener.selfNameSet(name);
		}
	}

	public synchronized Name getName() {
		return name;
	}

	public void handleRefresh() {
		for (final Listener listener : listeners) {
			listener.selfRefreshed();
		}
	}

	public synchronized boolean isWaitTeleport() {
		return waitTeleport;
	}

	public synchronized void setWaitTeleport(final boolean v) {
		waitTeleport = v;
	}

}
