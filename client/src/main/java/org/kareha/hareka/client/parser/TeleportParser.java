package org.kareha.hareka.client.parser;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class TeleportParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		if (!session.getMirrors().getSelfMirror().isWaitTeleport()) {
			return;
		}
		final FieldEntity entity = session.getMirrors().getSelfMirror().getFieldEntity();
		if (entity != null) {
			try {
				Thread.sleep(entity.getMotionWait());
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}

}
