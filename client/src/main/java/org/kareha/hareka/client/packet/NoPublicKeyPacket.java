package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class NoPublicKeyPacket extends Packet {

	public NoPublicKeyPacket(final int handlerId, final int version) {
		out.writeCompactUInt(handlerId);
		out.writeCompactUInt(version);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.NO_PUBLIC_KEY;
	}

}
