package org.kareha.hareka.client.parser;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.wait.Wait;

public class WaitParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final Wait wait = Wait.readFrom(in);

		session.getMirrors().getSelfMirror().getWaitTable().put(wait);
	}

}
