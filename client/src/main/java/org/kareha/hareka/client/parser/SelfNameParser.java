package org.kareha.hareka.client.parser;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.util.Name;

public class SelfNameParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final LocalEntityId id = LocalEntityId.readFrom(in);
		final Name name = Name.readFrom(in);

		final ChatEntity entity = session.getMirrors().getSelfMirror().getChatEntity();
		if (entity == null) {
			return;
		}
		if (!id.equals(entity.getLocalId())) {
			return;
		}
		session.getMirrors().getSelfMirror().handleSetName(name);
	}

}
