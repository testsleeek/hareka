package org.kareha.hareka.client.parser;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class CommandOutParser implements Parser<Session> {

	private static final String lineSeparator = System.getProperty("line.separator");

	@Override
	public void handle(final PacketInput in, final Session session) {
		final String result = in.readString();

		final String localResult = result.replaceAll("\n", lineSeparator);
		session.getMirrors().getAdminMirror().handleCommandOut(localResult);
	}

}
