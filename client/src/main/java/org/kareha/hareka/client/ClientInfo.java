package org.kareha.hareka.client;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.util.JaxbUtil;

public enum ClientInfo {

	INSTANCE;

	private static final String PATH = "org/kareha/hareka/client/ClientInfo.xml";

	private final String sourceCode;
	private final String documents;

	private ClientInfo() {
		final Adapted adapted;
		try (final InputStream in = ConnectionSettings.class.getClassLoader().getResourceAsStream(PATH)) {
			if (in == null) {
				throw new IOException("Resource not found");
			}
			adapted = JaxbUtil.unmarshal(in, JAXBContext.newInstance(Adapted.class));
		} catch (final IOException | JAXBException e) {
			sourceCode = ClientConstants.OFFICIAL_SOURCE_CODE;
			documents = ClientConstants.OFFICIAL_DOCUMENTS;
			return;
		}
		sourceCode = adapted.sourceCode != null ? adapted.sourceCode : ClientConstants.OFFICIAL_SOURCE_CODE;
		documents = adapted.documents != null ? adapted.documents : ClientConstants.OFFICIAL_DOCUMENTS;
	}

	@XmlRootElement(name = "clientInfo")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		@Private
		String sourceCode;
		@XmlElement
		@Private
		String documents;

	}

	public String getSourceCode() {
		return sourceCode;
	}

	public String getDocuments() {
		return documents;
	}

}
