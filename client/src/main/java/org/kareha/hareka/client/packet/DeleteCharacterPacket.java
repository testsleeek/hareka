package org.kareha.hareka.client.packet;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class DeleteCharacterPacket extends Packet {

	public DeleteCharacterPacket(final int requestId, final LocalEntityId localId) {
		out.writeCompactUInt(requestId);
		out.write(localId);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.DELETE_CHARACTER;
	}

}
