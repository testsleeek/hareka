package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class LoginUserPacket extends Packet {

	public LoginUserPacket(final int requestId) {
		out.writeCompactUInt(requestId);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.LOGIN_USER;
	}

}
