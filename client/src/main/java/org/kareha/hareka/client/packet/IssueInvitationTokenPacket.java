package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class IssueInvitationTokenPacket extends Packet {

	public IssueInvitationTokenPacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.ISSUE_INVITATION_TOKEN;
	}

}
