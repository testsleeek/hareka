package org.kareha.hareka.client.server;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.field.PositionMemory;
import org.kareha.hareka.key.KeyId;
import org.kareha.hareka.key.KeyXml;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class Server {

	@Private
	static final Logger logger = Logger.getLogger(Server.class.getName());

	private static final String FILENAME = "server.xml";
	private static final JAXBContext jaxbServer;

	static {
		try {
			jaxbServer = JAXBContext.newInstance(Adapted.class);
		} catch (final JAXBException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	@Private
	final KeyId id;
	@GuardedBy("this")
	@Private
	PublicKey publicKey;
	@GuardedBy("this")
	@Private
	final List<PositionMemory> positionMemories = new ArrayList<>();
	@GuardedBy("this")
	@Private
	final Map<LocalEntityId, ChatEntityEntry> chatEntityEntries = new HashMap<>();
	@GuardedBy("this")
	@Private
	final Map<LocalEntityId, FieldEntityEntry> fieldEntityEntries = new HashMap<>();
	@GuardedBy("this")
	@Private
	final Map<LocalEntityId, LocalEntityId> chatToFieldIdMap = new HashMap<>();
	@GuardedBy("this")
	private final Map<LocalEntityId, LocalEntityId> fieldToChatIdMap = new HashMap<>();

	@GuardedBy("this")
	private Session session;

	public Server(final KeyId id, final PublicKey publicKey) {
		this(id, publicKey, null, null, null, null);
	}

	@Private
	Server(final KeyId id, final PublicKey publicKey, final List<PositionMemory> positionMemories,
			final List<ChatEntityEntry> chatEntityEntries, final List<FieldEntityEntry> fieldEntityEntries,
			final List<EntityIdentity> entityIdentities) {
		this.id = id;
		this.publicKey = publicKey;
		if (positionMemories != null) {
			this.positionMemories.addAll(positionMemories);
		}
		if (chatEntityEntries != null) {
			for (final ChatEntityEntry entry : chatEntityEntries) {
				if (entry.getId() == null) {
					logger.warning("ChatEntityEntry has null id.");
					continue;
				}
				this.chatEntityEntries.put(entry.getId(), entry);
			}
		}
		if (fieldEntityEntries != null) {
			for (final FieldEntityEntry entry : fieldEntityEntries) {
				if (entry.getId() == null) {
					logger.warning("FieldEntityEntry has null id.");
					continue;
				}
				this.fieldEntityEntries.put(entry.getId(), entry);
			}
		}
		if (entityIdentities != null) {
			for (final EntityIdentity ei : entityIdentities) {
				if (ei.chatEntityId == null || ei.fieldEntityId == null) {
					logger.warning("EntityIdentity has null id.");
					continue;
				}
				chatToFieldIdMap.put(ei.chatEntityId, ei.fieldEntityId);
				fieldToChatIdMap.put(ei.fieldEntityId, ei.chatEntityId);
			}
		}
	}

	@XmlType(name = "entityIdentity")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class EntityIdentity {

		@XmlAttribute(name = "chat")
		@Private
		LocalEntityId chatEntityId;
		@XmlAttribute(name = "field")
		@Private
		LocalEntityId fieldEntityId;

		@SuppressWarnings("unused")
		private EntityIdentity() {
			// used by JAXB
		}

		@Private
		EntityIdentity(final LocalEntityId chatEntityId, final LocalEntityId fieldEntityId) {
			this.chatEntityId = chatEntityId;
			this.fieldEntityId = fieldEntityId;
		}

	}

	@XmlRootElement(name = "server")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlAttribute
		private KeyId id;
		@XmlElement(name = "id")
		private KeyId oldId; // XXX transient
		@XmlElement
		private KeyXml publicKey;
		@XmlElement(name = "positionMemory")
		private List<PositionMemory> positionMemories;
		@XmlElement(name = "chatEntity")
		private List<ChatEntityEntry> chatEntityEntries;
		@XmlElement(name = "fieldEntity")
		private List<FieldEntityEntry> fieldEntityEntries;
		@XmlElement(name = "entityIdentity")
		private List<EntityIdentity> entityIdentities;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Server v) {
			id = v.id;
			synchronized (v) {
				publicKey = new KeyXml(v.publicKey);
				if (!v.positionMemories.isEmpty()) {
					positionMemories = new ArrayList<>(v.positionMemories);
				}
				if (!v.chatEntityEntries.isEmpty()) {
					chatEntityEntries = new ArrayList<>(v.chatEntityEntries.values());
				}
				if (!v.fieldEntityEntries.isEmpty()) {
					fieldEntityEntries = new ArrayList<>(v.fieldEntityEntries.values());
				}
				if (!v.chatToFieldIdMap.isEmpty()) {
					entityIdentities = new ArrayList<>();
					for (final Map.Entry<LocalEntityId, LocalEntityId> entry : v.chatToFieldIdMap.entrySet()) {
						entityIdentities.add(new EntityIdentity(entry.getKey(), entry.getValue()));
					}
				}
			}
		}

		@Private
		Server unmarshal() {
			final KeyId trueId;
			if (id == null) {
				trueId = oldId;
			} else {
				trueId = id;
			}
			PublicKey pk = null;
			if (publicKey != null) {
				try {
					pk = (PublicKey) publicKey.toObject();
				} catch (final NoSuchAlgorithmException | InvalidKeySpecException e) {
					logger.log(Level.SEVERE, "", e);
				} catch (final ClassCastException e) {
					logger.log(Level.SEVERE, "", e);
				}
			}
			return new Server(trueId, pk, positionMemories, chatEntityEntries, fieldEntityEntries, entityIdentities);
		}

	}

	public static Server load(final File directory) throws JAXBException {
		final Adapted adapted = JaxbUtil.unmarshal(new File(directory, FILENAME), jaxbServer);
		return adapted.unmarshal();
	}

	@GuardedBy("Servers.this")
	public void save(final File directory) throws IOException, JAXBException {
		if (!directory.isDirectory()) {
			if (!directory.mkdir()) {
				throw new IOException("Cannot create directory: " + directory);
			}
		}
		JaxbUtil.marshal(new Adapted(this), new File(directory, FILENAME), jaxbServer);
	}

	public KeyId getId() {
		return id;
	}

	public synchronized PublicKey getPublicKey() {
		return publicKey;
	}

	public synchronized void setPublicKey(final PublicKey publicKey) {
		this.publicKey = publicKey;
	}

	public synchronized void addPositionMemory(final PositionMemory positionMemory) {
		positionMemories.add(positionMemory);
	}

	public synchronized void addPositionMemory(final int index, final PositionMemory positionMemory) {
		positionMemories.add(index, positionMemory);
	}

	public synchronized PositionMemory removePositionMemory(final int index) {
		return positionMemories.remove(index);
	}

	public synchronized Collection<PositionMemory> getPositionMemories() {
		return new ArrayList<>(positionMemories);
	}

	public synchronized void addChatName(final LocalEntityId id, final String value) {
		final ChatEntityEntry entry = chatEntityEntries.get(id);
		if (entry == null) {
			final ChatEntityEntry newEntry = new ChatEntityEntry(id);
			newEntry.addName(value);
			chatEntityEntries.put(id, newEntry);
		} else {
			entry.addName(value);
		}
	}

	public synchronized String getChatNickname(final LocalEntityId id) {
		final ChatEntityEntry entry = chatEntityEntries.get(id);
		if (entry == null) {
			return null;
		}
		return entry.getNickname();
	}

	public synchronized void addChatNickname(final LocalEntityId id, final String value) {
		final ChatEntityEntry entry = chatEntityEntries.get(id);
		if (entry == null) {
			final ChatEntityEntry newEntry = new ChatEntityEntry(id);
			newEntry.addNickname(value);
			chatEntityEntries.put(id, newEntry);
		} else {
			entry.addNickname(value);
		}
	}

	public synchronized void removeChatNickname(final LocalEntityId id) {
		final ChatEntityEntry entry = chatEntityEntries.get(id);
		if (entry == null) {
			return;
		}
		entry.clearNicknames();
		if (entry.isEmpty()) {
			chatEntityEntries.remove(id);
		}
	}

	public synchronized Collection<ChatEntityEntry> getChatEntityEntries() {
		return new ArrayList<>(chatEntityEntries.values());
	}

	public synchronized String getFieldNickname(final LocalEntityId id) {
		final FieldEntityEntry entry = fieldEntityEntries.get(id);
		if (entry == null) {
			return null;
		}
		return entry.getNickname();
	}

	public synchronized void addFieldNickname(final LocalEntityId id, final String value) {
		final FieldEntityEntry entry = fieldEntityEntries.get(id);
		if (entry == null) {
			final FieldEntityEntry newEntry = new FieldEntityEntry(id);
			newEntry.addNickname(value);
			fieldEntityEntries.put(id, newEntry);
		} else {
			entry.addNickname(value);
		}
	}

	public synchronized void removeFieldNickname(final LocalEntityId id) {
		final FieldEntityEntry entry = fieldEntityEntries.get(id);
		if (entry == null) {
			return;
		}
		entry.clearNicknames();
		if (entry.isEmpty()) {
			fieldEntityEntries.remove(id);
		}
	}

	public synchronized void addEntityIdentity(final LocalEntityId chatEntityId, final LocalEntityId fieldEntityId) {
		chatToFieldIdMap.put(chatEntityId, fieldEntityId);
		fieldToChatIdMap.put(fieldEntityId, chatEntityId);
	}

	public synchronized Session getSession() {
		return session;
	}

	public synchronized void setSession(final Session session) {
		this.session = session;
	}

	public synchronized FieldEntity getFieldEntity(final ChatEntity chatEntity) {
		final LocalEntityId id = chatToFieldIdMap.get(chatEntity.getLocalId());
		if (id == null) {
			return null;
		}
		return session.getMirrors().getEntityMirror().getFieldEntity(id);
	}

	public synchronized ChatEntity getChatEntity(final FieldEntity fieldEntity) {
		final LocalEntityId id = fieldToChatIdMap.get(fieldEntity.getLocalId());
		if (id == null) {
			return null;
		}
		return session.getMirrors().getEntityMirror().getChatEntity(id);
	}

}
