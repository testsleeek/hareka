package org.kareha.hareka.client.packet;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;
import org.kareha.hareka.wait.Wait;

public final class UseItemPacket extends Packet {

	public UseItemPacket(final Wait wait, final LocalEntityId itemId, final FieldEntity target) {
		out.writeCompactULong(wait.getId());
		out.write(itemId);
		out.write(target.getLocalId());
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.USE_ITEM;
	}

}
