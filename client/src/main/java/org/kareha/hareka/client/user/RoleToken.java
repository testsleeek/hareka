package org.kareha.hareka.client.user;

import org.kareha.hareka.LocalKeyId;

public class RoleToken {

	private final long id;
	private final long issuedDate;
	private final LocalKeyId issuer;
	private final String roleId;

	public RoleToken(final long id, final long issuedDate, final LocalKeyId issuer, final String roleId) {
		this.id = id;
		this.issuedDate = issuedDate;
		this.issuer = issuer;
		this.roleId = roleId;
	}

	public long getId() {
		return id;
	}

	public long getIssuedDate() {
		return issuedDate;
	}

	public LocalKeyId getIssuer() {
		return issuer;
	}

	public String getRoleId() {
		return roleId;
	}

}
