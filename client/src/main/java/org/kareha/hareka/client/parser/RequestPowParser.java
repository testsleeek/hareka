package org.kareha.hareka.client.parser;

import java.math.BigInteger;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class RequestPowParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int handlerId = in.readCompactUInt();
		final BigInteger target = in.readBigInteger();
		final byte[] data = in.readByteArray();
		final String message = in.readString();

		session.getMirrors().getSystemMirror().handleRequestPow(handlerId, target, data, message);
	}

}
