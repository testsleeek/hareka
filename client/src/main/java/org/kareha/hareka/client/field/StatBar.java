package org.kareha.hareka.client.field;

public class StatBar {

	public static final StatBar FULL = new StatBar(true, Byte.MAX_VALUE);

	private final boolean alive;
	private final int bar;

	public StatBar(final boolean alive, final int bar) {
		this.alive = alive;
		this.bar = bar;
	}

	public boolean isAlive() {
		return alive;
	}

	public int getBar() {
		return bar;
	}

	public boolean isDamaged() {
		return bar < Byte.MAX_VALUE;
	}

}
