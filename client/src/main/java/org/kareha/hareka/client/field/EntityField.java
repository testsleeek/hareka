package org.kareha.hareka.client.field;

import java.util.Collection;
import java.util.List;

import org.kareha.hareka.field.ArrayListHashMap;
import org.kareha.hareka.field.Vector;

public class EntityField {

	private final ArrayListHashMap<Vector, FieldEntity> entityMap = new ArrayListHashMap<>();

	public boolean addEntity(final FieldEntity entity) {
		return entityMap.put(entity.getPlacement().getPosition(), entity);
	}

	public boolean removeEntity(final FieldEntity entity) {
		return entityMap.remove(entity);
	}

	public boolean moveEntity(final FieldEntity entity) {
		return entityMap.move(entity.getPlacement().getPosition(), entity);
	}

	public Collection<FieldEntity> getEntities() {
		return entityMap.values();
	}

	public List<FieldEntity> getEntities(final Vector position) {
		return entityMap.get(position);
	}

}
