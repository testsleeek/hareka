package org.kareha.hareka.client.mirror;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.packet.UseItemPacket;
import org.kareha.hareka.game.ItemType;
import org.kareha.hareka.wait.Wait;
import org.kareha.hareka.wait.WaitTable;
import org.kareha.hareka.wait.WaitType;

public class InventoryMirror {

	public class Entry {

		private final LocalEntityId id;
		private final String shape;
		@GuardedBy("this")
		private long count;
		private final ItemType type;
		private final String name;
		private final int weight;
		private final int reach;
		private final WaitType waitType;
		private final int wait;

		public Entry(final LocalEntityId id, final String shape, final long count, final ItemType type, final String name,
				final int weight, final int reach, final WaitType waitType, final int wait) {
			this.id = id;
			this.shape = shape;
			this.count = count;
			this.type = type;
			this.name = name;
			this.weight = weight;
			this.reach = reach;
			this.waitType = waitType;
			this.wait = wait;
		}

		public LocalEntityId getId() {
			return id;
		}

		public String getShape() {
			return shape;
		}

		public synchronized long getCount() {
			return count;
		}

		public synchronized void setCount(final long v) {
			count = v;
		}

		public ItemType getType() {
			return type;
		}

		public String getName() {
			return name;
		}

		public int getWeight() {
			return weight;
		}

		public synchronized long getTotalWeight() {
			return weight * count;
		}

		public int getReach() {
			return reach;
		}

		public WaitType getWaitType() {
			return waitType;
		}

		public int getWait() {
			return wait;
		}

		public void use(final FieldEntity target) {
			final Wait wait = waitTable.get(waitType);
			if (wait != null) {
				session.write(new UseItemPacket(wait, id, target));
			}
		}

	}

	public interface Listener {

		void itemsCleared();

		void itemAdded(Entry entry);

		void itemRemoved(Entry entry);

		void itemCountSet(Entry entry);

		void weightBarChanged(int bar);
	}

	private final List<Listener> listeners = new CopyOnWriteArrayList<>();
	@Private
	final Session session;
	@Private
	final WaitTable waitTable;
	@GuardedBy("this")
	private final List<Entry> entries = new ArrayList<>();
	@GuardedBy("this")
	private int weightBar;

	public InventoryMirror(final Session session, final WaitTable waitTable) {
		this.session = session;
		this.waitTable = waitTable;
	}

	public void addListener(final Listener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(final Listener listener) {
		return listeners.remove(listener);
	}

	public Collection<Listener> getListeners() {
		return new ArrayList<>(listeners);
	}

	public void handleClear() {
		synchronized (this) {
			entries.clear();
		}
		for (final Listener listener : listeners) {
			listener.itemsCleared();
		}
	}

	public void handleAdd(final LocalEntityId id, final String shape, final long count, final ItemType type,
			final String name, final int weight, final int reach, final WaitType waitType, final int wait) {
		final Entry entry = new Entry(id, shape, count, type, name, weight, reach, waitType, wait);
		synchronized (this) {
			entries.add(entry);
		}
		for (final Listener listener : listeners) {
			listener.itemAdded(entry);
		}
	}

	public void handleRemove(final LocalEntityId id) {
		Entry entry = null;
		synchronized (this) {
			for (final Entry i : entries) {
				if (i.getId().equals(id)) {
					entry = i;
					break;
				}
			}
			if (entry == null) {
				return;
			}
			entries.remove(entry);
		}
		for (final Listener listener : listeners) {
			listener.itemRemoved(entry);
		}
	}

	public void handleSetCount(final LocalEntityId id, final long count) {
		Entry entry = null;
		synchronized (this) {
			for (final Entry i : entries) {
				if (i.getId().equals(id)) {
					entry = i;
					break;
				}
			}
		}
		if (entry == null) {
			return;
		}
		entry.setCount(count);
		for (final Listener listener : listeners) {
			listener.itemCountSet(entry);
		}
	}

	public void handleWeightBar(final int v) {
		synchronized (this) {
			weightBar = v;
		}
		for (final Listener listener : listeners) {
			listener.weightBarChanged(v);
		}
	}

	public synchronized Entry getEntry(final ItemType type) {
		for (final Entry entry : entries) {
			if (entry.getType().equals(type)) {
				return entry;
			}
		}
		return null;
	}

	public synchronized int getWeightBar() {
		return weightBar;
	}

}
