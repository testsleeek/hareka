package org.kareha.hareka.client.parser;

import java.security.KeyPair;

import org.kareha.hareka.Constants;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.packet.NoPublicKeyPacket;
import org.kareha.hareka.client.packet.PublicKeyPacket;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class RequestPublicKeyParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int handlerId = in.readCompactUInt();
		final int version = in.readCompactUInt();
		final byte[] nonce = in.readByteArray();

		final KeyPair keyPair = session.getMirrors().getSystemMirror().getKeyPair(version);
		if (keyPair == null) {
			session.write(new NoPublicKeyPacket(handlerId, version));
		} else {
			session.write(new PublicKeyPacket(handlerId, version, nonce, keyPair, Constants.SIGNATURE_ALGORITHM));
		}
	}

}
