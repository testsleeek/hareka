package org.kareha.hareka.client.field;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.field.Placement;

public class FieldEntity {

	public interface Listener {

		void fieldEntityPlaced(Placement placement, int motionWait);

		void fieldEntityShapeChanged(String shape);

	}

	private final List<Listener> listeners = new CopyOnWriteArrayList<>();
	private final LocalEntityId localId;
	private final ClientField field;
	@GuardedBy("this")
	private Placement placement;
	@GuardedBy("this")
	private String shape;
	@GuardedBy("this")
	private int motionWait;
	@GuardedBy("this")
	private StatBar healthBar;
	@GuardedBy("this")
	private StatBar magicBar;
	@GuardedBy("this")
	private long count;

	public FieldEntity(final LocalEntityId localId, final ClientField field, final Placement placement, final String shape,
			final long count) {
		this.localId = localId;
		this.field = field;
		this.placement = placement;
		this.shape = shape;
		this.count = count;
	}

	public void addListener(final Listener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(final Listener listener) {
		return listeners.remove(listener);
	}

	public Collection<Listener> getListeners() {
		return new ArrayList<>(listeners);
	}

	public LocalEntityId getLocalId() {
		return localId;
	}

	public synchronized Placement getPlacement() {
		return placement;
	}

	public synchronized String getShape() {
		return shape;
	}

	public void setShape(final String shape) {
		synchronized (this) {
			this.shape = shape;
		}
		for (final Listener listener : listeners) {
			listener.fieldEntityShapeChanged(shape);
		}
	}

	public synchronized int getMotionWait() {
		return motionWait;
	}

	public void place(final Placement placement, final int motionWait) {
		synchronized (this) {
			this.placement = placement;
			this.motionWait = motionWait;
		}
		if (field != null) {
			field.moveEntity(this);
		}
		for (final Listener listener : listeners) {
			listener.fieldEntityPlaced(placement, motionWait);
		}
	}

	public synchronized StatBar getHealthBar() {
		return healthBar;
	}

	public synchronized void setHealthBar(final StatBar healthBar) {
		this.healthBar = healthBar;
	}

	public synchronized StatBar getMagicBar() {
		return magicBar;
	}

	public synchronized void setMagicBar(final StatBar magicBar) {
		this.magicBar = magicBar;
	}

	public synchronized boolean isAlive() {
		return healthBar.isAlive();
	}

	public synchronized long getCount() {
		return count;
	}

}
