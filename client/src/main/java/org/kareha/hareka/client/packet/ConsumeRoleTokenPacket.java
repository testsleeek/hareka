package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class ConsumeRoleTokenPacket extends Packet {

	public ConsumeRoleTokenPacket(final byte[] token) {
		out.writeByteArray(token);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.CONSUME_ROLE_TOKEN;
	}

}
