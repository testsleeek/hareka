package org.kareha.hareka.client.parser;

import java.util.logging.Logger;

import org.kareha.hareka.client.ResponseHandler;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class RejectParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(RejectParser.class.getName());

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int requestId = in.readCompactUInt();
		final String message = in.readString();

		final ResponseHandler rh = session.removeResponseHandler(requestId);
		if (rh == null) {
			logger.warning("A nonexistent request has been rejected: " + requestId);
			return;
		}
		rh.rejected(message);
	}

}
