package org.kareha.hareka.client.parser;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.game.ItemType;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.wait.WaitType;

public class AddItemParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final LocalEntityId id = LocalEntityId.readFrom(in);
		final String shape = in.readString();
		final long count = in.readCompactULong();
		final ItemType type = ItemType.readFrom(in);
		final String name = in.readString();
		final int weight = in.readCompactUInt();
		final int reach = in.readCompactUInt();
		final WaitType waitType = WaitType.readFrom(in);
		final int wait = in.readCompactUInt();

		session.getMirrors().getInventoryMirror().handleAdd(id, shape, count, type, name, weight, reach, waitType,
				wait);
	}

}
