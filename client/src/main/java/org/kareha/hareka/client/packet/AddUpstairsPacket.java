package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class AddUpstairsPacket extends Packet {

	public AddUpstairsPacket() {

	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.ADD_UPSTAIRS;
	}

}
