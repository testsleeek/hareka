package org.kareha.hareka.client.packet;

import org.kareha.hareka.field.TilePattern;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class SetDefaultTilePatternPacket extends Packet {

	public SetDefaultTilePatternPacket(final TilePattern tilePattern) {
		out.write(tilePattern);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.SET_DEFAULT_TILE_PATTERN;
	}

}
