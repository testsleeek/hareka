package org.kareha.hareka.client.parser;

import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.LocalKeyId;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class InspectionParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final LocalEntityId chatLocalId = LocalEntityId.readFrom(in);
		final LocalEntityId fieldLocalId = LocalEntityId.readFrom(in);
		final int userCount = in.readCompactUInt();
		final List<LocalKeyId> userIds = new ArrayList<>();
		for (int i = 0; i < userCount; i++) {
			userIds.add(LocalKeyId.readFrom(in));
		}

		session.getMirrors().getGroupMirror().handleInspection(chatLocalId, fieldLocalId, userIds);
	}

}
