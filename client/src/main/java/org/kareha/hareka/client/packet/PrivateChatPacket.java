package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class PrivateChatPacket extends Packet {

	public PrivateChatPacket(final ChatEntity peer, final String content) {
		out.write(peer.getLocalId());
		out.writeString(content);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.PRIVATE_CHAT;
	}

}
