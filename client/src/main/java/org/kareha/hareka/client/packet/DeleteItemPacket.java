package org.kareha.hareka.client.packet;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class DeleteItemPacket extends Packet {

	public DeleteItemPacket(final LocalEntityId itemId, final long count) {
		out.write(itemId);
		out.writeCompactULong(count);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.DELETE_ITEM;
	}

}
