package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class AddDownstairsPacket extends Packet {

	public AddDownstairsPacket() {

	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.ADD_DOWNSTAIRS;
	}

}
