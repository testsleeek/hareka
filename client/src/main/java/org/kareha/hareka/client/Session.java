package org.kareha.hareka.client;

import java.net.Socket;
import java.security.PublicKey;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.client.mirror.Mirrors;
import org.kareha.hareka.client.packet.DeleteCharacterPacket;
import org.kareha.hareka.client.packet.LocalChatPacket;
import org.kareha.hareka.client.packet.LoginCharacterPacket;
import org.kareha.hareka.client.packet.LoginUserPacket;
import org.kareha.hareka.client.packet.LogoutCharacterPacket;
import org.kareha.hareka.client.packet.LogoutUserPacket;
import org.kareha.hareka.client.packet.NewCharacterPacket;
import org.kareha.hareka.client.packet.PrivateChatPacket;
import org.kareha.hareka.client.server.ChatEntityEntry;
import org.kareha.hareka.client.server.Server;
import org.kareha.hareka.key.KeyGrabber;
import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketSocket;
import org.kareha.hareka.packet.ParserTable;

public class Session implements AutoCloseable {

	private volatile ConnectionSettings.Entry connection;
	private volatile PacketSocket<ClientPacketType, Session> packetSocket;
	private final Context context;
	@GuardedBy("this")
	private Locale locale = Locale.getDefault();
	private final AtomicInteger nextRequestId = new AtomicInteger();
	private final Map<Integer, ResponseHandler> responseHandlers = new ConcurrentHashMap<>();
	private final KeyGrabber keyGrabber = new KeyGrabber();
	@GuardedBy("this")
	private PublicKey peerKey;
	@GuardedBy("this")
	private Server server;
	private volatile Mirrors mirrors;
	private final DiceRollTable diceRollTable = new DiceRollTable();

	// not private for test
	Session(final Context context) {
		this.context = context;
	}

	// not private for test
	PacketSocket<ClientPacketType, Session> newPacketSocket(final Socket socket,
			final ParserTable<ClientPacketType, Session> parserTable) {
		return new PacketSocket<ClientPacketType, Session>(socket, parserTable, this) {
			@Override
			public void finish() {
				getContext().getServers().logout();
			}
		};
	}

	// not private for test
	void initialize(final Socket socket, final ParserTable<ClientPacketType, Session> parserTable) {
		packetSocket = newPacketSocket(socket, parserTable);
		mirrors = new Mirrors(this);
	}

	public static Session newInstance(final Context context, final ConnectionSettings.Entry connection,
			final Socket socket, final ParserTable<ClientPacketType, Session> parserTable) {
		final Session session = new Session(context);
		session.connection = connection;
		session.initialize(socket, parserTable);
		return session;
	}

	public void start() {
		packetSocket.start();
	}

	@Override
	public void close() {
		packetSocket.close();
		final Mirrors m = mirrors;
		if (m != null) {
			m.close();
		}
	}

	public ConnectionSettings.Entry getConnection() {
		return connection;
	}

	public void write(final Packet packet) {
		packetSocket.write(packet);
	}

	public Context getContext() {
		return context;
	}

	public synchronized Locale getLocale() {
		return locale;
	}

	public synchronized void setLocale(final Locale locale) {
		this.locale = locale;
	}

	public ResponseHandler removeResponseHandler(final int requestId) {
		return responseHandlers.remove(requestId);
	}

	public KeyGrabber getKeyGrabber() {
		return keyGrabber;
	}

	public synchronized PublicKey getPeerKey() {
		return peerKey;
	}

	public synchronized void setPeerKey(final PublicKey peerKey) {
		this.peerKey = peerKey;
	}

	public synchronized Server getServer() {
		return server;
	}

	public synchronized void setServer(final Server server) {
		for (final ChatEntityEntry entry : server.getChatEntityEntries()) {
			getMirrors().getEntityMirror().loadChatEntity(entry.getId(), entry.getName());
		}
		server.setSession(this);
		this.server = server;
	}

	public Mirrors getMirrors() {
		return mirrors;
	}

	public DiceRollTable getDiceRollTable() {
		return diceRollTable;
	}

	public void writeLoginUser(final ResponseHandler responseHandler) {
		final int i = nextRequestId.getAndIncrement();
		responseHandlers.put(i, responseHandler);
		write(new LoginUserPacket(i));
	}

	public void writeLogoutUser(final ResponseHandler responseHandler) {
		final int i = nextRequestId.getAndIncrement();
		responseHandlers.put(i, responseHandler);
		write(new LogoutUserPacket(i));
	}

	public void writeNewCharacter(final ResponseHandler responseHandler) {
		final int i = nextRequestId.getAndIncrement();
		responseHandlers.put(i, responseHandler);
		write(new NewCharacterPacket(i));
	}

	public void writeDeleteCharacter(final LocalEntityId id, final ResponseHandler responseHandler) {
		final int i = nextRequestId.getAndIncrement();
		responseHandlers.put(i, responseHandler);
		write(new DeleteCharacterPacket(i, id));
	}

	public void writeLoginCharacter(final LocalEntityId id, final ResponseHandler responseHandler) {
		final int i = nextRequestId.getAndIncrement();
		responseHandlers.put(i, responseHandler);
		write(new LoginCharacterPacket(i, id));
	}

	public void writeLogoutCharacter(final ResponseHandler responseHandler) {
		final int i = nextRequestId.getAndIncrement();
		responseHandlers.put(i, responseHandler);
		write(new LogoutCharacterPacket(i));
	}

	public void writeLocalChat(final String content) {
		write(new LocalChatPacket(content));
	}

	public void writePrivateChat(final ChatEntity peer, final String content) {
		write(new PrivateChatPacket(peer, content));
	}

}
