package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class LogoutUserPacket extends Packet {

	public LogoutUserPacket(final int requestId) {
		out.writeCompactUInt(requestId);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.LOGOUT_USER;
	}

}
