package org.kareha.hareka.client.packet;

import java.util.Locale;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class LocalePacket extends Packet {

	public LocalePacket(final Locale locale) {
		out.writeLocale(locale);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.LOCALE;
	}

}
