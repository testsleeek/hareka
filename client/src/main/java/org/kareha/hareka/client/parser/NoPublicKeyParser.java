package org.kareha.hareka.client.parser;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.key.KeyGrabber;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class NoPublicKeyParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int handlerId = in.readCompactUInt();
		final int version = in.readCompactUInt();

		final KeyGrabber.Entry entry = session.getKeyGrabber().get(handlerId);

		if (entry != null) {
			entry.handle(version, null);
		}
	}

}
