package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class ChangeNamePacket extends Packet {

	public ChangeNamePacket(final String language, final String translation) {
		out.writeString(language);
		out.writeString(translation);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.CHANGE_NAME;
	}

}
