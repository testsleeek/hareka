package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class AutopilotPacket extends Packet {

	public AutopilotPacket(final boolean flag) {
		out.writeBoolean(flag);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.AUTOPILOT;
	}

}
