package org.kareha.hareka.client.packet;

import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class TilePacket extends Packet {

	public TilePacket(final TilePiece tilePiece) {
		out.write(tilePiece);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.TILE;
	}

}
