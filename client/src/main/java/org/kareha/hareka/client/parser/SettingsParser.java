package org.kareha.hareka.client.parser;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.user.UserRegistrationMode;

public class SettingsParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final UserRegistrationMode mode = UserRegistrationMode.readFrom(in);

		session.getMirrors().getAdminMirror().handleSettings(mode);
	}

}
