package org.kareha.hareka.client.parser;

import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class PrivateChatEchoParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(PrivateChatEchoParser.class.getName());

	@Override
	public void handle(final PacketInput in, final Session session) {
		final LocalEntityId id = LocalEntityId.readFrom(in);
		final String name = in.readString();
		final String content = in.readString();

		final ChatEntity entity = session.getMirrors().getEntityMirror().updateChatEntity(id, name);
		final ChatEntity self = session.getMirrors().getSelfMirror().getChatEntity();
		if (self == null) {
			logger.warning("Self chat entity is not found");
			return;
		}
		session.getMirrors().getChatMirror().getPrivateChatSession(entity).receiveChat(self, content);
	}

}
