package org.kareha.hareka.client.parser;

import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class RemoveFieldEntityParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(RemoveFieldEntityParser.class.getName());

	@Override
	public void handle(final PacketInput in, final Session session) {
		final LocalEntityId id = LocalEntityId.readFrom(in);

		if (!session.getMirrors().getEntityMirror().removeFieldEntity(id)) {
			logger.warning("A nonexistent field entity has been removed: " + id);
		}
	}

}
