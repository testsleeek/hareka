package org.kareha.hareka.client.parser;

import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class SelfFieldEntityParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(SelfFieldEntityParser.class.getName());

	@Override
	public void handle(final PacketInput in, final Session session) {
		final LocalEntityId id = LocalEntityId.readFrom(in);

		final FieldEntity entity = session.getMirrors().getEntityMirror().getFieldEntity(id);
		if (entity == null) {
			logger.warning("A nonexistent field entity has been set to self: " + id);
			return;
		}
		session.getMirrors().getSelfMirror().setFieldEntity(entity);
	}

}
