package org.kareha.hareka.client.parser;

import org.kareha.hareka.LocalSessionId;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class ResourceServerParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final String host = in.readString();
		final int port = in.readShort() & 0xffff;
		final boolean connectSecurely = in.readBoolean();
		final LocalSessionId sessionKey = LocalSessionId.readFrom(in);

		session.getMirrors().getSystemMirror().handleResourceServer(host, port, connectSecurely, sessionKey);
	}

}
