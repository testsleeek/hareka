package org.kareha.hareka.client.parser;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.game.AbilityType;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.wait.WaitType;

public class AddAbilityParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final AbilityType type = AbilityType.readFrom(in);
		final int reach = in.readCompactUInt();
		final WaitType waitType = WaitType.readFrom(in);
		final int wait = in.readCompactUInt();

		session.getMirrors().getAbilityMirror().handleAdd(type, reach, waitType, wait);
	}

}
