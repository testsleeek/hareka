package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class InspectFieldEntityPacket extends Packet {

	public InspectFieldEntityPacket(final FieldEntity target) {
		out.write(target.getLocalId());
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.INSPECT_FIELD_ENTITY;
	}

}
