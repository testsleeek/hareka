package org.kareha.hareka.client;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public final class Connector {

	private Connector() {
		throw new AssertionError();
	}

	public static Socket connectPlainly(final String host, final int port) throws UnknownHostException, IOException {
		return new Socket(host, port);
	}

	public static Socket connectSecurely(final String host, final int port, final X509TrustManager trustManager)
			throws UnknownHostException, SecurityException, IOException, KeyManagementException {
		final SSLContext sslContext;
		try {
			sslContext = SSLContext.getInstance("TLS");
		} catch (final NoSuchAlgorithmException e) {
			throw new AssertionError(e);
		}
		final TrustManager[] tm = new TrustManager[] { trustManager };
		sslContext.init(new KeyManager[0], tm, new SecureRandom());
		final SSLSocketFactory socketFactory = sslContext.getSocketFactory();
		return socketFactory.createSocket(host, port);
	}

}
