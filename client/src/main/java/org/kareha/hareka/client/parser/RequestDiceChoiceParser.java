package org.kareha.hareka.client.parser;

import java.util.logging.Logger;

import org.kareha.hareka.client.DiceRoll;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.packet.DiceChoicePacket;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;

public class RequestDiceChoiceParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(RequestDiceChoiceParser.class.getName());

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int id = in.readCompactUInt();
		final String hashAlgorithm = in.readString();
		final byte[] hash = in.readByteArray();
		final int faces = in.readCompactUInt();
		final int rate = in.readCompactUInt();

		final DiceRoll diceRoll = session.getDiceRollTable().getDiceRoll(id);
		if (diceRoll == null) {
			// TODO report error
			logger.warning("A nonexistent dice roll: id=" + id);
			return;
		}
		diceRoll.setHash(hashAlgorithm, hash);
		diceRoll.setFaces(faces);
		diceRoll.setRate(rate);

		session.write(new DiceChoicePacket(diceRoll));
	}

}
