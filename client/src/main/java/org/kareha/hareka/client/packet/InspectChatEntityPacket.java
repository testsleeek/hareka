package org.kareha.hareka.client.packet;

import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class InspectChatEntityPacket extends Packet {

	public InspectChatEntityPacket(final ChatEntity target) {
		out.write(target.getLocalId());
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.INSPECT_CHAT_ENTITY;
	}

}
