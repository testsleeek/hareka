package org.kareha.hareka.client.parser;

import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.user.CustomRole;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.user.Role;

public class RoleListParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int size = in.readCompactUInt();
		final List<Role> list = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			final Role role = CustomRole.readFrom(in);
			list.add(role);
		}

		session.getMirrors().getAdminMirror().handleRoleList(list);
	}

}
