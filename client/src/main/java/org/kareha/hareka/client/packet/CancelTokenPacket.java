package org.kareha.hareka.client.packet;

import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.packet.ServerPacketType;

public final class CancelTokenPacket extends Packet {

	public CancelTokenPacket(final int handlerId) {
		out.writeCompactUInt(handlerId);
	}

	@Override
	protected PacketType getType() {
		return ServerPacketType.CANCEL_TOKEN;
	}

}
