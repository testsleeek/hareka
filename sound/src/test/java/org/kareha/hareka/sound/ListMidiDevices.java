package org.kareha.hareka.sound;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequencer;
import javax.sound.midi.Synthesizer;
import javax.sound.midi.Transmitter;

final class ListMidiDevices {

	private ListMidiDevices() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		for (final MidiDevice.Info info : MidiSystem.getMidiDeviceInfo()) {
			System.out.println("----");
			System.out.println("Name: " + info.getName());
			System.out.println("Version: " + info.getVersion());
			System.out.println("Vendor: " + info.getVendor());
			System.out.println("Description: " + info.getDescription());

			System.out.print("Type: ");
			try {
				final MidiDevice device = MidiSystem.getMidiDevice(info);
				int count = 0;
				if (device instanceof Sequencer) {
					if (count > 0) {
						System.out.print(", ");
					}
					System.out.print("Sequencer");
					count++;
				}
				if (device instanceof Synthesizer) {
					if (count > 0) {
						System.out.print(", ");
					}
					System.out.print("Synthesizer");
					count++;
				}
				if (device instanceof Receiver) {
					if (count > 0) {
						System.out.print(", ");
					}
					System.out.print("Receiver");
					count++;
				}
				if (device instanceof Transmitter) {
					if (count > 0) {
						System.out.print(", ");
					}
					System.out.print("Transmitter");
					count++;
				}
			} catch (final MidiUnavailableException e) {
				System.out.print("Unavailable");
			}
			System.out.println();
		}
	}

}
