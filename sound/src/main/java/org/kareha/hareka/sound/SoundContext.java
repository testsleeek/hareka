package org.kareha.hareka.sound;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sound.midi.MidiUnavailableException;

public class SoundContext implements AutoCloseable {

	private static final Logger logger = Logger.getLogger(SoundContext.class.getName());

	private final SoundSettings settings;
	private final MidiPlayer midiPlayer;
	private final MusicPlayer musicPlayer;

	public SoundContext(final File dataDirectory) {
		settings = new SoundSettings(dataDirectory);
		MidiPlayer mp = null;
		try {
			mp = new MidiPlayer();
		} catch (final MidiUnavailableException e) {
			logger.log(Level.SEVERE, "", e);
		}
		midiPlayer = mp;

		musicPlayer = new MusicPlayer(midiPlayer);

		helperUpdate();
	}

	public SoundSettings getSettings() {
		return settings;
	}

	public MidiPlayer getMidiPlayer() {
		return midiPlayer;
	}

	public MusicPlayer getMusicPlayer() {
		return musicPlayer;
	}

	private void helperUpdate() {
		if (midiPlayer != null) {
			midiPlayer.setEnabled(settings.isMusicEnabled());
			try {
				midiPlayer.setDevice(settings.getMidiDevice());
			} catch (final MidiUnavailableException e) {
				logger.log(Level.SEVERE, "", e);
			}
		}
	}

	public void update() {
		helperUpdate();
	}

	@Override
	public void close() {
		if (midiPlayer != null) {
			midiPlayer.close();
		}
	}

}
