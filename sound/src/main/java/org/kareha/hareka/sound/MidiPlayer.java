package org.kareha.hareka.sound;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.Synthesizer;

import org.kareha.hareka.annotation.GuardedBy;

public class MidiPlayer implements AutoCloseable {

	private static final Logger logger = Logger.getLogger(MidiPlayer.class.getName());

	public static String getDefaultDeviceName() {
		final MidiDevice.Info[] infos;
		try {
			infos = MidiSystem.getMidiDeviceInfo();
		} catch (final UnsatisfiedLinkError e) {
			return null;
		}
		MidiDevice.Info candidate = null;
		// find Synthesizer that has best polyphony capability
		int maxPolyphony = -1;
		for (final MidiDevice.Info info : infos) {
			try {
				final MidiDevice device = MidiSystem.getMidiDevice(info);
				if (device instanceof Synthesizer) {
					final Synthesizer synthesizer = (Synthesizer) device;
					if (synthesizer.getMaxPolyphony() > maxPolyphony) {
						candidate = info;
						maxPolyphony = synthesizer.getMaxPolyphony();
					}
				}
			} catch (final MidiUnavailableException e) {
				continue;
			}
		}
		if (candidate == null) {
			// find Receiver
			for (final MidiDevice.Info info : infos) {
				try {
					final MidiDevice device = MidiSystem.getMidiDevice(info);
					if (device instanceof Receiver) {
						candidate = info;
						break;
					}
				} catch (final MidiUnavailableException e) {
					continue;
				}
			}
		}
		if (candidate == null) {
			return null;
		}
		return candidate.getName();
	}

	private static MidiDevice getDevice(final String name) throws MidiUnavailableException {
		final MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
		for (final MidiDevice.Info info : infos) {
			if (info.getName().equals(name)) {
				return MidiSystem.getMidiDevice(info);
			}
		}
		return null;
	}

	private final Sequencer sequencer;
	@GuardedBy("this")
	private boolean enabled = true;
	@GuardedBy("this")
	private boolean started = false;
	@GuardedBy("this")
	private String deviceName;
	@GuardedBy("this")
	private MidiDevice device;
	@GuardedBy("this")
	private Sequence sequence;

	public MidiPlayer() throws MidiUnavailableException {
		try {
			sequencer = MidiSystem.getSequencer(false);
		} catch (final UnsatisfiedLinkError e) {
			throw new MidiUnavailableException(e.getMessage());
		}
	}

	@GuardedBy("this")
	private void update() {
		if (!sequencer.isOpen()) {
			return;
		}
		if (enabled && started) {
			if (!sequencer.isRunning()) {
				if (sequencer.getSequence() != null) {
					sequencer.start();
				}
			}
		} else {
			if (sequencer.isRunning()) {
				sequencer.stop();
			}
		}
	}

	public synchronized void setEnabled(final boolean v) {
		enabled = v;
		update();
	}

	@GuardedBy("this")
	private void setDevice(final MidiDevice device) throws MidiUnavailableException {
		if (sequencer.isOpen()) {
			sequencer.close();
		}

		if (this.device != null && this.device.isOpen()) {
			this.device.close();
		}
		this.device = device;
		if (device != null && !device.isOpen()) {
			device.open();
		}

		sequencer.open();

		if (device != null) {
			sequencer.getTransmitter().setReceiver(device.getReceiver());
		}

		try {
			sequencer.setSequence(sequence);
		} catch (final InvalidMidiDataException e) {
			logger.log(Level.WARNING, "", e);
		}
		update();
	}

	public synchronized void setDevice(final String deviceName) throws MidiUnavailableException {
		if (this.deviceName == null) {
			if (deviceName == null) {
				return;
			}
		} else {
			if (this.deviceName.equals(deviceName)) {
				return;
			}
		}
		this.deviceName = deviceName;
		setDevice(getDevice(deviceName));
	}

	@Override
	public synchronized void close() {
		sequencer.close();
		if (device != null) {
			device.close();
		}
	}

	public synchronized void start() {
		started = true;
		update();
	}

	public synchronized void stop() {
		started = false;
		update();
	}

	public synchronized void setSequence(final Sequence sequence, final boolean loop) throws InvalidMidiDataException {
		this.sequence = sequence;
		sequencer.setSequence(sequence);
		if (loop) {
			sequencer.setLoopCount(Sequencer.LOOP_CONTINUOUSLY);
		} else {
			sequencer.setLoopCount(0);
		}
	}

}
