package org.kareha.hareka.updater;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class UpdaterUtil {

	private UpdaterUtil() {
		throw new AssertionError();
	}

	// @NotPure
	public static void runJar(final File file, final List<String> args) throws IOException {
		final List<String> argList = new ArrayList<>();
		argList.add(System.getProperty("java.home") + File.separator + "bin" + File.separator + "java");
		argList.add("-jar");
		argList.add(file.getAbsolutePath());
		argList.addAll(args);
		new ProcessBuilder(argList).directory(new File(System.getProperty("user.dir"))).start();
	}

}
