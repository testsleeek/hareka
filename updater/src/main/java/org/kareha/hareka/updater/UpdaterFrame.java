package org.kareha.hareka.updater;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;

@SuppressWarnings("serial")
public class UpdaterFrame extends JFrame {

	@Private
	static final Logger logger = Logger.getLogger(UpdaterFrame.class.getName());
	@SynchronizedWith("org.kareha.hareka.util.SingleLaunch")
	private static final String LOCK_FILENAME = "lock";
	private static final int BUFFER_SIZE = 8192;
	private static final int BAR_UPDATE_INTERVAL = 32;

	private enum BundleKey {
		Title, Message, Cancel,
	}

	@Private
	final UpdaterParameters parameters;
	@Private
	final Worker worker;
	@Private
	final JProgressBar bar;
	private FileOutputStream lockOut;
	@SuppressWarnings("unused")
	private FileLock lock;

	public UpdaterFrame(final UpdaterParameters parameters) {
		this.parameters = parameters;
		final ResourceBundle bundle = ResourceBundle.getBundle(UpdaterFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		worker = new Worker();

		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				worker.cancel(true);
				dispose();
			}
		});

		final JLabel label = new JLabel(bundle.getString(BundleKey.Message.name()));
		final JPanel labelPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 16, 16));
		labelPanel.add(label);
		add(labelPanel, BorderLayout.NORTH);

		bar = new JProgressBar();
		bar.setIndeterminate(true);
		final JPanel barPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 16, 16));
		barPanel.add(bar);
		add(barPanel, BorderLayout.CENTER);

		final JButton cancelButton = new JButton(bundle.getString(BundleKey.Cancel.name()));
		cancelButton.addActionListener(e -> worker.cancel(true));
		final JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 16, 16));
		buttonPanel.add(cancelButton);
		add(buttonPanel, BorderLayout.SOUTH);

		pack();
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public void downloadAndRun() {
		worker.execute();
	}

	@Private
	void lock() throws IOException {
		final File directory = parameters.getDataDirectory();
		if (!directory.isDirectory()) {
			if (!directory.mkdirs()) {
				throw new IOException("Cannot create directory");
			}
		}
		final File file = new File(directory, LOCK_FILENAME);
		lockOut = new FileOutputStream(file);
		final FileChannel channel = lockOut.getChannel();
		lock = channel.lock();
	}

	@Private
	void unlock() throws IOException {
		lockOut.close();
	}

	private class Worker extends SwingWorker<Void, Void> {

		@Private
		Worker() {

		}

		@Override
		protected Void doInBackground() {
			try {
				lock();
			} catch (final IOException e) {
				logger.log(Level.SEVERE, "", e);
				JOptionPane.showMessageDialog(UpdaterFrame.this, e.getMessage());
				cancel(true);
				return null;
			}
			boolean result = false;
			try {
				final URLConnection connection = parameters.getClientUrl().openConnection();
				final int contentLength = connection.getContentLength();
				if (contentLength != -1) {
					SwingUtilities.invokeLater(() -> {
						bar.setIndeterminate(false);
						bar.setMaximum(contentLength);
					});
				}
				try (final OutputStream out = new BufferedOutputStream(
						new FileOutputStream(parameters.getClientFile()))) {
					try (final InputStream in = new BufferedInputStream(connection.getInputStream())) {
						final byte[] buffer = new byte[BUFFER_SIZE];
						int total = 0;
						int count = 0;
						while (true) {
							if (Thread.currentThread().isInterrupted()) {
								return null;
							}
							final int length = in.read(buffer);
							if (length == -1) {
								break;
							}
							out.write(buffer, 0, length);
							total += length;
							count++;
							if (count % BAR_UPDATE_INTERVAL == 0) {
								if (contentLength != -1) {
									final int current = total;
									SwingUtilities.invokeLater(() -> bar.setValue(current));
								}
							}
						}
					}
					out.flush();
					result = true;
				}
			} catch (final IOException e) {
				logger.log(Level.SEVERE, "", e);
				JOptionPane.showMessageDialog(UpdaterFrame.this, e.getMessage());
				cancel(true);
				return null;
			} finally {
				if (!result) {
					parameters.getClientFile().delete();
				}
				try {
					unlock();
				} catch (final IOException e) {
					logger.log(Level.SEVERE, "", e);
					JOptionPane.showMessageDialog(UpdaterFrame.this, e.getMessage());
					cancel(true);
					return null;
				}
			}
			return null;
		}

		@Override
		protected void done() {
			try {
				if (isCancelled()) {
					return;
				}
				try {
					UpdaterUtil.runJar(parameters.getClientFile(), parameters.getClientArgs());
				} catch (final IOException e) {
					logger.log(Level.SEVERE, "", e);
					JOptionPane.showMessageDialog(UpdaterFrame.this, e.getMessage());
					return;
				}
			} finally {
				dispose();
			}
		}

	}

}
