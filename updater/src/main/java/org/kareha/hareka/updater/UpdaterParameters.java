package org.kareha.hareka.updater;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UpdaterParameters {

	private final boolean update;
	private final File dataDirectory;
	private final File clientFile;
	private final URL clientUrl;
	private final List<String> clientArgs;

	public UpdaterParameters(final String[] args) throws MalformedURLException {
		final PassingThroughParameters parameters = new PassingThroughParameters(args);

		update = parameters.getUnnamed().contains("--update");

		final String datadirOption = parameters.getNamed().get("datadir");
		if (datadirOption != null) {
			dataDirectory = new File(datadirOption);
		} else {
			dataDirectory = new File(UpdaterConstants.DATA_DIRECTORY_PATH);
		}

		final String pathOption = parameters.getNamed().get("path");
		if (pathOption != null) {
			clientFile = new File(pathOption);
		} else {
			clientFile = new File(UpdaterConstants.CLIENT_PATH);
		}

		final String urlOption = parameters.getNamed().get("url");
		if (urlOption != null) {
			clientUrl = new URL(urlOption);
		} else {
			clientUrl = new URL(UpdaterConstants.CLIENT_URL);
		}

		clientArgs = new ArrayList<>(parameters.getPassingThroughRaw());
		if (clientArgs.isEmpty() && datadirOption != null) {
			clientArgs.add("--datadir=" + datadirOption);
		}
	}

	public boolean isUpdate() {
		return update;
	}

	public File getDataDirectory() {
		return dataDirectory;
	}

	public File getClientFile() {
		return clientFile;
	}

	public URL getClientUrl() {
		return clientUrl;
	}

	public List<String> getClientArgs() {
		return Collections.unmodifiableList(clientArgs);
	}

}
