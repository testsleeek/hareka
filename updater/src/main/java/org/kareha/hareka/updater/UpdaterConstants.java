package org.kareha.hareka.updater;

import java.io.File;

public final class UpdaterConstants {

	private UpdaterConstants() {
		throw new AssertionError();
	}

	@SynchronizedWith("org.kareha.hareka.client.ClientConstants")
	public static final String DATA_DIRECTORY_PATH = System.getProperty("user.dir") + File.separator + "hareka";
	public static final String CLIENT_PATH = System.getProperty("user.dir") + File.separator + "hareka.jar";
	public static final String CLIENT_URL = "https://hareka.kareha.org/hareka.jar";

}
