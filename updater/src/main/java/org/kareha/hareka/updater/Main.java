package org.kareha.hareka.updater;

import java.awt.HeadlessException;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

final class Main {

	private static final Logger logger = Logger.getLogger(Main.class.getName());

	private Main() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		try {
			final UpdaterParameters parameters = new UpdaterParameters(args);
			if (!parameters.isUpdate() && parameters.getClientFile().isFile()) {
				UpdaterUtil.runJar(parameters.getClientFile(), parameters.getClientArgs());
				return;
			}
			SwingUtilities.invokeLater(() -> {
				new UpdaterFrame(parameters).downloadAndRun();
			});
		} catch (final Exception e) {
			logger.log(Level.SEVERE, "", e);
			showMessage(e.getMessage());
		}
	}

	private static void showMessage(final String message) {
		try {
			SwingUtilities.invokeAndWait(() -> {
				JOptionPane.showMessageDialog(null, message);
			});
		} catch (final HeadlessException | InvocationTargetException e) {
			logger.log(Level.SEVERE, "", e);
		} catch (final InterruptedException e) {
			logger.log(Level.SEVERE, "", e);
			Thread.currentThread().interrupt();
		}
	}

}
