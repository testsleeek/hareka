package org.kareha.hareka.updater;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PassingThroughParameters {

	private final List<String> raw = new ArrayList<>();
	private final List<String> unnamed = new ArrayList<>();
	private final Map<String, String> named = new HashMap<>();
	private final List<String> passingThroughRaw = new ArrayList<>();
	private final List<String> passingThroughUnnamed = new ArrayList<>();
	private final Map<String, String> passingThroughNamed = new HashMap<>();

	public PassingThroughParameters(final String[] args) {
		final Pattern pattern = Pattern.compile("^--(.+?)=(.*)$");
		int i = 0;
		for (; i < args.length; i++) {
			final String a = args[i];
			if (a == null) {
				continue;
			}
			if (a.equals("--")) {
				break;
			}
			raw.add(a);
			final Matcher m = pattern.matcher(a);
			if (m.matches()) {
				named.put(m.group(1), m.group(2));
			} else {
				unnamed.add(a);
			}
		}
		for (; i < args.length; i++) {
			final String a = args[i];
			if (a == null) {
				continue;
			}
			passingThroughRaw.add(a);
			final Matcher m = pattern.matcher(a);
			if (m.matches()) {
				passingThroughNamed.put(m.group(1), m.group(2));
			} else {
				passingThroughUnnamed.add(a);
			}
		}
	}

	public List<String> getRaw() {
		return Collections.unmodifiableList(raw);
	}

	public List<String> getUnnamed() {
		return Collections.unmodifiableList(unnamed);
	}

	public Map<String, String> getNamed() {
		return Collections.unmodifiableMap(named);
	}

	public List<String> getPassingThroughRaw() {
		return Collections.unmodifiableList(passingThroughRaw);
	}

	public List<String> getPassingThroughUnnamed() {
		return Collections.unmodifiableList(passingThroughUnnamed);
	}

	public Map<String, String> getPassingThroughNamed() {
		return Collections.unmodifiableMap(passingThroughNamed);
	}

}
