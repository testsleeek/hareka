package org.kareha.hareka.updater;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class PassingThroughParametersTest {

	private static final String[] raw = { "foo", "--bar=buz", "--qux", "--test=hello=world" };
	private static final String[] unnamed = { "foo", "--qux", };
	private static final Map<String, String> named;
	static {
		named = new HashMap<>();
		named.put("bar", "buz");
		named.put("test", "hello=world");
	}

	public static void main(final String[] args) {
		final PassingThroughParameters parameters = new PassingThroughParameters(raw);
		for (final String a : parameters.getUnnamed()) {
			System.out.println(a);
		}
	}

	@Test
	public void testRaw() {
		final PassingThroughParameters parameters = new PassingThroughParameters(raw);
		final String[] result = parameters.getRaw().toArray(new String[0]);
		assertArrayEquals(raw, result);
	}

	@Test
	public void testUnnamed() {
		final PassingThroughParameters parameters = new PassingThroughParameters(raw);
		final String[] result = parameters.getUnnamed().toArray(new String[0]);
		assertArrayEquals(unnamed, result);
	}

	@Test
	public void testNamed() {
		final PassingThroughParameters parameters = new PassingThroughParameters(raw);
		assertTrue(parameters.getNamed().equals(named));
	}

	@Test
	public void testNull() {
		final String[] args = { null, };
		final PassingThroughParameters parameters = new PassingThroughParameters(args);
		assertTrue(parameters.getRaw().size() == 0);
	}

	@Test
	public void testEmpty() {
		final String[] args = { "", };
		final PassingThroughParameters parameters = new PassingThroughParameters(args);
		assertTrue(parameters.getRaw().size() == 1);
		assertTrue(parameters.getRaw().get(0).isEmpty());
	}

}
