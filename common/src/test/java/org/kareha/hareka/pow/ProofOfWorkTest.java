package org.kareha.hareka.pow;

import java.math.BigInteger;

final class ProofOfWorkTest {

	private static final byte[] data = { 0, 1, 2, 3, 4, 5, 6, 7 };

	private ProofOfWorkTest() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		final int bits = 21;
		// final SecureRandom random = new SecureRandom();
		// final byte[] data = new byte[32];
		// random.nextBytes(data);
		final BigInteger target = BigInteger.ONE.shiftLeft(256 - bits).subtract(BigInteger.ONE);
		final ProofOfWork pow = new ProofOfWork(target, data);
		final long start = System.currentTimeMillis();
		final byte[] nonce;
		try {
			nonce = pow.generate(new ProofOfWork.Listener() {
				@Override
				public void progress(final int goal, final int current) {
					for (int i = 0; i < current; i++) {
						System.out.print("*");
					}
					for (int i = current; i < goal; i++) {
						System.out.print(".");
					}
					System.out.println();
				}
			});
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
			return;
		}
		final long end = System.currentTimeMillis();
		final float t = (end - start) / 1000f;
		System.out.println(t);

		final ProofOfWork pow2 = new ProofOfWork(target, data);
		final boolean result = pow2.verify(nonce);
		System.out.println(result);

		printByteArray(nonce);
		final byte[] hash = pow.naiveHash(nonce);
		printByteArray(hash);
	}

	private static void printByteArray(final byte[] b) {
		for (int i = 0; i < b.length; i++) {
			System.out.print(String.format("%02x", b[i] & 0xff));
		}
		System.out.println();
	}

}
