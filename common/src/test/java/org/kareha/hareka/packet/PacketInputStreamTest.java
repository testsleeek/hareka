package org.kareha.hareka.packet;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.Test;
import org.kareha.hareka.packet.PacketInputStream;
import org.kareha.hareka.packet.PacketOutputStream;

public class PacketInputStreamTest {

	private static final String TEST_STRING = "Hello World";

	@Test
	public void string() throws IOException {
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		try (final PacketOutputStream pout = new PacketOutputStream()) {
			pout.writeString(TEST_STRING);
			pout.writeTo(out);
		}
		final String resultString;
		try (final PacketInputStream pin = new PacketInputStream(out.toByteArray())) {
			resultString = pin.readString();
		}
		assertEquals(TEST_STRING, resultString);
	}

}
