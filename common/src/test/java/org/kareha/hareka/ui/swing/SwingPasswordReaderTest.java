package org.kareha.hareka.ui.swing;

import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

final class SwingPasswordReaderTest {

	private SwingPasswordReaderTest() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		SwingUtilities.invokeLater(() -> {
			final JFrame owner = null;
			final char[] password = new PasswordReader(owner).get();
			try {
				if (password == null) {
					JOptionPane.showMessageDialog(owner, "Canceled");
				} else {
					JOptionPane.showMessageDialog(owner, "Entered password: " + String.valueOf(password));
				}
			} finally {
				if (password != null) {
					Arrays.fill(password, '\0');
				}
			}
		});
	}

}
