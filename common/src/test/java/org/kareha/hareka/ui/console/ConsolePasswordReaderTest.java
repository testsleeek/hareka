package org.kareha.hareka.ui.console;

import java.util.Arrays;

final class ConsolePasswordReaderTest {

	private ConsolePasswordReaderTest() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		final char[] password = new ConsolePasswordReader().get();
		try {
			if (password == null) {
				System.out.println("Canceled");
			} else {
				System.out.print("Entered password: ");
				System.out.println(password);
			}
		} finally {
			if (password != null) {
				Arrays.fill(password, '\0');
			}
		}
	}

}
