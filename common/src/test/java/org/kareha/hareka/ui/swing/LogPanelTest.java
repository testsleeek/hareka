package org.kareha.hareka.ui.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.kareha.hareka.ui.swing.LogPanel;

final class LogPanelTest {

	private static final Logger logger = Logger.getLogger(LogPanelTest.class.getName());

	private LogPanelTest() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		final JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		final LogPanel logPanel = LogPanel.newInstance();
		logPanel.setMaxLogSize(4096);
		frame.add(logPanel, BorderLayout.CENTER);

		final JButton button = new JButton("Add Lines");
		final AtomicInteger count = new AtomicInteger();
		button.addActionListener(e -> {
			for (int i = 0; i < 256; i++) {
				logger.info(count + ": " + i + ": hello");
			}
			count.incrementAndGet();
		});
		frame.add(button, BorderLayout.SOUTH);

		frame.setPreferredSize(new Dimension(400, 400));
		frame.pack();
		frame.setVisible(true);
	}

}
