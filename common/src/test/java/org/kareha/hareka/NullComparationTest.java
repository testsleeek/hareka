package org.kareha.hareka;

final class NullComparationTest {

	private NullComparationTest() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		final boolean b = null == null;
		System.out.println(b);
	}

}
