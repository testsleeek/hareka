package org.kareha.hareka.field;

import org.kareha.hareka.packet.PacketElement;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.PacketOutput;

public enum TilePatternType implements PacketElement {

	SOLID, ONE_UNIFORM;

	private static final TilePatternType[] values = values();

	public static TilePatternType valueOf(final int ordinal) {
		if (ordinal < 0 || ordinal >= values.length) {
			throw new IllegalArgumentException("Out of bounds");
		}
		return values[ordinal];
	}

	public static TilePatternType readFrom(final PacketInput in) {
		final int ordinal = in.readCompactUInt();
		return valueOf(ordinal);
	}

	@Override
	public void writeTo(final PacketOutput out) {
		out.writeCompactUInt(ordinal());
	}

}
