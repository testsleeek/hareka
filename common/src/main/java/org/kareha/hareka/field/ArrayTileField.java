package org.kareha.hareka.field;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicReferenceArray;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.packet.PacketInputStream;
import org.kareha.hareka.packet.PacketOutputStream;

@ThreadSafe
public class ArrayTileField extends AbstractTileField {

	protected final int size;
	protected final AtomicReferenceArray<Tile> array;

	public ArrayTileField(final boolean mutable, final TilePattern pattern, final int size) {
		super(mutable, pattern);
		this.size = size;
		array = new AtomicReferenceArray<>(size == 0 ? 0 : (size * 2 - 1) * (size * 2 - 1));
	}

	@Private
	void saveData(final OutputStream out) throws IOException {
		final PacketOutputStream dout = new PacketOutputStream();
		for (int y = -size + 1; y <= size - 1; y++) {
			for (int x = -size + 1; x <= size - 1; x++) {
				final Vector position = Vector.valueOf(x, y);
				if (position.distance() >= size) {
					continue;
				}
				final int index = (position.y() + size - 1) * size + (position.x() + size - 1);
				Tile tile = array.get(index);
				if (tile == null) {
					tile = getBackgroundTile(position);
				}
				tile.writeTo(dout);
			}
		}
		dout.writeTo(out);
	}

	@Private
	void loadData(final byte[] data, final Palette palette) {
		final PacketInputStream din = new PacketInputStream(data);
		for (int y = -size + 1; y <= size - 1; y++) {
			for (int x = -size + 1; x <= size - 1; x++) {
				final Vector position = Vector.valueOf(x, y);
				if (position.distance() >= size) {
					continue;
				}
				final int index = (position.y() + size - 1) * size + (position.x() + size - 1);
				array.set(index, Tile.readFrom(din, palette));
			}
		}
	}

	@XmlType(name = "arrayTileField")
	@XmlAccessorType(XmlAccessType.NONE)
	protected static class Adapted extends AbstractTileField.Adapted {

		@XmlElement
		protected int size;
		@XmlElement
		protected Palette palette;
		@XmlElement
		protected byte[] data;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final ArrayTileField v) {
			super(v);
			size = v.size;
			palette = new Palette(TileType.NULL);
			try (final ByteArrayOutputStream out = new ByteArrayOutputStream()) {
				v.saveData(out);
				data = out.toByteArray();
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		protected ArrayTileField unmarshal() {
			final ArrayTileField hf = new ArrayTileField(mutable, pattern, size);
			if (regions != null) {
				hf.regions.addAll(regions);
			}
			hf.loadData(data, palette);
			return hf;
		}

	}

	@Override
	protected Adapted marshal() {
		return new Adapted(this);
	}

	@Override
	public Tile getTile(final Vector position) {
		if (position.distance() >= size) {
			return pattern.getTile(position);
		}
		final int index = (position.y() + size - 1) * (size * 2 - 1) + (position.x() + size - 1);
		final Tile tile = array.get(index);
		// XXX this conditional may be omitted if array is filled with non-null
		// values
		if (tile == null) {
			return getBackgroundTile(position);
		}
		return tile;
	}

	@Override
	public void setTile(final Vector position, final Tile tile, final boolean force) {
		if (position.distance() >= size) {
			return;
		}
		if (!force && !isMutable(position)) {
			return;
		}
		final int index = (position.y() + size - 1) * (size * 2 - 1) + (position.x() + size - 1);
		array.set(index, tile);
	}

	public void renderRegions() {
		for (int y = -size + 1; y <= size - 1; y++) {
			for (int x = -size + 1; x <= size - 1; x++) {
				final Vector position = Vector.valueOf(x, y);
				if (position.distance() >= size) {
					continue;
				}
				final int index = (position.y() + size - 1) * size + (position.x() + size - 1);
				array.set(index, getBackgroundTile(position));
			}
		}
	}

}
