package org.kareha.hareka.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.packet.PacketOutput;

@Immutable
public class SimpleRegion extends Region {

	public static class Builder extends Region.Builder {

		protected Vector center = Vector.ZERO;
		protected int size;

		public Builder center(final Vector v) {
			center = v;
			return this;
		}

		public Builder size(final int v) {
			size = v;
			return this;
		}

		@Override
		public SimpleRegion build() {
			return new SimpleRegion(this);
		}

	}

	protected final Vector center;
	protected final int size;

	protected SimpleRegion(final Builder builder) {
		super(builder);
		center = builder.center;
		size = builder.size;
	}

	public SimpleRegion(final TilePattern tilePattern, final boolean mutable, final Vector center, final int size) {
		super(tilePattern, mutable);
		this.center = center;
		this.size = size;
	}

	protected SimpleRegion(final SimpleRegion original) {
		super(original.tilePattern, original.mutable);
		center = original.center;
		size = original.size;
	}

	@Override
	public void writeTo(final PacketOutput out) {
		out.write(RegionType.SIMPLE);
		out.write(tilePattern);
		out.writeBoolean(mutable);
		out.write(center);
		out.writeCompactUInt(size);
	}

	@XmlType(name = "simpleRegion")
	@XmlAccessorType(XmlAccessType.NONE)
	protected static class Adapted extends Region.Adapted {

		@XmlElement
		protected Vector center;
		@XmlElement
		protected int size;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final SimpleRegion v) {
			super(v);
			center = v.center;
			size = v.size;
		}

		@Override
		protected SimpleRegion unmarshal() {
			final Builder builder = new Builder();
			builder.tilePattern(tilePattern).mutable(mutable);
			builder.center(center).size(size);
			return builder.build();
		}

	}

	@Override
	public Adapted marshal() {
		return new Adapted(this);
	}

	@Override
	public SimpleRegion transform(final Transformation transformation) {
		return new SimpleRegion(tilePattern, mutable, center.transform(transformation), size);
	}

	@Override
	public SimpleRegion invert(final Transformation transformation) {
		return new SimpleRegion(tilePattern, mutable, center.invert(transformation), size);
	}

	@Override
	public boolean contains(final Vector position) {
		return position.distance(center) <= size;
	}

	@Override
	public Tile getTile(final Vector position) {
		return super.getTile(position.subtract(center));
	}

	public Vector getCenter() {
		return center;
	}

	public int getSize() {
		return size;
	}

}
