package org.kareha.hareka.field;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.kareha.hareka.annotation.GuardedBy;

public abstract class FieldObjectWithView extends AbstractFieldObject {

	protected final Object viewLock = new Object();
	@GuardedBy("viewLock")
	protected Set<FieldObject> inViewObjects;
	protected final Object motionLock = new Object();

	public FieldObjectWithView(final Field field, final Placement placement) {
		super(field, placement);
	}

	@Override
	public boolean addInViewObject(final FieldObject o) {
		synchronized (viewLock) {
			if (inViewObjects == null) {
				return true;
			}
			return inViewObjects.add(o);
		}
	}

	@Override
	public boolean removeInViewObject(final FieldObject o) {
		synchronized (viewLock) {
			if (inViewObjects == null) {
				return true;
			}
			return inViewObjects.remove(o);
		}
	}

	@Override
	public void inViewObjectMoved(final FieldObject v, final Placement placement) {
		// nothing to do
	}

	@Override
	public Collection<FieldObject> getInViewObjects() {
		synchronized (viewLock) {
			if (inViewObjects != null) {
				if (inViewObjects.isEmpty()) {
					return Collections.emptyList();
				}
				return new ArrayList<>(inViewObjects);
			}
		}
		final Set<FieldObject> set = new HashSet<>(getField().getInViewObjects(getPlacement().getPosition()));
		synchronized (viewLock) {
			inViewObjects = set;
			if (inViewObjects.isEmpty()) {
				return Collections.emptyList();
			}
			return new ArrayList<>(inViewObjects);
		}
	}

	public boolean isMovable(final Vector position, final Vector prevPosition) {
		final Tile tile = field.getTile(position);
		if (!tile.type().isWalkable()) {
			return false;
		}
		if (isExclusive() && !tile.type().isStackable() && field.isExclusive(position)) {
			return false;
		}

		final Tile prevTile = field.getTile(prevPosition);
		final int diff = tile.elevation() - prevTile.elevation();
		if (diff > getJumpUp()) {
			return false;
		}

		return true;
	}

	@Override
	public Placement move(final Placement placement) {
		final Placement prevPlacement;
		synchronized (motionLock) {
			synchronized (placementLock) {
				if (!isMovable(placement.getPosition(), this.placement.getPosition())) {
					return null;
				}

				// if (!placement.getPosition().isShort()) {
				// return null;
				// }

				prevPlacement = this.placement;
				this.placement = placement;
			}

			getField().moveObject(this);

			final boolean exists;
			synchronized (viewLock) {
				if (inViewObjects == null) {
					exists = false;
				} else {
					exists = true;
					for (final FieldObject o : inViewObjects) {
						o.inViewObjectMoved(this, placement);
					}
				}
			}
			if (!exists) {
				final Set<FieldObject> set = new HashSet<>(getField().getInViewObjects(prevPlacement.getPosition()));
				synchronized (viewLock) {
					inViewObjects = set;
					for (final FieldObject o : inViewObjects) {
						o.inViewObjectMoved(this, placement);
					}
				}
			}

			updateInViewObjects();
		}

		return prevPlacement;
	}

	private void updateInViewObjects() {
		final Set<FieldObject> next = new HashSet<>(field.getInViewObjects(getPlacement().getPosition()));
		final Set<FieldObject> gone = new HashSet<>();
		final Set<FieldObject> arrived = new HashSet<>();
		synchronized (viewLock) {
			for (final FieldObject o : inViewObjects) {
				if (!next.contains(o)) {
					gone.add(o);
				}
			}
			for (final FieldObject o : next) {
				if (!inViewObjects.contains(o)) {
					arrived.add(o);
				}
			}
		}
		for (final FieldObject o : gone) {
			removeInViewObject(o);
			o.removeInViewObject(this);
		}
		for (final FieldObject o : arrived) {
			addInViewObject(o);
			o.addInViewObject(this);
		}
	}

	@Override
	public boolean containsInView(final FieldObject fieldObject) {
		synchronized (viewLock) {
			if (inViewObjects != null) {
				return inViewObjects.contains(fieldObject);
			}
		}
		return field.getInViewObjects(getPlacement().getPosition()).contains(fieldObject);
	}

}
