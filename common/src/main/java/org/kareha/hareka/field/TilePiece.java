package org.kareha.hareka.field;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.packet.PacketElement;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.PacketOutput;

@Immutable
public final class TilePiece implements PacketElement {

	private final Vector position;
	private final Tile tile;

	private TilePiece(final Vector position, final Tile tile) {
		if (position == null || tile == null) {
			throw new IllegalArgumentException("null");
		}
		this.position = position;
		this.tile = tile;
	}

	public static TilePiece valueOf(final Vector position, final Tile tile) {
		return new TilePiece(position, tile);
	}

	public static TilePiece readFrom(final PacketInput in) {
		final Vector position = Vector.readFrom(in);
		final Tile tile = Tile.readFrom(in);
		return valueOf(position, tile);
	}

	@Override
	public void writeTo(final PacketOutput out) {
		out.write(position);
		out.write(tile);
	}

	public Vector position() {
		return position;
	}

	public Tile tile() {
		return tile;
	}

	public TilePiece transform(final Transformation v) {
		return valueOf(position.transform(v), tile.transform(v));
	}

	public TilePiece invert(final Transformation v) {
		return valueOf(position.invert(v), tile.invert(v));
	}

}
