package org.kareha.hareka.field;

import java.util.Collection;

public interface FieldObject {

	Field getField();

	Placement getPlacement();

	boolean mount();

	Placement unmount();

	Placement move(Placement placement);

	boolean addInViewObject(FieldObject fieldObject);

	boolean removeInViewObject(FieldObject fieldObject);

	void inViewObjectMoved(FieldObject fieldObject, Placement placement);

	Collection<FieldObject> getInViewObjects();

	boolean containsInView(FieldObject fieldObject);

	boolean isExclusive();

	int getJumpDown();

	int getJumpUp();

}
