package org.kareha.hareka.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.packet.PacketElement;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.PacketOutput;

// See http://www.redblobgames.com/grids/hexagons/
/**
 * A vector class for horizontal hexagonal grid in axial coordinate system.
 * 
 * @author Aki Goto
 * @since Hareka0.1
 */
@Immutable
@XmlJavaTypeAdapter(Vector.Adapter.class)
public final class Vector implements PacketElement {

	private static final int PREDEFINED_SIZE = 128;
	private static final Vector[] predefined = new Vector[(PREDEFINED_SIZE * 2) * (PREDEFINED_SIZE * 2)];

	static {
		for (int y = -PREDEFINED_SIZE; y < PREDEFINED_SIZE; y++) {
			for (int x = -PREDEFINED_SIZE; x < PREDEFINED_SIZE; x++) {
				predefined[(PREDEFINED_SIZE * 2) * (y + PREDEFINED_SIZE) + (x + PREDEFINED_SIZE)] = new Vector(x, y);
			}
		}
	}

	/**
	 * The {@code Vector} object corresponding to the zero vector.
	 */
	public static final Vector ZERO = valueOf(0, 0);

	/**
	 * The axial x component.
	 */
	@Private
	final int x;

	/**
	 * The axial y component.
	 */
	@Private
	final int y;

	/**
	 * The constructor.
	 * 
	 * @param x
	 *            the axial x component.
	 * @param y
	 *            the axial y component.
	 */
	private Vector(final int x, final int y) {
		this.x = x;
		this.y = y;
	}

	// Is the method required?
	/**
	 * Returns the {@code Vector} object specified by the axial x, y components.
	 * 
	 * @param x
	 *            the axial x component.
	 * @param y
	 *            the axial y component.
	 * @return The {@code Vector} object specified by the axial x, y components.
	 */
	public static Vector valueOf(final int x, final int y) {
		if (x < -PREDEFINED_SIZE || x >= PREDEFINED_SIZE || y < -PREDEFINED_SIZE || y >= PREDEFINED_SIZE) {
			return new Vector(x, y);
		}
		return predefined[(PREDEFINED_SIZE * 2) * (y + PREDEFINED_SIZE) + (x + PREDEFINED_SIZE)];
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof Vector)) {
			return false;
		}
		final Vector point = (Vector) obj;
		return point.x == x && point.y == y;
	}

	@Override
	public int hashCode() {
		return x ^ (y << 16 | y >>> 16);
	}

	@Override
	public String toString() {
		return x + "," + y;
	}

	/**
	 * Reads the next {@code Vector} object from the {@code PacketInput}.
	 * 
	 * @param in
	 *            the {@code PacketInput} from which the {@code Vector} object is
	 *            read.
	 * @return the next {@code Vector} object.
	 */
	public static Vector readFrom(final PacketInput in) {
		final int x = in.readCompactInt();
		final int y = in.readCompactInt();
		return valueOf(x, y);
	}

	@Override
	public void writeTo(final PacketOutput out) {
		out.writeCompactInt(x);
		out.writeCompactInt(y);
	}

	@XmlType(name = "vector")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		private int x;
		@XmlElement
		private int y;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Vector v) {
			x = v.x;
			y = v.y;
		}

		@Private
		Vector unmarshal() {
			return valueOf(x, y);
		}

	}

	/**
	 * A converter method for {@code XmlAdapter}.
	 * 
	 * @return the {@code Adapted} object for the {@code Vector} object.
	 */
	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, Vector> {

		@Override
		public Adapted marshal(final Vector v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public Vector unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	/**
	 * Returns the axial q component of the {@code Vector} object.
	 * 
	 * @return the axial q component of the {@code Vector} object.
	 */
	public int x() {
		return x;
	}

	/**
	 * Returns the axial r component of the {@code Vector} object.
	 * 
	 * @return the axial r component of the {@code Vector} object.
	 */
	public int y() {
		return y;
	}

	/**
	 * Returns a {@code Vector} object which is the neighbor of the {@code Vector}
	 * object directed to the specified {@code Direction}.
	 * 
	 * @param direction
	 *            specifies which {@code Direction} the neighbor is.
	 * @return a {@code Vector} object which is the neighbor of the {@code Vector}
	 *         object directed to the specified {@code Direction} .
	 */
	public Vector neighbor(final Direction direction) {
		return add(direction.vector());
	}

	/**
	 * Returns the opposite {@code Vector}.
	 * 
	 * @return the opposite {@code Vector}.
	 */
	public Vector opposite() {
		return valueOf(-x, -y);
	}

	/**
	 * Returns the {@code Vector} multiplied by the specified scalar.
	 * 
	 * @param v
	 *            the scalar multiplier.
	 * @return the {@code Vector} multiplied by the specified scalar.
	 */
	public Vector multiply(final int v) {
		return valueOf(x * v, y * v);
	}

	public Vector add(final Vector v) {
		return valueOf(x + v.x, y + v.y);
	}

	public Vector subtract(final Vector v) {
		return valueOf(x - v.x, y - v.y);
	}

	public Vector rotate(final int v) {
		switch (Math.floorMod(v, 6)) {
		default:
			throw new AssertionError();
		case 0:
			// x, y, z
			return this;
		case 1:
			// -z, -x, -y
			return valueOf(-y, x + y);
		case 2:
			// y, z, x
			return valueOf(-x - y, x);
		case 3:
			// -x, -y, -z
			return valueOf(-x, -y);
		case 4:
			// z, x, y
			return valueOf(y, -x - y);
		case 5:
			// -y, -z, -x
			return valueOf(x + y, -x);
		}
	}

	public Vector reflect() {
		return valueOf(-x - y, y);
	}

	public Vector reflect(final boolean v) {
		if (!v) {
			return this;
		}
		return reflect();
	}

	public Vector transform(final Transformation v) {
		return subtract(v.translation).rotate(-v.rotation).reflect(v.reflection);
	}

	public Vector invert(final Transformation v) {
		return reflect(v.reflection).rotate(v.rotation).add(v.translation);
	}

	public Direction direction() {
		if (this.equals(ZERO)) {
			return Direction.NULL;
		}
		// XXX don't use FP
		final double u = Math.sqrt(3) * (x + y / 2.0);
		final double v = 3.0 / 2 * y;
		final double a = Math.atan2(v, u);
		if (a >= Math.PI * 5 / 6 || a < -Math.PI * 5 / 6) {
			return Direction.LEFT;
		} else if (a < -Math.PI * 3 / 6) {
			return Direction.UP_LEFT;
		} else if (a < -Math.PI * 1 / 6) {
			return Direction.UP_RIGHT;
		} else if (a < Math.PI * 1 / 6) {
			return Direction.RIGHT;
		} else if (a < Math.PI * 3 / 6) {
			return Direction.DOWN_RIGHT;
		} else { // if (a < Math.PI * 5 / 6) {
			return Direction.DOWN_LEFT;
		}
	}

	public int distance() {
		return (Math.abs(x) + Math.abs(-x - y) + Math.abs(y)) / 2;
	}

	public int distance(final Vector point) {
		return (Math.abs(point.x - x) + Math.abs((-point.x - point.y) - (-x - y)) + Math.abs(point.y - y)) / 2;
	}

}
