package org.kareha.hareka.field;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
public class ArrayListHashMap<K, V> {

	private final ReadWriteLock lock = new ReentrantReadWriteLock();
	private final Lock readLock = lock.readLock();
	private final Lock writeLock = lock.writeLock();
	@GuardedBy("lock")
	private final Map<K, List<V>> listMap = new HashMap<>();
	@GuardedBy("lock")
	private final Map<V, K> keyMap = new HashMap<>();

	public boolean contains(final V value) {
		readLock.lock();
		try {
			return keyMap.containsKey(value);
		} finally {
			readLock.unlock();
		}
	}

	public boolean put(final K key, final V value) {
		writeLock.lock();
		try {
			if (contains(value)) {
				return false;
			}
			List<V> list = listMap.get(key);
			if (list == null) {
				list = new ArrayList<>();
				listMap.put(key, list);
			}
			list.add(value);
			keyMap.put(value, key);
			return true;
		} finally {
			writeLock.unlock();
		}
	}

	public boolean remove(final V value) {
		writeLock.lock();
		try {
			final K key = keyMap.remove(value);
			if (key == null) {
				return false;
			}
			final List<V> list = listMap.get(key);
			if (list == null) {
				return false;
			}
			final boolean result = list.remove(value);
			if (list.isEmpty()) {
				listMap.remove(key);
			}
			return result;
		} finally {
			writeLock.unlock();
		}
	}

	public boolean move(final K key, final V value) {
		writeLock.lock();
		try {
			if (!remove(value)) {
				return false;
			}
			List<V> list = listMap.get(key);
			if (list == null) {
				list = new ArrayList<>();
				listMap.put(key, list);
			}
			list.add(value);
			keyMap.put(value, key);
			return true;
		} finally {
			writeLock.unlock();
		}
	}

	public List<V> get(final K key) {
		readLock.lock();
		try {
			final List<V> list = listMap.get(key);
			// no empty list exists in listMap
			if (list == null) {
				return Collections.emptyList();
			}
			return new ArrayList<>(list);
		} finally {
			readLock.unlock();
		}
	}

	public Collection<V> values() {
		readLock.lock();
		try {
			final List<V> values = new ArrayList<>();
			for (final List<V> list : listMap.values()) {
				values.addAll(list);
			}
			if (values.isEmpty()) {
				return Collections.emptyList();
			}
			return values;
		} finally {
			readLock.unlock();
		}
	}

}
