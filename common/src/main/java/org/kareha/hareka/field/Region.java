package org.kareha.hareka.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.packet.PacketElement;
import org.kareha.hareka.packet.PacketInput;

@Immutable
@XmlJavaTypeAdapter(Region.Adapter.class)
public abstract class Region implements PacketElement {

	protected static abstract class Builder {

		protected TilePattern tilePattern = TilePattern.SOLID_NULL_0;
		protected boolean mutable = true;

		public Builder tilePattern(final TilePattern v) {
			tilePattern = v;
			return this;
		}

		public Builder mutable(final boolean v) {
			mutable = v;
			return this;
		}

		public abstract Region build();

	}

	protected final TilePattern tilePattern;
	protected final boolean mutable;

	protected Region(final Builder builder) {
		tilePattern = builder.tilePattern;
		mutable = builder.mutable;
	}

	protected Region(final TilePattern tilePattern, final boolean mutable) {
		this.tilePattern = tilePattern;
		this.mutable = mutable;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		if (mutable) {
			sb.append("Mutable");
		} else {
			sb.append("Immutable");
		}
		return sb.toString();
	}

	public static Region readFrom(final PacketInput in) {
		switch (RegionType.readFrom(in)) {
		default:
			return null;
		case SIMPLE:
			final TilePattern tilePattern = TilePattern.readFrom(in);
			final boolean mutable = in.readBoolean();
			final Vector center = Vector.readFrom(in);
			final int size = in.readCompactUInt();
			return new SimpleRegion(tilePattern, mutable, center, size);
		}
	}

	@XmlType(name = "region")
	@XmlSeeAlso({ SimpleRegion.Adapted.class })
	@XmlAccessorType(XmlAccessType.NONE)
	protected static abstract class Adapted {

		@XmlElement
		protected TilePattern tilePattern;
		@XmlElement
		protected boolean mutable;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final Region v) {
			tilePattern = v.tilePattern;
			mutable = v.mutable;
		}

		protected abstract Region unmarshal();

	}

	protected abstract Adapted marshal();

	static class Adapter extends XmlAdapter<Adapted, Region> {

		@Override
		public Adapted marshal(final Region v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public Region unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public abstract Region transform(final Transformation transformation);

	public abstract Region invert(final Transformation transformation);

	public TilePattern getTilePattern() {
		return tilePattern;
	}

	public boolean isMutable() {
		return mutable;
	}

	public abstract boolean contains(Vector position);

	public Tile getTile(final Vector position) {
		return tilePattern.getTile(position);
	}

}
