package org.kareha.hareka.field;

import org.kareha.hareka.annotation.GuardedBy;

public abstract class AbstractFieldObject implements FieldObject {

	protected final Field field;
	protected final Object placementLock = new Object();
	@GuardedBy("placementLock")
	protected Placement placement;

	public AbstractFieldObject(final Field field, final Placement placement) {
		this.field = field;
		this.placement = placement;
	}

	@Override
	public Field getField() {
		return field;
	}

	@Override
	public Placement getPlacement() {
		synchronized (placementLock) {
			return placement;
		}
	}

	@Override
	public boolean mount() {
		return field.addObject(this);
	}

	@Override
	public Placement unmount() {
		synchronized (placementLock) {
			if (!field.removeObject(this)) {
				return null;
			}
			return placement;
		}
	}

}
