package org.kareha.hareka.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.packet.PacketElement;
import org.kareha.hareka.packet.PacketInput;

@Immutable
@XmlJavaTypeAdapter(TilePattern.Adapter.class)
public abstract class TilePattern implements PacketElement {

	public static final TilePattern SOLID_NULL_0 = new SolidTilePattern(Tile.NULL_0);

	public static abstract class Builder {

		public abstract TilePattern build();

	}

	protected TilePattern() {

	}

	public static TilePattern readFrom(final PacketInput in) {
		switch (TilePatternType.readFrom(in)) {
		default:
			return null;
		case SOLID:
			final Tile value = Tile.readFrom(in);
			return new SolidTilePattern(value);
		case ONE_UNIFORM:
			final Tile a = Tile.readFrom(in);
			final Tile b = Tile.readFrom(in);
			final Tile c = Tile.readFrom(in);
			return new OneUniformTilePattern(a, b, c);
		}
	}

	@XmlType(name = "tilePattern")
	@XmlSeeAlso({ SolidTilePattern.Adapted.class, OneUniformTilePattern.Adapted.class })
	@XmlAccessorType(XmlAccessType.NONE)
	protected static abstract class Adapted {

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final TilePattern v) {
			// main constructor
		}

		protected abstract TilePattern unmarshal();

	}

	protected abstract Adapted marshal();

	static class Adapter extends XmlAdapter<Adapted, TilePattern> {

		@Override
		public Adapted marshal(final TilePattern v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public TilePattern unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public abstract Tile getTile(Vector position);

}
