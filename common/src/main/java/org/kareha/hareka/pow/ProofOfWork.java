package org.kareha.hareka.pow;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;

// SHA_d-256: m -> SHA-256(SHA-256(0^512 || m))
public class ProofOfWork {

	private static final String HASH_ALGORITHM = "SHA-256";
	@Private
	static final BigInteger bound = BigInteger.ONE.shiftLeft(256);

	public interface Listener {

		void progress(int goal, int current);

	}

	@Private
	final BigInteger target;
	@Private
	final byte[] data;
	private final int threads;

	public ProofOfWork(final BigInteger target, final byte[] data, final int threads) {
		this.target = target;
		this.data = data.clone();
		this.threads = threads;
	}

	public ProofOfWork(final BigInteger target, final byte[] data) {
		this(target, data, Runtime.getRuntime().availableProcessors());
	}

	private class Total {

		private final Listener listener;
		@GuardedBy("this")
		private BigInteger min = bound;
		private final int targetBits = target.bitLength();

		Total(final Listener listener) {
			this.listener = listener;
		}

		void init() {
			listener.progress(256 - targetBits, 0);
		}

		synchronized BigInteger update(final BigInteger candidate) {
			if (candidate.compareTo(min) == -1) {
				min = candidate;
				listener.progress(256 - targetBits, 256 - min.bitLength());
			}
			return min;
		}

	}

	private class Task implements Callable<byte[]> {

		private final int offset;
		private final BigInteger step;
		private final Total total;
		private final MessageDigest md0;

		Task(final int offset, final int step, final Total total) {
			this.offset = offset;
			this.step = BigInteger.valueOf(step);
			this.total = total;

			final MessageDigest md;
			try {
				md = MessageDigest.getInstance(HASH_ALGORITHM);
			} catch (final NoSuchAlgorithmException e) {
				throw new AssertionError(e);
			}
			boolean clonable = true;
			try {
				md.clone();
			} catch (final CloneNotSupportedException e) {
				clonable = false;
			}
			if (clonable) {
				md.update(new byte[64]);
				md.update(data);
				md0 = md;
			} else {
				md0 = null;
			}
		}

		@Override
		public byte[] call() throws InterruptedException {
			BigInteger nonce = BigInteger.valueOf(offset);
			BigInteger min = bound;
			while (!Thread.currentThread().isInterrupted()) {
				final byte[] b = nonce.toByteArray();
				final BigInteger current = new BigInteger(1, hash(b));
				if (current.compareTo(min) == -1) {
					min = total.update(current);
				}
				if (current.compareTo(target) == -1) {
					return b;
				}
				nonce = nonce.add(step);
			}
			throw new InterruptedException();
		}

		private byte[] hash(final byte[] nonce) {
			final MessageDigest md;
			if (md0 == null) {
				try {
					md = MessageDigest.getInstance(HASH_ALGORITHM);
				} catch (final NoSuchAlgorithmException e) {
					throw new AssertionError(e);
				}
				md.update(new byte[64]);
				md.update(data);
			} else {
				try {
					md = (MessageDigest) md0.clone();
				} catch (final CloneNotSupportedException e) {
					throw new AssertionError();
				}
			}
			return md.digest(md.digest(nonce));
		}

	}

	public byte[] generate(final Listener listener) throws InterruptedException {
		final ExecutorService executorService = Executors.newFixedThreadPool(threads);
		final CompletionService<byte[]> completionService = new ExecutorCompletionService<>(executorService);
		final Total total = new Total(listener);
		total.init();
		try {
			for (int i = 0; i < threads; i++) {
				completionService.submit(new Task(i, threads, total));
			}
			final Future<byte[]> future = completionService.take();
			try {
				return future.get();
			} catch (final ExecutionException e) {
				throw new RuntimeException(e);
			}
		} finally {
			executorService.shutdownNow();
		}
	}

	byte[] naiveHash(final byte[] nonce) {
		final MessageDigest md;
		try {
			md = MessageDigest.getInstance(HASH_ALGORITHM);
		} catch (final NoSuchAlgorithmException e) {
			throw new AssertionError(e);
		}
		md.update(new byte[64]);
		md.update(data);
		return md.digest(md.digest(nonce));
	}

	public boolean verify(final byte[] nonce) {
		return new BigInteger(1, naiveHash(nonce)).compareTo(target) == -1;
	}

}
