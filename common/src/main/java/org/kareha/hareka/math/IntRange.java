package org.kareha.hareka.math;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;

@Immutable
@XmlJavaTypeAdapter(IntRange.Adapter.class)
public class IntRange {

	@Private
	final int min;
	@Private
	final int max;

	public IntRange(final int min, final int max) {
		this.min = min;
		this.max = max;
	}

	@XmlType(name = "intRange")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		private int min;
		@XmlElement
		private int max;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final IntRange v) {
			min = v.min;
			max = v.max;
		}

		@Private
		IntRange unmarshal() {
			return new IntRange(min, max);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, IntRange> {

		@Override
		public Adapted marshal(final IntRange v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public IntRange unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}

}
