package org.kareha.hareka;

import java.util.Arrays;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.packet.PacketElement;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.PacketOutput;

@Immutable
@XmlJavaTypeAdapter(LocalKeyId.Adapter.class)
public final class LocalKeyId implements PacketElement {

	private final byte[] value;

	private LocalKeyId(final byte[] value) {
		if (value == null) {
			throw new IllegalArgumentException("null");
		}
		if (value.length != Constants.ID_CIPHER_KEY_LENGTH) {
			throw new IllegalArgumentException("value.length is not " + Constants.ID_CIPHER_KEY_LENGTH);
		}
		this.value = value.clone();
	}

	private LocalKeyId(final String hexString) {
		if (hexString == null) {
			throw new IllegalArgumentException("null");
		}
		// DatatypeConverter.parseHexBinary throws IllegalArgumentException
		value = DatatypeConverter.parseHexBinary(hexString);
	}

	public static LocalKeyId valueOf(final byte[] value) {
		return new LocalKeyId(value);
	}

	public static LocalKeyId valueOf(final String hexString) {
		return new LocalKeyId(hexString);
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof LocalKeyId)) {
			return false;
		}
		final LocalKeyId localId = (LocalKeyId) obj;
		return Arrays.equals(localId.value, value);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(value);
	}

	@Override
	public String toString() {
		return DatatypeConverter.printHexBinary(value).toLowerCase();
	}

	public static LocalKeyId readFrom(final PacketInput in) {
		final byte[] value = in.readByteArray();
		return valueOf(value);
	}

	@Override
	public void writeTo(final PacketOutput out) {
		out.writeByteArray(value);
	}

	@XmlType(name = "localKeyId")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlValue
		private String value;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final LocalKeyId v) {
			value = v.toString();
		}

		@Private
		LocalKeyId unmarshal() {
			return valueOf(value);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, LocalKeyId> {

		@Override
		public Adapted marshal(final LocalKeyId v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public LocalKeyId unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public byte[] value() {
		return value.clone();
	}

}
