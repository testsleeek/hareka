package org.kareha.hareka.persistent;

import java.io.DataInput;
import java.io.IOException;

public interface PersistentInput extends DataInput {

	long getFilePointer();

	int readCompactUInt() throws IOException;

	String readString() throws IOException;

	byte[] readByteArray() throws IOException;

}
