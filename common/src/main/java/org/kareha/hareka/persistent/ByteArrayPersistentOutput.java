package org.kareha.hareka.persistent;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.kareha.hareka.util.CompactNumbers;

public class ByteArrayPersistentOutput implements PersistentOutput {

	protected final ByteArrayOutputStream bout = new ByteArrayOutputStream();
	protected final DataOutputStream dout = new DataOutputStream(bout);

	public byte[] toByteArray() {
		return bout.toByteArray();
	}

	@Override
	public void write(final int b) throws IOException {
		dout.write(b);
	}

	@Override
	public void write(final byte[] b) throws IOException {
		dout.write(b);
	}

	@Override
	public void write(final byte[] b, final int off, final int len) throws IOException {
		dout.write(b, off, len);
	}

	@Override
	public void writeBoolean(final boolean v) throws IOException {
		dout.writeBoolean(v);
	}

	@Override
	public void writeByte(final int v) throws IOException {
		dout.writeByte(v);
	}

	@Override
	public void writeBytes(final String s) throws IOException {
		dout.writeBytes(s);
	}

	@Override
	public void writeChar(final int v) throws IOException {
		dout.writeChar(v);
	}

	@Override
	public void writeChars(final String s) throws IOException {
		dout.writeChars(s);
	}

	@Override
	public void writeDouble(final double v) throws IOException {
		dout.writeDouble(v);
	}

	@Override
	public void writeFloat(final float v) throws IOException {
		dout.writeFloat(v);
	}

	@Override
	public void writeInt(final int v) throws IOException {
		dout.writeInt(v);
	}

	@Override
	public void writeLong(final long v) throws IOException {
		dout.writeLong(v);
	}

	@Override
	public void writeShort(final int v) throws IOException {
		dout.writeShort(v);
	}

	@Override
	public void writeUTF(final String s) throws IOException {
		dout.writeUTF(s);
	}

	@Override
	public long getFilePointer() {
		return bout.size();
	}

	@Override
	public void flush() throws IOException {
		dout.flush();
	}

	@Override
	public void writeCompactUInt(final int v) throws IOException {
		CompactNumbers.writeUInt(dout, v);
	}

	@Override
	public void writeString(final String v) throws IOException {
		byte[] b;
		try {
			b = v.getBytes("UTF-8");
		} catch (final UnsupportedEncodingException e) {
			// UTF-8 encoding is expected to be always supported
			throw new AssertionError(e);
		}
		writeCompactUInt(b.length);
		write(b);
	}

	@Override
	public void writeByteArray(final byte[] v) throws IOException {
		writeCompactUInt(v.length);
		write(v);
	}

}
