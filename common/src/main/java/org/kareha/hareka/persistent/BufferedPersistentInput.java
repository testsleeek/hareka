package org.kareha.hareka.persistent;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;

import org.kareha.hareka.annotation.NotThreadSafe;
import org.kareha.hareka.util.CompactNumbers;

@NotThreadSafe
public class BufferedPersistentInput implements PersistentInput {

	protected static final int DEFAULT_BUFFER_SIZE = 8192;

	protected final RandomAccessFile in;
	protected long filePointer;
	protected final byte[] buffer;
	protected int bytesRead;
	protected int index;
	protected final InputStream is = new InputStream() {
		@Override
		public int read() throws IOException {
			return bufferedRead();
		}
	};

	public BufferedPersistentInput(final RandomAccessFile in) throws IOException {
		this(in, DEFAULT_BUFFER_SIZE);
	}

	public BufferedPersistentInput(final RandomAccessFile in, final int bufferSize) throws IOException {
		this.in = in;
		filePointer = in.getFilePointer();
		buffer = new byte[bufferSize];
	}

	@Override
	public long getFilePointer() {
		return filePointer;
	}

	protected int bufferedRead() throws IOException {
		final int c = bufferedPeek();
		if (c == -1) {
			return -1;
		}
		index++;
		filePointer++;
		return c;
	}

	protected int bufferedPeek() throws IOException {
		if (index >= bytesRead) {
			bytesRead = in.read(buffer);
			if (bytesRead == -1) {
				return -1;
			}
			index = 0;
		}
		return buffer[index] & 0xff;
	}

	@Override
	public boolean readBoolean() throws IOException {
		final int c = bufferedRead();
		if (c == -1) {
			throw new EOFException();
		}
		return c != 0;
	}

	@Override
	public byte readByte() throws IOException {
		final int c = bufferedRead();
		if (c == -1) {
			throw new EOFException();
		}
		return (byte) c;
	}

	@Override
	public char readChar() throws IOException {
		final int c0 = bufferedRead();
		if (c0 == -1) {
			throw new EOFException();
		}
		final int c1 = bufferedRead();
		if (c1 == -1) {
			throw new EOFException();
		}
		return (char) (c0 << 8 | c1);
	}

	@Override
	public double readDouble() throws IOException {
		return Double.longBitsToDouble(readLong());
	}

	@Override
	public float readFloat() throws IOException {
		return Float.intBitsToFloat(readInt());
	}

	@Override
	public void readFully(byte[] b) throws IOException {
		readFully(b, 0, b.length);
	}

	@Override
	public void readFully(byte[] b, int off, int len) throws IOException {
		for (int i = 0; i < len; i++) {
			final int c = bufferedRead();
			if (c == -1) {
				throw new EOFException();
			}
			b[off + i] = (byte) c;
		}
	}

	@Override
	public int readInt() throws IOException {
		final int c0 = bufferedRead();
		if (c0 == -1) {
			throw new EOFException();
		}
		final int c1 = bufferedRead();
		if (c1 == -1) {
			throw new EOFException();
		}
		final int c2 = bufferedRead();
		if (c2 == -1) {
			throw new EOFException();
		}
		final int c3 = bufferedRead();
		if (c3 == -1) {
			throw new EOFException();
		}
		return c0 << 24 | c1 << 16 | c2 << 8 | c3;
	}

	@Override
	public String readLine() throws IOException {
		final StringBuilder sb = new StringBuilder();
		while (true) {
			final int c = bufferedRead();
			if (c == -1) {
				if (sb.length() < 1) {
					return null;
				}
				return sb.toString();
			} else if (c == '\n') {
				return sb.toString();
			} else if (c == '\r') {
				final int cn = bufferedPeek();
				if (cn == '\n') {
					bufferedRead();
				}
				return sb.toString();
			} else {
				sb.append((char) c);
			}
		}
	}

	@Override
	public long readLong() throws IOException {
		final int c0 = bufferedRead();
		if (c0 == -1) {
			throw new EOFException();
		}
		final int c1 = bufferedRead();
		if (c1 == -1) {
			throw new EOFException();
		}
		final int c2 = bufferedRead();
		if (c2 == -1) {
			throw new EOFException();
		}
		final int c3 = bufferedRead();
		if (c3 == -1) {
			throw new EOFException();
		}
		final int c4 = bufferedRead();
		if (c4 == -1) {
			throw new EOFException();
		}
		final int c5 = bufferedRead();
		if (c5 == -1) {
			throw new EOFException();
		}
		final int c6 = bufferedRead();
		if (c6 == -1) {
			throw new EOFException();
		}
		final int c7 = bufferedRead();
		if (c7 == -1) {
			throw new EOFException();
		}
		return (long) c0 << 56 | (long) c1 << 48 | (long) c2 << 40 | (long) c3 << 32 | c4 << 24 | c5 << 16 | c6 << 8
				| c7;
	}

	@Override
	public short readShort() throws IOException {
		final int c0 = bufferedRead();
		if (c0 == -1) {
			throw new EOFException();
		}
		final int c1 = bufferedRead();
		if (c1 == -1) {
			throw new EOFException();
		}
		return (short) (c0 << 8 | c1);
	}

	@Override
	public String readUTF() throws IOException {
		return DataInputStream.readUTF(this);
	}

	@Override
	public int readUnsignedByte() throws IOException {
		final int c = bufferedRead();
		if (c == -1) {
			throw new EOFException();
		}
		return c;
	}

	@Override
	public int readUnsignedShort() throws IOException {
		final int c0 = bufferedRead();
		if (c0 == -1) {
			throw new EOFException();
		}
		final int c1 = bufferedRead();
		if (c1 == -1) {
			throw new EOFException();
		}
		return c0 << 8 | c1;
	}

	@Override
	public int skipBytes(final int n) throws IOException {
		in.seek(filePointer);
		final int skipped = in.skipBytes(n);
		filePointer = in.getFilePointer();
		bytesRead = 0;
		index = 0;
		return skipped;
	}

	@Override
	public int readCompactUInt() throws IOException {
		return CompactNumbers.readUInt(is);
	}

	@Override
	public String readString() throws IOException {
		final int length = readCompactUInt();
		final ByteArrayOutputStream os = new ByteArrayOutputStream();
		for (int i = 0; i < length; i++) {
			final byte b = readByte();
			os.write(b);
		}
		final byte[] ba = os.toByteArray();
		try {
			return new String(ba, 0, ba.length, "UTF-8");
		} catch (final UnsupportedEncodingException e) {
			throw new AssertionError(e);
		}
	}

	@Override
	public byte[] readByteArray() throws IOException {
		final int length = readCompactUInt();
		final byte[] b = new byte[length];
		for (int i = 0; i < length; i++) {
			b[i] = readByte();
		}
		return b;
	}

}
