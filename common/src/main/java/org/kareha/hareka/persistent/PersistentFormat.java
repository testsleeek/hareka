package org.kareha.hareka.persistent;

import java.io.IOException;

public interface PersistentFormat<T> {

	T read(PersistentInput in) throws IOException;

	void write(PersistentOutput out, T v) throws IOException;

}
