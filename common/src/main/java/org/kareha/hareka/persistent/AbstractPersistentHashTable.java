package org.kareha.hareka.persistent;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.kareha.hareka.annotation.GuardedBy;

public abstract class AbstractPersistentHashTable<K, V> implements PersistentHashTable<K, V> {

	private static final String HASH_ALGORITHM = "MD5";
	protected static final int BUCKET_SIZE = 0x10000;

	@GuardedBy("this")
	protected final PersistentFormat<K> keyFormat;
	@GuardedBy("this")
	protected final PersistentFormat<V> valueFormat;

	protected AbstractPersistentHashTable(final PersistentFormat<K> keyFormat, final PersistentFormat<V> valueFormat) {
		this.keyFormat = keyFormat;
		this.valueFormat = valueFormat;
	}

	@GuardedBy("this")
	protected int getHashCode(final K key) {
		final ByteArrayPersistentOutput out = new ByteArrayPersistentOutput();
		try {
			keyFormat.write(out, key);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		final MessageDigest md;
		try {
			md = MessageDigest.getInstance(HASH_ALGORITHM);
		} catch (final NoSuchAlgorithmException e) {
			throw new AssertionError(e);
		}
		final byte[] d = md.digest(out.toByteArray());
		return (d[d.length - 2] & 0xff) << 8 | (d[d.length - 1] & 0xff);
	}

}
