package org.kareha.hareka.persistent;

import java.io.IOException;

public interface PersistentHashTable<K, V> extends AutoCloseable {

	V get(K key) throws IOException;

	V put(K key, V value) throws IOException;

	V remove(K key) throws IOException;

}
