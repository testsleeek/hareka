package org.kareha.hareka.game;

import org.kareha.hareka.packet.PacketElement;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.PacketOutput;

public enum ItemType implements PacketElement {

	ACORN,

	BARK,

	CHOCOLATE_DONUT,

	COIN,

	CUP,

	DEW,

	DONUT,

	EGG,

	FORK,

	NEEDLE,

	NOTICE,

	TWIG,

	;

	private static final ItemType[] values = values();

	public static ItemType valueOf(final int ordinal) {
		if (ordinal < 0 || ordinal >= values.length) {
			throw new IllegalArgumentException("Out of bounds");
		}
		return values[ordinal];
	}

	public static ItemType readFrom(final PacketInput in) {
		final int ordinal = in.readCompactUInt();
		return valueOf(ordinal);
	}

	@Override
	public void writeTo(final PacketOutput out) {
		out.writeCompactUInt(ordinal());
	}

}
