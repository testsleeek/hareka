package org.kareha.hareka.game;

import org.kareha.hareka.packet.PacketElement;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.PacketOutput;

public enum AbilityType implements PacketElement {

	TELEPORT_RANDOMLY(TargetType.NULL),

	ATTACK(TargetType.FIELD_ENTITY),

	GREET(TargetType.FIELD_ENTITY),

	PICK_UP(TargetType.FIELD_ENTITY),

	POSSESS(TargetType.FIELD_ENTITY),

	PULL(TargetType.FIELD_ENTITY),

	PUSH(TargetType.FIELD_ENTITY),

	REVIVE(TargetType.FIELD_ENTITY),

	BREAK_WALL(TargetType.TILE),

	;

	private final TargetType targetType;

	private AbilityType(final TargetType targetType) {
		this.targetType = targetType;
	}

	public TargetType getTargetType() {
		return targetType;
	}

	private static final AbilityType[] values = values();

	public static AbilityType valueOf(final int ordinal) {
		if (ordinal < 0 || ordinal >= values.length) {
			throw new IllegalArgumentException("Out of bounds");
		}
		return values[ordinal];
	}

	public static AbilityType readFrom(final PacketInput in) {
		final int ordinal = in.readCompactUInt();
		return valueOf(ordinal);
	}

	@Override
	public void writeTo(final PacketOutput out) {
		out.writeCompactUInt(ordinal());
	}

}
