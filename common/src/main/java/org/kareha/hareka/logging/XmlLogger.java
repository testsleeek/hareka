package org.kareha.hareka.logging;

import java.io.IOException;
import java.util.Collection;

import javax.xml.bind.JAXBException;

public interface XmlLogger<T extends XmlLogRecord> extends AutoCloseable {

	Collection<T> get(long id, int count) throws IOException, JAXBException;

	void add(T object) throws IOException, JAXBException;

	long size();

	long find(long date, boolean reverse) throws IOException, JAXBException;

}
