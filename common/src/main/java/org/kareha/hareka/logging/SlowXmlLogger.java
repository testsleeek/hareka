package org.kareha.hareka.logging;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.JAXBException;

public class SlowXmlLogger<T extends XmlLogRecord> extends AbstractXmlLogger<T> {

	public SlowXmlLogger(final File directory, final String filename, final Class<?> clazz, final boolean format)
			throws IOException, JAXBException {
		super(directory, filename, clazz, format);
	}

	@Override
	public synchronized Collection<T> get(final long id, final int count) throws IOException, JAXBException {
		final long indexOffset = id * INDEX_RECORD_SIZE;
		final long bodyBlockOffset;
		try (final RandomAccessFile file = new RandomAccessFile(getIndexFile(), "r")) {
			if (file.length() < indexOffset + INDEX_RECORD_SIZE) {
				return Collections.emptyList();
			}
			file.seek(indexOffset);
			bodyBlockOffset = file.readLong();
		} catch (final FileNotFoundException e) {
			return Collections.emptyList();
		}
		try (final RandomAccessFile file = new RandomAccessFile(getBodyFile(), "r")) {
			file.seek(bodyBlockOffset);
			final XmlFragmentInput input = new XmlFragmentInput(file);
			final List<T> list = new ArrayList<>();
			for (int i = 0; i < count; i++) {
				final byte[] b = input.next();
				if (b == null) {
					return list;
				}
				list.add(unmarshal(b));
			}
			return list;
		}
	}

	@Override
	public synchronized void add(final T object) throws IOException, JAXBException {
		final long date = System.currentTimeMillis();
		object.setId(nextId);
		object.setDate(date);
		try (final RandomAccessFile bodyFile = new RandomAccessFile(getBodyFile(), "rw")) {
			final long bodyLength = bodyFile.length();
			try (final RandomAccessFile indexFile = new RandomAccessFile(getIndexFile(), "rw")) {
				indexFile.seek(indexFile.length());
				indexFile.writeLong(bodyLength);
				indexFile.writeLong(date);
			}
			bodyFile.seek(bodyLength);
			bodyFile.write(marshal(object));
		}
		nextId++;
	}

	@Override
	public void close() throws Exception {
		// do nothing
	}

	@Override
	public synchronized long getDate(final long id) throws IOException {
		final long indexOffset = id * INDEX_RECORD_SIZE;
		try (final RandomAccessFile file = new RandomAccessFile(getIndexFile(), "r")) {
			if (file.length() < indexOffset + INDEX_RECORD_SIZE) {
				throw new IOException();
			}
			file.seek(indexOffset + Long.BYTES);
			return file.readLong();
		}
	}

}
