package org.kareha.hareka.logging;

public interface XmlLogRecord {

	long getId();

	void setId(long id);

	long getDate();

	void setDate(long date);

}
