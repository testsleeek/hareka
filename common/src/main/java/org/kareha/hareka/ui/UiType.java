package org.kareha.hareka.ui;

import java.awt.HeadlessException;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.kareha.hareka.util.Parameters;

public enum UiType {

	SWING, CONSOLE,;

	private static final Logger logger = Logger.getLogger(UiType.class.getName());

	public static UiType parse(final Parameters parameters) {
		final String uiOption = parameters.getNamed().get("ui");
		if (uiOption == null || uiOption.equals("swing")) {
			return UiType.SWING;
		} else if (uiOption.equals("console")) {
			return UiType.CONSOLE;
		} else {
			throw new IllegalArgumentException("Unknown UI option: ui=" + uiOption);
		}
	}

	public void showMessage(final String message) {
		switch (this) {
		default:
			throw new IllegalArgumentException("Unknown UiType");
		case SWING:
			try {
				SwingUtilities.invokeAndWait(() -> {
					JOptionPane.showMessageDialog(null, message);
				});
			} catch (final HeadlessException | InvocationTargetException | InterruptedException e) {
				logger.log(Level.SEVERE, "", e);
			}
			break;
		case CONSOLE:
			System.out.println(message);
			break;
		}
	}

}
