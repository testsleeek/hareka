package org.kareha.hareka.ui.swing;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BoxLayout;
import javax.swing.JComponent;

import org.kareha.hareka.annotation.Private;

@SuppressWarnings("serial")
public class LiveList<I extends Component> extends JComponent {

	public static interface Selectable {

		void setSelected(boolean v);

	}

	public enum Axis {

		X(BoxLayout.X_AXIS, KeyEvent.VK_LEFT, KeyEvent.VK_RIGHT),

		Y(BoxLayout.Y_AXIS, KeyEvent.VK_UP, KeyEvent.VK_DOWN),

		;

		private final int axisCode;
		private final int prevKeyCode;
		private final int nextKeyCode;

		private Axis(final int axisCode, final int prevKeyCode, final int nextKeyCode) {
			this.axisCode = axisCode;
			this.prevKeyCode = prevKeyCode;
			this.nextKeyCode = nextKeyCode;
		}

		int getAxisCode() {
			return axisCode;
		}

		int getPrevKeyCode() {
			return prevKeyCode;
		}

		int getNextKeyCode() {
			return nextKeyCode;
		}

	}

	private final MouseListener mouseListener;
	private final KeyListener keyListener;
	private I selectedItem;

	public LiveList(final Axis axis) {
		setLayout(new BoxLayout(this, axis.getAxisCode()));
		mouseListener = new MouseAdapter() {
			@Override
			public void mousePressed(final MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1 || e.getButton() == MouseEvent.BUTTON3) {
					@SuppressWarnings("unchecked")
					final I source = (I) e.getSource();
					setSelectedItem(source);
					source.requestFocus();
				}
			}
		};
		keyListener = new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent e) {
				if (e.getKeyCode() == axis.getPrevKeyCode()) {
					selectPrev();
					return;
				} else if (e.getKeyCode() == axis.getNextKeyCode()) {
					selectNext();
					return;
				}
				final int i = getOneBaseNumber(e.getKeyCode());
				if (i != -1) {
					final int index = i - 1;
					if (index >= 0 && index < getItemCount()) {
						setSelectedItem(index);
					}
					return;
				}
			}
		};
	}

	private int getNumber(final int keyCode) {
		switch (keyCode) {
		default:
			return -1;
		case KeyEvent.VK_0:
			return 0;
		case KeyEvent.VK_1:
			return 1;
		case KeyEvent.VK_2:
			return 2;
		case KeyEvent.VK_3:
			return 3;
		case KeyEvent.VK_4:
			return 4;
		case KeyEvent.VK_5:
			return 5;
		case KeyEvent.VK_6:
			return 6;
		case KeyEvent.VK_7:
			return 7;
		case KeyEvent.VK_8:
			return 8;
		case KeyEvent.VK_9:
			return 9;
		}
	}

	@Private
	int getOneBaseNumber(final int keyCode) {
		final int i = getNumber(keyCode);
		if (i == -1) {
			return -1;
		}
		if (i == 0) {
			return 10;
		}
		return i;
	}

	public void addItem(final I v) {
		add(v);
		v.addMouseListener(mouseListener);
		v.addKeyListener(keyListener);
	}

	public void removeItem(final I v) {
		remove(v);
		v.removeMouseListener(mouseListener);
		v.removeKeyListener(keyListener);
		if (selectedItem == v) {
			selectedItem = null;
		}
	}

	public void clearItems() {
		for (final Component c : getComponents()) {
			@SuppressWarnings("unchecked")
			final I i = (I) c;
			removeItem(i);
		}
	}

	public int getItemCount() {
		return getComponentCount();
	}

	public I getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(final I v) {
		for (final Component c : getComponents()) {
			@SuppressWarnings("unchecked")
			final I i = (I) c;
			if (i instanceof Selectable) {
				final Selectable s = (Selectable) i;
				s.setSelected(i == v);
			}
			if (i == v) {
				selectedItem = v;
			}
		}
	}

	public void setSelectedItem(final int v) {
		final Component c = getComponent(v);
		@SuppressWarnings("unchecked")
		final I i = (I) c;
		setSelectedItem(i);
	}

	private int indexOf(final Component component) {
		final Component[] components = getComponents();
		for (int i = 0; i < components.length; i++) {
			if (component.equals(components[i])) {
				return i;
			}
		}
		return -1;
	}

	@Private
	void selectPrev() {
		final int index = indexOf(selectedItem);
		if (index == -1) {
			return;
		}
		final int prevIndex;
		if (index > 0) {
			prevIndex = index - 1;
		} else {
			prevIndex = getItemCount() - 1;
		}
		@SuppressWarnings("unchecked")
		final I i = (I) getComponent(prevIndex);
		setSelectedItem(i);
	}

	@Private
	void selectNext() {
		final int index = indexOf(selectedItem);
		if (index == -1) {
			return;
		}
		final int nextIndex;
		if (index < getComponentCount() - 1) {
			nextIndex = index + 1;
		} else {
			nextIndex = 0;
		}
		@SuppressWarnings("unchecked")
		final I i = (I) getComponent(nextIndex);
		setSelectedItem(i);
	}

}
