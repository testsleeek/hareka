package org.kareha.hareka.user;

import java.util.Collection;
import java.util.EnumSet;
import java.util.Set;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Immutable;

@Immutable
public abstract class AbstractRole implements Role {

	protected final String id;
	protected final int rank;
	@GuardedBy("this")
	protected final Set<Permission> permissions;

	public AbstractRole(final String id, final int rank, final Collection<Permission> permissions) {
		if (id == null) {
			throw new IllegalArgumentException("null");
		}
		this.id = id;
		this.rank = rank;
		if (permissions == null || permissions.isEmpty()) {
			this.permissions = EnumSet.noneOf(Permission.class);
		} else {
			this.permissions = EnumSet.copyOf(permissions);
		}
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof Role)) {
			return false;
		}
		final Role role = (Role) obj;
		return role.getId().equals(id);
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public String toString() {
		return id + " (" + rank + ")";
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public int getRank() {
		return rank;
	}

	@Override
	public synchronized boolean isAbleTo(final Permission permission) {
		if (permission == null) {
			throw new IllegalArgumentException("null");
		}
		return permissions.contains(permission);
	}

	@Override
	public synchronized Collection<Permission> getPermissions() {
		return EnumSet.copyOf(permissions);
	}

}
