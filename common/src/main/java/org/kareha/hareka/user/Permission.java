package org.kareha.hareka.user;

import org.kareha.hareka.packet.PacketElement;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.PacketOutput;

public enum Permission implements PacketElement {

	ISSUE_INVITATION_TOKENS,

	MANAGE_SETTINGS,

	MANAGE_ROLES,

	ISSUE_ROLE_TOKENS,

	MANAGE_ROLE_TOKENS,

	INSPECT_ENTITIES,

	SHUTDOWN, REBOOT,

	EDIT_TILE_FIELDS,

	FORCE_EDIT_TILE_FIELDS,

	EDIT_TILE_FIELDS_WITHOUT_POW,

	EDIT_STAIRS,

	EDIT_GATES,

	EDIT_FIELDS,

	EDIT_REGIONS,

	TELEPORT_TO_POSITION_MEMORY,

	DELETE_ENTITIES,

	;

	private static final Permission[] values = values();

	public static Permission valueOf(final int ordinal) {
		if (ordinal < 0 || ordinal >= values.length) {
			throw new IllegalArgumentException("Out of bounds");
		}
		return values[ordinal];
	}

	public static Permission readFrom(final PacketInput in) {
		final int ordinal = in.readCompactUInt();
		return valueOf(ordinal);
	}

	@Override
	public void writeTo(final PacketOutput out) {
		out.writeCompactUInt(ordinal());
	}

}
