package org.kareha.hareka.user;

import java.util.Collection;
import java.util.EnumSet;

class SuperRole implements Role {

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof Role)) {
			return false;
		}
		final Role role = (Role) obj;
		return role.getId().equals(getId());
	}

	@Override
	public int hashCode() {
		return getId().hashCode();
	}

	@Override
	public String toString() {
		return getId() + " (" + getRank() + ")";
	}

	@Override
	public String getId() {
		return "Super";
	}

	@Override
	public int getRank() {
		return Integer.MAX_VALUE;
	}

	@Override
	public boolean isAbleTo(final Permission permission) {
		if (permission == null) {
			throw new IllegalArgumentException("null");
		}
		return true;
	}

	@Override
	public Collection<Permission> getPermissions() {
		return EnumSet.allOf(Permission.class);
	}

}
