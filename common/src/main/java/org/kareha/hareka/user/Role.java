package org.kareha.hareka.user;

import java.util.Collection;

import org.kareha.hareka.packet.PacketElement;
import org.kareha.hareka.packet.PacketOutput;

public interface Role extends PacketElement {

	Role SUPER = new SuperRole();

	String getId();

	int getRank();

	boolean isAbleTo(Permission permission);

	Collection<Permission> getPermissions();

	@Override
	default void writeTo(final PacketOutput out) {
		out.writeString(getId());
		out.writeCompactUInt(getRank());
		final Collection<Permission> permissions = getPermissions();
		out.writeCompactUInt(permissions.size());
		for (final Permission permission : permissions) {
			out.write(permission);
		}
	}

}
