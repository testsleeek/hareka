package org.kareha.hareka.user;

import org.kareha.hareka.packet.PacketElement;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.PacketOutput;

public enum UserRegistrationMode implements PacketElement {

	INVITED_ONLY, ACCEPT_ALL;

	private static final UserRegistrationMode[] values = values();

	public static UserRegistrationMode valueOf(final int ordinal) {
		if (ordinal < 0 || ordinal >= values.length) {
			throw new IllegalArgumentException("Out of bounds");
		}
		return values[ordinal];
	}

	public static UserRegistrationMode readFrom(final PacketInput in) {
		final int ordinal = in.readCompactUInt();
		return valueOf(ordinal);
	}

	@Override
	public void writeTo(final PacketOutput out) {
		out.writeCompactUInt(ordinal());
	}

}
