package org.kareha.hareka.key;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.kareha.hareka.annotation.NotThreadSafe;

@NotThreadSafe
@XmlType(name = "passwordProtection")
@XmlAccessorType(XmlAccessType.NONE)
public class PasswordProtection {

	@XmlElement
	private byte[] salt;
	@XmlElement
	private int iterationCount;
	@XmlElement
	private int keyLength;
	@XmlElement
	private String keyAlgorithm;
	@XmlElement
	private String cipherAlgorithm;
	@XmlElement
	private byte[] iv;

	@SuppressWarnings("unused")
	private PasswordProtection() {
		// used by JAXB
	}

	public PasswordProtection(final int iterationCount, final int keyLength, final String keyAlgorithm,
			final String cipherAlgorithm) {
		this.iterationCount = iterationCount;
		this.keyLength = keyLength;
		this.keyAlgorithm = keyAlgorithm;
		this.cipherAlgorithm = cipherAlgorithm;

		final SecureRandom random = new SecureRandom();
		salt = new byte[(int) Math.ceil(keyLength / 8.0)];
		random.nextBytes(salt);
	}

	public PasswordProtection(final PasswordProtection original) {
		if (original.salt != null) {
			salt = Arrays.copyOf(original.salt, original.salt.length);
		}
		iterationCount = original.iterationCount;
		keyLength = original.keyLength;
		keyAlgorithm = original.keyAlgorithm;
		cipherAlgorithm = original.cipherAlgorithm;
		if (original.iv != null) {
			iv = Arrays.copyOf(original.iv, original.iv.length);
		}
	}

	private boolean isType1() {
		return ("PBKDF2WithHmacSHA1".equals(keyAlgorithm) && (keyLength == 128 || keyLength == 192 || keyLength == 256)
				&& "AES/CBC/PKCS5Padding".equals(cipherAlgorithm));
	}

	public byte[] encrypt(final byte[] data, final char[] password) throws EncryptionException {
		if (isType1()) {
			final PBEKeySpec keySpec = new PBEKeySpec(password, salt, iterationCount, keyLength);
			try {
				final SecretKeyFactory keyFactory;
				try {
					keyFactory = SecretKeyFactory.getInstance(keyAlgorithm);
				} catch (final NoSuchAlgorithmException e) {
					throw new AssertionError(e);
				}
				final SecretKey key;
				try {
					key = keyFactory.generateSecret(keySpec);
				} catch (final InvalidKeySpecException e) {
					throw new EncryptionException(e);
				}
				final SecretKeySpec secretKeySpec = new SecretKeySpec(key.getEncoded(), "AES");
				final Cipher cipher;
				try {
					cipher = Cipher.getInstance(cipherAlgorithm);
				} catch (final NoSuchAlgorithmException e) {
					throw new AssertionError(e);
				} catch (final NoSuchPaddingException e) {
					throw new AssertionError(e);
				}
				try {
					cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
				} catch (final InvalidKeyException e) {
					throw new EncryptionException(e);
				}
				iv = cipher.getIV();
				try {
					return cipher.doFinal(data);
				} catch (final IllegalBlockSizeException e) {
					throw new EncryptionException(e);
				} catch (final BadPaddingException e) {
					throw new EncryptionException(e);
				}
			} finally {
				keySpec.clearPassword();
			}
		}
		throw new UnsupportedOperationException(
				"Algorithm not supported yet: " + keyAlgorithm + " ; " + keyLength + " ; " + cipherAlgorithm);
	}

	public byte[] decrypt(final byte[] data, final char[] password) throws DecryptionException {
		if (isType1()) {
			final PBEKeySpec keySpec = new PBEKeySpec(password, salt, iterationCount, keyLength);
			try {
				final SecretKeyFactory keyFactory;
				try {
					keyFactory = SecretKeyFactory.getInstance(keyAlgorithm);
				} catch (final NoSuchAlgorithmException e) {
					throw new AssertionError(e);
				}
				final SecretKey key;
				try {
					key = keyFactory.generateSecret(keySpec);
				} catch (final InvalidKeySpecException e) {
					throw new DecryptionException(e);
				}
				final SecretKeySpec secretKeySpec = new SecretKeySpec(key.getEncoded(), "AES");
				final Cipher cipher;
				try {
					cipher = Cipher.getInstance(cipherAlgorithm);
				} catch (final NoSuchAlgorithmException e) {
					throw new AssertionError(e);
				} catch (final NoSuchPaddingException e) {
					throw new AssertionError(e);
				}
				final IvParameterSpec params = new IvParameterSpec(iv);
				try {
					cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, params);
				} catch (final InvalidKeyException e) {
					throw new DecryptionException(e);
				} catch (final InvalidAlgorithmParameterException e) {
					throw new DecryptionException(e);
				}
				try {
					return cipher.doFinal(data);
				} catch (final IllegalBlockSizeException e) {
					throw new DecryptionException(e);
				} catch (final BadPaddingException e) {
					throw new DecryptionException(e);
				}
			} finally {
				keySpec.clearPassword();
			}
		}
		throw new UnsupportedOperationException(
				"Algorithm not supported yet: " + keyAlgorithm + " ; " + keyLength + " ; " + cipherAlgorithm);
	}

}
