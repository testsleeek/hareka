package org.kareha.hareka.key;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

public abstract class SimpleTrustManager implements X509TrustManager {

	private final SimpleKeyStore simpleKeyStore;
	private final String host;

	public SimpleTrustManager(final SimpleKeyStore simpleKeyStore, final String host) {
		this.simpleKeyStore = simpleKeyStore;
		this.host = host;
	}

	@Override
	public void checkClientTrusted(final X509Certificate[] chain, final String authType) throws CertificateException {
		// not used
	}

	@Override
	public void checkServerTrusted(final X509Certificate[] chain, final String authType) throws CertificateException {
		if (chain.length != 1) {
			throw new CertificateException();
		}
		final X509Certificate cert = chain[0];
		try {
			cert.verify(cert.getPublicKey());
		} catch (final InvalidKeyException e) {
			throw new CertificateException(e);
		} catch (final NoSuchAlgorithmException e) {
			throw new CertificateException(e);
		} catch (final NoSuchProviderException e) {
			throw new CertificateException(e);
		} catch (final SignatureException e) {
			throw new CertificateException(e);
		}

		final Certificate prevCert = simpleKeyStore.get(host);
		// accept first time
		try {
			if (prevCert == null) {
				simpleKeyStore.put(host, cert);
			} else if (!cert.equals(prevCert)) {
				// when previous certificate changed, check by user
				if (acceptCertificate(cert)) {
					simpleKeyStore.put(host, cert);
				} else {
					throw new CertificateException();
				}
			}
		} catch (final IOException e) {
			throw new CertificateException(e);
		}
	}

	@Override
	public X509Certificate[] getAcceptedIssuers() {
		return new X509Certificate[0];
	}

	protected abstract boolean acceptCertificate(X509Certificate cert);

}
