package org.kareha.hareka.key;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;

@Immutable
@XmlJavaTypeAdapter(KeyId.Adapter.class)
public final class KeyId {

	@Private
	final long value;

	private KeyId(final long value) {
		this.value = value;
	}

	public static KeyId valueOf(final long value) {
		return new KeyId(value);
	}

	public static KeyId valueOf(final String s) {
		// Long.parseLong throws NumberFormatException
		final long value = Long.parseLong(s, 16);
		return valueOf(value);
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof KeyId)) {
			return false;
		}
		final KeyId id = (KeyId) obj;
		return id.value == value;
	}

	@Override
	public int hashCode() {
		return (int) (value >>> 32 ^ value);
	}

	@Override
	public String toString() {
		return Long.toHexString(value);
	}

	@XmlType(name = "keyId")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlValue
		private String value;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final KeyId v) {
			value = v.toString();
		}

		@Private
		KeyId unmarshal() {
			return valueOf(value);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, KeyId> {

		@Override
		public Adapted marshal(final KeyId v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public KeyId unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public long value() {
		return value;
	}

}
