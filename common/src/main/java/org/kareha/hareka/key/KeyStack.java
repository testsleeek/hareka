package org.kareha.hareka.key;

import java.io.File;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.Constants;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.NotThreadSafe;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.util.FileUtil;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class KeyStack {

	private static final Logger logger = Logger.getLogger(KeyStack.class.getName());

	@NotThreadSafe
	@XmlRootElement(name = "keyStack")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Xml {

		@XmlElement(name = "keyPair")
		private List<KeyPairXml> list;

		@SuppressWarnings("unused")
		private Xml() {
			// used by JAXB
		}

		Xml(final KeyPairXml keyPairXml) {
			list = new ArrayList<>();
			list.add(keyPairXml);
		}

		KeyPairXml get(final int version) {
			if (list == null || version < 0 || version >= list.size()) {
				return null;
			}
			return list.get(version);
		}

		KeyPairXml set(final int version, final KeyPairXml keyPairXml) {
			if (list == null || version < 0 || version >= list.size()) {
				throw new IllegalArgumentException("No such version: " + version);
			}
			return list.set(version, keyPairXml);
		}

		void push(final KeyPairXml keyPairXml) {
			if (list == null) {
				list = new ArrayList<>();
				list.add(keyPairXml);
			} else {
				list.add(0, keyPairXml);
			}
		}

		void clear() {
			list = null;
		}

	}

	private final File file;
	@GuardedBy("this")
	private Xml xml;
	@GuardedBy("this")
	private final Map<KeyPairXml, KeyPair> cache = new IdentityHashMap<>();

	public KeyStack(final File file) {
		if (file == null) {
			throw new IllegalArgumentException("null");
		}
		this.file = file;
	}

	private static KeyPair generate() {
		final KeyPairGenerator gen;
		try {
			gen = KeyPairGenerator.getInstance(Constants.KEY_PAIR_ALGORITHM);
		} catch (final NoSuchAlgorithmException e) {
			throw new UnsupportedOperationException(e);
		}
		gen.initialize(Constants.KEY_PAIR_BIT_LENGTH);
		return gen.generateKeyPair();
	}

	@GuardedBy("KeyStack.this")
	private static void doMarshal(final Xml xml, final File f) throws JAXBException {
		try {
			if (!FileUtil.setPermissionReadableAndWritableOnlyByOwner(f)) {
				logger.warning("The KeyStack file's permission is not secure on the file system");
			}
		} catch (final IOException e) {
			throw new JAXBException(e);
		}
		JaxbUtil.marshal(xml, f);
	}

	public synchronized void newKeyPair() throws JAXBException {
		if (xml == null) {
			if (!file.isFile()) {
				doMarshal(new Xml(new KeyPairXml(generate())), file);
				return;
			}
			xml = JaxbUtil.unmarshal(file, Xml.class);
		}
		xml.push(new KeyPairXml(generate()));
		doMarshal(xml, file);
		cache.clear();
	}

	public synchronized void replaceKeyPair(final KeyPair keyPair) throws JAXBException {
		if (keyPair == null) {
			throw new IllegalArgumentException("null");
		}
		xml.clear();
		xml.push(new KeyPairXml(keyPair));
		doMarshal(xml, file);
		cache.clear();
	}

	@GuardedBy("this")
	private KeyPairXml getKeyPairXml(final int version) throws JAXBException {
		if (xml == null) {
			if (!file.isFile()) {
				doMarshal(new Xml(new KeyPairXml(generate())), file);
			}
			xml = JaxbUtil.unmarshal(file, Xml.class);
		}
		return xml.get(version);
	}

	public synchronized boolean isProtected(final int version) throws JAXBException {
		final KeyPairXml keyPairXml = getKeyPairXml(version);
		if (keyPairXml == null) {
			throw new IllegalStateException("No such version: " + version);
		}
		return keyPairXml.isProtected();
	}

	private static KeyPair unlock(final KeyPairXml keyPairXml, final char[] password) {
		if (!keyPairXml.isProtected()) {
			try {
				return keyPairXml.toObject();
			} catch (final NoSuchAlgorithmException | InvalidKeySpecException e) {
				return null;
			}
		}
		try {
			return keyPairXml.toObject(password);
		} catch (final NoSuchAlgorithmException | InvalidKeySpecException | DecryptionException e) {
			return null;
		}
	}

	public synchronized KeyPair get(final int version, final char[] password) throws JAXBException {
		final KeyPairXml keyPairXml = getKeyPairXml(version);
		if (keyPairXml == null) {
			return null;
		}
		final KeyPair cachedKeyPair = cache.get(keyPairXml);
		if (cachedKeyPair != null) {
			return cachedKeyPair;
		}
		final KeyPair keyPair = unlock(keyPairXml, password);
		if (keyPair == null) {
			return null;
		}
		cache.put(keyPairXml, keyPair);
		return keyPair;
	}

	private static KeyPair unlock(final KeyPairXml xml, final Supplier<char[]> reader)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		if (!xml.isProtected()) {
			return xml.toObject();
		}
		while (true) {
			final char[] password = reader.get();
			if (password == null) {
				return null;
			}
			try {
				return xml.toObject(password);
			} catch (final DecryptionException e) {
				continue;
			} finally {
				Arrays.fill(password, '\0');
			}
		}
	}

	public synchronized KeyPair get(final int version, final Supplier<char[]> reader) throws JAXBException {
		final KeyPairXml keyPairXml = getKeyPairXml(version);
		if (keyPairXml == null) {
			return null;
		}
		final KeyPair cachedKeyPair = cache.get(keyPairXml);
		if (cachedKeyPair != null) {
			return cachedKeyPair;
		}
		final KeyPair keyPair;
		try {
			keyPair = unlock(keyPairXml, reader);
		} catch (final NoSuchAlgorithmException | InvalidKeySpecException e) {
			return null;
		}
		if (keyPair == null) {
			return null;
		}
		cache.put(keyPairXml, keyPair);
		return keyPair;
	}

	public synchronized boolean changePassword(final int version, final char[] newPassword)
			throws EncryptionException, JAXBException {
		final KeyPairXml keyPairXml = getKeyPairXml(version);
		if (keyPairXml == null) {
			return false;
		}
		final KeyPair cachedKeyPair = cache.get(keyPairXml);
		if (cachedKeyPair == null) {
			return false;
		}
		final KeyPairXml newKeyPairXml;
		if (newPassword == null) {
			newKeyPairXml = new KeyPairXml(cachedKeyPair);
		} else {
			final PasswordProtection protection = new PasswordProtection(Constants.PASSWORD_PROTECTION_ITERATION_COUNT,
					Constants.PASSWORD_PROTECTION_KEY_LENGTH, Constants.PASSWORD_PROTECTION_KEY_ALGORITHM,
					Constants.PASSWORD_PROTECTION_CIPHER_ALGORITHM);
			newKeyPairXml = new KeyPairXml(cachedKeyPair, protection, newPassword);
		}
		xml.set(version, newKeyPairXml);
		doMarshal(xml, file);
		cache.remove(keyPairXml);
		return true;
	}

	public synchronized void clearCache() {
		cache.clear();
	}

}
