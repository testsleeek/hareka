package org.kareha.hareka.key;

import java.io.IOException;

import org.kareha.hareka.persistent.PersistentFormat;
import org.kareha.hareka.persistent.PersistentInput;
import org.kareha.hareka.persistent.PersistentOutput;

public class KeyIdPersistentFormat implements PersistentFormat<KeyId> {

	@Override
	public KeyId read(final PersistentInput in) throws IOException {
		return KeyId.valueOf(in.readLong());
	}

	@Override
	public void write(final PersistentOutput out, final KeyId v) throws IOException {
		out.writeLong(v.value());
	}

}
