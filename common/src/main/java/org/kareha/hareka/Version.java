package org.kareha.hareka;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.packet.PacketElement;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.PacketOutput;

@Immutable
public class Version implements PacketElement {

	private final String value;

	public Version(final String value) {
		if (value == null) {
			throw new IllegalArgumentException("null");
		}
		this.value = value;
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof Version)) {
			return false;
		}
		final Version version = (Version) obj;
		return version.value.equals(value);
	}

	@Override
	public int hashCode() {
		return value.hashCode();
	}

	@Override
	public String toString() {
		return value;
	}

	public static Version readFrom(final PacketInput in) {
		final String value = in.readString();
		return new Version(value);
	}

	@Override
	public void writeTo(final PacketOutput out) {
		out.writeString(value);
	}

	public String getValue() {
		return value;
	}

}
