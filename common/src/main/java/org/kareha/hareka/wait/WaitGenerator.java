package org.kareha.hareka.wait;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;

import org.kareha.hareka.annotation.GuardedBy;

public class WaitGenerator {

	@GuardedBy("this")
	private long count;
	@GuardedBy("this")
	private final Map<WaitType, Wait> map = new EnumMap<>(WaitType.class);

	public WaitGenerator() {
		for (final WaitType type : EnumSet.allOf(WaitType.class)) {
			first(type);
		}
	}

	private Wait first(final WaitType type) {
		final Wait prev = map.get(type);
		if (prev != null) {
			throw new IllegalStateException("Already initialized");
		}

		final Wait wait = new Wait(count++, type, 0);
		map.put(type, wait);

		return wait;
	}

	public synchronized Wait get(final WaitType type) {
		return map.get(type);
	}

	public synchronized Collection<Wait> getWaits() {
		return new ArrayList<>(map.values());
	}

	public synchronized WaitResult next(final long id, final WaitType type, final int duration,
			final WaitPenalty penalty) {
		final Wait prev = map.get(type);

		if (prev == null) {
			throw new IllegalStateException("Not initialized yet");
		}

		if (id != prev.getId()) {
			if (id > prev.getId()) {
				penalty.put(type, duration);
			}
			final Wait wait = new Wait(prev.getId(), type, prev.getRemaining() + penalty.get(type));
			return new WaitResult(false, wait);
		}

		if (!prev.isExpiredWithPenalty(penalty.get(type))) {
			penalty.put(type, duration);
			final Wait wait = new Wait(prev.getId(), type, prev.getRemaining() + duration);
			return new WaitResult(false, wait);
		}

		final Wait wait = new Wait(count++, type, duration);
		map.put(type, wait);

		penalty.remove(type);
		return new WaitResult(true, wait);
	}

	public synchronized WaitResult next(final WaitType type, final int duration) {
		final Wait prev = map.get(type);

		if (prev == null) {
			throw new IllegalStateException("Not initialized yet");
		}

		if (!prev.isExpiredWithDelay(0)) {
			return new WaitResult(false, prev);
		}

		final Wait wait = new Wait(count++, type, duration);
		map.put(type, wait);

		return new WaitResult(true, wait);
	}

}
