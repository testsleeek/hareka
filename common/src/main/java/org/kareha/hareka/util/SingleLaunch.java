package org.kareha.hareka.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.SynchronizedWith;
import org.kareha.hareka.ui.UiType;

public enum SingleLaunch {

	INSTANCE;

	private enum BundleKey {
		AlreadyLaunched,
	}

	private static final Logger logger = Logger.getLogger(SingleLaunch.class.getName());
	@SynchronizedWith("org.kareha.hareka.updater.UpdaterFrame")
	private static final String filename = "lock";

	@GuardedBy("this")
	private boolean invoked;
	@GuardedBy("this")
	private FileLock lock;

	@SuppressWarnings("resource")
	public synchronized void ensure(final File dataDirectory, final UiType uiType) {
		if (dataDirectory == null || uiType == null) {
			throw new IllegalArgumentException("null");
		}
		if (invoked) {
			throw new IllegalStateException("Already invoked");
		}
		invoked = true;
		final File file = new File(dataDirectory, filename);
		final FileOutputStream out;
		try {
			out = new FileOutputStream(file);
		} catch (final FileNotFoundException e) {
			logger.log(Level.SEVERE, "", e);
			die(e.getMessage(), uiType);
			return;
		}
		final FileChannel channel = out.getChannel();
		try {
			lock = channel.tryLock();
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
			die(e.getMessage(), uiType);
			return;
		}
		if (lock != null) {
			return;
		}
		final ResourceBundle bundle = ResourceBundle.getBundle(SingleLaunch.class.getName());
		die(bundle.getString(BundleKey.AlreadyLaunched.name()), uiType);
	}

	private static void die(final String message, final UiType uiType) {
		uiType.showMessage(message);
		System.exit(1);
	}

}
