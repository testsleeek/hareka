package org.kareha.hareka.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

import org.kareha.hareka.ui.UiType;

public final class FileLogger {

	private static final Logger logger = Logger.getLogger(FileLogger.class.getName());

	private static final String directoryName = "log";
	private static final String filenameExtension = ".txt";
	// keep strong reference to keep the application root logger from being
	// garbage collected
	@SuppressWarnings("unused")
	private static Logger appRootLogger;
	private static final String DATE_FORMAT = "yyyyMMdd_HHmmss";

	private FileLogger() {
		throw new AssertionError();
	}

	public static void connect(final Logger appRootLogger, final File dataDirectory, final UiType uiType) {
		FileLogger.appRootLogger = appRootLogger;
		final File directory = new File(dataDirectory, directoryName);
		try {
			FileUtil.ensureDirectoryExists(directory);
		} catch (final IOException e) {
			logger.log(Level.WARNING, "", e);
			uiType.showMessage(e.getMessage());
			return;
		}
		final DateFormat format = new SimpleDateFormat(DATE_FORMAT);
		final File file = new File(directory, format.format(new Date()) + filenameExtension);
		final OutputStream out;
		try {
			out = new FileOutputStream(file);
		} catch (final FileNotFoundException e) {
			logger.log(Level.WARNING, "", e);
			uiType.showMessage(e.getMessage());
			return;
		}
		final Handler handler = new StreamHandler(out, new SimpleFormatter()) {
			@Override
			public synchronized void publish(final LogRecord record) {
				super.publish(record);
				flush();
			}
		};
		final Level rootLevel = appRootLogger.getLevel();
		if (rootLevel != null) {
			handler.setLevel(rootLevel);
		}
		appRootLogger.addHandler(handler);
	}

}
