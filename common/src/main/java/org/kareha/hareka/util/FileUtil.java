package org.kareha.hareka.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.util.EnumSet;
import java.util.ResourceBundle;
import java.util.Set;

import org.kareha.hareka.ui.UiType;

public final class FileUtil {

	private enum BundleKey {
		CannotCreateDirectory,
	}

	private FileUtil() {
		throw new AssertionError();
	}

	public static void ensureDirectoryExistsOrExit(final File directory, UiType uiType) {
		if (directory.isDirectory()) {
			return;
		}
		if (directory.exists()) {
			helperExit(uiType);
			return;
		}
		if (!directory.mkdirs()) {
			helperExit(uiType);
			return;
		}
	}

	private static void helperExit(final UiType uiType) {
		final ResourceBundle bundle = ResourceBundle.getBundle(FileUtil.class.getName());
		uiType.showMessage(bundle.getString(BundleKey.CannotCreateDirectory.name()));
		System.exit(1);
	}

	public static boolean ensureDirectoryExists(final File directory) throws IOException {
		if (directory.isDirectory()) {
			return false;
		}
		if (directory.exists()) {
			throw new IOException("Cannot create directory");
		}
		if (!directory.mkdirs()) {
			throw new IOException("Cannot create directory");
		}
		return true;
	}

	public static boolean setPermissionReadableAndWritableOnlyByOwner(final File file) throws IOException {
		file.createNewFile();
		if (!file.isFile()) {
			return false;
		}
		try {
			final Path path = file.toPath();
			final Set<PosixFilePermission> perms = EnumSet.of(PosixFilePermission.OWNER_READ,
					PosixFilePermission.OWNER_WRITE);
			Files.setPosixFilePermissions(path, perms);
			final Set<PosixFilePermission> resultPerms = Files.getPosixFilePermissions(path);
			return resultPerms.size() == 2 && resultPerms.contains(PosixFilePermission.OWNER_READ)
					&& resultPerms.contains(PosixFilePermission.OWNER_WRITE);
		} catch (final UnsupportedOperationException e) {
			return false;
		}
	}

}
