package org.kareha.hareka.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODO more specific name is preferred
public class Parameters {

	private final List<String> raw = new ArrayList<>();
	private final List<String> unnamed = new ArrayList<>();
	private final Map<String, String> named = new HashMap<>();

	public Parameters(final String[] args) {
		final Pattern pattern = Pattern.compile("^--(.+?)=(.*)$");
		for (final String a : args) {
			if (a == null) {
				continue;
			}
			raw.add(a);
			final Matcher m = pattern.matcher(a);
			if (m.matches()) {
				named.put(m.group(1), m.group(2));
			} else {
				unnamed.add(a);
			}
		}
	}

	public List<String> getRaw() {
		return Collections.unmodifiableList(raw);
	}

	public List<String> getUnnamed() {
		return Collections.unmodifiableList(unnamed);
	}

	public Map<String, String> getNamed() {
		return Collections.unmodifiableMap(named);
	}

}
