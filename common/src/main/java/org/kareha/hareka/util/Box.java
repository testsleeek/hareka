package org.kareha.hareka.util;

public class Box<T> {

	private volatile T value;

	public void set(final T value) {
		this.value = value;
	}

	public T get() {
		return value;
	}

}
