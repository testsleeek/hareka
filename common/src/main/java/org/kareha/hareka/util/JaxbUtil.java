package org.kareha.hareka.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public final class JaxbUtil {

	private JaxbUtil() {
		throw new AssertionError();
	}

	public static void marshal(final Object jaxbObject, final OutputStream out, final JAXBContext jaxbContext)
			throws JAXBException {
		final Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		marshaller.marshal(jaxbObject, out);
	}

	public static <T> T unmarshal(final InputStream in, final JAXBContext jaxbContext) throws JAXBException {
		final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		@SuppressWarnings("unchecked")
		final T jaxbObject = (T) unmarshaller.unmarshal(in);
		return jaxbObject;
	}

	public static void marshal(final Object jaxbObject, final File xmlFile, final JAXBContext jaxbContext)
			throws JAXBException {
		try (final OutputStream out = new BufferedOutputStream(new FileOutputStream(xmlFile))) {
			marshal(jaxbObject, out, jaxbContext);
		} catch (final IOException e) {
			throw new JAXBException(e);
		}
	}

	public static <T> T unmarshal(final File xmlFile, final JAXBContext jaxbContext) throws JAXBException {
		try (final InputStream in = new BufferedInputStream(new FileInputStream(xmlFile))) {
			return unmarshal(in, jaxbContext);
		} catch (final IOException e) {
			throw new JAXBException(e);
		}
	}

	public static void marshal(final Object jaxbObject, final File xmlFile) throws JAXBException {
		marshal(jaxbObject, xmlFile, JAXBContext.newInstance(jaxbObject.getClass()));
	}

	public static <T> T unmarshal(final File xmlFile, final Class<T> type) throws JAXBException {
		return unmarshal(xmlFile, JAXBContext.newInstance(type));
	}

}
