package org.kareha.hareka.util;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Singleton;
import org.kareha.hareka.annotation.ThreadSafe;

/**
 * A simple logger to use when java.util.logging framework does not work.
 */
@ThreadSafe
@Singleton
public enum SimpleLogger {

	INSTANCE;

	// OutputStream is synchronized with itself
	private final PrintStream out = System.err;
	@GuardedBy("out")
	// SimpleDateFormat is not thread-safe
	private final DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@GuardedBy("out")
	private String header() {
		final StackTraceElement s = Thread.currentThread().getStackTrace()[3];
		return format.format(new Date()) + " " + s.getClassName() + " " + s.getMethodName();
	}

	public void log(final String message) {
		synchronized (out) {
			out.println(header());
			out.println(message);
		}
		out.flush();
	}

	public void log(final Throwable t) {
		synchronized (out) {
			out.println(header());
			t.printStackTrace(out);
		}
		out.flush();
	}

}
