package org.kareha.hareka.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.packet.PacketElement;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.PacketOutput;

@ThreadSafe
@XmlJavaTypeAdapter(Name.Adapter.class)
public class Name implements PacketElement {

	private static final String STANDARD_LANGUAGE = "en";

	@GuardedBy("this")
	@Private
	final Map<String, String> translations = new HashMap<>();

	public Name(final String standardTranslation) {
		if (standardTranslation != null && !standardTranslation.isEmpty()) {
			translations.put(STANDARD_LANGUAGE, standardTranslation);
		}
	}

	@Private
	Name(final Collection<Translation> translations) {
		for (final Translation i : translations) {
			if (i.value == null || i.value.isEmpty()) {
				continue;
			}
			this.translations.put(i.language, i.value);
		}
	}

	public Name(final Name original) {
		synchronized (original) {
			translations.putAll(original.translations);
		}
	}

	public static Name readFrom(final PacketInput in) {
		final int size = in.readCompactUInt();
		final List<Translation> list = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			final String language = in.readString();
			final String translation = in.readString();
			list.add(new Translation(language, translation));
		}
		return new Name(list);
	}

	@Override
	public synchronized void writeTo(final PacketOutput out) {
		out.writeCompactUInt(translations.size());
		for (final Map.Entry<String, String> entry : translations.entrySet()) {
			out.writeString(entry.getKey());
			out.writeString(entry.getValue());
		}
	}

	@XmlType(name = "nameTranslation")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Translation {

		@XmlAttribute
		@Private
		String language;
		@XmlValue
		@Private
		String value;

		@SuppressWarnings("unused")
		private Translation() {
			// used by JAXB
		}

		@Private
		Translation(final String language, final String value) {
			this.language = language;
			this.value = value;
		}

	}

	@XmlType(name = "name")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement(name = "translation")
		private List<Translation> translations;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Name v) {
			translations = new ArrayList<>();
			synchronized (v) {
				for (final Map.Entry<String, String> entry : v.translations.entrySet()) {
					translations.add(new Translation(entry.getKey(), entry.getValue()));
				}
			}
		}

		@Private
		Name unmarshal() {
			return new Name(translations);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, Name> {

		@Override
		public Adapted marshal(final Name v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public Name unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public synchronized boolean isEmpty() {
		// value != null && !value.isEmpty()
		return translations.isEmpty();
	}

	public synchronized String get(final String language) {
		final String translation = translations.get(language);
		if (translation != null) {
			return translation;
		}
		final String standardTranslation = translations.get(STANDARD_LANGUAGE);
		if (standardTranslation != null) {
			return standardTranslation;
		}
		for (final String anyTranslation : translations.values()) {
			// anyTranslation != null
			return anyTranslation;
		}
		return "";
	}

	public synchronized void put(final String language, final String translation) {
		if (translation == null || translation.isEmpty()) {
			translations.remove(language);
			return;
		}
		translations.put(language, translation);
	}

	public synchronized Map<String, String> getMap() {
		return new HashMap<>(translations);
	}

}
