package org.kareha.hareka.util;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class CompactNumbers {

	private CompactNumbers() {
		throw new AssertionError();
	}

	public static int readUInt(final InputStream in) throws IOException {
		int result = 0;
		int i = 0;
		while (true) {
			final int b = in.read();
			if (b == -1) {
				throw new EOFException();
			}
			result <<= 7;
			result |= b & 0x7f;
			if ((b & 0x80) == 0) {
				return result;
			}
			i++;
			if (i >= 5) {
				throw new IOException("terminater not found");
			}
		}
	}

	@SuppressWarnings("all") // parameter-assignment
	public static void writeUInt(final OutputStream out, int v) throws IOException {
		final int[] array = new int[5];
		int i = 0;
		while (v != 0) {
			array[i] = v & 0x7f;
			i++;
			v >>>= 7;
		}
		while (i > 1) {
			i--;
			out.write(0x80 | array[i]);
		}
		out.write(array[0]);
	}

	public static int readInt(final InputStream in) throws IOException {
		int result = 0;
		int i = 0;
		int max = 0x40;
		while (true) {
			final int b = in.read();
			if (b == -1) {
				throw new EOFException();
			}
			result <<= 7;
			result |= b & 0x7f;
			if ((b & 0x80) == 0) {
				return result < max ? result : result - (max << 1);
			}
			i++;
			max <<= 7;
			if (i >= 5) {
				throw new IOException("terminater not found");
			}
		}
	}

	@SuppressWarnings("all") // parameter-assignment
	public static void writeInt(final OutputStream out, int v) throws IOException {
		final int[] array = new int[5];
		int i = 0;
		while (true) {
			array[i] = v & 0x7f;
			i++;
			if (v >= -0x40 && v < 0x40) {
				break;
			}
			v >>>= 7;
		}
		while (i > 1) {
			i--;
			out.write(0x80 | array[i]);
		}
		out.write(array[0]);
	}

	public static long readULong(final InputStream in) throws IOException {
		long result = 0;
		int i = 0;
		while (true) {
			final int b = in.read();
			if (b == -1) {
				throw new EOFException();
			}
			result <<= 7;
			result |= b & 0x7f;
			if ((b & 0x80) == 0) {
				return result;
			}
			i++;
			if (i >= 10) {
				throw new IOException("terminater not found");
			}
		}
	}

	@SuppressWarnings("all") // parameter-assignment
	public static void writeULong(final OutputStream out, long v) throws IOException {
		final int[] array = new int[10];
		int i = 0;
		while (v != 0) {
			array[i] = (int) (v & 0x7f);
			i++;
			v >>>= 7;
		}
		while (i > 1) {
			i--;
			out.write(0x80 | array[i]);
		}
		out.write(array[0]);
	}

	public static long readLong(final InputStream in) throws IOException {
		long result = 0;
		int i = 0;
		long max = 0x40;
		while (true) {
			final int b = in.read();
			if (b == -1) {
				throw new EOFException();
			}
			result <<= 7;
			result |= b & 0x7f;
			if ((b & 0x80) == 0) {
				return result < max ? result : result - (max << 1);
			}
			i++;
			max <<= 7;
			if (i >= 10) {
				throw new IOException("terminater not found");
			}
		}
	}

	@SuppressWarnings("all") // parameter-assignment
	public static void writeLong(final OutputStream out, long v) throws IOException {
		final int[] array = new int[10];
		int i = 0;
		while (true) {
			array[i] = (int) (v & 0x7f);
			i++;
			if (v >= -0x40 && v < 0x40) {
				break;
			}
			v >>>= 7;
		}
		while (i > 1) {
			i--;
			out.write(0x80 | array[i]);
		}
		out.write(array[0]);
	}

}
