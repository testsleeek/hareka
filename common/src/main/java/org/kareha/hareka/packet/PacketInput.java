package org.kareha.hareka.packet;

import java.math.BigInteger;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Locale;

public interface PacketInput {

	boolean readBoolean();

	byte readByte();

	char readChar();

	double readDouble();

	float readFloat();

	int readInt();

	long readLong();

	short readShort();

	int readCompactUInt();

	int readCompactInt();

	long readCompactULong();

	long readCompactLong();

	String readString();

	byte[] readByteArray();

	BigInteger readBigInteger();

	Locale readLocale();

	Key readKey() throws NoSuchAlgorithmException, InvalidKeySpecException;

}
