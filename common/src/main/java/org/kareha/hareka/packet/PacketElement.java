package org.kareha.hareka.packet;

/**
 * {@code PacketElement} is an element of a packet, which can be written to a
 * {@code PacketOutput}.
 * 
 * @author Aki Goto
 * @since Hareka0.1
 */
public interface PacketElement {

	/**
	 * Writes the binary representation of the object to the specified
	 * {@code PacketOutput}.
	 * 
	 * @param out
	 *            the {@code PacketOutput} to which to write the data.
	 */
	void writeTo(PacketOutput out);

}
