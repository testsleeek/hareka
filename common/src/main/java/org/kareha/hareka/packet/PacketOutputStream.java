package org.kareha.hareka.packet;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.Key;
import java.util.Locale;

import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.util.CompactNumbers;

@ThreadSafe
public class PacketOutputStream extends OutputStream implements PacketOutput {

	private final ByteArrayOutputStream bout;
	private final DataOutputStream dout;

	public PacketOutputStream() {
		bout = new ByteArrayOutputStream();
		dout = new DataOutputStream(bout);
	}

	public PacketOutputStream(final int size) {
		bout = new ByteArrayOutputStream(size);
		dout = new DataOutputStream(bout);
	}

	@Override
	public synchronized void close() {
		try {
			dout.close();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void flush() {
		try {
			dout.flush();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void write(final byte[] b) {
		try {
			dout.write(b);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void write(final byte[] b, final int off, final int len) {
		try {
			dout.write(b, off, len);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void write(final int b) {
		try {
			dout.write(b);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	public synchronized int size() {
		return bout.size();
	}

	public synchronized void writeTo(final OutputStream out) throws IOException {
		bout.writeTo(out);
	}

	public synchronized byte[] toByteArray() {
		return bout.toByteArray();
	}

	@Override
	public synchronized void writeBoolean(final boolean v) {
		try {
			dout.writeBoolean(v);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void writeByte(final int v) {
		try {
			dout.writeByte(v);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void writeChar(final int v) {
		try {
			dout.writeChar(v);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void writeDouble(final double v) {
		try {
			dout.writeDouble(v);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void writeFloat(final float v) {
		try {
			dout.writeFloat(v);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void writeInt(final int v) {
		try {
			dout.writeInt(v);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void writeLong(final long v) {
		try {
			dout.writeLong(v);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void writeShort(final int v) {
		try {
			dout.writeShort(v);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void writeCompactUInt(final int v) {
		try {
			CompactNumbers.writeUInt(dout, v);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void writeCompactInt(final int v) {
		try {
			CompactNumbers.writeInt(dout, v);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void writeCompactULong(final long v) {
		try {
			CompactNumbers.writeULong(dout, v);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void writeCompactLong(final long v) {
		try {
			CompactNumbers.writeLong(dout, v);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void writeString(final String v) {
		byte[] b;
		try {
			b = v.getBytes("UTF-8");
		} catch (final UnsupportedEncodingException e) {
			// UTF-8 encoding is expected to be always supported
			throw new AssertionError(e);
		}
		writeCompactUInt(b.length);
		write(b);
	}

	@Override
	public synchronized void writeByteArray(final byte[] v) {
		writeCompactUInt(v.length);
		write(v);
	}

	@Override
	public void writeBigInteger(final BigInteger v) {
		writeByteArray(v.toByteArray());
	}

	@Override
	public synchronized void write(final PacketElement v) {
		v.writeTo(this);
	}

	@Override
	public synchronized void writeLocale(final Locale v) {
		writeString(v.toLanguageTag());
	}

	@Override
	public synchronized void writeKey(final Key v) {
		writeString(v.getAlgorithm());
		writeString(v.getFormat());
		writeByteArray(v.getEncoded());
	}

}
