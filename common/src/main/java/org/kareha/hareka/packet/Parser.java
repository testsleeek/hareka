package org.kareha.hareka.packet;

public interface Parser<S> {

	void handle(PacketInput in, S session);

}
