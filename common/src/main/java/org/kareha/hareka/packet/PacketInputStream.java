package org.kareha.hareka.packet;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Locale;

import org.kareha.hareka.annotation.NotThreadSafe;
import org.kareha.hareka.key.KeyXml;
import org.kareha.hareka.util.CompactNumbers;

@NotThreadSafe
public class PacketInputStream extends InputStream implements PacketInput {

	public static final int MAX_STRING_LENGTH = 4096;
	public static final int MAX_BYTE_ARRAY_LENGTH = 4096;
	public static final int MAX_KEY_LENGTH = 4096;

	protected final DataInputStream din;

	public PacketInputStream(final byte[] buf) {
		din = new DataInputStream(new ByteArrayInputStream(buf));
	}

	public PacketInputStream(final byte[] buf, final int offset, final int length) {
		din = new DataInputStream(new ByteArrayInputStream(buf, offset, length));
	}

	@Override
	public int available() {
		try {
			return din.available();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void close() {
		try {
			din.close();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void mark(final int readlimit) {
		din.mark(readlimit);
	}

	@Override
	public boolean markSupported() {
		return din.markSupported();
	}

	@Override
	public int read() {
		try {
			return din.read();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int read(final byte[] b) {
		try {
			return din.read(b);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int read(final byte[] b, final int off, final int len) {
		try {
			return din.read(b, off, len);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void reset() {
		try {
			din.reset();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public long skip(final long n) {
		try {
			return din.skip(n);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int readCompactUInt() {
		try {
			return CompactNumbers.readUInt(din);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int readCompactInt() {
		try {
			return CompactNumbers.readInt(din);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public long readCompactULong() {
		try {
			return CompactNumbers.readULong(din);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public long readCompactLong() {
		try {
			return CompactNumbers.readLong(din);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	private String readString(final int maxLength) {
		final int length = readCompactUInt();
		if (length > maxLength) {
			throw new RuntimeException("Max length exceeded");
		}
		final ByteArrayOutputStream os = new ByteArrayOutputStream();
		for (int i = 0; i < length; i++) {
			final byte b = readByte();
			os.write(b);
		}
		final byte[] ba = os.toByteArray();
		try {
			return new String(ba, 0, ba.length, "UTF-8");
		} catch (final UnsupportedEncodingException e) {
			throw new AssertionError(e);
		}
	}

	@Override
	public String readString() {
		return readString(MAX_STRING_LENGTH);
	}

	private byte[] readByteArray(final int maxLength) {
		final int length = readCompactUInt();
		if (length > maxLength) {
			throw new RuntimeException("Max length exceeded");
		}
		final byte[] b = new byte[length];
		for (int i = 0; i < length; i++) {
			b[i] = readByte();
		}
		return b;
	}

	@Override
	public byte[] readByteArray() {
		return readByteArray(MAX_BYTE_ARRAY_LENGTH);
	}

	@Override
	public BigInteger readBigInteger() {
		return new BigInteger(readByteArray());
	}

	@Override
	public Locale readLocale() {
		final String tag = readString();
		return new Locale.Builder().setLanguageTag(tag).build();
	}

	private Key readKey(final int maxLength) throws NoSuchAlgorithmException, InvalidKeySpecException {
		final String algorithm = readString();
		final String format = readString();
		final byte[] encoded = readByteArray(maxLength);
		final KeyXml xml = new KeyXml(algorithm, format, encoded);
		return xml.toObject();
	}

	@Override
	public Key readKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
		return readKey(MAX_KEY_LENGTH);
	}

	@Override
	public boolean readBoolean() {
		try {
			return din.readBoolean();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public byte readByte() {
		try {
			return din.readByte();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public char readChar() {
		try {
			return din.readChar();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public double readDouble() {
		try {
			return din.readDouble();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public float readFloat() {
		try {
			return din.readFloat();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int readInt() {
		try {
			return din.readInt();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public long readLong() {
		try {
			return din.readLong();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public short readShort() {
		try {
			return din.readShort();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

}
