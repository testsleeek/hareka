package org.kareha.hareka.packet;

import java.io.IOException;
import java.io.OutputStream;

import org.kareha.hareka.util.CompactNumbers;

public abstract class Packet {

	protected final PacketOutput out = new PacketOutputStream();

	protected abstract PacketType getType();

	public final void write(final OutputStream os) throws IOException {
		final PacketOutputStream s = (PacketOutputStream) out;
		CompactNumbers.writeUInt(os, getType().typeValue());
		CompactNumbers.writeUInt(os, s.size());
		s.writeTo(os);
	}

}
