package org.kareha.hareka.packet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.SSLHandshakeException;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.util.CompactNumbers;

@ThreadSafe
public abstract class PacketSocket<T extends Enum<?> & PacketType, S> implements AutoCloseable {

	@Private
	static final Logger logger = Logger.getLogger(PacketSocket.class.getName());

	@GuardedBy("this")
	@Private
	final Socket socket;
	@Private
	final ReaderTask reader;
	@Private
	final WriterTask writer;
	@Private
	volatile boolean disconnected;

	public PacketSocket(final Socket socket, final ParserTable<T, S> table, final S session) {
		this.socket = socket;

		try {
			final InputStream in = new BufferedInputStream(socket.getInputStream());
			reader = new ReaderTask(in, table, session);
			final OutputStream out = new BufferedOutputStream(socket.getOutputStream());
			writer = new WriterTask(out);
		} catch (final IOException e) {
			throw new IllegalArgumentException(e.getMessage(), e);
		}
	}

	// multiple call causes IllegalThreadStateException
	public void start() {
		reader.start();
		writer.start();
	}

	@Override
	public void close() {
		reader.close();
	}

	public void write(final Packet packet) {
		writer.add(packet);
	}

	public boolean isDisconnected() {
		return disconnected;
	}

	public abstract void finish();

	public synchronized InetAddress getInetAddress() {
		return socket.getInetAddress();
	}

	private class ReaderTask extends Thread implements AutoCloseable {

		private static final int PACKET_SIZE_LIMIT = 1024 * 1024 * 16;

		private final InputStream in;
		private final ParserTable<T, S> table;
		private final S session;

		ReaderTask(final InputStream in, final ParserTable<T, S> table, final S session) {
			this.in = in;
			this.table = table;
			this.session = session;
		}

		@Override
		public void close() {
			try {
				synchronized (PacketSocket.this) {
					socket.close();
				}
			} catch (final IOException e) {
				// ignore
			}
		}

		@Override
		public void run() {
			try {
				// when blocked by reading
				// actual termination may be caused by
				// socket.close
				while (!Thread.currentThread().isInterrupted()) {
					try {
						final int type = CompactNumbers.readUInt(in);
						final int size = CompactNumbers.readUInt(in);
						if (size >= PACKET_SIZE_LIMIT) {
							break;
						}
						final byte[] data = new byte[size];
						readFull(data);

						final Parser<S> parser = table.get(type);
						if (parser == null) {
							logger.severe("Unknown type: " + type);
							break;
						}
						final PacketInputStream pin = new PacketInputStream(data);
						try {
							parser.handle(pin, session);
						} catch (final Exception e) {
							logger.log(Level.SEVERE, "", e);
							break;
						}
						if (pin.available() > 0) {
							logger.warning("Bad packet size");
							break;
						}
					} catch (final SSLHandshakeException e) {
						logger.info(e.getMessage() + " (" + socket.getInetAddress() + ")");
						break;
					} catch (final SocketException e) {
						break;
					} catch (final EOFException e) {
						break;
					} catch (final IOException e) {
						logger.log(Level.SEVERE, "", e);
						break;
					}
				}
			} catch (final Exception e) {
				logger.log(Level.SEVERE, "", e);
			} finally {
				writer.interrupt();
				finish();
				disconnected = true;
			}
		}

		private void readFull(final byte[] b) throws IOException {
			for (int i = 0; i < b.length;) {
				final int result = in.read(b, i, b.length - i);
				if (result == -1) {
					throw new EOFException();
				}
				i += result;
			}
		}

	}

	private class WriterTask extends Thread {

		private final OutputStream out;

		// if bounded queue is used, canceling may suffer
		private final BlockingQueue<Packet> queue = new LinkedBlockingQueue<>();

		WriterTask(final OutputStream out) {
			this.out = out;
		}

		void add(Packet packet) {
			if (!queue.offer(packet)) {
				logger.severe("Overflow in writing queue in PacketSocket");
			}
		}

		@Override
		public void run() {
			try {
				while (!Thread.currentThread().isInterrupted()) {
					final Packet packet = queue.take();
					try {
						packet.write(out);
						out.flush();
					} catch (final SSLHandshakeException e) {
						logger.info(e.getMessage() + " (" + socket.getInetAddress() + ")");
						break;
					} catch (final SocketException e) {
						break;
					} catch (final IOException e) {
						logger.log(Level.SEVERE, "", e);
						break;
					}
				}
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			} catch (final Exception e) {
				logger.log(Level.SEVERE, "", e);
			} finally {
				reader.close();
			}
		}

	}

}
