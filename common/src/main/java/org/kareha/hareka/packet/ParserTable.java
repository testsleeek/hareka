package org.kareha.hareka.packet;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.util.CamelCase;

@ThreadSafe
public class ParserTable<T extends Enum<?> & PacketType, S> {

	private final Map<Integer, Parser<S>> map;

	public ParserTable(final String base, final Class<T> c, final String postfix) {
		final Method method;
		try {
			method = c.getMethod("values");
		} catch (final SecurityException e) {
			throw new RuntimeException(e);
		} catch (final NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
		final T[] values;
		try {
			@SuppressWarnings("unchecked")
			final T[] invoke = (T[]) method.invoke(null);
			values = invoke;
		} catch (final IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (final IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (final InvocationTargetException e) {
			throw new RuntimeException(e);
		}

		final Map<Integer, Parser<S>> m = new HashMap<>();
		for (final T element : values) {
			final String camelCase = CamelCase.snakeToCamel(element.name());
			if (camelCase == null) {
				throw new RuntimeException("The name must be underscore: " + element.name());
			}
			final StringBuilder sb = new StringBuilder(base).append(".").append(camelCase).append(postfix);
			Class<?> clazz;
			try {
				clazz = Class.forName(sb.toString());
			} catch (final ClassNotFoundException e) {
				throw new IllegalArgumentException(e.getMessage(), e);
			}
			final Object o;
			try {
				o = clazz.newInstance();
			} catch (InstantiationException e) {
				throw new IllegalArgumentException(e.getMessage(), e);
			} catch (IllegalAccessException e) {
				throw new IllegalArgumentException(e.getMessage(), e);
			}
			if (!(o instanceof Parser<?>)) {
				throw new IllegalArgumentException(o.getClass().getName() + " is not instance of Parser");
			}
			// cause ClassCastException when loaded Parser and
			// parameterized type are incompatible
			@SuppressWarnings("unchecked")
			final Parser<S> p = (Parser<S>) o;
			m.put(element.typeValue(), p);
		}
		map = m;
	}

	public Parser<S> get(final int type) {
		return map.get(type);
	}

}
