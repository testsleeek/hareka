package org.kareha.hareka.server.game.user;

import static org.junit.Assert.assertArrayEquals;

import java.security.SecureRandom;

import org.junit.Test;

public class StorageCipherTest {

	@Test
	public void testEncrypt() {
		final StorageCipher sc = new StorageCipher();
		final byte[] plain = new byte[32];
		new SecureRandom().nextBytes(plain);
		final byte[] toBeEncrypted = new byte[32];
		new SecureRandom().nextBytes(toBeEncrypted);
		final byte[] encrypted = sc.encrypt(plain, toBeEncrypted);
		assertArrayEquals(plain, StorageCipher.getPlain(encrypted));
		final byte[] decrypted = sc.decrypt(encrypted);
		assertArrayEquals(toBeEncrypted, decrypted);
	}

}
