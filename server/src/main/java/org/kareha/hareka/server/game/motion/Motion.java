package org.kareha.hareka.server.game.motion;

import org.kareha.hareka.server.game.entity.CharacterEntity;

public interface Motion {

	void startMotion();

	void stopMotion();

	void addHate(CharacterEntity attacker, int hate);

	void addLove(CharacterEntity attacker, int hate);

}
