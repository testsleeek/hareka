package org.kareha.hareka.server.game.item;

import org.kareha.hareka.field.TileType;
import org.kareha.hareka.game.ItemType;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.entity.FieldEntity;
import org.kareha.hareka.server.game.entity.ItemEntity;
import org.kareha.hareka.util.Name;
import org.kareha.hareka.wait.WaitType;

public interface Item {

	ItemType getType();

	Name getName();

	String getShape();

	boolean isExclusive();

	boolean isStackable();

	boolean isConsumable();

	int getWeight();

	int getReach();

	WaitType getWaitType();

	int getWait(CharacterEntity entity, TileType tileType);

	void use(CharacterEntity characterEntity, ItemEntity itemEntity, FieldEntity target);

}
