package org.kareha.hareka.server.game.motion;

import java.util.Random;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.field.Direction;
import org.kareha.hareka.field.FieldObject;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.game.AbilityType;
import org.kareha.hareka.game.Power;
import org.kareha.hareka.server.game.Global;
import org.kareha.hareka.server.game.ability.ReviveAbility;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.entity.FieldEntity;
import org.kareha.hareka.server.game.field.ServerFieldObject;
import org.kareha.hareka.server.game.stat.SkillType;
import org.kareha.hareka.server.game.stat.SkillVariation;
import org.kareha.hareka.wait.WaitResult;
import org.kareha.hareka.wait.WaitType;

public class NormalMotion extends AbstractMotion implements Runnable {

	private static final int CHASE_DISTANCE = 4;
	private static final int BAR_TO_RUN = 64;
	private static final int RUN_DISTANCE = 8;
	private static final int MIN_ACTION_HP = 96;

	private enum Mode {
		STOP, WALK, CHASE, ATTACK, RUN, PURSUE, HEAL,
	}

	private final CharacterEntity characterEntity;
	@GuardedBy("this")
	private ScheduledFuture<?> future;
	@GuardedBy("this")
	private Mode mode = Mode.STOP;
	@GuardedBy("this")
	private Direction direction = Direction.NULL;
	@GuardedBy("this")
	private int count;
	@GuardedBy("this")
	private Interests interests;

	public NormalMotion(final CharacterEntity characterEntity) {
		this.characterEntity = characterEntity;
	}

	private Random random() {
		return ThreadLocalRandom.current();
	}

	@Override
	public synchronized void startMotion() {
		if (future == null) {
			final FieldObject fo = characterEntity.getFieldObject();
			final TileType tileType;
			if (fo == null) {
				tileType = TileType.NULL;
			} else {
				tileType = fo.getField().getTile(fo.getPlacement().getPosition()).type();
			}
			future = Global.INSTANCE.scheduledExecutor().schedule(this,
					characterEntity.getStat().getMotionWait(tileType), TimeUnit.MILLISECONDS);
		}
	}

	@Override
	public synchronized void stopMotion() {
		if (future != null) {
			future.cancel(false);
			future = null;
		}
	}

	@Override
	public synchronized void addLove(final CharacterEntity entity, final int value) {
		if (interests == null) {
			interests = new Interests();
		}
		interests.addLove(entity, value);
	}

	@Override
	public synchronized void addHate(final CharacterEntity entity, final int value) {
		if (interests == null) {
			interests = new Interests();
		}
		interests.addHate(entity, value);
	}

	private synchronized Mode getMode() {
		return mode;
	}

	private synchronized void setMode(final Mode v) {
		mode = v;
	}

	private synchronized Direction getDirection() {
		return direction;
	}

	private synchronized void setDirection(final Direction v) {
		direction = v;
	}

	private synchronized int getCount() {
		return count;
	}

	private synchronized void setCount(final int v) {
		count = v;
	}

	private synchronized void decrementCount() {
		count--;
	}

	private synchronized CharacterEntity decideLoveTarget() {
		if (interests == null) {
			return null;
		}
		return interests.decideLoveTarget();
	}

	private synchronized CharacterEntity decideHateTarget() {
		if (interests == null) {
			return null;
		}
		return interests.decideHateTarget();
	}

	private synchronized void removeLoveEntity(final CharacterEntity entity) {
		if (interests == null) {
			return;
		}
		interests.removeLoveEntity(entity);
		if (interests.isEmpty()) {
			interests = null;
		}
	}

	private synchronized void removeHateEntity(final CharacterEntity entity) {
		if (interests == null) {
			return;
		}
		interests.removeHateEntity(entity);
		if (interests.isEmpty()) {
			interests = null;
		}
	}

	@Override
	public void run() {
		synchronized (this) {
			if (future == null) {
				return;
			}
		}

		CharacterEntity target;
		CharacterEntity loveTarget;
		Mode m;
		while (true) {
			target = decideHateTarget();
			loveTarget = decideLoveTarget();
			m = getMode();
			if (target == null && loveTarget == null) {
				break;
			}

			if (target != null) {
				final ServerFieldObject fo = characterEntity.getFieldObject();
				final FieldObject targetFo = target.getFieldObject();
				final int distance = fo.getField().getBoundary().distance(fo.getPlacement().getPosition(),
						targetFo.getPlacement().getPosition());
				if (!target.getStat().getHealthPoints().isAlive()) {
					removeHateEntity(target);
					setMode(Mode.STOP);
				} else if (fo.getField() != targetFo.getField()) {
					removeHateEntity(target);
					setMode(Mode.STOP);
				} else if (m == Mode.RUN && distance > RUN_DISTANCE) {
					removeHateEntity(target);
					setMode(Mode.STOP);
				} else if (m == Mode.CHASE && distance > CHASE_DISTANCE) {
					removeHateEntity(target);
					setMode(Mode.STOP);
				} else {
					if (characterEntity.getStat().hasAbility(AbilityType.ATTACK)) {
						if (characterEntity.getStat().getHealthPoints().getBar() < BAR_TO_RUN) {
							setMode(Mode.RUN);
						} else if (distance > 1) {
							setMode(Mode.CHASE);
						} else {
							setMode(Mode.ATTACK);
						}
					} else {
						setMode(Mode.RUN);
					}
					break;
				}
			}

			if (loveTarget != null) {
				final ServerFieldObject fo = characterEntity.getFieldObject();
				final FieldObject targetFo = loveTarget.getFieldObject();
				final int distance = fo.getField().getBoundary().distance(fo.getPlacement().getPosition(),
						targetFo.getPlacement().getPosition());
				if (loveTarget.getStat().getHealthPoints().isAlive()) {
					removeLoveEntity(loveTarget);
					setMode(Mode.STOP);
				} else if (fo.getField() != targetFo.getField()) {
					removeLoveEntity(loveTarget);
					setMode(Mode.STOP);
				} else if (m == Mode.PURSUE && distance > CHASE_DISTANCE) {
					removeLoveEntity(loveTarget);
					setMode(Mode.STOP);
				} else {
					if (characterEntity.getStat().hasAbility(AbilityType.REVIVE)) {
						if (distance > 1) {
							setMode(Mode.PURSUE);
						} else {
							setMode(Mode.HEAL);
						}
					} else {
						removeLoveEntity(loveTarget);
						setMode(Mode.STOP);
					}
					break;
				}
			}
		}

		m = getMode();

		if (m == Mode.STOP || m == Mode.WALK) {
			if (getCount() <= 0) {
				if (characterEntity.getStat().getHealthPoints().getBar() < MIN_ACTION_HP) {
					setMode(Mode.STOP);
				} else if (random().nextInt(2) < 1) {
					setMode(Mode.STOP);
				} else {
					setMode(Mode.WALK);
					setDirection(Direction.valueOf(random().nextInt(6)));
				}
				setCount(1 + random().nextInt(8));
			}
			decrementCount();
		}

		switch (mode) {
		case STOP: {
			// nothing to do
		}
			break;
		case WALK: {
			if (!checkMotionWait()) {
				break;
			}
			if (characterEntity.isStuck()) {
				break;
			}
			final ServerFieldObject fo = characterEntity.getFieldObject();
			final Placement placement = fo.getPlacement();
			final Direction d = getDirection();
			if (!helperWalk(d)) {
				if (fo.getField().getTile(placement.getPosition()).type().isStackable()) {
					fo.move(placement);
				}
				setCount(0);
			}
			if (characterEntity.getStat().getSpecies().getMotion() == MotionType.AGGRESSIVE) {
				for (final FieldObject o : fo.getInViewObjects()) {
					if (o == fo) {
						continue;
					}
					if (!fo.isVisible((ServerFieldObject) o)) {
						continue;
					}
					final FieldEntity entity = ((ServerFieldObject) o).getFieldEntity();
					if (entity instanceof CharacterEntity) {
						final CharacterEntity ce = (CharacterEntity) entity;
						if (!ce.getStat().hasPower(Power.IMMUNE_TO_ATTACK)) {
							if (fo.getField().getBoundary().distance(placement.getPosition(),
									o.getPlacement().getPosition()) <= CHASE_DISTANCE) {
								addHate(ce, 1);
							}
						}
					}
				}
			}
			if (characterEntity.getStat().hasAbility(AbilityType.REVIVE)) {
				for (final FieldObject o : fo.getInViewObjects()) {
					if (o == fo) {
						continue;
					}
					final FieldEntity entity = ((ServerFieldObject) o).getFieldEntity();
					if (entity instanceof CharacterEntity) {
						final CharacterEntity ce = (CharacterEntity) entity;
						if (!ce.getStat().getHealthPoints().isAlive()) {
							if (fo.getField().getBoundary().distance(placement.getPosition(),
									o.getPlacement().getPosition()) <= CHASE_DISTANCE) {
								addLove(ce, 1);
							}
						}
					}
				}
			}
		}
			break;
		case CHASE: {
			if (!checkMotionWait()) {
				break;
			}
			if (characterEntity.isStuck()) {
				break;
			}
			final ServerFieldObject fo = characterEntity.getFieldObject();
			@SuppressWarnings("null")
			final FieldObject targetFo = target.getFieldObject();
			final Direction d = fo.getField().getBoundary().direction(fo.getPlacement().getPosition(),
					targetFo.getPlacement().getPosition());
			if (!helperWalk(d)) {
				setMode(Mode.WALK);
			}
		}
			break;
		case ATTACK: {
			if (!checkAttackWait()) {
				break;
			}
			if (characterEntity.isStuck()) {
				break;
			}

			final ServerFieldObject fo = characterEntity.getFieldObject();
			@SuppressWarnings("null")
			final ServerFieldObject targetFo = target.getFieldObject();
			final Tile tile = fo.getField().getTile(fo.getPlacement().getPosition());
			final Tile targetTile = targetFo.getField().getTile(targetFo.getPlacement().getPosition());
			final int delta = targetTile.elevation() - tile.elevation();
			if (delta >= 0) {
				if (delta > characterEntity.getJumpUp()) {
					break;
				}
			} else {
				if (-delta > characterEntity.getJumpDown()) {
					break;
				}
			}

			characterEntity.attack(target);
		}
			break;
		case RUN: {
			if (!checkMotionWait()) {
				break;
			}
			if (characterEntity.isStuck()) {
				break;
			}
			final ServerFieldObject fo = characterEntity.getFieldObject();
			@SuppressWarnings("null")
			final FieldObject targetFo = target.getFieldObject();
			final Direction d = fo.getField().getBoundary().direction(targetFo.getPlacement().getPosition(),
					fo.getPlacement().getPosition());
			if (!helperWalk(d)) {
				final int distance = fo.getField().getBoundary().distance(fo.getPlacement().getPosition(),
						targetFo.getPlacement().getPosition());
				if (distance < 2) {
					setMode(Mode.ATTACK);

					final Tile tile = fo.getField().getTile(fo.getPlacement().getPosition());
					final Tile targetTile = targetFo.getField().getTile(targetFo.getPlacement().getPosition());
					final int delta = targetTile.elevation() - tile.elevation();
					if (delta >= 0) {
						if (delta > characterEntity.getJumpUp()) {
							break;
						}
					} else {
						if (-delta > characterEntity.getJumpDown()) {
							break;
						}
					}

					characterEntity.attack(target);
				}
			}
		}
			break;
		case HEAL: {
			if (characterEntity.isStuck()) {
				break;
			}

			final ServerFieldObject fo = characterEntity.getFieldObject();
			@SuppressWarnings("null")
			final ServerFieldObject targetFo = loveTarget.getFieldObject();
			final Tile tile = fo.getField().getTile(fo.getPlacement().getPosition());
			final Tile targetTile = targetFo.getField().getTile(targetFo.getPlacement().getPosition());
			final int delta = targetTile.elevation() - tile.elevation();
			if (delta >= 0) {
				if (delta > characterEntity.getJumpUp()) {
					break;
				}
			} else {
				if (-delta > characterEntity.getJumpDown()) {
					break;
				}
			}

			new ReviveAbility().use(characterEntity, loveTarget);
			removeLoveEntity(loveTarget);
			setMode(Mode.WALK);
		}
			break;
		case PURSUE: {
			if (!checkMotionWait()) {
				break;
			}
			if (characterEntity.isStuck()) {
				break;
			}
			final FieldObject fo = characterEntity.getFieldObject();
			@SuppressWarnings("null")
			final FieldObject targetFo = loveTarget.getFieldObject();
			final Direction d = targetFo.getPlacement().getPosition().subtract(fo.getPlacement().getPosition())
					.direction();
			if (!helperWalk(d)) {
				setMode(Mode.WALK);
			}
		}
			break;
		}

		final int wait;
		m = getMode();
		final FieldObject fo = characterEntity.getFieldObject();
		final TileType tileType = fo.getField().getTile(fo.getPlacement().getPosition()).type();
		if (m == Mode.ATTACK) {
			wait = characterEntity.getStat().getAttackWait(tileType);
		} else {
			wait = characterEntity.getStat().getMotionWait(tileType);
		}

		synchronized (this) {
			if (future == null) {
				return;
			}
			future = Global.INSTANCE.scheduledExecutor().schedule(this, wait, TimeUnit.MILLISECONDS);
		}
	}

	private boolean checkMotionWait() {
		final FieldObject fo = characterEntity.getFieldObject();
		final TileType tileType;
		if (fo == null) {
			tileType = TileType.NULL;
		} else {
			tileType = fo.getField().getTile(fo.getPlacement().getPosition()).type();
		}

		final WaitResult result = characterEntity.getWaitGenerator().next(WaitType.MOTION,
				characterEntity.getStat().getMotionWait(tileType));
		if (!result.isSuccess()) {
			return false;
		}
		characterEntity.sendWait(result.getWait());
		return true;
	}

	private boolean checkAttackWait() {
		final FieldObject fo = characterEntity.getFieldObject();
		final TileType tileType;
		if (fo == null) {
			tileType = TileType.NULL;
		} else {
			tileType = fo.getField().getTile(fo.getPlacement().getPosition()).type();
		}

		final WaitResult result = characterEntity.getWaitGenerator().next(WaitType.ATTACK,
				characterEntity.getStat().getAttackWait(tileType));
		if (!result.isSuccess()) {
			return false;
		}
		characterEntity.sendWait(result.getWait());
		return true;
	}

	private boolean helperWalk(final Direction d) {
		final ServerFieldObject fo = characterEntity.getFieldObject();
		final Placement placement = fo.getPlacement();
		Direction direction = d;
		Placement newPlacement = Placement.valueOf(placement.getPosition().neighbor(d), d);
		Placement oldPlacement = fo.move(newPlacement);
		if (oldPlacement == null) {
			if (ThreadLocalRandom.current().nextBoolean()) {
				direction = d.rotate(1);
				newPlacement = Placement.valueOf(placement.getPosition().neighbor(direction), direction);
				oldPlacement = fo.move(newPlacement);
				if (oldPlacement == null) {
					direction = d.rotate(-1);
					newPlacement = Placement.valueOf(placement.getPosition().neighbor(direction), direction);
					oldPlacement = fo.move(newPlacement);
					if (oldPlacement == null) {
						return false;
					}
				}
			} else {
				direction = d.rotate(-1);
				newPlacement = Placement.valueOf(placement.getPosition().neighbor(direction), direction);
				oldPlacement = fo.move(newPlacement);
				if (oldPlacement == null) {
					direction = d.rotate(1);
					newPlacement = Placement.valueOf(placement.getPosition().neighbor(direction), direction);
					oldPlacement = fo.move(newPlacement);
					if (oldPlacement == null) {
						return false;
					}
				}
			}
		}
		characterEntity.getStat().getSkills().get(SkillType.WALK).addValue(SkillVariation.WALK_INCREMENT);
		if (!characterEntity.isFieldDriversEmpty()) {
			Global.INSTANCE.context().getCharacterSpawner().trySpawnCharacter(fo, direction,
					newPlacement.getPosition());
			Global.INSTANCE.context().getItemSpawner().trySpawnItem(fo, direction, newPlacement.getPosition());
		}
		return true;
	}

}
