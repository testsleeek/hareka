package org.kareha.hareka.server.game.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.user.AbstractRole;
import org.kareha.hareka.user.Permission;

@Immutable
@XmlJavaTypeAdapter(CustomRole.Adapter.class)
public class CustomRole extends AbstractRole {

	public CustomRole(final String id, final int rank, final Collection<Permission> permissions) {
		super(id, rank, permissions);
	}

	public static CustomRole readFrom(final PacketInput in) {
		final String id = in.readString();
		final int rank = in.readCompactUInt();
		final int size = in.readCompactUInt();
		final Set<Permission> permissions = EnumSet.noneOf(Permission.class);
		for (int i = 0; i < size; i++) {
			final Permission permission = Permission.readFrom(in);
			if (permission == null) {
				throw new RuntimeException("Permission not found");
			}
			permissions.add(permission);
		}
		return new CustomRole(id, rank, permissions);
	}

	@XmlType(name = "customRole")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		private String id;
		@XmlElement
		private int rank;
		@XmlElement(name = "permission")
		private List<Permission> permissions;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		// XXX synthetic-access
		@SuppressWarnings("synthetic-access")
		@Private
		Adapted(final CustomRole v) {
			id = v.id;
			rank = v.rank;
			synchronized (v) {
				permissions = new ArrayList<>(v.permissions);
			}
		}

		@Private
		CustomRole unmarshal() {
			return new CustomRole(id, rank, permissions);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, CustomRole> {

		@Override
		public Adapted marshal(final CustomRole v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public CustomRole unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

}
