package org.kareha.hareka.server.game.stat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.game.ItemType;
import org.kareha.hareka.server.game.item.Item;

@ThreadSafe
@XmlJavaTypeAdapter(AbstractItemStat.Adapter.class)
public abstract class AbstractItemStat implements ItemStat {

	public static abstract class Builder {

		protected Item brand;
		protected String shape;

		public Builder() {

		}

		public Builder(final AbstractItemStat stat) {
			brand = stat.brand;
			synchronized (stat) {
				shape = stat.shape;
			}
		}

		public Builder brand(final Item v) {
			brand = v;
			return this;
		}

		public Builder shape(final String v) {
			shape = v;
			return this;
		}

		public abstract AbstractItemStat build();

	}

	protected final Item brand;
	@GuardedBy("this")
	protected String shape;

	protected AbstractItemStat(final Builder builder) {
		brand = builder.brand;
		shape = builder.shape;
	}

	@XmlType(name = "abstractItemStat")
	@XmlSeeAlso({ NormalItemStat.Adapted.class })
	@XmlAccessorType(XmlAccessType.NONE)
	protected static abstract class Adapted {

		@XmlElement
		protected ItemType type;
		@XmlElement
		protected String shape;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final AbstractItemStat v) {
			type = v.brand.getType();
			synchronized (v) {
				shape = v.shape;
			}
		}

		protected abstract AbstractItemStat unmarshal();

	}

	protected abstract Adapted marshal();

	static class Adapter extends XmlAdapter<Adapted, AbstractItemStat> {

		@Override
		public Adapted marshal(final AbstractItemStat v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public AbstractItemStat unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	@Override
	public synchronized String getShape() {
		return shape;
	}

	@Override
	public boolean isExclusive() {
		return brand.isExclusive();
	}

	@Override
	public Item getBrand() {
		return brand;
	}

}
