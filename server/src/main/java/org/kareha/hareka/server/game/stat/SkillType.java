package org.kareha.hareka.server.game.stat;

public enum SkillType {

	// physical

	HEALTH,

	STRENGTH,

	FANG,

	SKIN,

	AGILITY,

	DEXTERITY,

	DODGE,

	RECOVERY,

	WALK,

	JUMP_DOWN,

	JUMP_UP,
	
	SWIM,

	// magic

	MAGIC,

	MIND,

	MAGIC_RECOVERY,

	// mental

	LOVE,

	HATE,

	UNSOUNDNESS,

}
