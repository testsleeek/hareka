package org.kareha.hareka.server.game.ipacket;

import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.ResourceInternalServerPacketType;
import org.kareha.hareka.server.game.GameInternalPacket;

public final class KeepAlivePacket extends GameInternalPacket {

	public KeepAlivePacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return ResourceInternalServerPacketType.KEEP_ALIVE;
	}

}
