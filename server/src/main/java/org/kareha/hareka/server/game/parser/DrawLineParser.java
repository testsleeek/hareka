package org.kareha.hareka.server.game.parser;

import java.math.BigInteger;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.Constants;
import org.kareha.hareka.field.TilePattern;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.pow.PowGrabber;
import org.kareha.hareka.server.game.GameServerConstants;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.field.ServerField;
import org.kareha.hareka.server.game.field.ServerFieldObject;
import org.kareha.hareka.server.game.packet.RequestPowPacket;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;

public final class DrawLineParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(DrawLineParser.class.getName());
	private static final int MAX_SIZE = 1024;

	private enum BundleKey {
		YouCannotUseThisFunction, ThisCharacterCannotEditTileFields, PowTitle,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(DrawLineParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		final Vector a = Vector.readFrom(in);
		final Vector b = Vector.readFrom(in);
		final TilePattern tilePattern = TilePattern.readFrom(in);
		final boolean rawForce = in.readBoolean();

		final int size = b.distance(a);
		if (size < 0 || size > MAX_SIZE) {
			return;
		}

		final User user = session.getUser();
		if (user == null) {
			return;
		}
		final RoleSet roles = session.getContext().getAccessController().getRoleSet(user);
		if (!roles.isAbleTo(Permission.EDIT_TILE_FIELDS)) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot edit tile fields");
			return;
		}
		final boolean force = rawForce && roles.isAbleTo(Permission.FORCE_EDIT_TILE_FIELDS);

		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}
		final CharacterEntity entity = player.getEntity();
		if (!roles.isAbleTo(Permission.FORCE_EDIT_TILE_FIELDS) && !entity.isAbleToEditTileFields()) {
			inform(session, BundleKey.ThisCharacterCannotEditTileFields.name());
			return;
		}
		final ServerFieldObject fo = entity.getFieldObject();
		if (fo == null) {
			return;
		}
		final ServerField field = fo.getField();
		final Vector newA = a.invert(player.getTransformation());
		final Vector newB = b.invert(player.getTransformation());
		if (field.getBoundary().distance(fo.getPlacement().getPosition(), newA) >= field.getViewSize()
				&& field.getBoundary().distance(fo.getPlacement().getPosition(), newB) >= field.getViewSize()) {
			return;
		}

		final PowGrabber.Handler powHandler = new PowGrabber.Handler() {
			@Override
			public void handle(boolean result) {
				if (!result) {
					return;
				}
				field.drawLine(newA, newB, tilePattern, force);
			}

			@Override
			public void canceled() {
				// nothing to do
			}
		};
		if (roles.isAbleTo(Permission.EDIT_TILE_FIELDS_WITHOUT_POW)) {
			powHandler.handle(true);
			return;
		}
		final int scale = (int) (Math.log(size) / Math.log(2));
		final BigInteger target = BigInteger.ONE.shiftLeft(256 - GameServerConstants.DRAWING_POW_BASE_SCALE - scale);
		final PowGrabber.Entry powEntry = session.getPowGrabber().add(powHandler, target);
		if (powEntry == null) {
			logger.severe(session.getStamp() + "PowGrabber overflow");
			return;
		}
		final byte[] data = powEntry.newData(Constants.POW_DATA_BYTE_LENGTH);
		final ResourceBundle bundle = session.getBundle(DrawLineParser.class.getName());
		session.write(
				new RequestPowPacket(powEntry.getId(), target, data, bundle.getString(BundleKey.PowTitle.name())));
	}

}
