package org.kareha.hareka.server.game.parser;

import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.field.Direction;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.GameServerConstants;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.field.FieldId;
import org.kareha.hareka.server.game.packet.CharactersPacket;
import org.kareha.hareka.server.game.user.User;

public final class NewCharacterParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(NewCharacterParser.class.getName());

	private enum BundleKey {
		LoginUserFirst, CannotCreateCharacter, NewCharacterCreated,
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final int requestId = in.readCompactUInt();

		final ResourceBundle bundle = session.getBundle(NewCharacterParser.class.getName());

		// check state
		final User user = session.getUser();
		if (user == null) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.LoginUserFirst.name()));
			logger.fine(session.getStamp() + "Not logged in");
			return;
		}

		if (user.getCharacterEntriesSize() >= GameServerConstants.MAX_CHARACTERS_PER_USER) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.CannotCreateCharacter.name()),
					GameServerConstants.MAX_CHARACTERS_PER_USER);
			logger.fine(session.getStamp() + "Too many characters");
			return;
		}

		// create character
		final FieldId fieldId = session.getContext().getFields().getRootField().getId();
		final Placement placement = Placement.valueOf(Vector.ZERO, Direction.NULL);
		final CharacterEntity entity = session.getContext().getEntities().createSoulEntity(fieldId, placement);
		if (user.addCharacterEntry(entity.getId())) {
			entity.addOwner(user.getId());
		}

		session.writeAcceptPacket(requestId, bundle.getString(BundleKey.NewCharacterCreated.name()));
		logger.info(session.getStamp() + "Created character=" + entity.getId());

		session.write(new CharactersPacket(user.getCharacterEntries(), session.getContext().getEntities(),
				user.getChatEntityIdCipher(), session.getLocale().getLanguage()));
	}

}
