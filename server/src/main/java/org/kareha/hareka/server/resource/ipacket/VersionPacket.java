package org.kareha.hareka.server.resource.ipacket;

import org.kareha.hareka.Version;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.ResourceInternalClientPacketType;
import org.kareha.hareka.server.resource.ResourceInternalPacket;

public class VersionPacket extends ResourceInternalPacket {

	public VersionPacket(final Version version) {
		out.write(version);
	}

	@Override
	protected PacketType getType() {
		return ResourceInternalClientPacketType.VERSION;
	}

}
