package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.stat.LongStatPoints;

public final class WeightBarPacket extends GamePacket {

	public WeightBarPacket(final LongStatPoints points) {
		out.writeByte(points.getBar());
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.WEIGHT_BAR;
	}

}
