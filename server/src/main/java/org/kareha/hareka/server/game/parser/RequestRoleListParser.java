package org.kareha.hareka.server.game.parser;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.packet.RoleListPacket;
import org.kareha.hareka.server.game.user.User;

public final class RequestRoleListParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final User user = session.getUser();
		if (user == null) {
			return;
		}
		session.write(
				new RoleListPacket(session.getContext().getRoles(), user, session.getContext().getAccessController()));
	}

}
