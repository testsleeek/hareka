package org.kareha.hareka.server.game.parser;

import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.packet.CharactersPacket;
import org.kareha.hareka.server.game.user.User;

public final class LogoutCharacterParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(LogoutCharacterParser.class.getName());

	private enum BundleKey {
		LoginUserFirst, LoginCharacterFirst, LogoutCharacterSuccess,
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final int requestId = in.readCompactUInt();

		final ResourceBundle bundle = session.getBundle(LogoutCharacterParser.class.getName());

		// check state
		final User user = session.getUser();
		if (user == null) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.LoginUserFirst.name()));
			logger.fine(session.getStamp() + "Not logged in");
			return;
		}
		final Player player = session.getPlayer();
		if (player == null) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.LoginCharacterFirst.name()));
			logger.fine(session.getStamp() + "Character not logged in");
			return;
		}

		// process
		session.logoutCharacter();
		session.writeAcceptPacket(requestId, bundle.getString(BundleKey.LogoutCharacterSuccess.name()));

		session.write(new CharactersPacket(user.getCharacterEntries(), session.getContext().getEntities(),
				user.getChatEntityIdCipher(), session.getLocale().getLanguage()));
	}

}
