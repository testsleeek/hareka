package org.kareha.hareka.server;

import java.io.File;

import org.kareha.hareka.Constants;
import org.kareha.hareka.ui.UiType;
import org.kareha.hareka.util.Parameters;

public class ServerParameters {

	private final UiType uiType;
	private final File dataDirectory;
	private final int gamePort;
	private final int resourceInternalPort;
	private final boolean noTls;
	private final boolean gameServerOnly;
	private final boolean resourceServerOnly;

	public ServerParameters(final String[] args) {
		final Parameters parameters = new Parameters(args);

		uiType = UiType.parse(parameters);

		final String datadirOption = parameters.getNamed().get("datadir");
		if (datadirOption != null) {
			dataDirectory = new File(datadirOption);
		} else {
			dataDirectory = new File(ServerConstants.DATA_DIRECTORY_PATH);
		}

		final String gamePortOption = parameters.getNamed().get("gport");
		if (gamePortOption != null) {
			gamePort = Integer.parseInt(gamePortOption); // throws
															// NumberFormatException
		} else {
			gamePort = Constants.GAME_SERVER_PORT;
		}

		final String resourceInternalPortOption = parameters.getNamed().get("riport");
		if (resourceInternalPortOption != null) {
			resourceInternalPort = Integer.parseInt(resourceInternalPortOption); // throws
			// NumberFormatException
		} else {
			resourceInternalPort = Constants.RESOURCE_SERVER_INTERNAL_PORT;
		}

		noTls = parameters.getUnnamed().contains("--notls");

		gameServerOnly = parameters.getUnnamed().contains("--game");
		resourceServerOnly = parameters.getUnnamed().contains("--resource");
	}

	public UiType getUiType() {
		return uiType;
	}

	public File getDataDirectory() {
		return dataDirectory;
	}

	public int getGamePort() {
		return gamePort;
	}

	public int getResourceInternalPort() {
		return resourceInternalPort;
	}

	public boolean isNoTls() {
		return noTls;
	}

	public boolean isGameServerOnly() {
		return gameServerOnly;
	}

	public boolean isResourceServerOnly() {
		return resourceServerOnly;
	}

}
