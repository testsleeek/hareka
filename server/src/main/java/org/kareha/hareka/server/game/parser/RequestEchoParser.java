package org.kareha.hareka.server.game.parser;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.packet.EchoPacket;

public final class RequestEchoParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		session.write(new EchoPacket());
	}

}
