package org.kareha.hareka.server.game.parser;

import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.packet.VersionPacket;
import org.kareha.hareka.server.game.user.User;

public final class LogoutUserParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(LogoutUserParser.class.getName());

	private enum BundleKey {
		LoginUserFirst, LogoutUserSuccess,
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final int requestId = in.readCompactUInt();

		final ResourceBundle bundle = session.getBundle(LogoutUserParser.class.getName());

		// check state
		final User user = session.getUser();
		if (user == null) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.LoginUserFirst.name()));
			logger.fine(session.getStamp() + "Not logged in");
			return;
		}

		// process
		session.logoutUser();
		session.writeAcceptPacket(requestId, bundle.getString(BundleKey.LogoutUserSuccess.name()));

		session.write(new VersionPacket(session.getContext().getVersion()));
	}

}
