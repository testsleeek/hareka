package org.kareha.hareka.server.game.parser;

import java.security.KeyPair;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.Constants;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.packet.NoPublicKeyPacket;
import org.kareha.hareka.server.game.packet.PublicKeyPacket;

public final class RequestPublicKeyParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(RequestPublicKeyParser.class.getName());

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int handlerId = in.readCompactUInt();
		final int version = in.readCompactUInt();
		final byte[] nonce = in.readByteArray();

		final KeyPair keyPair;
		try {
			keyPair = session.getContext().getKeyStack().get(version, (char[]) null);
		} catch (final JAXBException e) {
			logger.log(Level.SEVERE, "", e);
			session.write(new NoPublicKeyPacket(handlerId, version));
			return;
		}
		if (keyPair == null) {
			session.write(new NoPublicKeyPacket(handlerId, version));
		} else {
			session.write(new PublicKeyPacket(handlerId, version, nonce, keyPair, Constants.SIGNATURE_ALGORITHM));
		}
	}

}
