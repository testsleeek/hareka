package org.kareha.hareka.server.game.field;

import org.kareha.hareka.field.Vector;
import org.kareha.hareka.packet.PacketElement;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.PacketInputStream;
import org.kareha.hareka.packet.PacketOutput;
import org.kareha.hareka.packet.PacketOutputStream;
import org.kareha.hareka.server.game.entity.EntityId;
import org.kareha.hareka.server.game.entity.FieldEntity;
import org.kareha.hareka.server.game.user.StorageCipher;

public class PositionMemory implements PacketElement {

	private static final String TYPE = PositionMemory.class.getSimpleName();

	private final FieldPosition position;
	private final EntityId entityId;
	private final long date;

	public PositionMemory(final FieldPosition position, final EntityId entityId, final long date) {
		this.position = position;
		this.entityId = entityId;
		this.date = date;
	}

	public PositionMemory(final ServerField field, final Vector position, final FieldEntity entity) {
		this(FieldPosition.valueOf(field.getId(), position), entity.getId(), System.currentTimeMillis());
	}

	public static PositionMemory readFrom(final PacketInput in, final long date) {
		final long fieldIdValue = in.readCompactULong();
		final Vector position = Vector.readFrom(in);
		final long entityIdValue = in.readCompactULong();

		final FieldPosition fieldPosition = FieldPosition.valueOf(FieldId.valueOf(fieldIdValue), position);
		final EntityId entityId = EntityId.valueOf(entityIdValue);

		return new PositionMemory(fieldPosition, entityId, date);
	}

	@Override
	public void writeTo(final PacketOutput out) {
		out.writeCompactULong(position.getFieldId().value());
		out.write(position.getPosition());
		out.writeCompactULong(entityId.value());
	}

	@SuppressWarnings("resource")
	public static PositionMemory decrypt(final StorageCipher cipher, final byte[] data) {
		final byte[] plain = StorageCipher.getPlain(data);
		final PacketInputStream plainIn = new PacketInputStream(plain);
		final String type = plainIn.readString();
		if (!TYPE.equals(type)) {
			return null;
		}
		final long date = plainIn.readCompactLong();

		final byte[] decrypted = cipher.decrypt(data);
		if (decrypted == null) {
			return null;
		}
		final PacketInputStream in = new PacketInputStream(decrypted);
		return readFrom(in, date);
	}

	public byte[] encrypt(final StorageCipher cipher) {
		@SuppressWarnings("resource")
		final PacketOutputStream plainOut = new PacketOutputStream();
		plainOut.writeString(TYPE);
		plainOut.writeCompactLong(date);
		final PacketOutputStream toBeEncryptedOut = new PacketOutputStream();
		writeTo(toBeEncryptedOut);
		return cipher.encrypt(plainOut.toByteArray(), toBeEncryptedOut.toByteArray());
	}

	public FieldPosition getPosition() {
		return position;
	}

	public EntityId getEntityId() {
		return entityId;
	}

	public long getDate() {
		return date;
	}

}
