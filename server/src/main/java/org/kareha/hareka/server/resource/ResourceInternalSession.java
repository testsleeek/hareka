package org.kareha.hareka.server.resource;

import java.net.Socket;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.packet.PacketSocket;
import org.kareha.hareka.packet.ParserTable;
import org.kareha.hareka.server.ResourceInternalServerPacketType;
import org.kareha.hareka.server.resource.ipacket.ExitPacket;

public class ResourceInternalSession {

	private volatile PacketSocket<ResourceInternalServerPacketType, ResourceInternalSession> packetSocket;
	@Private
	final ResourceContext context;
	private final ResourceSessionId id;
	@GuardedBy("this")
	private Locale locale = Locale.US;

	private ResourceInternalSession(final ResourceContext context) {
		this.context = context;
		id = context.getInternalSessionIdTable().next();
	}

	PacketSocket<ResourceInternalServerPacketType, ResourceInternalSession> newPacketSocket(final Socket socket,
			final ParserTable<ResourceInternalServerPacketType, ResourceInternalSession> parserTable) {
		return new PacketSocket<ResourceInternalServerPacketType, ResourceInternalSession>(socket, parserTable, this) {
			@Override
			public void finish() {
				// logoutUser();
				context.getInternalSessionTable().removeSession(ResourceInternalSession.this);
			}
		};
	}

	private void initialize(final Socket socket,
			final ParserTable<ResourceInternalServerPacketType, ResourceInternalSession> parserTable) {
		packetSocket = newPacketSocket(socket, parserTable);
	}

	public static ResourceInternalSession newInstance(final ResourceContext context, final Socket socket,
			final ParserTable<ResourceInternalServerPacketType, ResourceInternalSession> parserTable) {
		final ResourceInternalSession session = new ResourceInternalSession(context);
		session.initialize(socket, parserTable);
		return session;
	}

	public void start() {
		packetSocket.start();
	}

	public void close() {
		packetSocket.close();
	}

	public void write(final ResourceInternalPacket packet) {
		packetSocket.write(packet);
	}

	public ResourceContext getContext() {
		return context;
	}

	public ResourceSessionId getId() {
		return id;
	}

	public String getStamp() {
		final StringBuilder sb = new StringBuilder();
		sb.append("[");
		// final User user = getUser();
		// if (user != null) {
		// sb.append("User=" + user.getId() + " ");
		// }
		sb.append("InternalSession=" + id + " ");
		sb.append("Address=" + packetSocket.getInetAddress());
		sb.append("] ");
		return sb.toString();
	}

	public synchronized Locale getLocale() {
		return locale;
	}

	public synchronized void setLocale(final Locale locale) {
		this.locale = locale;
	}

	public ResourceBundle getBundle(final String baseName) {
		return ResourceBundle.getBundle(baseName, getLocale());
	}

	public void writeExitPacket(final String pattern, final Object... arguments) {
		final String message = MessageFormat.format(pattern, arguments);
		write(new ExitPacket(message));
	}

}
