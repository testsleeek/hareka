package org.kareha.hareka.server.resource;

import org.kareha.hareka.annotation.Immutable;

@Immutable
public class ResourceSessionId {

	private final long value;

	private ResourceSessionId(final long value) {
		this.value = value;
	}

	public static ResourceSessionId valueOf(final long value) {
		return new ResourceSessionId(value);
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof ResourceSessionId)) {
			return false;
		}
		final ResourceSessionId id = (ResourceSessionId) obj;
		return id.value == value;
	}

	@Override
	public int hashCode() {
		return (int) (value >>> 32 ^ value);
	}

	@Override
	public String toString() {
		return Long.toString(value);
	}

	public long value() {
		return value;
	}

}
