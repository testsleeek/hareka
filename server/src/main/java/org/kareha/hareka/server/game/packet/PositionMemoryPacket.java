package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.field.Vector;
import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.field.PositionMemory;
import org.kareha.hareka.server.game.field.ServerField;
import org.kareha.hareka.server.game.user.StorageCipher;

public final class PositionMemoryPacket extends GamePacket {

	public PositionMemoryPacket(final CharacterEntity entity, final ServerField field, final Vector position,
			final StorageCipher cipher) {
		out.writeByteArray(new PositionMemory(field, position, entity).encrypt(cipher));
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.POSITION_MEMORY;
	}

}
