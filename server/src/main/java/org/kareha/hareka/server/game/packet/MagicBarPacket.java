package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.entity.FieldEntity;
import org.kareha.hareka.server.game.stat.StatPoints;
import org.kareha.hareka.server.game.user.IdCipher;

public final class MagicBarPacket extends GamePacket {

	public MagicBarPacket(final FieldEntity fieldEntity, final IdCipher fieldEntityIdCipher, final StatPoints points) {
		out.write(fieldEntityIdCipher.encrypt(fieldEntity.getId()));
		out.writeBoolean(points.isAlive());
		out.writeByte(points.getBar());
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.MAGIC_BAR;
	}

}
