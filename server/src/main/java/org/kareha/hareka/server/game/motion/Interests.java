package org.kareha.hareka.server.game.motion;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.server.game.entity.CharacterEntity;

public class Interests {

	@GuardedBy("this")
	private Interest love;
	@GuardedBy("this")
	private Interest hate;

	public synchronized boolean isEmpty() {
		return love == null && hate == null;
	}

	public synchronized void addLove(final CharacterEntity entity, final int value) {
		if (love == null) {
			love = new Interest();
		}
		love.addValue(entity, value);
	}

	public synchronized void addHate(final CharacterEntity entity, final int value) {
		if (hate == null) {
			hate = new Interest();
		}
		hate.addValue(entity, value);
	}

	public synchronized CharacterEntity decideLoveTarget() {
		if (love == null) {
			return null;
		}
		return love.decideTarget();
	}

	public synchronized CharacterEntity decideHateTarget() {
		if (hate == null) {
			return null;
		}
		return hate.decideTarget();
	}

	public synchronized void removeLoveEntity(final CharacterEntity entity) {
		if (love == null) {
			return;
		}
		love.removeEntity(entity);
		if (love.isEmpty()) {
			love = null;
		}
	}

	public synchronized void removeHateEntity(final CharacterEntity entity) {
		if (hate == null) {
			return;
		}
		hate.removeEntity(entity);
		if (hate.isEmpty()) {
			hate = null;
		}
	}

}
