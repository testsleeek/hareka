package org.kareha.hareka.server.resource;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import org.kareha.hareka.LocalSessionId;
import org.kareha.hareka.annotation.GuardedBy;

public class ResourceSessionKeyTable {

	@GuardedBy("this")
	private final Map<Key, Entry> map = new HashMap<>();

	public synchronized boolean contains(final LocalSessionId key, final InetAddress inetAddress) {
		final Entry entry = map.get(new Key(key));
		if (entry == null) {
			return false;
		}
		return inetAddress.equals(entry.getInetAddress());
	}

	public synchronized void putIfAbsent(final LocalSessionId key, final InetAddress inetAddress) {
		final Key k = new Key(key);
		if (map.containsKey(k)) {
			return;
		}
		map.put(k, new Entry(inetAddress));
	}

	public synchronized void remove(final LocalSessionId key) {
		map.remove(new Key(key));
	}

	private static class Key {

		private final LocalSessionId key;

		Key(final LocalSessionId key) {
			this.key = key;
		}

		@Override
		public boolean equals(final Object obj) {
			if (!(obj instanceof Key)) {
				return false;
			}
			final Key k = (Key) obj;
			return k.key.equals(key);
		}

		@Override
		public int hashCode() {
			return key.hashCode();
		}

	}

	public static class Entry {

		private final InetAddress inetAddress;

		Entry(final InetAddress inetAddress) {
			this.inetAddress = inetAddress;
		}

		public InetAddress getInetAddress() {
			return inetAddress;
		}
	}

}
