package org.kareha.hareka.server.game.parser;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.logging.Logger;

import org.kareha.hareka.Constants;
import org.kareha.hareka.key.KeyGrabber;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;

public final class PublicKeyParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(PublicKeyParser.class.getName());

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int handlerId = in.readCompactUInt();
		final int version = in.readCompactUInt();
		Key key = null;
		try {
			key = in.readKey();
		} catch (final NoSuchAlgorithmException | InvalidKeySpecException e) {
			logger.fine(session.getStamp() + "Invalid key");
		}
		final String signatureAlgorithm = in.readString();
		final byte[] signature = in.readByteArray();

		final KeyGrabber.Entry entry = session.getKeyGrabber().get(handlerId);
		if (entry == null) {
			logger.fine(session.getStamp() + "Not found handlerId=" + handlerId);
			return;
		}

		if (key == null) {
			// have been informed above
			return;
		}

		if (!Arrays.asList(Constants.getAcceptablePublicKeyAlgorithms()).contains(key.getAlgorithm())) {
			logger.fine(session.getStamp() + "Not acceptable algorithm=" + key.getAlgorithm());
			return;
		}
		if (!Arrays.asList(Constants.getAcceptablePublicKeyFormats()).contains(key.getFormat())) {
			logger.fine(session.getStamp() + "Not acceptable format=" + key.getFormat());
			return;
		}

		if (!(key instanceof PublicKey)) {
			logger.fine(session.getStamp() + "Not a PublicKey");
			return;
		}
		final PublicKey publicKey = (PublicKey) key;

		if (!entry.verify(publicKey, signatureAlgorithm, signature)) {
			logger.fine(session.getStamp() + "Invalid signature");
			return;
		}
		entry.handle(version, publicKey);
	}

}
