package org.kareha.hareka.server.game.parser;

import java.io.IOException;
import java.util.Base64;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.packet.TextPacket;
import org.kareha.hareka.server.game.user.InvitationToken;
import org.kareha.hareka.server.game.user.InvitationTokensLogRecord;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;

public final class IssueInvitationTokenParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(IssueInvitationTokenParser.class.getName());

	private enum BundleKey {
		LoginUserFirst, YouCannotUseThisFunction,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(IssueRoleTokenParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments

		// check state
		final User user = session.getUser();
		if (user == null) {
			inform(session, BundleKey.LoginUserFirst.name());
			logger.fine(session.getStamp() + "User not logged in");
			return;
		}
		final RoleSet roles = session.getContext().getAccessController().getRoleSet(user);
		if (!roles.isAbleTo(Permission.ISSUE_INVITATION_TOKENS)) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot use issue invitation token function");
			return;
		}

		// process
		final InvitationToken token = session.getContext().getInvitationTokens().issue(user);
		try {
			session.getContext().getInvitationTokensLogger().add(new InvitationTokensLogRecord(token));
		} catch (final IOException | JAXBException e) {
			logger.log(Level.SEVERE, "", e);
		}
		final String base64 = Base64.getEncoder().encodeToString(token.getValue());
		session.write(new TextPacket("Invitation token: " + base64 + "\n"));
	}

}
