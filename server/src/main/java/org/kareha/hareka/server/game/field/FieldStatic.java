package org.kareha.hareka.server.game.field;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicLong;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class FieldStatic {

	private static final String STATIC_PATH = "org/kareha/hareka/server/game/resource/FieldStatic.xml";

	private final File file;
	@Private
	final AtomicLong count;

	public FieldStatic(final File file) throws JAXBException {
		this.file = file;
		final Long v = load();
		if (v == null) {
			count = new AtomicLong();
		} else {
			count = new AtomicLong(v);
		}
	}

	@XmlRootElement(name = "fieldStatic")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		@Private
		String count;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final FieldStatic v) {
			count = Long.toHexString(v.count.get());
		}

	}

	private Long load() throws JAXBException {
		if (!file.isFile()) {
			try (final InputStream in = getClass().getClassLoader().getResourceAsStream(STATIC_PATH)) {
				if (in == null) {
					return null;
				}
				final Adapted adapted = JaxbUtil.unmarshal(in, JAXBContext.newInstance(Adapted.class));
				return Long.parseLong(adapted.count, 16);
			} catch (final IOException e) {
				return null;
			}
		}
		final Adapted adapted = JaxbUtil.unmarshal(file, Adapted.class);
		return Long.parseLong(adapted.count, 16);
	}

	public void save() throws JAXBException {
		JaxbUtil.marshal(new Adapted(this), file);
	}

	public FieldId next() {
		return FieldId.valueOf(count.getAndIncrement());
	}

	public long getCount() {
		return count.get();
	}

}
