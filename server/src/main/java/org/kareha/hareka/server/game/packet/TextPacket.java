package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;

public final class TextPacket extends GamePacket {

	public TextPacket(final String text) {
		out.writeString(text);
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.TEXT;
	}

}
