package org.kareha.hareka.server.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.game.ItemType;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.entity.Entity;
import org.kareha.hareka.server.game.entity.EntityId;
import org.kareha.hareka.server.game.entity.FieldDriver;
import org.kareha.hareka.server.game.entity.ItemEntity;
import org.kareha.hareka.server.game.packet.AddItemPacket;
import org.kareha.hareka.server.game.packet.RemoveItemPacket;
import org.kareha.hareka.server.game.packet.SetItemCountPacket;
import org.kareha.hareka.server.game.packet.WeightBarPacket;
import org.kareha.hareka.server.game.stat.LongStatPoints;
import org.kareha.hareka.server.game.stat.NormalItemStat;

@ThreadSafe
@XmlJavaTypeAdapter(Inventory.Adapter.class)
public class Inventory {

	public static class Entry {

		private final EntityId id;
		@GuardedBy("this")
		private ItemEntity entity;

		public Entry(final EntityId id) {
			this.id = id;
		}

		public Entry(final ItemEntity entity) {
			id = entity.getId();
			this.entity = entity;
		}

		@Override
		public boolean equals(final Object obj) {
			if (!(obj instanceof Entry)) {
				return false;
			}
			final Entry entry = (Entry) obj;
			return entry.id.equals(id);
		}

		@Override
		public int hashCode() {
			return id.hashCode();
		}

		public EntityId getId() {
			return id;
		}

		public synchronized ItemEntity getEntity() {
			if (entity == null) {
				final Entity tmp = Global.INSTANCE.context().getEntities().get(id);
				if (tmp instanceof ItemEntity) {
					entity = (ItemEntity) tmp;
				}
			}
			return entity;
		}

	}

	@Private
	final EntityId ownerId;
	@GuardedBy("this")
	private CharacterEntity owner;
	@GuardedBy("this")
	@Private
	List<Entry> entries;
	@GuardedBy("this")
	@Private
	Long weight;

	public Inventory(final EntityId ownerId) {
		this.ownerId = ownerId;
	}

	public Inventory(final Inventory original) {
		ownerId = original.ownerId;
		synchronized (original) {
			if (original.entries != null) {
				entries = new ArrayList<>(original.entries);
			}
		}
	}

	@Private
	Inventory(final EntityId ownerId, final Collection<EntityId> ids) {
		this.ownerId = ownerId;
		if (ids != null && !ids.isEmpty()) {
			entries = new ArrayList<>();
			for (final EntityId id : ids) {
				entries.add(new Entry(id));
			}
		}
	}

	@XmlType(name = "inventory")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlAttribute
		private EntityId ownerId;
		@XmlElement(name = "id")
		private List<EntityId> ids;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Inventory v) {
			ownerId = v.ownerId;
			ids = new ArrayList<>();
			synchronized (v) {
				for (final Entry entry : v.entries) {
					ids.add(entry.getId());
				}
			}
		}

		@Private
		Inventory unmarshal() {
			return new Inventory(ownerId, ids);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, Inventory> {

		@Override
		public Adapted marshal(final Inventory v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public Inventory unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public EntityId getOwnerId() {
		return ownerId;
	}

	public synchronized CharacterEntity getOwner() {
		if (owner == null) {
			final Entity entity = Global.INSTANCE.context().getEntities().get(ownerId);
			if (entity instanceof CharacterEntity) {
				owner = (CharacterEntity) entity;
			}
		}
		return owner;
	}

	public synchronized boolean isEmpty() {
		return entries == null || entries.isEmpty();
	}

	public synchronized Collection<ItemEntity> getItems() {
		if (entries == null) {
			return Collections.emptyList();
		}
		final List<ItemEntity> list = new ArrayList<>();
		for (final Entry entry : entries) {
			list.add(entry.getEntity());
		}
		return list;
	}

	public synchronized Collection<ItemEntity> getItemsByType(final ItemType type) {
		if (entries == null) {
			return Collections.emptyList();
		}
		final List<ItemEntity> list = new ArrayList<>();
		for (final Entry entry : entries) {
			final ItemEntity entity = entry.getEntity();
			if (entity == null) {
				continue;
			}
			if (entity.getStat().getBrand().getType() != type) {
				continue;
			}
			list.add(entity);
		}
		return list;
	}

	public synchronized ItemEntity getItemByType(final ItemType type) {
		if (entries == null) {
			return null;
		}
		for (final Entry entry : entries) {
			final ItemEntity entity = entry.getEntity();
			if (entity == null) {
				continue;
			}
			if (entity.getStat().getBrand().getType() != type) {
				continue;
			}
			return entity;
		}
		return null;
	}

	public synchronized ItemEntity put(final ItemEntity entity) {
		final ItemEntity found = getItemByType(entity.getStat().getBrand().getType());
		if (found != null && found.getStat().getBrand().isStackable()) {
			final long count = entity.getStat().drainCount();
			// lock order: Inventory, ItemStat
			found.getStat().addCount(count);
			Global.INSTANCE.context().getEntities().destroyEntity(entity);
			weight = null;
			sendCount(found);
			return found;
		}
		if (entries == null) {
			entries = new ArrayList<>();
		}
		entries.add(new Entry(entity));
		weight = null;
		sendAdd(entity);
		return entity;
	}

	private void sendCount(final ItemEntity entity) {
		final CharacterEntity owner = getOwner();
		if (owner == null) {
			return;
		}
		final LongStatPoints weightPoints = owner.getWeightPoints();
		for (final FieldDriver driver : owner.getFieldDrivers()) {
			if (!(driver instanceof Player)) {
				continue;
			}
			final Player player = (Player) driver;
			player.getSession().write(new SetItemCountPacket(entity, player.getUser().getItemEntityIdCipher()));
			player.getSession().write(new WeightBarPacket(weightPoints));
		}
	}

	private void sendAdd(final ItemEntity entity) {
		final CharacterEntity owner = getOwner();
		if (owner == null) {
			return;
		}
		final LongStatPoints weightPoints = owner.getWeightPoints();
		for (final FieldDriver driver : owner.getFieldDrivers()) {
			if (!(driver instanceof Player)) {
				continue;
			}
			final Player player = (Player) driver;
			player.getSession().write(new AddItemPacket(entity, player.getUser().getItemEntityIdCipher(),
					player.getSession().getLocale().getLanguage(), owner));
			player.getSession().write(new WeightBarPacket(weightPoints));
		}
	}

	public synchronized ItemEntity getItem(final EntityId id) {
		if (entries == null) {
			return null;
		}
		for (final Entry entry : entries) {
			if (!entry.getId().equals(id)) {
				continue;
			}
			final ItemEntity entity = entry.getEntity();
			if (entity == null) {
				continue;
			}
			return entity;
		}
		return null;
	}

	@GuardedBy("this")
	private ItemEntity removeItem(final EntityId id) {
		if (entries == null) {
			return null;
		}
		for (final Iterator<Entry> i = entries.iterator(); i.hasNext();) {
			final Entry entry = i.next();
			if (!entry.getId().equals(id)) {
				continue;
			}
			final ItemEntity entity = entry.getEntity();
			if (entity == null) {
				continue;
			}
			i.remove();
			return entity;
		}
		return null;
	}

	public synchronized ItemEntity pick(final EntityId id, final long count) {
		final ItemEntity found = getItem(id);
		if (found == null) {
			return null;
		}
		// lock order: Inventory, ItemStat
		final long foundCount = found.getStat().getCount();
		if (count > foundCount) {
			return null;
		} else if (count == foundCount) {
			removeItem(id);
			weight = null;
			sendRemove(found);
			return found;
		} else if (found.getStat().getBrand().isStackable()) {
			// lock order: Inventory, ItemStat
			final long over = found.getStat().addCount(-count);
			final ItemEntity.Builder builder = new ItemEntity.Builder(found);
			final NormalItemStat.Builder statBuilder = new NormalItemStat.Builder((NormalItemStat) found.getStat());
			statBuilder.count(count + over);
			builder.stat(statBuilder.build());
			final ItemEntity part = Global.INSTANCE.context().getEntities().createItemEntity(builder);
			weight = null;
			sendCount(found);
			return part;
		} else {
			return null;
		}
	}

	private void sendRemove(final ItemEntity entity) {
		final CharacterEntity owner = getOwner();
		if (owner == null) {
			return;
		}
		final LongStatPoints weightPoints = owner.getWeightPoints();
		for (final FieldDriver driver : owner.getFieldDrivers()) {
			if (!(driver instanceof Player)) {
				continue;
			}
			final Player player = (Player) driver;
			player.getSession().write(new RemoveItemPacket(entity, player.getUser().getItemEntityIdCipher()));
			player.getSession().write(new WeightBarPacket(weightPoints));
		}
	}

	public synchronized boolean remove(final EntityId id, final long count) {
		final ItemEntity found = getItem(id);
		if (found == null) {
			return false;
		}
		// lock order: Inventory, ItemStat
		final long foundCount = found.getStat().getCount();
		if (found.getStat().getBrand().isStackable() && count < foundCount) {
			// lock order: Inventory, ItemStat
			found.getStat().addCount(-count);
			weight = null;
			sendCount(found);
		} else {
			removeItem(id);
			weight = null;
			sendRemove(found);
		}
		return true;
	}

	public synchronized long getWeight() {
		if (weight == null) {
			long wt = 0;
			if (entries != null) {
				for (final Entry entry : entries) {
					final ItemEntity entity = entry.getEntity();
					if (entity == null) {
						continue;
					}
					wt += entity.getStat().getWeight();
					if (wt < 0) {
						wt = Long.MAX_VALUE;
						break;
					}
				}
			}
			weight = wt;
		}
		return weight;
	}

}
