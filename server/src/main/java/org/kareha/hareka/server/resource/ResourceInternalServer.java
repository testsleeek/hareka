package org.kareha.hareka.server.resource;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.text.MessageFormat;
import java.util.logging.Logger;

import org.kareha.hareka.packet.ParserTable;
import org.kareha.hareka.server.PlainServer;
import org.kareha.hareka.server.ResourceInternalServerPacketType;
import org.kareha.hareka.server.SecureServer;
import org.kareha.hareka.server.Server;
import org.kareha.hareka.server.resource.ipacket.VersionPacket;

public class ResourceInternalServer implements Server {

	private static final Logger logger = Logger.getLogger(ResourceInternalServer.class.getName());

	private final Server server;

	public ResourceInternalServer(final ResourceContext context, final boolean noTls) {
		final ParserTable<ResourceInternalServerPacketType, ResourceInternalSession> parserTable = new ParserTable<>(
				"org.kareha.hareka.server.resource.iparser", ResourceInternalServerPacketType.class, "Parser");
		class Accepter {
			void accepted(final Socket clientSocket) {
				final ResourceInternalSession session = ResourceInternalSession.newInstance(context, clientSocket,
						parserTable);
				context.getInternalSessionTable().addSession(session);
				session.start();

				session.write(new VersionPacket(context.getVersion()));
			}
		}
		final Accepter accepter = new Accepter();

		if (noTls) {
			server = new PlainServer() {
				@Override
				protected void accepted(final Socket clientSocket) {
					accepter.accepted(clientSocket);
				}
			};
		} else {
			final File keyStoreFile = new File(context.getDataDirectory(), ResourceServerConstants.KEY_STORE_FILENAME);
			server = new SecureServer(keyStoreFile, ResourceServerConstants.KEY_STORE_PASSWORD.toCharArray(),
					ResourceServerConstants.KEY_PASSWORD.toCharArray()) {
				@Override
				protected void accepted(final Socket clientSocket) {
					accepter.accepted(clientSocket);
				}
			};
		}
	}

	@Override
	public void start(final int port) throws IOException {
		server.start(port);
		logger.info(MessageFormat.format("Listening to port {0}", port));
		logger.info("Waiting for connections..");
	}

	@Override
	public boolean stop(final long millis) throws IOException, InterruptedException {
		logger.info("Stop Listening..");
		final boolean result = server.stop(millis);
		logger.info("Listening stopped");
		return result;
	}

}
