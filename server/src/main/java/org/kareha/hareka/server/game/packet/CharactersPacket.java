package org.kareha.hareka.server.game.packet;

import java.util.Collection;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.entity.Entities;
import org.kareha.hareka.server.game.entity.Entity;
import org.kareha.hareka.server.game.user.IdCipher;
import org.kareha.hareka.server.game.user.User;

public class CharactersPacket extends GamePacket {

	public CharactersPacket(final Collection<User.CharacterEntry> entries, final Entities entityTable,
			final IdCipher chatEntityIdCipher, final String language) {
		out.writeCompactUInt(entries.size());
		for (final User.CharacterEntry entry : entries) {
			out.write(chatEntityIdCipher.encrypt(entry.getId()));
			final Entity entity = entityTable.get(entry.getId());
			if (!(entity instanceof CharacterEntity)) {
				out.writeString("(null)");
				out.writeString("Undefined");
			} else {
				final CharacterEntity ce = (CharacterEntity) entity;
				final String name = ce.getName(language);
				if (name == null) {
					out.writeString("Undefined");
				} else {
					out.writeString(name);
				}
				final String shape = ce.getShape();
				if (shape == null) {
					out.writeString("Undefined");
				} else {
					out.writeString(shape);
				}
			}
		}
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.CHARACTERS;
	}

}
