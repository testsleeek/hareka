package org.kareha.hareka.server.game.stat;

import java.util.Collection;

import org.kareha.hareka.game.Power;
import org.kareha.hareka.game.AbilityType;
import org.kareha.hareka.math.IntRange;
import org.kareha.hareka.server.game.motion.MotionType;
import org.kareha.hareka.server.game.relationship.SpeciesRelationships;
import org.kareha.hareka.util.Name;

public interface Species {

	String getId();

	Name getName();

	Collection<String> getShapes();

	Collection<AbilityType> getAbilities();

	boolean hasAbility(AbilityType v);

	Collection<Power> getPowers();

	boolean hasPower(Power v);

	IntRange getSkillRange(SkillType type);

	SpeciesRelationships getRelationships();

	MotionType getMotion();

	VisualType getVisualType();
		
	boolean isVisible(VisualType visualType);

}
