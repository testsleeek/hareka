package org.kareha.hareka.server.game.parser;

import java.io.IOException;
import java.math.BigInteger;
import java.security.PublicKey;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.Constants;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.key.KeyGrabber;
import org.kareha.hareka.key.KeyId;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.pow.PowGrabber;
import org.kareha.hareka.server.game.InternalSession;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.TokenGrabber;
import org.kareha.hareka.server.game.ipacket.AddKeysPacket;
import org.kareha.hareka.server.game.packet.CharactersPacket;
import org.kareha.hareka.server.game.packet.MessagePacket;
import org.kareha.hareka.server.game.packet.RequestPowPacket;
import org.kareha.hareka.server.game.packet.RequestPublicKeyPacket;
import org.kareha.hareka.server.game.packet.RequestTokenPacket;
import org.kareha.hareka.server.game.user.InvitationToken;
import org.kareha.hareka.server.game.user.InvitationTokensLogRecord;
import org.kareha.hareka.server.game.user.User;

public final class LoginUserParser implements Parser<Session> {

	@Private
	static final Logger logger = Logger.getLogger(LoginUserParser.class.getName());

	private static final int DIG_WAIT = 2000;

	private enum BundleKey {
		LogoutUserFirst, FailedToLogin, CannotCreateUser,

		EnterInvitationToken, CreatingNewUser,

		NewUserCreated, Canceled, LoginSuccess,
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final int requestId = in.readCompactUInt();

		final ResourceBundle bundle = session.getBundle(LoginUserParser.class.getName());

		if (session.getUser() != null) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.LogoutUserFirst.name()));
			logger.fine(session.getStamp() + "Already logged in");
			return;
		}

		final KeyGrabber.Handler handler = new KeyGrabber.Handler() {
			@Override
			public void handle(final int version, final PublicKey publicKey) {
				if (publicKey != null) {
					login(session, requestId, this, version, publicKey);
					return;
				}
				final PublicKey latest = session.getKeyGrabber().getLatest();
				if (latest == null) {
					session.getKeyGrabber().setLatest(null);
					session.writeRejectPacket(requestId, bundle.getString(BundleKey.FailedToLogin.name()));
					logger.fine(session.getStamp() + "Failed to login");
					return;
				}
				session.getKeyGrabber().setLatest(null);

				final Consumer<InvitationToken> creator = token -> {
					final KeyId parentId;
					if (token == null) {
						parentId = null;
					} else {
						parentId = token.getIssuer();
					}
					// create user
					final User a = session.getContext().getUsers().createUser(parentId, latest);
					if (a == null) {
						session.writeRejectPacket(requestId, bundle.getString(BundleKey.CannotCreateUser.name()));
						logger.fine(session.getStamp() + "Failed to create");
						return;
					}

					// TODO log
					if (token != null) {
						try {
							session.getContext().getInvitationTokensLogger()
									.add(new InvitationTokensLogRecord(token, a.getId()));
						} catch (final IOException | JAXBException e) {
							logger.log(Level.SEVERE, "", e);
						}
					}

					session.write(new MessagePacket(bundle.getString(BundleKey.NewUserCreated.name())));
					logger.info(session.getStamp() + "Created");

					login(session, requestId, null, version, latest);
				};

				switch (session.getContext().getGameServerSettings().getUserRegistrationMode()) {
				default:
					throw new RuntimeException();
				case INVITED_ONLY: {
					final TokenGrabber.Handler tokenHandler = new TokenGrabber.Handler() {
						@Override
						public void handle(final byte[] token) {
							final InvitationToken t = session.getContext().getInvitationTokens().consume(token);
							if (t == null) {
								session.writeRejectPacket(requestId,
										bundle.getString(BundleKey.CannotCreateUser.name()));
								logger.fine(session.getStamp() + "Invalid invitation token");
								return;
							}
							creator.accept(t);
						}

						@Override
						public void canceled() {
							session.writeRejectPacket(requestId, bundle.getString(BundleKey.Canceled.name()));
						}
					};
					final TokenGrabber.Entry tokenEntry = session.getTokenGrabber().add(tokenHandler);
					if (tokenEntry == null) {
						session.writeRejectPacket(requestId, bundle.getString(BundleKey.CannotCreateUser.name()));
						logger.severe(session.getStamp() + "TokenGrabber overflow");
						return;
					}
					session.write(new RequestTokenPacket(tokenEntry.getId(),
							bundle.getString(BundleKey.EnterInvitationToken.name())));
					return;
				}
				case ACCEPT_ALL: {
					final PowGrabber.Handler powHandler = new PowGrabber.Handler() {
						@Override
						public void handle(boolean result) {
							if (!result) {
								session.writeRejectPacket(requestId,
										bundle.getString(BundleKey.CannotCreateUser.name()));
								logger.fine(session.getStamp() + "Invalid PoW");
								return;
							}
							creator.accept(null);
						}

						@Override
						public void canceled() {
							session.writeRejectPacket(requestId, bundle.getString(BundleKey.Canceled.name()));
						}
					};
					final BigInteger target = Constants.NEW_USER_POW_TARGET;
					final PowGrabber.Entry powEntry = session.getPowGrabber().add(powHandler, target);
					if (powEntry == null) {
						session.writeRejectPacket(requestId, bundle.getString(BundleKey.CannotCreateUser.name()));
						logger.severe(session.getStamp() + "PowGrabber overflow");
						return;
					}
					final byte[] data = powEntry.newData(Constants.POW_DATA_BYTE_LENGTH);
					session.write(new RequestPowPacket(powEntry.getId(), target, data,
							bundle.getString(BundleKey.CreatingNewUser.name())));
					return;
				}
				}
			}
		};

		final KeyGrabber.Entry entry = session.getKeyGrabber().add(handler);
		if (entry == null) {
			logger.severe(session.getStamp() + "KeyGrabber overflow");
			return;
		}
		final byte[] keyNonce = entry.newNonce(Constants.NONCE_BYTE_LENGTH);
		session.write(new RequestPublicKeyPacket(entry.getId(), 0, keyNonce));
	}

	public static void login(final Session session, final int requestId, final KeyGrabber.Handler handler,
			final int version, final PublicKey loginKey) {
		final User user = session.getContext().getUsers().login(loginKey, session);
		if (user == null) {
			if (version == 0) {
				session.getKeyGrabber().setLatest(loginKey);
			}
			try {
				Thread.sleep(DIG_WAIT);
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			final KeyGrabber.Entry entry = session.getKeyGrabber().add(handler);
			if (entry == null) {
				logger.severe(session.getStamp() + "KeyGrabber overflow");
				return;
			}
			final byte[] keyNonce = entry.newNonce(Constants.NONCE_BYTE_LENGTH);
			session.write(new RequestPublicKeyPacket(entry.getId(), version + 1, keyNonce));
			return;
		}
		if (version > 0) {
			final PublicKey latest = session.getKeyGrabber().getLatest();
			if (latest != null) {
				user.setPublicKey(latest);
				try {
					session.getContext().getKeyIndex().put(latest, user.getId());
					session.getContext().getKeyIndex().remove(loginKey);
				} catch (final IOException e) {
					logger.log(Level.SEVERE, "", e);
					final ResourceBundle bundle = session.getBundle(LoginUserParser.class.getName());
					session.writeRejectPacket(requestId, bundle.getString(BundleKey.FailedToLogin.name()));
					return;
				}
			}
		}
		session.getKeyGrabber().setLatest(null);
		final ResourceBundle bundle = session.getBundle(LoginUserParser.class.getName());
		session.writeAcceptPacket(requestId, bundle.getString(BundleKey.LoginSuccess.name()));
		logger.info(session.getStamp() + "Logged in");

		final InternalSession internalSession = session.getContext().getInternalConnector().getSession();
		if (internalSession != null) {
			internalSession.write(new AddKeysPacket(session));
		} else {
			logger.info("Resource server is not connected");
		}

		session.write(new CharactersPacket(user.getCharacterEntries(), session.getContext().getEntities(),
				user.getChatEntityIdCipher(), session.getLocale().getLanguage()));
	}

}
