package org.kareha.hareka.server.game.ipacket;

import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.ResourceInternalServerPacketType;
import org.kareha.hareka.server.game.GameInternalPacket;
import org.kareha.hareka.server.game.Session;

public final class RemoveKeysPacket extends GameInternalPacket {

	public RemoveKeysPacket(final Session session) {
		out.write(session.getContext().getSessionStatic().toResourceSessionKey(session.getId()));
	}

	@Override
	protected PacketType getType() {
		return ResourceInternalServerPacketType.REMOVE_KEYS;
	}

}
