package org.kareha.hareka.server.game.stat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.game.AbilityType;
import org.kareha.hareka.game.Power;
import org.kareha.hareka.server.game.motion.MotionType;
import org.kareha.hareka.server.game.relationship.SpeciesRelationships;
import org.kareha.hareka.util.Name;

@ThreadSafe
@XmlJavaTypeAdapter(AbstractSpecies.Adapter.class)
public abstract class AbstractSpecies implements Species {

	public static abstract class Builder {

		protected Name name;
		protected List<String> shapes;
		protected List<AbilityType> abilities;
		protected List<Power> powers;
		protected SpeciesRelationships relationships;
		protected MotionType motion;
		protected VisualType visualType;

		public Builder name(final Name v) {
			name = new Name(v);
			return this;
		}

		public Builder shapes(final List<String> v) {
			shapes = v;
			return this;
		}

		public Builder abilities(final List<AbilityType> v) {
			abilities = v;
			return this;
		}

		public Builder powers(final List<Power> v) {
			powers = v;
			return this;
		}

		public Builder relationships(final SpeciesRelationships v) {
			relationships = v;
			return this;
		}

		public Builder motion(final MotionType v) {
			motion = v;
			return this;
		}

		public Builder visualType(final VisualType v) {
			visualType = v;
			return this;
		}

		public abstract AbstractSpecies build(String id);

	}

	protected final String id;
	protected final Name name;
	@GuardedBy("this")
	protected final List<String> shapes;
	@GuardedBy("this")
	protected final Set<AbilityType> abilities;
	@GuardedBy("this")
	protected final Set<Power> powers;
	@GuardedBy("this")
	protected final SpeciesRelationships relationships;
	protected final MotionType motion;
	protected final VisualType visualType;

	protected AbstractSpecies(final String id, final Builder builder) {
		this.id = id;
		name = builder.name;
		shapes = builder.shapes;
		if (builder.abilities == null) {
			abilities = EnumSet.noneOf(AbilityType.class);
		} else {
			abilities = EnumSet.copyOf(builder.abilities);
		}
		if (builder.powers == null) {
			powers = EnumSet.noneOf(Power.class);
		} else {
			powers = EnumSet.copyOf(builder.powers);
		}
		relationships = builder.relationships;
		motion = builder.motion;
		if (builder.visualType == null) {
			visualType = VisualType.NORMAL;
		} else {
			visualType = builder.visualType;
		}
	}

	@XmlRootElement(name = "abstractSpecies")
	@XmlSeeAlso({ NormalSpecies.Adapted.class })
	@XmlAccessorType(XmlAccessType.NONE)
	protected static abstract class Adapted {

		@XmlAttribute
		protected String id;
		@XmlElement
		protected Name name;
		@XmlElement
		protected List<String> shape;
		@XmlElement(name = "ability")
		protected List<AbilityType> abilities;
		@XmlElement(name = "power")
		protected List<Power> powers;
		@XmlElement
		protected SpeciesRelationships relationships;
		@XmlElement
		protected MotionType motion;
		@XmlElement(name = "visual")
		protected VisualType visualType;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final AbstractSpecies v) {
			id = v.id;
			name = new Name(v.name);
			synchronized (v) {
				if (v.shapes != null) {
					shape = new ArrayList<>(v.shapes);
				}
				if (v.abilities != null) {
					abilities = new ArrayList<>(v.abilities);
				}
				if (v.powers != null) {
					powers = new ArrayList<>(v.powers);
				}
			}
			relationships = v.relationships;
			motion = v.motion;
			visualType = v.visualType;
		}

		protected abstract AbstractSpecies unmarshal();

	}

	protected abstract Adapted marshal();

	static class Adapter extends XmlAdapter<Adapted, AbstractSpecies> {

		@Override
		public Adapted marshal(final AbstractSpecies v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public AbstractSpecies unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public Name getName() {
		return name;
	}

	@Override
	public synchronized Collection<String> getShapes() {
		if (shapes == null || shapes.isEmpty()) {
			return Collections.emptyList();
		}
		return new ArrayList<>(shapes);
	}

	@Override
	public synchronized Collection<AbilityType> getAbilities() {
		if (abilities == null || abilities.isEmpty()) {
			return Collections.emptyList();
		}
		return new ArrayList<>(abilities);
	}

	@Override
	public synchronized boolean hasAbility(final AbilityType v) {
		if (abilities == null) {
			return false;
		}
		return abilities.contains(v);
	}

	@Override
	public synchronized Collection<Power> getPowers() {
		if (powers == null || powers.isEmpty()) {
			return Collections.emptyList();
		}
		return new ArrayList<>(powers);
	}

	@Override
	public synchronized boolean hasPower(final Power v) {
		if (powers == null) {
			return false;
		}
		return powers.contains(v);
	}

	@Override
	public SpeciesRelationships getRelationships() {
		return relationships;
	}

	@Override
	public MotionType getMotion() {
		return motion;
	}

	@Override
	public VisualType getVisualType() {
		return visualType;
	}

	@Override
	public synchronized boolean isVisible(final VisualType visualType) {
		return powers.contains(visualType.power());
	}

}
