package org.kareha.hareka.server.game.parser;

import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.field.Direction;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.field.ServerFieldObject;
import org.kareha.hareka.server.game.packet.WaitPacket;
import org.kareha.hareka.server.game.stat.SkillType;
import org.kareha.hareka.server.game.stat.SkillVariation;
import org.kareha.hareka.wait.WaitResult;
import org.kareha.hareka.wait.WaitType;

public final class MoveParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(MoveParser.class.getName());

	private enum BundleKey {
		LoginCharacterFirst,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(MoveParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final long waitId = in.readCompactULong();
		final Vector position = Vector.readFrom(in);

		// check state
		final Player player = session.getPlayer();
		if (player == null) {
			inform(session, BundleKey.LoginCharacterFirst.name());
			logger.fine(session.getStamp() + "Character not logged in");
			return;
		}
		final ServerFieldObject fo = player.getEntity().getFieldObject();
		if (fo == null) {
			logger.warning(session.getStamp() + "FieldObject not found");
			return;
		}
		final Vector newPosition = position.invert(player.getTransformation());

		final TileType tileType = fo.getField().getTile(newPosition).type();
		final WaitResult result = player.getEntity().getWaitGenerator().next(waitId, WaitType.MOTION,
				player.getEntity().getStat().getMotionWait(tileType), player.getWaitPenalty());
		if (!result.isSuccess()) {
			session.write(new WaitPacket(result.getWait()));
			return;
		}
		player.getEntity().sendWait(result.getWait());

		if (player.getEntity().isStuck()) {
			return;
		}

		final Vector prevPosition = player.getPosition();
		if (prevPosition.distance(newPosition) > 1) {
			return;
		}
		final Direction newDirection = newPosition.subtract(prevPosition).direction();
		final Placement newPlacement = Placement.valueOf(newPosition, newDirection);
		Placement prevPlacement2 = fo.move(newPlacement);
		if (prevPlacement2 == null) {
			// not moved, OK
			return;
		}
		player.getEntity().getStat().getSkills().get(SkillType.WALK).addValue(SkillVariation.WALK_INCREMENT);

		session.getContext().getCharacterSpawner().trySpawnCharacter(fo, newDirection, newPosition);
		session.getContext().getItemSpawner().trySpawnItem(fo, newDirection, newPosition);
	}

}
