package org.kareha.hareka.server.game.parser;

import java.util.logging.Logger;

import org.kareha.hareka.key.KeyGrabber;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;

public final class NoPublicKeyParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(NoPublicKeyParser.class.getName());

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int handlerId = in.readCompactUInt();
		final int version = in.readCompactUInt();

		final KeyGrabber.Entry entry = session.getKeyGrabber().get(handlerId);
		if (entry == null) {
			logger.fine(session.getStamp() + "Not found handlerId=" + handlerId);
			return;
		}
		entry.handle(version, null);
	}

}
