package org.kareha.hareka.server.game.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.server.game.stat.AbstractItemStat;
import org.kareha.hareka.server.game.stat.ItemStat;
import org.kareha.hareka.server.game.stat.NormalItemStat;
import org.kareha.hareka.server.game.stat.VisualType;

@ThreadSafe
public class ItemEntity extends AbstractEntity {

	private static final int MOTION_WAIT = 500;

	public static class Builder extends AbstractEntity.Builder {

		protected ItemStat stat;

		public Builder() {

		}

		public Builder(final ItemEntity entity) {
			super(entity);
			final ItemStat origStat;
			synchronized (entity) {
				origStat = entity.stat;
			}
			if (origStat instanceof NormalItemStat) {
				stat = new NormalItemStat.Builder((NormalItemStat) origStat).build();
			}
		}

		public Builder stat(final ItemStat v) {
			stat = v;
			return this;
		}

		@Override
		public ItemEntity build(final EntityId id) {
			return new ItemEntity(id, this);
		}

	}

	protected final ItemStat stat;

	protected ItemEntity(final EntityId id, final Builder builder) {
		super(id, builder);
		stat = builder.stat;
	}

	@XmlRootElement(name = "itemEntity")
	@XmlType(name = "itemEntity")
	@XmlAccessorType(XmlAccessType.NONE)
	protected static class Adapted extends AbstractEntity.Adapted {

		@XmlElement
		protected AbstractItemStat stat;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final ItemEntity v) {
			super(v);
			if (v.stat instanceof AbstractItemStat) {
				stat = (AbstractItemStat) v.stat;
			}
		}

		@Override
		protected ItemEntity unmarshal() {
			final Builder builder = new Builder();
			builder.shape(shape);
			if (stat != null) {
				builder.stat(stat);
			}
			builder.fieldId(fieldId);
			builder.placement(placement);
			builder.transformation(transformation);
			builder.mounted(mounted);
			return builder.build(id);
		}

	}

	@Override
	public Adapted marshal() {
		return new Adapted(this);
	}

	@Override
	public int getMotionWait(final TileType tileType) {
		return MOTION_WAIT;
	}

	@Override
	public ItemStat getStat() {
		return stat;
	}

	@Override
	public int getJumpDown() {
		return stat.getJumpDown();
	}

	@Override
	public int getJumpUp() {
		return stat.getJumpUp();
	}

	@Override
	public VisualType getVisualType() {
		return VisualType.NORMAL;
	}

	@Override
	public boolean isVisible(final VisualType visualType) {
		switch (visualType) {
		default:
			return false;
		case NORMAL:
			return true;
		}
	}

	@Override
	public void startMotion() {
		// do nothing
	}

	@Override
	public void stopMotion() {
		// do nothing
	}

	@Override
	public void startHealthRegeneration() {
		// do nothing
	}

	@Override
	public void stopHealthRegeneration() {
		// do nothing
	}

	@Override
	public void startMagicRegeneration() {
		// do nothing
	}

	@Override
	public void stopMagicRegeneration() {
		// do nothing
	}

}
