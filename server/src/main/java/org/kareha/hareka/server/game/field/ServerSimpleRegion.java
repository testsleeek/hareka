package org.kareha.hareka.server.game.field;

import org.kareha.hareka.field.SimpleRegion;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TilePattern;
import org.kareha.hareka.field.Transformation;
import org.kareha.hareka.field.Vector;

public class ServerSimpleRegion extends SimpleRegion {

	protected final ServerField field;

	public ServerSimpleRegion(final TilePattern tilePattern, final boolean mutable, final Vector center, final int size, final ServerField field) {
		super(tilePattern, mutable, center, size);
		this.field = field;
	}

	public ServerSimpleRegion(final SimpleRegion original, final ServerField field) {
		super(original);
		this.field = field;
	}

	@Override
	public boolean contains(final Vector position) {
		return field.getBoundary().distance(center, position) <= size;
	}

	@Override
	public Tile getTile(final Vector position) {
		return super.getTile(field.getBoundary().subtract(center, position));
	}

	@Override
	public ServerSimpleRegion transform(final Transformation transformation) {
		return new ServerSimpleRegion(tilePattern, mutable, center.transform(transformation), size, field);
	}

	@Override
	public ServerSimpleRegion invert(final Transformation transformation) {
		return new ServerSimpleRegion(tilePattern, mutable, center.invert(transformation), size, field);
	}

}
