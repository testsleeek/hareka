package org.kareha.hareka.server.game;

import java.io.File;
import java.io.IOException;
import java.security.Key;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.Version;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.key.KeyId;
import org.kareha.hareka.key.KeyIdPersistentFormat;
import org.kareha.hareka.key.KeyPersistentFormat;
import org.kareha.hareka.key.KeyStack;
import org.kareha.hareka.key.KeyStatic;
import org.kareha.hareka.key.SimpleKeyStore;
import org.kareha.hareka.logging.SlowXmlLogger;
import org.kareha.hareka.logging.XmlLogger;
import org.kareha.hareka.persistent.FastPersistentHashTable;
import org.kareha.hareka.persistent.PersistentHashTable;
import org.kareha.hareka.server.Server;
import org.kareha.hareka.server.game.ability.Abilities;
import org.kareha.hareka.server.game.entity.Entities;
import org.kareha.hareka.server.game.entity.EntityStatic;
import org.kareha.hareka.server.game.field.FieldStatic;
import org.kareha.hareka.server.game.field.Fields;
import org.kareha.hareka.server.game.item.Items;
import org.kareha.hareka.server.game.stat.SpeciesTable;
import org.kareha.hareka.server.game.user.AccessController;
import org.kareha.hareka.server.game.user.InvitationTokens;
import org.kareha.hareka.server.game.user.InvitationTokensLogRecord;
import org.kareha.hareka.server.game.user.RoleTokens;
import org.kareha.hareka.server.game.user.Roles;
import org.kareha.hareka.server.game.user.RolesLogRecord;
import org.kareha.hareka.server.game.user.Users;
import org.kareha.hareka.server.game.user.Workspace;

public class Context {

	private final Version version;
	private final File dataDirectory;
	private final Rebooter rebooter;
	private final GameServerSettings gameServerSettings;
	private final SimpleKeyStore resourceSimpleKeyStore;
	private final SessionStatic sessionStatic;
	private final KeyStack keyStack;
	private final KeyStatic userStatic;
	private final PersistentHashTable<Key, KeyId> keyIndex;
	private final Roles roles;
	private final XmlLogger<RolesLogRecord> rolesLogger;
	private final AccessController accessController;
	private final Workspace workspace;
	private final Users users;
	private final InvitationTokens invitationTokens;
	private final XmlLogger<InvitationTokensLogRecord> invitationTokensLogger;
	private final RoleTokens roleTokens;
	private final Sessions sessions;
	private final Clock clock;
	private final FieldStatic fieldStatic;
	private final Fields fields;
	private final EntityStatic entityStatic;
	private final Entities entities;
	private final SpeciesTable speciesTable;
	private final Abilities abilities;
	private final Items items;
	private final CharacterSpawner characterSpawner;
	private final ItemSpawner itemSpawner;
	@GuardedBy("this")
	private Server server;
	@GuardedBy("this")
	private InternalConnector internalConnector;

	public Context(final Version version, final File dataDirectory) throws IOException, JAXBException {
		this.version = version;
		this.dataDirectory = dataDirectory;
		rebooter = new Rebooter(new File(dataDirectory, "rebooting"));
		gameServerSettings = new GameServerSettings(new File(dataDirectory, "GameServerSettings.xml"));
		resourceSimpleKeyStore = new SimpleKeyStore(new File(dataDirectory, "ResourceKeyStore"));
		sessionStatic = new SessionStatic(new File(dataDirectory, "SessionStatic.xml"));
		keyStack = new KeyStack(new File(dataDirectory, "KeyStack.xml"));
		userStatic = new KeyStatic(new File(dataDirectory, "UserStatic.xml"));
		keyIndex = new FastPersistentHashTable<>(new File(dataDirectory, "KeyIndex.dat"), new KeyPersistentFormat(),
				new KeyIdPersistentFormat());
		roles = new Roles(new File(dataDirectory, "Roles.xml"));
		rolesLogger = new SlowXmlLogger<>(dataDirectory, "RolesLog", RolesLogRecord.class, true);
		accessController = new AccessController(new File(dataDirectory, "AccessController.xml"), roles);
		workspace = new Workspace(new File(dataDirectory, "Workspace.xml"));
		users = new Users(new File(dataDirectory, "user"), userStatic, keyIndex);
		invitationTokens = new InvitationTokens(new File(dataDirectory, "InvitationTokens.xml"));
		invitationTokensLogger = new SlowXmlLogger<>(dataDirectory, "InvitationTokensLog",
				InvitationTokensLogRecord.class, true);
		roleTokens = new RoleTokens(new File(dataDirectory, "RoleTokens.xml"), accessController);
		sessions = new Sessions();
		clock = new Clock(new File(dataDirectory, "Clock.xml"));
		fieldStatic = new FieldStatic(new File(dataDirectory, "FieldStatic.xml"));
		fields = new Fields(new File(dataDirectory, "field"), fieldStatic);
		entityStatic = new EntityStatic(new File(dataDirectory, "EntityStatic.xml"));
		entities = new Entities(new File(dataDirectory, "entity"), entityStatic, fields);
		speciesTable = new SpeciesTable(new File(dataDirectory, "species"));
		abilities = new Abilities();
		items = new Items();
		characterSpawner = new CharacterSpawner(new File(dataDirectory, "CharacterSpawner.xml"));
		itemSpawner = new ItemSpawner(new File(dataDirectory, "ItemSpawner.xml"));
	}

	public Version getVersion() {
		return version;
	}

	public File getDataDirectory() {
		return dataDirectory;
	}

	public Rebooter getRebooter() {
		return rebooter;
	}

	public GameServerSettings getGameServerSettings() {
		return gameServerSettings;
	}

	public SimpleKeyStore getResourceSimpleKeyStore() {
		return resourceSimpleKeyStore;
	}

	public SessionStatic getSessionStatic() {
		return sessionStatic;
	}

	public KeyStack getKeyStack() {
		return keyStack;
	}

	public PersistentHashTable<Key, KeyId> getKeyIndex() {
		return keyIndex;
	}

	public Roles getRoles() {
		return roles;
	}

	public XmlLogger<RolesLogRecord> getRolesLogger() {
		return rolesLogger;
	}

	public AccessController getAccessController() {
		return accessController;
	}

	public Workspace getWorkspace() {
		return workspace;
	}

	public Users getUsers() {
		return users;
	}

	public InvitationTokens getInvitationTokens() {
		return invitationTokens;
	}

	public XmlLogger<InvitationTokensLogRecord> getInvitationTokensLogger() {
		return invitationTokensLogger;
	}

	public RoleTokens getRoleTokens() {
		return roleTokens;
	}

	public Sessions getSessions() {
		return sessions;
	}

	public Clock getClock() {
		return clock;
	}

	public Fields getFields() {
		return fields;
	}

	public Entities getEntities() {
		return entities;
	}

	public SpeciesTable getSpeciesTable() {
		return speciesTable;
	}

	public Abilities getAbilities() {
		return abilities;
	}

	public Items getItems() {
		return items;
	}

	public CharacterSpawner getCharacterSpawner() {
		return characterSpawner;
	}

	public ItemSpawner getItemSpawner() {
		return itemSpawner;
	}

	public synchronized Server getServer() {
		return server;
	}

	public synchronized void setServer(final Server server) {
		this.server = server;
	}

	public synchronized InternalConnector getInternalConnector() {
		return internalConnector;
	}

	public synchronized void setInternalConnector(final InternalConnector internalConnector) {
		this.internalConnector = internalConnector;
	}

	public synchronized void load() throws IOException, JAXBException {
		// fields must be loaded before entities
		fields.load();
		// races must be loaded before entities
		speciesTable.load();
		entities.load();
	}

	public synchronized void save() throws JAXBException {
		gameServerSettings.save();
		sessionStatic.save();
		userStatic.save();
		try {
			keyIndex.close(); // XXX using close instead of save
		} catch (final Exception e) {
			throw new JAXBException(e);
		}
		roles.save();
		try {
			rolesLogger.close(); // XXX using close instead of save
		} catch (final Exception e) {
			throw new JAXBException(e);
		}
		accessController.save();
		invitationTokens.save();
		try {
			invitationTokensLogger.close(); // XXX using close instead of save
		} catch (final Exception e) {
			throw new JAXBException(e);
		}
		roleTokens.save();
		clock.save();
		fieldStatic.save();
		fields.save();
		speciesTable.save();
		entityStatic.save();
		entities.save();
		characterSpawner.save();
		itemSpawner.save();
	}

}
