package org.kareha.hareka.server.game;

import java.io.IOException;
import java.util.Collection;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.util.SimpleLogger;

public class ShutdownHook extends Thread {

	private static final long SERVER_STOP_WAIT = 30000;
	private static final long INTERNAL_STOP_WAIT = 10000;

	private enum BundleKey {
		ServerShutdown,
	}

	private final Context context;

	public ShutdownHook(final Context context) {
		this.context = context;
	}

	public void forceExitAllConnections() {
		final Collection<Session> sessions = context.getSessions().getSessions();
		for (final Session s : sessions) {
			final ResourceBundle bundle = s.getBundle(ShutdownHook.class.getName());
			s.writeExitPacket(bundle.getString(BundleKey.ServerShutdown.name()));
		}
		try {
			Thread.sleep(100);
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		for (final Session s : sessions) {
			s.close();
		}
		final long waitStart = System.currentTimeMillis();
		while (context.getSessions().sizeOfSessions() > 0) {
			try {
				Thread.sleep(100);
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			if (System.currentTimeMillis() - waitStart >= 60 * 1000) {
				break;
			}
		}
	}

	@Override
	// Logger does not work at shutdown. Use SimpleLogger instead.
	public void run() {
		SimpleLogger.INSTANCE.log("Shutdown..");

		try {
			context.getServer().stop(SERVER_STOP_WAIT);
			context.getInternalConnector().stop(INTERNAL_STOP_WAIT);
		} catch (final IOException e) {
			SimpleLogger.INSTANCE.log(e);
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
			SimpleLogger.INSTANCE.log(e);
		}

		context.getEntities().stopMotion();

		forceExitAllConnections();

		// TODO stop motion

		try {
			context.save();
		} catch (final JAXBException e) {
			SimpleLogger.INSTANCE.log("Failed to save");
		}

		SimpleLogger.INSTANCE.log("Shutdown finished.");
		if (context.getRebooter().isRebooting()) {
			SimpleLogger.INSTANCE.log("To be rebooted..");
		}
	}

}
