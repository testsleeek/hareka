package org.kareha.hareka.server.game.parser;

import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.DiceRoll;
import org.kareha.hareka.server.game.GameServerConstants;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.packet.DiceResultPacket;

public final class DiceChoiceParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(DiceChoiceParser.class.getName());

	private enum BundleKey {
		TheChoiceIsTooLong, NoSuchDiceRoll, IllegalChoice,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(DiceChoiceParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int id = in.readCompactUInt();
		final int length = in.readCompactUInt();
		if (length > GameServerConstants.MAX_DICE_ROLL_CHOICE_LENGTH) {
			inform(session, BundleKey.TheChoiceIsTooLong.name());
			logger.fine(session.getStamp() + "Too long id=" + id + " length=" + length);
			return;
		}
		final int[] choice = new int[length];
		for (int i = 0; i < length; i++) {
			choice[i] = in.readCompactUInt();
		}

		final DiceRoll diceRoll = session.getDiceRollTable().getDiceRoll(id);
		if (diceRoll == null) {
			inform(session, BundleKey.NoSuchDiceRoll.name());
			logger.fine(session.getStamp() + "Not found id=" + id);
			return;
		}
		if (!diceRoll.setChoice(choice)) {
			inform(session, BundleKey.IllegalChoice.name());
			logger.fine(session.getStamp() + "Illegal choice id=" + id);
			return;
		}
		session.write(new DiceResultPacket(diceRoll));
	}

}
