package org.kareha.hareka.server.game.ability;

import org.kareha.hareka.field.TileType;
import org.kareha.hareka.game.AbilityType;
import org.kareha.hareka.game.Power;
import org.kareha.hareka.server.game.Global;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.entity.FieldDriver;
import org.kareha.hareka.server.game.entity.FieldEntity;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.wait.WaitType;

public class PossessAbility implements Ability {

	@Override
	public AbilityType getType() {
		return AbilityType.POSSESS;
	}

	@Override
	public int getReach() {
		return 1;
	}

	@Override
	public WaitType getWaitType() {
		return WaitType.ATTACK;
	}

	@Override
	public int getWait(final CharacterEntity entity, final TileType tileType) {
		return entity.getStat().getAttackWait(tileType);
	}

	@Override
	public void use(final CharacterEntity entity, final FieldEntity target) {
		if (target == entity) {
			return;
		}
		if (!(target instanceof CharacterEntity)) {
			return;
		}
		final CharacterEntity targetChar = (CharacterEntity) target;
		if (!targetChar.getStat().getHealthPoints().isAlive()) {
			return;
		}

		if (targetChar.getStat().hasPower(Power.IMMUNE_TO_POSSESSION)) {
			return;
		}

		for (final FieldDriver driver : entity.getFieldDrivers()) {
			if (!(driver instanceof Player)) {
				continue;
			}
			final Player player = (Player) driver;
			final Session session = player.getSession();
			final User user = player.getUser();
			session.logoutCharacter();
			final Player newPlayer = new Player(targetChar, session, user);
			if (!session.setPlayer(newPlayer)) {
				continue;
			}
			if (user.removeCharacterEntry(entity.getId())) {
				entity.removeOwner(user.getId());
				if (user.addCharacterEntry(targetChar.getId())) {
					targetChar.addOwner(user.getId());
				}
			}
			newPlayer.synchronize();
		}

		if (entity.getStat().hasPower(Power.VOLATILE)) {
			entity.unmount();
			Global.INSTANCE.context().getEntities().destroyEntity(entity);
		}

	}

}
