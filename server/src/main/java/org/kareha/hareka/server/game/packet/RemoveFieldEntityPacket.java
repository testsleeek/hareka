package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.entity.FieldEntity;
import org.kareha.hareka.server.game.user.IdCipher;

public final class RemoveFieldEntityPacket extends GamePacket {

	public RemoveFieldEntityPacket(final FieldEntity entity, final IdCipher fieldEntityIdCipher) {
		out.write(fieldEntityIdCipher.encrypt(entity.getId()));
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.REMOVE_FIELD_ENTITY;
	}

}
