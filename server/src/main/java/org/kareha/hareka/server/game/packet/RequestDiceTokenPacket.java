package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.DiceRoll;
import org.kareha.hareka.server.game.GamePacket;

public final class RequestDiceTokenPacket extends GamePacket {

	public RequestDiceTokenPacket(final DiceRoll diceRoll) {
		out.writeCompactUInt(diceRoll.getId());
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.REQUEST_DICE_TOKEN;
	}

}
