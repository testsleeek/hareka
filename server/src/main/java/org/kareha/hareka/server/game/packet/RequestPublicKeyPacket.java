package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;

public final class RequestPublicKeyPacket extends GamePacket {

	public RequestPublicKeyPacket(final int handlerId, final int version, final byte[] nonce) {
		out.writeCompactUInt(handlerId);
		out.writeCompactUInt(version);
		out.writeByteArray(nonce);
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.REQUEST_PUBLIC_KEY;
	}

}
