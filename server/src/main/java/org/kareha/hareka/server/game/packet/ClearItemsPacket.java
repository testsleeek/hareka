package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;

public final class ClearItemsPacket extends GamePacket {

	public ClearItemsPacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.CLEAR_ITEMS;
	}

}
