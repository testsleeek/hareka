package org.kareha.hareka.server.game.ability;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.game.AbilityType;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.entity.ChatDriver;
import org.kareha.hareka.server.game.entity.EntityId;
import org.kareha.hareka.server.game.entity.FieldEntity;
import org.kareha.hareka.server.game.packet.MessagePacket;
import org.kareha.hareka.server.game.stat.SkillType;
import org.kareha.hareka.wait.WaitType;

public class GreetAbility implements Ability {

	private static final int DURATION = 1000 * 60 * 8;

	private static class FromTo {

		private final EntityId from;
		private final EntityId to;

		FromTo(final EntityId from, final EntityId to) {
			this.from = from;
			this.to = to;
		}

		@Override
		public boolean equals(final Object obj) {
			if (!(obj instanceof FromTo)) {
				return false;
			}
			final FromTo ft = (FromTo) obj;
			return ft.from.equals(from) && ft.to.equals(to);
		}

		@Override
		public int hashCode() {
			return from.hashCode() ^ to.hashCode();
		}

	}

	private static final Map<FromTo, Long> logMap = new ConcurrentHashMap<>();
	@GuardedBy("GreetAbility.class")
	private static long lastCleanTime = System.currentTimeMillis();

	private enum BundleKey {

		Greeted, BeGreeted,
	}

	@Override
	public AbilityType getType() {
		return AbilityType.GREET;
	}

	@Override
	public int getReach() {
		return 4;
	}

	@Override
	public WaitType getWaitType() {
		return WaitType.ATTACK;
	}

	@Override
	public int getWait(final CharacterEntity entity, final TileType tileType) {
		return entity.getStat().getAttackWait(tileType);
	}

	@Override
	public void use(final CharacterEntity entity, final FieldEntity target) {
		if (target == entity) {
			return;
		}
		if (!(target instanceof CharacterEntity)) {
			return;
		}

		final FromTo ft = new FromTo(entity.getId(), target.getId());
		final Long last = logMap.get(ft);
		final long currentTime = System.currentTimeMillis();
		if (last != null) {
			if (currentTime - last < DURATION) {
				return;
			}
		}
		logMap.put(ft, currentTime);

		final boolean toBeCleaned;
		synchronized (GreetAbility.class) {
			if (currentTime - lastCleanTime >= DURATION) {
				toBeCleaned = true;
				lastCleanTime = currentTime;
			} else {
				toBeCleaned = false;
			}
		}
		if (toBeCleaned) {
			for (final Map.Entry<FromTo, Long> entry : logMap.entrySet()) {
				if (currentTime - entry.getValue() >= DURATION) {
					logMap.remove(entry.getKey());
				}
			}
		}

		final CharacterEntity targetChar = (CharacterEntity) target;
		targetChar.getStat().getRelationships().addLove(entity.getId(), 1);
		targetChar.getStat().getSpecies().getRelationships().addLove(entity.getStat().getSpecies().getId(), 1);
		targetChar.getStat().getSkills().get(SkillType.LOVE).addValue(4);
		targetChar.getStat().getSkills().get(SkillType.HATE).addValue(-1);

		for (final ChatDriver driver : entity.getChatDrivers()) {
			if (!(driver instanceof Player)) {
				continue;
			}
			final Player player = (Player) driver;
			final ResourceBundle bundle = player.getSession().getBundle(GreetAbility.class.getName());
			final String message = MessageFormat.format(bundle.getString(BundleKey.Greeted.name()),
					targetChar.getName(player.getSession().getLocale().getLanguage()));
			player.getSession().write(new MessagePacket(message));
		}

		for (final ChatDriver driver : targetChar.getChatDrivers()) {
			if (!(driver instanceof Player)) {
				continue;
			}
			final Player player = (Player) driver;
			final ResourceBundle bundle = player.getSession().getBundle(GreetAbility.class.getName());
			final String message = MessageFormat.format(bundle.getString(BundleKey.BeGreeted.name()),
					entity.getName(player.getSession().getLocale().getLanguage()));
			player.getSession().write(new MessagePacket(message));
		}
	}

}
