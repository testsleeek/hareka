package org.kareha.hareka.server.resource;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.Version;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.server.Server;

public class ResourceContext {

	private final Version version;
	private final File dataDirectory;
	private final ResourceInternalSessionIdTable internalSessionIdTable;
	private final ResourceInternalSessionTable internalSessionTable;
	@GuardedBy("this")
	private Server internalServer;
	private final ResourceSessionKeyTable sessionKeyTable;

	public ResourceContext(final Version version, final File dataDirectory) throws IOException {
		this.version = version;
		this.dataDirectory = dataDirectory;
		internalSessionIdTable = new ResourceInternalSessionIdTable(dataDirectory);
		internalSessionTable = new ResourceInternalSessionTable();
		sessionKeyTable = new ResourceSessionKeyTable();
	}

	public Version getVersion() {
		return version;
	}

	public File getDataDirectory() {
		return dataDirectory;
	}

	public ResourceInternalSessionIdTable getInternalSessionIdTable() {
		return internalSessionIdTable;
	}

	public ResourceInternalSessionTable getInternalSessionTable() {
		return internalSessionTable;
	}

	public synchronized Server getInternalServer() {
		return internalServer;
	}

	public synchronized void setInternalServer(final Server internalServer) {
		this.internalServer = internalServer;
	}

	public ResourceSessionKeyTable getSessionKeyTable() {
		return sessionKeyTable;
	}

	public synchronized void save() throws JAXBException {
		internalSessionIdTable.save();
	}

}
