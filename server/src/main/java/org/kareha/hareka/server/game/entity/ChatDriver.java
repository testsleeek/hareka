package org.kareha.hareka.server.game.entity;

public interface ChatDriver {

	void handleReceiveLocalChat(ChatEntity speaker, String content);

	void handleSendPrivateChatEcho(ChatEntity peer, String content);

	void handleReceivePrivateChat(ChatEntity peer, String content);

}
