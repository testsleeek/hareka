package org.kareha.hareka.server.resource;

import java.io.IOException;
import java.util.Collection;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.util.SimpleLogger;

public class ResourceShutdownHook extends Thread {

	private enum BundleKey {

		ServerShutdown,

	}

	private static final long SERVER_STOP_WAIT = 30000;

	private final ResourceContext context;

	public ResourceShutdownHook(final ResourceContext context) {
		this.context = context;
	}

	public void forceExitAllConnections() {
		final Collection<ResourceInternalSession> internalSessions = context.getInternalSessionTable().getSessions();
		for (final ResourceInternalSession s : internalSessions) {
			final ResourceBundle bundle = s.getBundle(ResourceShutdownHook.class.getName());
			s.writeExitPacket(bundle.getString(BundleKey.ServerShutdown.name()));
		}
		try {
			Thread.sleep(100);
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		for (final ResourceInternalSession s : internalSessions) {
			s.close();
		}
		final long waitStart = System.currentTimeMillis();
		while (context.getInternalSessionTable().sizeOfSessions() > 0) {
			try {
				Thread.sleep(100);
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			if (System.currentTimeMillis() - waitStart >= 60 * 1000) {
				break;
			}
		}
	}

	@Override
	// Logger does not work at shutdown. Use SimpleLogger instead.
	public void run() {
		SimpleLogger.INSTANCE.log("Shutdown..");

		try {
			context.getInternalServer().stop(SERVER_STOP_WAIT);
		} catch (final IOException e) {
			SimpleLogger.INSTANCE.log(e);
		} catch (final InterruptedException e) {
			Thread.currentThread().interrupt();
			SimpleLogger.INSTANCE.log(e);
		}

		// context.getEntityTable().stopMotion();

		forceExitAllConnections();

		// TODO stop motion

		try {
			context.save();
		} catch (final JAXBException e) {
			SimpleLogger.INSTANCE.log("Failed to save");
		}

		SimpleLogger.INSTANCE.log("Shutdown finished.");
	}

}
