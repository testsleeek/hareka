package org.kareha.hareka.server.game;

import java.io.File;
import java.util.concurrent.atomic.AtomicLong;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.LocalSessionId;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.server.game.user.IdCipher;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class SessionStatic {

	private final File file;
	@Private
	final AtomicLong count;
	@Private
	final IdCipher resourceSessionKeyCipher;

	public SessionStatic(final File file) throws JAXBException {
		this.file = file;
		final Adapted adapted = load();
		if (adapted == null) {
			count = new AtomicLong();
			resourceSessionKeyCipher = new IdCipher();
		} else {
			count = new AtomicLong(Long.parseLong(adapted.count, 16));
			resourceSessionKeyCipher = adapted.resourceSessionKeyCipher;
		}
	}

	@XmlRootElement(name = "sessionStatic")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		@Private
		String count;
		@XmlElement
		@Private
		IdCipher resourceSessionKeyCipher;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final SessionStatic v) {
			count = Long.toHexString(v.count.get());
			resourceSessionKeyCipher = v.resourceSessionKeyCipher;
		}

	}

	private Adapted load() throws JAXBException {
		if (!file.isFile()) {
			return null;
		}
		return JaxbUtil.unmarshal(file, Adapted.class);
	}

	public void save() throws JAXBException {
		JaxbUtil.marshal(new Adapted(this), file);
	}

	public SessionId next() {
		return SessionId.valueOf(count.getAndIncrement());
	}

	public LocalSessionId toResourceSessionKey(final SessionId id) {
		return resourceSessionKeyCipher.encrypt(id);
	}

}
