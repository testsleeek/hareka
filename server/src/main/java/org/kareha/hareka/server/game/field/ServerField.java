package org.kareha.hareka.server.game.field;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.field.AbstractTileField;
import org.kareha.hareka.field.Field;
import org.kareha.hareka.field.FieldObject;
import org.kareha.hareka.field.HashTileField;
import org.kareha.hareka.field.ObjectField;
import org.kareha.hareka.field.Region;
import org.kareha.hareka.field.SimpleRegion;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TilePattern;
import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.field.Vectors;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.entity.Entity;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
@XmlJavaTypeAdapter(ServerField.Adapter.class)
public class ServerField implements Field {

	private static final JAXBContext jaxbContext;

	static {
		try {
			jaxbContext = JAXBContext.newInstance(Adapted.class);
		} catch (final JAXBException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public static class Builder {

		@Private
		AbstractTileField tileField;
		@Private
		TilePattern defaultTilePattern = TilePattern.SOLID_NULL_0;
		@Private
		Boundary boundary = NullBoundary.INSTANCE;
		@Private
		int viewSize = 11;
		@Private
		List<Gate> gates;
		@Private
		FieldId downstairs;
		@Private
		FieldId upstairs;

		public Builder tileField(final AbstractTileField v) {
			tileField = v;
			return this;
		}

		public Builder defaultTilePattern(final TilePattern v) {
			defaultTilePattern = v;
			return this;
		}

		public Builder boundary(final Boundary v) {
			boundary = v;
			return this;
		}

		public Builder viewSize(final int v) {
			viewSize = v;
			return this;
		}

		public Builder gates(final List<Gate> v) {
			gates = v;
			return this;
		}

		public Builder downstairs(final FieldId v) {
			downstairs = v;
			return this;
		}

		public Builder upstairs(final FieldId v) {
			upstairs = v;
			return this;
		}

		public ServerField build(final FieldId id) {
			return new ServerField(id, this);
		}

	}

	@Private
	final FieldId id;
	@Private
	volatile Boundary boundary;
	@Private
	final AbstractTileField tileField;
	private final ObjectField objectField;
	@Private
	final Map<Vector, Gate> gates = new ConcurrentHashMap<>();
	@Private
	volatile FieldId downstairs;
	@Private
	volatile FieldId upstairs;

	protected ServerField(final FieldId id, final Builder builder) {
		this.id = id;
		boundary = builder.boundary;
		if (builder.tileField != null) {
			tileField = builder.tileField;
			final List<Region> regions = new ArrayList<>();
			for (final Region region : tileField.getRegions()) {
				if (region instanceof SimpleRegion) {
					regions.add(new ServerSimpleRegion((SimpleRegion) region, this));
				} else {
					regions.add(region);
				}
			}
			tileField.setRegions(regions);
		} else {
			tileField = new HashTileField(true, builder.defaultTilePattern);
		}
		objectField = new ServerObjectField(this, builder.viewSize);
		if (builder.gates != null) {
			for (final Gate gate : builder.gates) {
				gates.put(gate.getSource().getPosition(), gate);
			}
		}
		downstairs = builder.downstairs;
		upstairs = builder.upstairs;
	}

	@XmlRootElement(name = "serverField")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlAttribute
		private FieldId id;
		@XmlElement
		private Boundary boundary;
		@XmlElement
		private AbstractTileField tileField;
		@XmlElement
		private int viewSize;
		@XmlElement
		private List<Gate> gate;
		@XmlElement
		private FieldId downstairs;
		@XmlElement
		private FieldId upstairs;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final ServerField v) {
			id = v.id;
			boundary = v.boundary;
			tileField = v.tileField;
			viewSize = v.getViewSize();
			if (v.gates != null && v.gates.size() > 0) {
				gate = new ArrayList<>(v.gates.values());
			}
			downstairs = v.downstairs;
			upstairs = v.upstairs;
		}

		@Private
		ServerField unmarshal() {
			final Builder builder = new Builder();
			builder.boundary(boundary);
			builder.tileField(tileField);
			builder.viewSize(viewSize);
			builder.gates(gate);
			builder.downstairs(downstairs);
			builder.upstairs(upstairs);
			return builder.build(id);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, ServerField> {

		@Override
		public Adapted marshal(final ServerField v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public ServerField unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public static ServerField load(final File file) throws IOException, JAXBException {
		final Adapted adapted = JaxbUtil.unmarshal(file, jaxbContext);
		return adapted.unmarshal();
	}

	public static ServerField load(final InputStream in) throws IOException, JAXBException {
		final Adapted adapted = JaxbUtil.unmarshal(in, jaxbContext);
		return adapted.unmarshal();
	}

	public void save(final File file) throws JAXBException {
		JaxbUtil.marshal(new Adapted(this), file, jaxbContext);
	}

	@Override
	public Tile getTile(final Vector position) {
		return tileField.getTile(boundary.confine(position));
	}

	public void setTile(final Vector position, final Tile tile, final boolean force) {
		tileField.setTile(boundary.confine(position), tile, force);
	}

	@Override
	public int getViewSize() {
		return objectField.getViewSize();
	}

	@Override
	public boolean addObject(final FieldObject object) {
		return objectField.addObject(object);
	}

	@Override
	public boolean removeObject(final FieldObject object) {
		return objectField.removeObject(object);
	}

	@Override
	public boolean moveObject(final FieldObject object) {
		return objectField.moveObject(object);
	}

	@Override
	public List<FieldObject> getObjects(final Vector position) {
		return objectField.getObjects(position);
	}

	@Override
	public List<FieldObject> getInViewObjects(final Vector center) {
		return objectField.getInViewObjects(center);
	}

	@Override
	public boolean isExclusive(final Vector position) {
		return objectField.isExclusive(position);
	}

	public FieldId getId() {
		return id;
	}

	public Boundary getBoundary() {
		return boundary;
	}

	public void addGate(final Gate gate) {
		gates.put(gate.getSource().getPosition(), gate);
	}

	public void removeGate(final Gate gate) {
		gates.remove(gate.getSource().getPosition());
	}

	public Gate getGate(final Vector position) {
		return gates.get(position);
	}

	public FieldId getDownstairs() {
		return downstairs;
	}

	public void setDownstairs(final FieldId v) {
		downstairs = v;
	}

	public FieldId getUpstairs() {
		return upstairs;
	}

	public void setUpstairs(final FieldId v) {
		upstairs = v;
	}

	public boolean isMutable(final Vector position) {
		return tileField.isMutable(position);
	}

	public List<Region> getRegions() {
		return tileField.getRegions();
	}

	public void setRegions(final Collection<Region> regions) {
		tileField.setRegions(regions);
	}

	public void setDefaultTilePattern(final TilePattern defaultTilePattern) {
		tileField.setPattern(defaultTilePattern);
	}

	private void syncTile(final Vector position) {
		final TilePiece newTilePiece = TilePiece.valueOf(position, getTile(position));

		final List<FieldObject> list = getInViewObjects(position);
		Collections.shuffle(list);
		for (final FieldObject o : list) {
			final Entity entity = ((ServerFieldObject) o).getFieldEntity();
			if (!(entity instanceof CharacterEntity)) {
				continue;
			}
			final CharacterEntity characterEntity = (CharacterEntity) entity;
			characterEntity.handleSetTile(newTilePiece);
		}
	}

	public void setAndSyncTile(final Vector position, final Tile tile, final boolean force) {
		setTile(position, tile, force);
		syncTile(position);
	}

	public void fillRange(final Vector center, final int size, final TilePattern tilePattern, final boolean force) {
		for (final Vector v : Vectors.range(center, size)) {
			if (getTile(v).type().isSpecial()) {
				continue;
			}
			final Tile tile = tilePattern.getTile(v);
			if (tile.type().isSpecial()) {
				continue;
			}
			setAndSyncTile(v, tile, force);
		}
	}

	public void drawRing(final Vector center, final int size, final TilePattern tilePattern, final boolean force) {
		for (final Vector v : Vectors.ring(center, size)) {
			if (getTile(v).type().isSpecial()) {
				continue;
			}
			final Tile tile = tilePattern.getTile(v);
			if (tile.type().isSpecial()) {
				continue;
			}
			setAndSyncTile(v, tile, force);
		}
	}

	public void drawLine(final Vector a, final Vector b, final TilePattern tilePattern, final boolean force) {
		for (final Vector v : Vectors.line(a, b)) {
			if (getTile(v).type().isSpecial()) {
				continue;
			}
			final Tile tile = tilePattern.getTile(v);
			if (tile.type().isSpecial()) {
				continue;
			}
			setAndSyncTile(v, tile, force);
		}
	}

	public void reduceToBackgroundTiles() {
		if (!(tileField instanceof HashTileField)) {
			return;
		}
		((HashTileField) tileField).reduceToBackgroundTiles();
	}

}
