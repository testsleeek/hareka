package org.kareha.hareka.server.game.iparser;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.InternalSession;

public class ExitParser implements Parser<InternalSession> {

	@Override
	public void handle(final PacketInput in, final InternalSession session) {
		final String message = in.readString();

		System.out.println(message); // XXX debug print

		session.close();
	}

}
