package org.kareha.hareka.server.game.stat;

public class LongStatPoints {

	private final long max;
	private final long current;

	public LongStatPoints(final long max, final long current) {
		this.max = max;
		this.current = current;
	}

	public long getMax() {
		return max;
	}

	public long getCurrent() {
		return current;
	}

	public boolean isAlive() {
		return current > 0;
	}

	public boolean isDamaged() {
		return current < max;
	}

	public int getBar() {
		if (max <= 0) {
			return Byte.MAX_VALUE;
		}
		if (current > max) {
			return Byte.MAX_VALUE;
		}
		return (int) (Byte.MAX_VALUE * current / max);
	}

}
