package org.kareha.hareka.server.game.stat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.server.game.Global;
import org.kareha.hareka.server.game.item.Item;

@ThreadSafe
public class NormalItemStat extends AbstractItemStat {

	private static final StatPoints points = new StatPoints(1, 1);

	public static class Builder extends AbstractItemStat.Builder {

		protected long count;

		public Builder() {

		}

		public Builder(final NormalItemStat stat) {
			super(stat);
			synchronized (stat) {
				count = stat.count;
			}
		}

		public Builder count(final long v) {
			count = v;
			return this;
		}

		@Override
		public NormalItemStat build() {
			return new NormalItemStat(this);
		}

	}

	@GuardedBy("this")
	protected long count;

	protected NormalItemStat(final Builder builder) {
		super(builder);
		count = builder.count;
	}

	@XmlType(name = "normalItemStat")
	@XmlAccessorType(XmlAccessType.NONE)
	protected static class Adapted extends AbstractItemStat.Adapted {

		@XmlElement
		protected long count;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final NormalItemStat v) {
			super(v);
			synchronized (v) {
				count = v.count;
			}
		}

		@Override
		protected NormalItemStat unmarshal() {
			final Builder builder = new Builder();
			final Item brand = Global.INSTANCE.context().getItems().get(type);
			builder.brand(brand).shape(shape);
			builder.count(count);
			return builder.build();
		}

	}

	@Override
	protected Adapted marshal() {
		return new Adapted(this);
	}

	@Override
	public StatPoints getHealthPoints() {
		return points;
	}

	@Override
	public StatPoints getMagicPoints() {
		return points;
	}

	@Override
	public synchronized long getCount() {
		return count;
	}

	@Override
	public int getJumpDown() {
		return 127;
	}

	@Override
	public int getJumpUp() {
		return 127;
	}

	@Override
	public synchronized long drainCount() {
		final long c = count;
		count = 0;
		return c;
	}

	@Override
	public synchronized long addCount(final long v) {
		final long nextCount = count + v;
		if (nextCount < 0) {
			if (v >= 0) {
				count = Long.MAX_VALUE;
				return nextCount - Long.MAX_VALUE;
			}
			count = 0;
			return nextCount;
		}
		count = nextCount;
		return 0;
	}

	@Override
	public synchronized long getWeight() {
		final long wt = brand.getWeight() * count;
		if (wt < 0) {
			return Long.MAX_VALUE;
		}
		return wt;
	}

}
