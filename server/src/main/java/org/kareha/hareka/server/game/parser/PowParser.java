package org.kareha.hareka.server.game.parser;

import java.util.logging.Logger;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.pow.PowGrabber;
import org.kareha.hareka.server.game.Session;

public final class PowParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(PowParser.class.getName());

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int handlerId = in.readCompactUInt();
		final byte[] nonce = in.readByteArray();

		final PowGrabber.Entry entry = session.getPowGrabber().get(handlerId);
		if (entry == null) {
			logger.fine(session.getStamp() + "Not found handlerId=" + handlerId);
			return;
		}
		entry.handle(entry.verify(nonce));
	}

}
