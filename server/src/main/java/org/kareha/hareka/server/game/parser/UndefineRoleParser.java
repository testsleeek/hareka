package org.kareha.hareka.server.game.parser;

import java.io.IOException;
import java.util.EnumSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.packet.RoleListPacket;
import org.kareha.hareka.server.game.user.RolesLogRecord;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.Role;
import org.kareha.hareka.user.RoleSet;

public final class UndefineRoleParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(UndefineRoleParser.class.getName());

	private enum BundleKey {
		LoginUserFirst, YouCannotUseThisFunction, InsufficientRank,

		PermissionCannotBeRemoved,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(UndefineRoleParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final String roleId = in.readString();

		// check state
		final User user = session.getUser();
		if (user == null) {
			inform(session, BundleKey.LoginUserFirst.name());
			return;
		}
		final RoleSet roleSet = session.getContext().getAccessController().getRoleSet(user);
		final Role manageRole = roleSet.getHighestRole(Permission.MANAGE_ROLES);
		if (manageRole == null) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			return;
		}
		final Role oldRole = session.getContext().getRoles().get(roleId);
		if (oldRole == null) {
			return;
		}
		if (manageRole.getRank() <= oldRole.getRank()) {
			inform(session, BundleKey.InsufficientRank.name());
			return;
		}
		final Set<Permission> oldPermissions = EnumSet.copyOf(oldRole.getPermissions());
		for (final Permission permission : oldPermissions) {
			final Role myRole = roleSet.getHighestRole(permission);
			if (myRole == null) {
				inform(session, BundleKey.PermissionCannotBeRemoved.name());
				return;
			}
			if (myRole.getRank() <= oldRole.getRank()) {
				inform(session, BundleKey.PermissionCannotBeRemoved.name());
				return;
			}
		}

		// process
		if (session.getContext().getRoles().remove(roleId) != null) {
			try {
				session.getContext().getRolesLogger().add(new RolesLogRecord(user.getId(), roleId));
			} catch (final IOException | JAXBException e) {
				logger.log(Level.SEVERE, "", e);
			}
			session.write(new RoleListPacket(session.getContext().getRoles(), user,
					session.getContext().getAccessController()));
		}
	}

}
