package org.kareha.hareka.server.game;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.function.Supplier;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.Constants;
import org.kareha.hareka.server.Server;
import org.kareha.hareka.server.game.user.InvitationToken;
import org.kareha.hareka.server.game.user.RoleToken;

public final class Starter {

	private static final Logger logger = Logger.getLogger(Starter.class.getName());

	private Starter() {
		throw new AssertionError();
	}

	public static void start(final File dataDirectory, final int port, final boolean noTls,
			final Supplier<char[]> passwordReader) throws IOException, JAXBException {
		// Create context
		final Context context = new Context(Constants.VERSION, dataDirectory);
		Global.INSTANCE.context = context;

		// Show version
		logger.info("Version=" + context.getVersion());

		// Booted or rebooted
		if (context.getRebooter().isRebooted()) {
			logger.info("Rebooting");
		} else {
			logger.info("Booting");
		}

		// Load data
		context.load();

		// Issue initial invitation token
		final InvitationToken invitationToken = context.getInvitationTokens().issueInitial();
		if (invitationToken != null) {
			final String base64 = Base64.getEncoder().encodeToString(invitationToken.getValue());
			logger.info("Initial invitation token: " + base64);
		}

		// Issue initial role token
		final RoleToken roleToken = context.getRoleTokens().issueInitial();
		if (roleToken != null) {
			final String base64 = Base64.getEncoder().encodeToString(roleToken.getValue());
			logger.info("Initial role token (" + roleToken.getRoleId() + "): " + base64);
		}

		// Create internal connector
		final InternalConnector internalConnector = new InternalConnector(context);
		context.setInternalConnector(internalConnector);

		// Create server
		final Server server = new GameServer(context, noTls);
		context.setServer(server);

		// Add shutdown hook
		Runtime.getRuntime().addShutdownHook(new ShutdownHook(context));

		// Start motion
		context.getEntities().startMotion();

		// Start internal connector
		internalConnector.start();

		// Start listening
		server.start(port);
	}

}
