package org.kareha.hareka.server.game.parser;

import java.util.logging.Logger;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.TokenGrabber;

public final class TokenParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(TokenParser.class.getName());

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int handlerId = in.readCompactUInt();
		final byte[] token = in.readByteArray();

		final TokenGrabber.Entry entry = session.getTokenGrabber().get(handlerId);
		if (entry == null) {
			logger.fine(session.getStamp() + "Not found handlerId=" + handlerId);
			return;
		}
		entry.handle(token);
	}

}
