package org.kareha.hareka.server.game.parser;

import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.ChatEntity;
import org.kareha.hareka.server.game.entity.Entity;
import org.kareha.hareka.server.game.entity.EntityId;

public final class PrivateChatParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(PrivateChatParser.class.getName());

	private enum BundleKey {
		LoginCharacterFirst, PeerNotFound,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(PrivateChatParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final LocalEntityId localId = LocalEntityId.readFrom(in);
		final String content = in.readString();

		// check state
		final Player player = session.getPlayer();
		if (player == null) {
			inform(session, BundleKey.LoginCharacterFirst.name());
			logger.fine(session.getStamp() + "Character not logged in");
			return;
		}
		final EntityId id = session.getUser().getChatEntityIdCipher().decrypt(localId);
		if (id == null) {
			inform(session, BundleKey.PeerNotFound.name());
			logger.fine(session.getStamp() + "EntityId not found");
			return;
		}
		final Entity peerEntity = session.getContext().getEntities().get(id);
		if (!(peerEntity instanceof ChatEntity)) {
			inform(session, BundleKey.PeerNotFound.name());
			logger.fine(session.getStamp() + "Not a ChatEntity id=" + id);
			return;
		}
		final ChatEntity peer = (ChatEntity) peerEntity;
		final ChatEntity self = player.getEntity();
		self.sendPrivateChat(peer, content);
	}

}
