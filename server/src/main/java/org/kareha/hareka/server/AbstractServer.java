package org.kareha.hareka.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;

public abstract class AbstractServer implements Server {

	@Private
	static final Logger logger = Logger.getLogger(AbstractServer.class.getName());

	@GuardedBy("this")
	private ServerThread thread;

	protected abstract ServerSocket createServerSocket(int port) throws IOException;

	@Override
	public synchronized void start(final int port) throws IOException {
		if (thread != null) {
			return;
		}
		thread = new ServerThread(createServerSocket(port));
		thread.start();
	}

	@Override
	public synchronized boolean stop(final long millis) throws IOException, InterruptedException {
		if (thread == null) {
			return false;
		}
		thread.interrupt();
		try {
			thread.join(millis);
		} finally {
			thread = null;
		}
		return true;
	}

	protected abstract void accepted(Socket clientSocket);

	private class ServerThread extends Thread {

		private final ServerSocket serverSocket;

		ServerThread(final ServerSocket serverSocket) {
			this.serverSocket = serverSocket;
		}

		@Override
		public void run() {
			try {
				while (!Thread.currentThread().isInterrupted()) {
					final Socket clientSocket;
					try {
						clientSocket = serverSocket.accept();
					} catch (final SocketException e) {
						break;
					} catch (final IOException e) {
						logger.log(Level.SEVERE, "", e);
						break;
					}
					accepted(clientSocket);
				}
			} finally {
				try {
					serverSocket.close();
				} catch (final IOException e) {
					logger.log(Level.SEVERE, "", e);
				}
			}
		}

		@Override
		public void interrupt() {
			try {
				serverSocket.close();
			} catch (final IOException e) {
				logger.log(Level.SEVERE, "", e);
			}
			super.interrupt();
		}

	}

}
