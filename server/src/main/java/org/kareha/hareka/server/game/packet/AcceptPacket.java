package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;

public final class AcceptPacket extends GamePacket {

	public AcceptPacket(final int requestId, final String message) {
		out.writeCompactUInt(requestId);
		out.writeString(message);
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.ACCEPT;
	}

}
