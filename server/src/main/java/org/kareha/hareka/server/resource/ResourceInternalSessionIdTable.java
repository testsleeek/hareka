package org.kareha.hareka.server.resource;

import java.io.File;
import java.util.concurrent.atomic.AtomicLong;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class ResourceInternalSessionIdTable {

	private static final String FILENAME = "InternalSessionId.xml";

	private final File dataDirectory;
	@Private
	final AtomicLong value;

	public ResourceInternalSessionIdTable(final File dataDirectory) {
		this.dataDirectory = dataDirectory;

		final Long v = load();
		if (v == null) {
			value = new AtomicLong();
		} else {
			value = new AtomicLong(v);
		}
	}

	@XmlRootElement(name = "sessionId")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		@Private
		long value;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final ResourceInternalSessionIdTable v) {
			value = v.value.get();
		}

	}

	private File getFile() {
		return new File(dataDirectory, FILENAME);
	}

	private Long load() {
		final File file = getFile();
		if (!file.isFile()) {
			return null;
		}
		final JAXBContext context;
		try {
			context = JAXBContext.newInstance(Adapted.class);
		} catch (final JAXBException e) {
			throw new RuntimeException(e);
		}
		final Adapted adapted;
		try {
			adapted = JaxbUtil.unmarshal(file, context);
		} catch (final JAXBException e) {
			throw new RuntimeException(e);
		}
		return adapted.value;
	}

	public void save() throws JAXBException {
		final JAXBContext context = JAXBContext.newInstance(Adapted.class);
		final Adapted adapted = new Adapted(this);
		final File file = getFile();
		JaxbUtil.marshal(adapted, file, context);
	}

	public ResourceSessionId next() {
		return ResourceSessionId.valueOf(value.getAndIncrement());
	}

}
