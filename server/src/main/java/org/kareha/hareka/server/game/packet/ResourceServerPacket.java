package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.GameServerSettings;
import org.kareha.hareka.server.game.Session;

public final class ResourceServerPacket extends GamePacket {

	public ResourceServerPacket(final GameServerSettings.ConnectionEntry entry, final Session session) {
		out.writeString(entry.getHost());
		out.writeShort(entry.getPort());
		out.writeBoolean(entry.isConnectSecurely());
		out.write(session.getContext().getSessionStatic().toResourceSessionKey(session.getId()));
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.RESOURCE_SERVER;
	}

}
