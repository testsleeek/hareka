package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.entity.ChatEntity;
import org.kareha.hareka.server.game.user.IdCipher;

public final class PrivateChatEchoPacket extends GamePacket {

	public PrivateChatEchoPacket(final ChatEntity peer, final String echo, final IdCipher chatEntityIdCipher,
			final String language) {
		out.write(chatEntityIdCipher.encrypt(peer.getId()));
		out.writeString(peer.getName(language));
		out.writeString(echo);
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.PRIVATE_CHAT_ECHO;
	}

}
