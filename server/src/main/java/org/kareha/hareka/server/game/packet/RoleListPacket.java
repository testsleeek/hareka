package org.kareha.hareka.server.game.packet;

import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.user.AccessController;
import org.kareha.hareka.server.game.user.Roles;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Role;

public final class RoleListPacket extends GamePacket {

	public RoleListPacket(final Roles roles, final User user, final AccessController accessController) {
		final int rank = accessController.getRoleSet(user).getHighestRank();
		final List<Role> list = new ArrayList<>();
		for (final Role role : roles.getRoles()) {
			if (role.getRank() <= rank) {
				list.add(role);
			}
		}
		out.writeCompactUInt(list.size());
		for (final Role role : list) {
			out.write(role);
		}
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.ROLE_LIST;
	}

}
