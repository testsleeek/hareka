package org.kareha.hareka.server.game.user;

import java.io.File;
import java.io.IOException;
import java.security.Key;
import java.security.PublicKey;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.key.KeyId;
import org.kareha.hareka.key.KeyStatic;
import org.kareha.hareka.persistent.PersistentHashTable;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.util.FileUtil;

public class Users {

	private enum BundleKey {
		DuplicatedUserLogin, LogoutUserFirst,
	}

	private static final Logger logger = Logger.getLogger(Users.class.getName());

	private final File directory;
	private final KeyStatic idTable;
	private final PersistentHashTable<Key, KeyId> keyIndexTable;
	@GuardedBy("this")
	private final Map<KeyId, User> loggedInUsers = new HashMap<>();

	public Users(final File directory, final KeyStatic idTable, final PersistentHashTable<Key, KeyId> keyIndexTable)
			throws IOException {
		this.directory = directory;
		this.idTable = idTable;
		this.keyIndexTable = keyIndexTable;
		FileUtil.ensureDirectoryExists(directory);
	}

	private File getDirectory(final KeyId id) {
		return new File(directory, id.toString());
	}

	private boolean exists(final KeyId id) {
		final File d = getDirectory(id);
		return d.exists();
	}

	private KeyId nextId() {
		KeyId id = idTable.next();
		while (exists(id)) {
			logger.severe(MessageFormat.format("ID {0} already exists", id));
			id = idTable.next();
		}
		return id;
	}

	public synchronized User createUser(final KeyId parentId, final PublicKey publicKey) {
		try {
			if (keyIndexTable.get(publicKey) != null) {
				return null;
			}
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
			return null;
		}
		final KeyId id = nextId();
		final File d = getDirectory(id);
		if (d.exists()) {
			return null;
		}
		if (!d.mkdirs()) {
			return null;
		}

		final User a = new User(id, publicKey, parentId);
		try {
			a.save(d);
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
			return null;
		} catch (final JAXBException e) {
			logger.log(Level.SEVERE, "", e);
			return null;
		}
		try {
			keyIndexTable.put(publicKey, id);
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
			return null;
		}

		if (parentId != null) {
			final User tmp = getLoggedInUser(parentId);
			final User parent;
			if (tmp == null) {
				parent = load(parentId);
			} else {
				parent = tmp;
			}
			if (parent != null) {
				parent.addChildId(a.getId());
				if (tmp == null) {
					save(parent);
				}
			}
		}

		return a;
	}

	@GuardedBy("this")
	protected User load(final KeyId id) {
		final File d = getDirectory(id);
		if (!d.isDirectory()) {
			return null;
		}
		final User user;
		try {
			user = User.load(d);
		} catch (final JAXBException e) {
			logger.log(Level.SEVERE, "", e);
			return null;
		}
		return user;
	}

	@GuardedBy("this")
	protected void save(final User user) {
		final File d = getDirectory(user.getId());
		try {
			user.save(d);
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
			return;
		} catch (final JAXBException e) {
			logger.log(Level.SEVERE, "", e);
			return;
		}
	}

	public synchronized User login(final PublicKey publicKey, final Session session) {
		final KeyId id;
		try {
			id = keyIndexTable.get(publicKey);
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
			return null;
		}
		if (id == null) {
			return null;
		}
		final User user = load(id);
		if (user == null) {
			return null;
		}
		if (!publicKey.equals(user.getPublicKey())) {
			return null;
		}
		final User loggedInUser = loggedInUsers.get(id);
		if (loggedInUser != null) {
			final Session anotherSession = loggedInUser.getSession();
			if (anotherSession != null) {
				final ResourceBundle peerBundle = anotherSession.getBundle(Users.class.getName());
				anotherSession.kick(peerBundle.getString(BundleKey.DuplicatedUserLogin.name()));
			}
			final ResourceBundle bundle = session.getBundle(Users.class.getName());
			session.kick(bundle.getString(BundleKey.DuplicatedUserLogin.name()));
			return null;
		}
		if (session.getUser() != null) {
			session.kick("User logout first");
			return null;
		}
		session.setUser(user);
		user.setSession(session);
		loggedInUsers.put(id, user);
		return user;
	}

	public synchronized boolean logout(final User user) {
		final User loggedInUser = loggedInUsers.remove(user.getId());
		if (loggedInUser == null) {
			return false;
		}
		save(loggedInUser);
		final Session session = loggedInUser.getSession();
		if (session == null) {
			logger.warning(MessageFormat.format("Logging out user {0} has no session", loggedInUser.getId()));
			return true;
		}
		if (session.getUser() == null) {
			logger.warning(MessageFormat.format("Logging out session from {0} has no user", session.getInetAddress()));
		}
		session.setUser(null);
		loggedInUser.setSession(null);
		return true;
	}

	public synchronized User getLoggedInUser(final KeyId id) {
		return loggedInUsers.get(id);
	}

}
