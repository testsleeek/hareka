package org.kareha.hareka.server.game.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;

@Immutable
@XmlJavaTypeAdapter(FieldId.Adapter.class)
public class FieldId {

	@Private
	final long value;

	private FieldId(final long value) {
		this.value = value;
	}

	public static FieldId valueOf(final long value) {
		return new FieldId(value);
	}

	public static FieldId valueOf(final String s) {
		// Long.parseLong throws NumberFormatException
		final long value = Long.parseLong(s, 16);
		return valueOf(value);
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof FieldId)) {
			return false;
		}
		final FieldId id = (FieldId) obj;
		return id.value == value;
	}

	@Override
	public int hashCode() {
		return (int) (value >>> 32 ^ value);
	}

	@Override
	public String toString() {
		return Long.toHexString(value);
	}

	@XmlType(name = "fieldId")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlValue
		private String value;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final FieldId v) {
			value = v.toString();
		}

		@Private
		FieldId unmarshal() {
			return valueOf(value);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, FieldId> {

		@Override
		public Adapted marshal(final FieldId v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public FieldId unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public long value() {
		return value;
	}

}
