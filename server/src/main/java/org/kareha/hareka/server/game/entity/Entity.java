package org.kareha.hareka.server.game.entity;

public interface Entity {

	EntityId getId();

	boolean isPersistent();

	void setPersistent(boolean persistent);

}
