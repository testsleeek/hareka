package org.kareha.hareka.server.game.stat;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.util.FileUtil;
import org.kareha.hareka.util.JaxbUtil;

public class SpeciesTable {

	private static final Logger logger = Logger.getLogger(SpeciesTable.class.getName());
	private static final String resourceDirectory = "org/kareha/hareka/server/game/resource/species";

	private final File directory;
	private final Map<String, Species> map = new ConcurrentHashMap<>();

	public SpeciesTable(final File directory) throws IOException {
		this.directory = directory;
		FileUtil.ensureDirectoryExists(directory);
	}

	public void load() throws IOException, JAXBException {
		map.clear();
		if (!directory.isDirectory()) {
			logger.severe("Cannot find directory: " + directory);
			return;
		}

		final File[] list = directory.listFiles();
		if (list == null) {
			logger.severe("Cannot list directory: " + directory);
			return;
		}
		int count = 0;
		final JAXBContext raceJaxbContext;
		try {
			raceJaxbContext = JAXBContext.newInstance(AbstractSpecies.Adapted.class);
		} catch (final JAXBException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		for (final File file : list) {
			if (!file.isFile()) {
				continue;
			}
			final AbstractSpecies species;
			try (final InputStream in = new BufferedInputStream(new FileInputStream(file))) {
				final AbstractSpecies.Adapted adapted = JaxbUtil.unmarshal(in, raceJaxbContext);
				species = adapted.unmarshal();
			}
			map.put(species.getId(), species);
			count++;
		}
		if (count < 1) {
			loadFromResource();
		}
	}

	private File getFile(final String id) {
		return new File(directory, id + ".xml");
	}

	public void save() throws JAXBException {
		final JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(AbstractSpecies.Adapted.class);
		} catch (final JAXBException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		for (final Species species : map.values()) {
			if (species instanceof AbstractSpecies) {
				final AbstractSpecies as = (AbstractSpecies) species;
				final File file = getFile(species.getId());
				JaxbUtil.marshal(as.marshal(), file, jaxbContext);
			}
		}
	}

	private void loadFromResource() throws IOException, JAXBException {
		final JAXBContext listJaxbContext;
		try {
			listJaxbContext = JAXBContext.newInstance(ListXml.class);
		} catch (final JAXBException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		final String listPath = resourceDirectory + "/" + "list.xml";
		final List<String> list;
		try (final InputStream in = getClass().getClassLoader().getResourceAsStream(listPath)) {
			final ListXml listXml = JaxbUtil.unmarshal(in, listJaxbContext);
			list = listXml.item;
		}

		final JAXBContext raceJaxbContext;
		try {
			raceJaxbContext = JAXBContext.newInstance(AbstractSpecies.Adapted.class);
		} catch (final JAXBException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		map.clear();
		for (final String id : list) {
			final String racePath = resourceDirectory + "/" + id + ".xml";
			try (final InputStream in = getClass().getClassLoader().getResourceAsStream(racePath)) {
				final AbstractSpecies.Adapted adapted = JaxbUtil.unmarshal(in, raceJaxbContext);
				map.put(id, adapted.unmarshal());
			}
		}
	}

	@XmlRootElement(name = "list")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class ListXml {

		@XmlElement
		@Private
		List<String> item;

		private ListXml() {
			// used by JAXB
		}

	}

	public Species getSpecies(final String id) {
		return map.get(id);
	}

	public Collection<String> getIds() {
		return Collections.unmodifiableSet(map.keySet());
	}

}
