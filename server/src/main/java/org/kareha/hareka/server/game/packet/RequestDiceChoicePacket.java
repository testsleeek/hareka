package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.DiceRoll;
import org.kareha.hareka.server.game.GamePacket;

public final class RequestDiceChoicePacket extends GamePacket {

	public RequestDiceChoicePacket(final DiceRoll diceRoll) {
		out.writeCompactUInt(diceRoll.getId());
		final byte[] hash = diceRoll.getHash();
		out.writeString(diceRoll.getHashAlgorithm());
		out.writeByteArray(hash);
		out.writeCompactUInt(diceRoll.getFaces());
		out.writeCompactUInt(diceRoll.getRate());
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.REQUEST_DICE_CHOICE;
	}

}
