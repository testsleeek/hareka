package org.kareha.hareka.server;

import org.kareha.hareka.packet.PacketType;

public enum ResourceInternalServerPacketType implements PacketType {

	KEEP_ALIVE,

	ADD_KEYS, REMOVE_KEYS,

	;

	@Override
	public int typeValue() {
		return ordinal();
	}

}
