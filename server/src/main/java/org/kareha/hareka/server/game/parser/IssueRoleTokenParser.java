package org.kareha.hareka.server.game.parser;

import java.util.Base64;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.packet.TextPacket;
import org.kareha.hareka.server.game.user.RoleToken;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.Role;
import org.kareha.hareka.user.RoleSet;

public final class IssueRoleTokenParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(IssueRoleTokenParser.class.getName());

	private enum BundleKey {
		LoginUserFirst, YouCannotUseThisFunction, InvalidRoleId, InsufficientRank,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(IssueRoleTokenParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final String roleId = in.readString();

		// check state
		final User user = session.getUser();
		if (user == null) {
			inform(session, BundleKey.LoginUserFirst.name());
			logger.fine(session.getStamp() + "User not logged in");
			return;
		}
		final RoleSet roles = session.getContext().getAccessController().getRoleSet(user);
		final Role role = roles.getHighestRole(Permission.ISSUE_ROLE_TOKENS);
		if (role == null) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot use issue role token function");
			return;
		}
		final Role targetRole = session.getContext().getRoles().get(roleId);
		if (targetRole == null) {
			inform(session, BundleKey.InvalidRoleId.name());
			logger.fine(session.getStamp() + "Invalid role ID");
			return;
		}
		if (targetRole.getRank() >= role.getRank()) {
			inform(session, BundleKey.InsufficientRank.name());
			logger.fine(session.getStamp() + "Insufficient rank");
			return;
		}

		// process
		final RoleToken token = session.getContext().getRoleTokens().issue(user, roleId);
		final String base64 = Base64.getEncoder().encodeToString(token.getValue());
		session.write(new TextPacket("Role token (" + token.getRoleId() + "): " + base64 + "\n"));
	}

}
