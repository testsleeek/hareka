package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.entity.ChatEntity;
import org.kareha.hareka.server.game.user.IdCipher;

public final class SelfChatEntityPacket extends GamePacket {

	public SelfChatEntityPacket(final ChatEntity entity, final IdCipher chatEntityIdCipher, final String language) {
		out.write(chatEntityIdCipher.encrypt(entity.getId()));
		out.writeString(entity.getName(language));
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.SELF_CHAT_ENTITY;
	}

}
