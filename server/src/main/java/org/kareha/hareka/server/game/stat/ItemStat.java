package org.kareha.hareka.server.game.stat;

import org.kareha.hareka.server.game.item.Item;

public interface ItemStat extends Stat {

	Item getBrand();

	long drainCount();

	long addCount(long v);

	long getWeight();

}
