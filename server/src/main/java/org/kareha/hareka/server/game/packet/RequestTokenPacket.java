package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;

public final class RequestTokenPacket extends GamePacket {

	public RequestTokenPacket(final int handlerId, final String message) {
		out.writeCompactUInt(handlerId);
		out.writeString(message);
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.REQUEST_TOKEN;
	}

}
