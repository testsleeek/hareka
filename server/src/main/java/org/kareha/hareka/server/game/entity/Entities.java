package org.kareha.hareka.server.game.entity;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.Transformation;
import org.kareha.hareka.server.game.Global;
import org.kareha.hareka.server.game.field.FieldId;
import org.kareha.hareka.server.game.field.Fields;
import org.kareha.hareka.server.game.stat.NormalCharacterStat;
import org.kareha.hareka.server.game.stat.Species;
import org.kareha.hareka.util.FileUtil;
import org.kareha.hareka.util.Name;

public class Entities {

	private static final Logger logger = Logger.getLogger(Entities.class.getName());

	private final File directory;
	private final EntityStatic idTable;
	private final Fields fieldTable;
	private final Map<EntityId, Entity> map = new ConcurrentHashMap<>();

	public Entities(final File directory, final EntityStatic idTable, final Fields fieldTable) throws IOException {
		this.directory = directory;
		this.idTable = idTable;
		this.fieldTable = fieldTable;
		FileUtil.ensureDirectoryExists(directory);
	}

	private File getFile(final EntityId id) {
		return new File(directory, id + ".xml");
	}

	public void load() throws IOException, JAXBException {
		map.clear();
		if (!directory.isDirectory()) {
			logger.severe("Cannot find directory: " + directory);
			return;
		}

		final File[] list = directory.listFiles();
		if (list == null) {
			logger.severe("Cannot list directory: " + directory);
			return;
		}
		for (final File file : list) {
			if (!file.isFile()) {
				continue;
			}
			final AbstractEntity entity = AbstractEntity.load(file);
			if (entity.isMounted()) {
				if (!entity.mount(fieldTable)) {
					logger.warning(MessageFormat.format("Failed to mount: {0}", entity.getId()));
				}
			}
			map.put(entity.getId(), entity);
		}
	}

	public void save() throws JAXBException {
		for (final Entity entity : map.values()) {
			if (entity instanceof AbstractEntity) {
				final AbstractEntity ae = (AbstractEntity) entity;
				ae.helperUnmount();
				final File file = getFile(entity.getId());
				ae.save(file);
			}
		}
	}

	private EntityId nextId() {
		EntityId id = idTable.next();
		while (map.containsKey(id)) {
			logger.severe(MessageFormat.format("ID {0} already exists", id));
			id = idTable.next();
		}
		return id;
	}

	public Entity get(final EntityId id) {
		return map.get(id);
	}

	public ItemEntity createItemEntity(final ItemEntity.Builder builder) {
		final ItemEntity itemEntity = builder.build(nextId());
		// itemEntity.mount(fieldTable);
		map.put(itemEntity.getId(), itemEntity);
		return itemEntity;
	}

	public CharacterEntity createCharacterEntity(final CharacterEntity.Builder builder) {
		final CharacterEntity characterEntity = builder.build(nextId());
		// characterEntity.mount(fieldTable);
		map.put(characterEntity.getId(), characterEntity);
		return characterEntity;
	}

	public CharacterEntity createSoulEntity(final FieldId fieldId, final Placement placement) {
		final String raceId = "Soul";
		final String shape = "Soul";
		final Species race = Global.INSTANCE.context().getSpeciesTable().getSpecies(raceId);
		final Name name = race.getName();
		final CharacterEntity.Builder builder = new CharacterEntity.Builder();
		builder.name(name).shape(shape);
		final NormalCharacterStat.Builder statBuilder = new NormalCharacterStat.Builder();
		statBuilder.species(race).shape(shape);
		builder.stat(statBuilder.build());
		builder.fieldId(fieldId);
		builder.placement(placement);
		builder.transformation(Transformation.random(placement.getPosition()));
		final CharacterEntity entity = createCharacterEntity(builder);
		return entity;
	}

	public boolean destroyEntity(final Entity entity) {
		final Entity exists = map.remove(entity.getId());
		if (exists == null) {
			return false;
		}
		if (!exists.isPersistent()) {
			return true;
		}
		final File file = getFile(entity.getId());
		if (!file.delete()) {
			logger.severe("Cannot delete file=" + file);
		}
		return true;
	}

	public void startMotion() {
		for (final Entity entity : map.values()) {
			if (entity instanceof FieldEntity) {
				final FieldEntity fe = (FieldEntity) entity;
				if (!fe.getStat().getHealthPoints().isAlive()) {
					continue;
				}
			}
			if (entity instanceof AbstractEntity) {
				final AbstractEntity ae = (AbstractEntity) entity;
				ae.startMotion();
				ae.startHealthRegeneration();
				ae.startMagicRegeneration();
			}
		}
	}

	public void stopMotion() {
		for (final Entity entity : map.values()) {
			if (entity instanceof AbstractEntity) {
				final AbstractEntity ae = (AbstractEntity) entity;
				ae.stopMotion();
			}
		}
	}

	public void removeOrphans() {
		for (final Entity entity : map.values()) {
			if (entity instanceof CharacterEntity) {
				final CharacterEntity ce = (CharacterEntity) entity;
				if (!ce.getOwners().isEmpty()) {
					continue;
				}
			}
			if (entity instanceof ItemEntity) {
				final ItemEntity ie = (ItemEntity) entity;
				if (!ie.isMounted()) {
					continue;
				}
			}
			if (entity instanceof FieldEntity) {
				final FieldEntity fe = (FieldEntity) entity;
				fe.unmount();
			}
			destroyEntity(entity);
		}
	}

}
