package org.kareha.hareka.server.game.packet;

import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.user.AccessController;
import org.kareha.hareka.server.game.user.IdCipher;
import org.kareha.hareka.server.game.user.RoleToken;
import org.kareha.hareka.server.game.user.RoleTokens;
import org.kareha.hareka.server.game.user.Roles;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.Role;

public final class RoleTokenListPacket extends GamePacket {

	public RoleTokenListPacket(final RoleTokens roleTokens, final User user, final AccessController accessController,
			final Roles roles, final IdCipher userIdCipher) {
		final int rank;
		final Role mrole = accessController.getRoleSet(user).getHighestRole(Permission.MANAGE_ROLE_TOKENS);
		if (mrole == null) {
			rank = 0;
		} else {
			rank = mrole.getRank();
		}
		final List<RoleToken> list = new ArrayList<>();
		for (final RoleToken token : roleTokens.getRoleTokens()) {
			if (token.getIssuer().equals(user.getId())) {
				list.add(token);
				continue;
			}
			final Role role = roles.get(token.getRoleId());
			if (role != null && role.getRank() <= rank) {
				list.add(token);
				continue;

			}
		}
		out.writeCompactUInt(list.size());
		for (final RoleToken token : list) {
			out.writeCompactULong(token.getId());
			out.writeCompactLong(token.getIssuedDate());
			out.write(userIdCipher.encrypt(token.getIssuer()));
			out.writeString(token.getRoleId());
		}
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.ROLE_TOKEN_LIST;
	}

}
