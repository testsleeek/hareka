package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.field.TileType;
import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.entity.ItemEntity;
import org.kareha.hareka.server.game.item.Item;
import org.kareha.hareka.server.game.user.IdCipher;

public final class AddItemPacket extends GamePacket {

	public AddItemPacket(final ItemEntity itemEntity, final IdCipher itemEntityIdCipher, final String language,
			final CharacterEntity entity) {
		out.write(itemEntityIdCipher.encrypt(itemEntity.getId()));
		out.writeString(itemEntity.getShape());
		out.writeCompactULong(itemEntity.getStat().getCount());
		final Item item = itemEntity.getStat().getBrand();
		out.write(item.getType());
		out.writeString(item.getName().get(language));
		out.writeCompactUInt(item.getWeight());
		out.writeCompactUInt(item.getReach());
		out.write(item.getWaitType());
		out.writeCompactUInt(item.getWait(entity, TileType.NULL));
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.ADD_ITEM;
	}

}
