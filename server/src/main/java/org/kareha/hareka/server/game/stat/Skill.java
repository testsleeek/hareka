package org.kareha.hareka.server.game.stat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.PackagePrivate;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
@XmlJavaTypeAdapter(Skill.Adapter.class)
public class Skill {

	public static final long MAX_VALUE = (1L << 23) - 1;

	@GuardedBy("this")
	@PackagePrivate
	long value;

	public Skill() {

	}

	public Skill(final long value) {
		this.value = value;
	}

	@XmlType(name = "skill")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlValue
		private long value;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Skill v) {
			synchronized (v) {
				value = v.value;
			}
		}

		@Private
		Skill unmarshal() {
			return new Skill(value);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, Skill> {

		@Override
		public Adapted marshal(final Skill v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public Skill unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public synchronized long getValue() {
		return value;
	}

	public synchronized void addValue(final long v) {
		final long newValue = value + v;
		if (newValue < 0) {
			value = 0;
		} else if (newValue > MAX_VALUE) {
			value = MAX_VALUE;
		} else {
			value = newValue;
		}
	}

}
