package org.kareha.hareka.server.game;

import java.net.InetAddress;
import java.net.Socket;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.game.Power;
import org.kareha.hareka.key.KeyGrabber;
import org.kareha.hareka.packet.PacketSocket;
import org.kareha.hareka.packet.ParserTable;
import org.kareha.hareka.packet.ServerPacketType;
import org.kareha.hareka.pow.PowGrabber;
import org.kareha.hareka.server.game.ipacket.RemoveKeysPacket;
import org.kareha.hareka.server.game.packet.AcceptPacket;
import org.kareha.hareka.server.game.packet.ExitPacket;
import org.kareha.hareka.server.game.packet.InformPacket;
import org.kareha.hareka.server.game.packet.RejectPacket;
import org.kareha.hareka.server.game.packet.RequestDiceTokenPacket;
import org.kareha.hareka.server.game.packet.VersionPacket;
import org.kareha.hareka.server.game.user.User;

public class Session {

	private static final Logger logger = Logger.getLogger(Session.class.getName());

	private volatile PacketSocket<ServerPacketType, Session> packetSocket;
	@Private
	final Context context;
	private final SessionId id;
	@GuardedBy("this")
	private Locale locale = Locale.US;
	private final KeyGrabber keyGrabber = new KeyGrabber();
	@GuardedBy("this")
	private TokenGrabber tokenGrabber;
	@GuardedBy("this")
	private PowGrabber powGrabber;
	@GuardedBy("this")
	private User user;
	@GuardedBy("this")
	private Player player;
	private final DiceRollTable diceRollTable = new DiceRollTable(GameServerConstants.DICE_ROLL_HASH_ALGORITHM);

	// not private for test
	Session(final Context context) {
		this.context = context;
		id = context.getSessionStatic().next();
	}

	// not private for test
	PacketSocket<ServerPacketType, Session> newPacketSocket(final Socket socket,
			final ParserTable<ServerPacketType, Session> parserTable) {
		return new PacketSocket<ServerPacketType, Session>(socket, parserTable, this) {
			@Override
			public void finish() {
				logoutUser();
				context.getSessions().removeSession(Session.this);
			}
		};
	}

	// not private for test
	void initialize(final Socket socket, final ParserTable<ServerPacketType, Session> parserTable) {
		packetSocket = newPacketSocket(socket, parserTable);
	}

	public static Session newInstance(final Context context, final Socket socket,
			final ParserTable<ServerPacketType, Session> parserTable) {
		final Session session = new Session(context);
		session.initialize(socket, parserTable);
		return session;
	}

	public void logoutCharacter() {
		unsetSelf();
	}

	public void logoutUser() {
		logoutCharacter();
		final User a = getUser();
		if (a != null) {
			context.getUsers().logout(a);

			final InternalSession internalSession = context.getInternalConnector().getSession();
			if (internalSession != null) {
				internalSession.write(new RemoveKeysPacket(this));
			} else {
				logger.info("Resource server is not connected");
			}
		}
	}

	public void start() {
		packetSocket.start();
	}

	public void close() {
		packetSocket.close();
	}

	public void write(final GamePacket packet) {
		packetSocket.write(packet);
	}

	public InetAddress getInetAddress() {
		return packetSocket.getInetAddress();
	}

	public Context getContext() {
		return context;
	}

	public SessionId getId() {
		return id;
	}

	public String getStamp() {
		final StringBuilder sb = new StringBuilder();
		sb.append("[");
		final User user = getUser();
		if (user != null) {
			sb.append("User=" + user.getId() + " ");
		}
		sb.append("Session=" + id + " ");
		sb.append("Address=" + packetSocket.getInetAddress());
		sb.append("] ");
		return sb.toString();
	}

	public synchronized Locale getLocale() {
		return locale;
	}

	public synchronized void setLocale(final Locale locale) {
		this.locale = locale;
	}

	public ResourceBundle getBundle(final String baseName) {
		return ResourceBundle.getBundle(baseName, getLocale());
	}

	public KeyGrabber getKeyGrabber() {
		return keyGrabber;
	}

	public synchronized TokenGrabber getTokenGrabber() {
		if (tokenGrabber == null) {
			tokenGrabber = new TokenGrabber();
		}
		return tokenGrabber;
	}

	public synchronized PowGrabber getPowGrabber() {
		if (powGrabber == null) {
			powGrabber = new PowGrabber(GameServerConstants.POW_GRABBER_CAPACITY);
		}
		return powGrabber;
	}

	public synchronized User getUser() {
		return user;
	}

	public synchronized void setUser(final User user) {
		this.user = user;
	}

	public synchronized Player getPlayer() {
		return player;
	}

	public boolean setPlayer(final Player player) {
		synchronized (this) {
			if (this.player != null) {
				return false;
			}
			// player.getEntity().stopMotion();
			player.getEntity().addChatDriver(player);
			player.getEntity().addFieldDriver(player);
			this.player = player;
		}
		logger.info(getStamp() + "Login EntityId=" + player.getEntity().getId() + " Name="
				+ player.getEntity().getName(getLocale().getLanguage()));
		return true;
	}

	public boolean unsetSelf() {
		final Player backupPlayer;
		synchronized (this) {
			if (player == null) {
				return false;
			}
			player.getEntity().removeChatDriver(player);
			player.getEntity().removeFieldDriver(player);
			if (player.getEntity().isFieldDriversEmpty()) {
				if (player.getEntity().getStat().hasPower(Power.VOLATILE)) {
					player.getEntity().unmount();
				} else {
					if (player.getEntity().getStat().getHealthPoints().isAlive()) {
						player.getEntity().startMotion();
					}
				}
			}
			backupPlayer = player;
			player = null;
		}
		logger.info(getStamp() + "Logout EntityId=" + backupPlayer.getEntity().getId() + " Name="
				+ backupPlayer.getEntity().getName(getLocale().getLanguage()));
		return true;
	}

	public DiceRollTable getDiceRollTable() {
		return diceRollTable;
	}

	public void rollDice(final int faces, final int rate) {
		final DiceRoll diceRoll = diceRollTable.initiate(faces, rate);
		write(new RequestDiceTokenPacket(diceRoll));
	}

	public void writeVersionPacket() {
		write(new VersionPacket(getContext().getVersion()));
	}

	public void writeExitPacket(final String pattern, final Object... arguments) {
		final String message = MessageFormat.format(pattern, arguments);
		write(new ExitPacket(message));
	}

	public void kick(final String pattern, final Object... arguments) {
		writeExitPacket(pattern, arguments);
		close();
	}

	public void writeInformPacket(final String pattern, final Object... arguments) {
		final String message = MessageFormat.format(pattern, arguments);
		write(new InformPacket(message));
	}

	public void writeRejectPacket(final int requestId, final String pattern, final Object... arguments) {
		final String message = MessageFormat.format(pattern, arguments);
		write(new RejectPacket(requestId, message));
	}

	public void writeAcceptPacket(final int requestId, final String pattern, final Object... arguments) {
		final String message = MessageFormat.format(pattern, arguments);
		write(new AcceptPacket(requestId, message));
	}

}
