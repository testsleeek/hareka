package org.kareha.hareka.server.game;

import java.net.Socket;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.packet.PacketSocket;
import org.kareha.hareka.packet.ParserTable;
import org.kareha.hareka.server.ResourceInternalClientPacketType;
import org.kareha.hareka.server.game.ipacket.KeepAlivePacket;

public class InternalSession implements AutoCloseable {

	private volatile String host;
	private volatile PacketSocket<ResourceInternalClientPacketType, InternalSession> packetSocket;
	private final Context context;
	@GuardedBy("this")
	private ScheduledFuture<?> keepAliveFuture;

	private InternalSession(final Context context) {
		this.context = context;
	}

	PacketSocket<ResourceInternalClientPacketType, InternalSession> newPacketSocket(final Socket socket,
			final ParserTable<ResourceInternalClientPacketType, InternalSession> parserTable) {
		return new PacketSocket<ResourceInternalClientPacketType, InternalSession>(socket, parserTable, this) {
			@Override
			public void finish() {
				// currently, nothing to do
			}
		};
	}

	private void initialize(final Socket socket,
			final ParserTable<ResourceInternalClientPacketType, InternalSession> parserTable) {
		packetSocket = newPacketSocket(socket, parserTable);
	}

	public static InternalSession newInstance(final Context context, final String host, final Socket socket,
			final ParserTable<ResourceInternalClientPacketType, InternalSession> parserTable) {
		final InternalSession session = new InternalSession(context);
		session.host = host;
		session.initialize(socket, parserTable);
		return session;
	}

	public void start() {
		packetSocket.start();
		synchronized (this) {
			keepAliveFuture = Global.INSTANCE.scheduledExecutor().scheduleWithFixedDelay(new Runnable() {
				@Override
				public void run() {
					write(new KeepAlivePacket());
				}
			}, 1, 1, TimeUnit.MINUTES);
		}
	}

	@Override
	public void close() {
		synchronized (this) {
			if (keepAliveFuture != null) {
				keepAliveFuture.cancel(false);
			}
		}
		packetSocket.close();
	}

	public boolean isDisconnected() {
		return packetSocket.isDisconnected();
	}

	public String getHost() {
		return host;
	}

	public void write(final GameInternalPacket packet) {
		packetSocket.write(packet);
	}

	public Context getContext() {
		return context;
	}

}
