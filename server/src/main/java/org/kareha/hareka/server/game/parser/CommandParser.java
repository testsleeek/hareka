package org.kareha.hareka.server.game.parser;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.packet.CommandOutPacket;

public final class CommandParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final int length = in.readCompactUInt();
		final String[] args = new String[length];
		for (int i = 0; i < length; i++) {
			args[i] = in.readString();
		}

		session.write(new CommandOutPacket("hello\n"));
		// TODO handle command
	}

}
