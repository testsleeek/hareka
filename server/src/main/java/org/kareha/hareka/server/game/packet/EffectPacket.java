package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.field.Transformation;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.field.Boundary;

public final class EffectPacket extends GamePacket {

	public EffectPacket(final String effectId, final Vector origin, final Vector target,
			final Transformation transformation, final Vector position, final Boundary boundary) {
		out.writeString(effectId);
		out.write(boundary.confine(origin, position).transform(transformation));
		out.write(boundary.confine(target, position).transform(transformation));
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.EFFECT;
	}

}
