package org.kareha.hareka.server.game;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.kareha.hareka.annotation.GuardedBy;

public class Rebooter {

	private static final Logger logger = Logger.getLogger(Rebooter.class.getName());

	private final File file;
	private final boolean rebooted;
	@GuardedBy("this")
	private boolean rebooting;
	@GuardedBy("this")
	private boolean shutdownStarted;

	public Rebooter(final File file) {
		this.file = file;
		rebooted = file.exists();
		if (rebooted) {
			if (!file.delete()) {
				logger.warning("Cannot delete " + file);
			}
		}
	}

	public boolean isRebooted() {
		return rebooted;
	}

	private void shutdownHelper() {
		new Thread() {
			@Override
			public void run() {
				// exit never returns
				System.exit(0);
			}
		}.start();
	}

	public boolean shutdown() {
		synchronized (this) {
			if (shutdownStarted) {
				return false;
			}
			if (file.exists()) {
				if (!file.delete()) {
					logger.warning("Cannot delete " + file);
				}
			}
			rebooting = false;
			shutdownStarted = true;
		}
		shutdownHelper();
		return true;
	}

	public boolean reboot() {
		synchronized (this) {
			if (shutdownStarted) {
				return false;
			}
			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (final IOException e) {
					logger.log(Level.WARNING, "", e);
				}
			}
			rebooting = true;
			shutdownStarted = true;
		}
		shutdownHelper();
		return true;
	}

	public synchronized boolean isRebooting() {
		return rebooting;
	}

}
