package org.kareha.hareka.server.game;

import java.util.concurrent.ScheduledThreadPoolExecutor;

public enum Global {

	INSTANCE;

	private final ScheduledThreadPoolExecutor scheduledExecutor = new ScheduledThreadPoolExecutor(8);
	volatile Context context;

	public ScheduledThreadPoolExecutor scheduledExecutor() {
		return scheduledExecutor;
	}

	public Context context() {
		return context;
	}

}
