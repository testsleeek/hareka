package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;

public final class TeleportPacket extends GamePacket {

	public TeleportPacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.TELEPORT;
	}

}
