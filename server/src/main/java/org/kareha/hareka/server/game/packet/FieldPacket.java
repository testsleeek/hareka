package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.field.Field;
import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;

public final class FieldPacket extends GamePacket {

	public FieldPacket(final Field field) {
		out.writeCompactUInt(field.getViewSize());
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.FIELD;
	}

}
