package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.entity.ChatEntity;
import org.kareha.hareka.server.game.entity.FieldEntity;
import org.kareha.hareka.server.game.user.IdCipher;

public final class EntityIdentityPacket extends GamePacket {

	public EntityIdentityPacket(final ChatEntity chatEntity, final IdCipher chatEntityIdCipher,
			final FieldEntity fieldEntity, final IdCipher fieldEntityIdCipher) {
		out.write(chatEntityIdCipher.encrypt(chatEntity.getId()));
		out.write(fieldEntityIdCipher.encrypt(fieldEntity.getId()));
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.ENTITY_IDENTITY;
	}

}
