package org.kareha.hareka.server.game.stat;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
@XmlJavaTypeAdapter(Skills.Adapter.class)
public class Skills {

	@GuardedBy("this")
	@Private
	final Map<SkillType, Skill> map;

	public Skills() {
		this(null);
	}

	@Private
	Skills(final Map<SkillType, Skill> map) {
		if (map == null) {
			this.map = new EnumMap<>(SkillType.class);
		} else {
			this.map = new EnumMap<>(map);
		}
		final Set<SkillType> missings = EnumSet.allOf(SkillType.class);
		missings.removeAll(this.map.keySet());
		for (final SkillType type : missings) {
			this.map.put(type, new Skill());
		}
	}

	@XmlType(name = "skillEntry")
	private static class SkillEntry {

		@XmlAttribute
		@Private
		SkillType type;
		@XmlValue
		private long value;

		@SuppressWarnings("unused")
		private SkillEntry() {
			// used by JAXB
		}

		@Private
		SkillEntry(final SkillType type, final Skill skill) {
			this.type = type;
			synchronized (skill) {
				value = skill.value;
			}
		}

		@Private
		Skill getSkill() {
			return new Skill(value);
		}

	}

	@XmlType(name = "skills")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement(name = "skill")
		private List<SkillEntry> skillEntries;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Skills v) {
			skillEntries = new ArrayList<>();
			synchronized (v) {
				for (final Map.Entry<SkillType, Skill> entry : v.map.entrySet()) {
					skillEntries.add(new SkillEntry(entry.getKey(), entry.getValue()));
				}
			}
		}

		@Private
		Skills unmarshal() {
			final Map<SkillType, Skill> map = new EnumMap<>(SkillType.class);
			for (final SkillEntry entry : skillEntries) {
				map.put(entry.type, entry.getSkill());
			}
			return new Skills(map);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, Skills> {

		@Override
		public Adapted marshal(final Skills v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public Skills unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public synchronized Skill get(final SkillType type) {
		return map.get(type);
	}

}
