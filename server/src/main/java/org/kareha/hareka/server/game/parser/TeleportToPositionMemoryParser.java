package org.kareha.hareka.server.game.parser;

import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.field.PositionMemory;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;

public final class TeleportToPositionMemoryParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(TeleportToPositionMemoryParser.class.getName());

	private enum BundleKey {
		YouCannotUseThisFunction,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(TeleportToPositionMemoryParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		final byte[] data = in.readByteArray();

		final User user = session.getUser();
		if (user == null) {
			return;
		}
		final RoleSet roles = session.getContext().getAccessController().getRoleSet(user);
		if (!roles.isAbleTo(Permission.TELEPORT_TO_POSITION_MEMORY)) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot teleport");
			return;
		}

		final PositionMemory memory = PositionMemory.decrypt(user.getStorageCipher(), data);
		if (memory == null) {
			return;
		}

		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}
		final CharacterEntity entity = player.getEntity();
		if (!memory.getEntityId().equals(entity.getId())) {
			return;
		}
		entity.teleport(memory.getPosition());
	}

}
