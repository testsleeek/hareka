package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;

public final class RejectPacket extends GamePacket {

	public RejectPacket(final int requestId, final String message) {
		out.writeCompactUInt(requestId);
		out.writeString(message);
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.REJECT;
	}

}
