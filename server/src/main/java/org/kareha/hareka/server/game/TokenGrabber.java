package org.kareha.hareka.server.game;

import java.util.HashMap;
import java.util.Map;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.ThreadSafe;

@ThreadSafe
public class TokenGrabber {

	public interface Handler {

		void handle(byte[] token);

		void canceled();

	}

	public static class Entry {

		private final int id;
		private final Handler handler;
		@GuardedBy("this")
		private byte[] data;

		Entry(final int id, final Handler handler) {
			this.id = id;
			this.handler = handler;
		}

		public int getId() {
			return id;
		}

		public void handle(byte[] token) {
			handler.handle(token);
		}

		public void cancel() {
			handler.canceled();
		}

	}

	private final int capacity;
	@GuardedBy("this")
	private int count;
	@GuardedBy("this")
	private final Map<Integer, Entry> entryMap;

	public TokenGrabber(final int capacity) {
		this.capacity = capacity;
		entryMap = new HashMap<>(Math.min(16, (int) Math.ceil(capacity / 0.75)));
	}

	public TokenGrabber() {
		this(1);
	}

	public synchronized Entry add(final Handler v) {
		if (entryMap.size() >= capacity) {
			return null;
		}
		final int id = count++;
		final Entry entry = new Entry(id, v);
		entryMap.put(id, entry);
		return entry;
	}

	public synchronized Entry get(final int id) {
		return entryMap.remove(id);
	}

}
