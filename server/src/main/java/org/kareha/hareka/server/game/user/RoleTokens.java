package org.kareha.hareka.server.game.user;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.key.KeyId;
import org.kareha.hareka.user.Role;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class RoleTokens {

	private final File file;
	private final AccessController accessController;
	@GuardedBy("this")
	@Private
	long count;
	@GuardedBy("this")
	@Private
	long initialId = -1;
	@GuardedBy("this")
	@Private
	final List<RoleToken> list = new ArrayList<>();

	public RoleTokens(final File file, final AccessController accessController) throws JAXBException {
		this.file = file;
		this.accessController = accessController;

		final Adapted adapted = load();
		if (adapted != null) {
			count = Long.parseLong(adapted.count, 16);
			initialId = Long.parseLong(adapted.initialId, 16);
			if (adapted.list != null) {
				list.addAll(adapted.list);
			}
		}
	}

	@XmlRootElement(name = "roleTokens")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		@Private
		String count;
		@XmlElement
		@Private
		String initialId;
		@XmlElement(name = "roleToken")
		@Private
		List<RoleToken> list;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final RoleTokens v) {
			synchronized (v) {
				count = Long.toHexString(v.count);
				initialId = Long.toHexString(v.initialId);
				list = new ArrayList<>(v.list);
			}
		}

	}

	private Adapted load() throws JAXBException {
		if (!file.isFile()) {
			return null;
		}
		return JaxbUtil.unmarshal(file, Adapted.class);
	}

	public void save() throws JAXBException {
		JaxbUtil.marshal(new Adapted(this), file);
	}

	public synchronized RoleToken get(final long id) {
		for (final RoleToken token : list) {
			if (token.getId() == id) {
				return token;
			}
		}
		return null;
	}

	public synchronized boolean remove(final long id) {
		for (final Iterator<RoleToken> i = list.iterator(); i.hasNext();) {
			final RoleToken token = i.next();
			if (token.getId() == id) {
				i.remove();
				return true;
			}
		}
		return false;
	}

	public synchronized Collection<RoleToken> getRoleTokens() {
		return new ArrayList<>(list);
	}

	public synchronized RoleToken issueInitial() {
		if (initialId == -1) {
			final RoleToken token = new RoleToken(count, KeyId.valueOf(-1), Role.SUPER.getId());
			initialId = count;
			count++;
			list.add(token);
			return token;
		}
		for (final RoleToken token : list) {
			if (token.getId() == initialId) {
				return token;
			}
		}
		return null;
	}

	public synchronized RoleToken issue(final User user, final String roleId) {
		final RoleToken token = new RoleToken(count, user.getId(), roleId);
		count++;
		list.add(token);
		return token;
	}

	public synchronized Role consume(final User user, final byte[] token) {
		for (final Iterator<RoleToken> i = list.iterator(); i.hasNext();) {
			final RoleToken t = i.next();
			if (Arrays.equals(token, t.getValue())) {
				final Role result = accessController.addRole(user, t.getRoleId());
				i.remove();
				return result;
			}
		}
		return null;
	}

}
