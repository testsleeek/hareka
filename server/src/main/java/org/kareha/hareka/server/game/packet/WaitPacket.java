package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.wait.Wait;

public final class WaitPacket extends GamePacket {

	public WaitPacket(final Wait wait) {
		out.write(wait);
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.WAIT;
	}

}
