package org.kareha.hareka.server.game.parser;

import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.DiceRoll;
import org.kareha.hareka.server.game.GameServerConstants;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.packet.RequestDiceChoicePacket;

public final class DiceTokenParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(DiceTokenParser.class.getName());

	private enum BundleKey {
		TheTokenIsTooLong, NoSuchDiceRoll,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(DiceTokenParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int id = in.readCompactUInt();
		final byte[] token = in.readByteArray();
		if (token.length > GameServerConstants.MAX_DICE_ROLL_TOKEN_LENGTH) {
			inform(session, BundleKey.TheTokenIsTooLong.name());
			logger.fine(session.getStamp() + "Too long id=" + id + " length=" + token.length);
			return;
		}

		final DiceRoll diceRoll = session.getDiceRollTable().getDiceRoll(id);
		if (diceRoll == null) {
			inform(session, BundleKey.NoSuchDiceRoll.name());
			logger.fine(session.getStamp() + "Not found id=" + id);
			return;
		}
		diceRoll.setToken(token);
		session.write(new RequestDiceChoicePacket(diceRoll));
	}

}
