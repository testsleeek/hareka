package org.kareha.hareka.server.game.parser;

import java.util.ResourceBundle;

import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.SolidTilePattern;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.field.FieldId;
import org.kareha.hareka.server.game.field.ServerField;
import org.kareha.hareka.server.game.field.ServerFieldObject;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;

public final class AddUpstairsParser implements Parser<Session> {

	private enum BundleKey {
		ThisCharacterCannotEditTileFields, YouCannotEditStairs, YouCannotEditThisTile, ThisIsSpecialTile, YouCannotAddFields, ANewFieldHasBeenCreated, YouCannotEditUpstairsTile, UpstairsTileIsSpecial, StairsHaveBeenPlaced,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(AddUpstairsParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		final User user = session.getUser();
		if (user == null) {
			return;
		}
		final RoleSet roles = session.getContext().getAccessController().getRoleSet(user);
		if (!roles.isAbleTo(Permission.EDIT_STAIRS)) {
			inform(session, BundleKey.YouCannotEditStairs.name());
			return;
		}
		final boolean force = roles.isAbleTo(Permission.FORCE_EDIT_TILE_FIELDS);

		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}
		final CharacterEntity entity = player.getEntity();
		if (!force && !entity.isAbleToEditTileFields()) {
			inform(session, BundleKey.ThisCharacterCannotEditTileFields.name());
			return;
		}
		final ServerFieldObject fo = entity.getFieldObject();
		if (fo == null) {
			return;
		}
		final ServerField field = fo.getField();
		final Placement placement = fo.getPlacement();
		if (!force && !field.isMutable(placement.getPosition())) {
			inform(session, BundleKey.YouCannotEditThisTile.name());
			return;
		}
		final Tile originalTile = field.getTile(placement.getPosition());
		if (originalTile.type().isSpecial()) {
			inform(session, BundleKey.ThisIsSpecialTile.name());
			return;
		}
		final FieldId targetId = field.getUpstairs();
		final ServerField targetField = targetId == null ? null : session.getContext().getFields().getField(targetId);
		if (targetField == null) {
			if (!roles.isAbleTo(Permission.EDIT_FIELDS)) {
				inform(session, BundleKey.YouCannotAddFields.name());
				return;
			}
			final ServerField.Builder builder = new ServerField.Builder();
			builder.defaultTilePattern(new SolidTilePattern(Tile.valueOf(TileType.AIR, 0)));
			builder.boundary(field.getBoundary());
			builder.viewSize(field.getViewSize());
			final ServerField newField = session.getContext().getFields().createField(builder);
			newField.setAndSyncTile(placement.getPosition(), Tile.valueOf(TileType.DOWNSTAIRS, 0), force);
			field.setUpstairs(newField.getId());
			newField.setDownstairs(field.getId());
			inform(session, BundleKey.ANewFieldHasBeenCreated.name());
		} else {
			if (!force && targetField.isMutable(placement.getPosition())) {
				inform(session, BundleKey.YouCannotEditUpstairsTile.name());
				return;
			}
			final Tile targetTile = targetField.getTile(placement.getPosition());
			if (targetTile.type().isSpecial()) {
				inform(session, BundleKey.UpstairsTileIsSpecial.name());
				return;
			}
			final Tile newTile = Tile.valueOf(TileType.DOWNSTAIRS, targetTile.elevation());
			targetField.setAndSyncTile(placement.getPosition(), newTile, force);
		}
		field.setAndSyncTile(placement.getPosition(), Tile.valueOf(TileType.UPSTAIRS, originalTile.elevation()), force);
		inform(session, BundleKey.StairsHaveBeenPlaced.name());
	}

}
