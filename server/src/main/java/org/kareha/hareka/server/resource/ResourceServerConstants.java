package org.kareha.hareka.server.resource;

public final class ResourceServerConstants {

	private ResourceServerConstants() {
		throw new AssertionError();
	}

	public static final String KEY_STORE_FILENAME = "KeyStore";
	// Using keytool, you must specify minimum 6 character password
	public static final String KEY_STORE_PASSWORD = "foobar";
	public static final String KEY_PASSWORD = "foobar";
	public static final int INTERNAL_CONNECTION_SIZE = 2;

}
