package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;

public final class ClearAbilitiesPacket extends GamePacket {

	public ClearAbilitiesPacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.CLEAR_ABILITIES;
	}

}
