package org.kareha.hareka.server;

import java.io.File;

public final class ServerConstants {

	private ServerConstants() {
		throw new AssertionError();
	}

	public static final String DATA_DIRECTORY_PATH = System.getProperty("user.dir") + File.separator + "harekaserv";

}
