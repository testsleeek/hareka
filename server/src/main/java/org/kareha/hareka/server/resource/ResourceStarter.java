package org.kareha.hareka.server.resource;

import java.io.File;
import java.io.IOException;
import java.util.function.Supplier;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.Constants;
import org.kareha.hareka.server.Server;

public final class ResourceStarter {

	private ResourceStarter() {
		throw new AssertionError();
	}

	public static void start(final File dataDirectory, final int port, final boolean noTls,
			final Supplier<char[]> passwordReader) throws IOException, JAXBException {
		// Create context
		final ResourceContext context = new ResourceContext(Constants.VERSION, dataDirectory);

		// Load data
		// context.load();

		// Create server
		final Server internalServer = new ResourceInternalServer(context, noTls);
		context.setInternalServer(internalServer);

		// Add shutdown hook
		Runtime.getRuntime().addShutdownHook(new ResourceShutdownHook(context));

		// Start motion
		// context.getEntityTable().startMotion();

		// Start listening
		internalServer.start(port);
	}

}
