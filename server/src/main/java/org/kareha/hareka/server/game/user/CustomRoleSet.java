package org.kareha.hareka.server.game.user;

import org.kareha.hareka.user.AbstractRoleSet;
import org.kareha.hareka.user.Role;
import org.kareha.hareka.user.RoleSet;

public class CustomRoleSet extends AbstractRoleSet {

	public CustomRoleSet() {
	}

	public CustomRoleSet(final RoleSet original) {
		super(original);
	}

	synchronized boolean isEmpty() {
		return roles.isEmpty();
	}

	synchronized boolean add(final Role role) {
		if (role == null) {
			throw new IllegalArgumentException("null");
		}
		return roles.add(role);
	}

	synchronized boolean remove(final Role role) {
		if (role == null) {
			throw new IllegalArgumentException("null");
		}
		return roles.remove(role);
	}

}
