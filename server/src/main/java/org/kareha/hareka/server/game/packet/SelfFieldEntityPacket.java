package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.entity.FieldEntity;
import org.kareha.hareka.server.game.user.IdCipher;

public final class SelfFieldEntityPacket extends GamePacket {

	public SelfFieldEntityPacket(final FieldEntity entity, final IdCipher fieldEntityIdCipher) {
		out.write(fieldEntityIdCipher.encrypt(entity.getId()));
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.SELF_FIELD_ENTITY;
	}

}
