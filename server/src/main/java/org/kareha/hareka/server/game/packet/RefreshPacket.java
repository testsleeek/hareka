package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;

public final class RefreshPacket extends GamePacket {

	public RefreshPacket() {
		// do nothing
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.REFRESH;
	}

}
