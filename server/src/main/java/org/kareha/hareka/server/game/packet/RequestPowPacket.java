package org.kareha.hareka.server.game.packet;

import java.math.BigInteger;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;

public final class RequestPowPacket extends GamePacket {

	public RequestPowPacket(final int handlerId, final BigInteger target, final byte[] data, final String message) {
		out.writeCompactUInt(handlerId);
		out.writeBigInteger(target);
		out.writeByteArray(data);
		out.writeString(message);
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.REQUEST_POW;
	}

}
