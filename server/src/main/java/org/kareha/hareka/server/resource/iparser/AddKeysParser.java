package org.kareha.hareka.server.resource.iparser;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Logger;

import org.kareha.hareka.LocalSessionId;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.resource.ResourceInternalSession;

public class AddKeysParser implements Parser<ResourceInternalSession> {

	private static final Logger logger = Logger.getLogger(AddKeysParser.class.getName());

	@Override
	public void handle(final PacketInput in, final ResourceInternalSession session) {
		final LocalSessionId sessionKey = LocalSessionId.readFrom(in);
		final byte[] rawAddress = in.readByteArray();

		final InetAddress inetAddress;
		try {
			inetAddress = InetAddress.getByAddress(rawAddress);
		} catch (final UnknownHostException e) {
			logger.info(e.getMessage());
			return;
		}

		session.getContext().getSessionKeyTable().putIfAbsent(sessionKey, inetAddress);

		logger.info("Keys added");
	}

}
