package org.kareha.hareka.server.game.stat;

import java.util.Collection;

import org.kareha.hareka.field.TileType;
import org.kareha.hareka.game.AbilityType;
import org.kareha.hareka.game.Power;
import org.kareha.hareka.server.game.relationship.IndividualRelationships;

public interface CharacterStat extends Stat {

	Species getSpecies();

	int getMotionWait(TileType tileType);

	int getAttackWait(TileType tileType);

	StatResult addHealthPoints(int v);

	StatResult addMagicPoints(int v);

	boolean revive();

	Collection<AbilityType> getAbilities();

	boolean hasAbility(AbilityType v);

	Collection<Power> getPowers();

	boolean hasPower(Power v);

	int getSkill(SkillType type);

	int getWeaponClass();

	int getArmorClass();

	int getAttackPoints(int targetArmorClass);

	int getAccuracy(int targetDodge);

	long getWeightCapacity();

	Skills getSkills();

	IndividualRelationships getRelationships();

	VisualType getVisualType();
	
	boolean isVisible(VisualType visualType);

}
