package org.kareha.hareka.server.game.stat;

import org.kareha.hareka.game.Power;

public enum VisualType {

	SUPER(Power.ABLE_TO_SEE_SUPER),

	SOUL(Power.ABLE_TO_SEE_SOUL),

	SPIRIT(Power.ABLE_TO_SEE_SPIRIT),

	FAIRY(Power.ABLE_TO_SEE_FAIRY),

	NORMAL(Power.ABLE_TO_SEE_NORMAL),

	;

	private final Power power;

	private VisualType(final Power power) {
		this.power = power;
	}

	public Power power() {
		return power;
	}

}
