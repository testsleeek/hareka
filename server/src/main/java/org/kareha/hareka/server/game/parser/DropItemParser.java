package org.kareha.hareka.server.game.parser;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Inventory;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.EntityId;
import org.kareha.hareka.server.game.entity.ItemEntity;
import org.kareha.hareka.server.game.field.ServerFieldObject;

public final class DropItemParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final LocalEntityId itemLocalId = LocalEntityId.readFrom(in);
		final long count = in.readCompactULong();

		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}
		final ServerFieldObject fo = player.getEntity().getFieldObject();
		if (fo == null) {
			return;
		}
		final EntityId itemId = session.getUser().getItemEntityIdCipher().decrypt(itemLocalId);
		final Inventory inventory = player.getEntity().getInventory();
		final ItemEntity dropItem = inventory.pick(itemId, count);
		if (dropItem == null) {
			return;
		}
		dropItem.mount(session.getContext().getFields(), fo.getField().getId(), fo.getPlacement());
	}

}
