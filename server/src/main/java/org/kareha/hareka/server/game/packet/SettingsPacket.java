package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.GameServerSettings;

public final class SettingsPacket extends GamePacket {

	public SettingsPacket(final GameServerSettings settings) {
		out.write(settings.getUserRegistrationMode());
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.SETTINGS;
	}

}
