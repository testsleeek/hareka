package org.kareha.hareka.server.game.stat;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.math.IntRange;
import org.kareha.hareka.server.game.motion.MotionType;
import org.kareha.hareka.server.game.relationship.SpeciesRelationships;

@ThreadSafe
public class NormalSpecies extends AbstractSpecies {

	public static class Builder extends AbstractSpecies.Builder {

		protected Map<SkillType, IntRange> skillRanges;

		public Builder skillRanges(final Map<SkillType, IntRange> v) {
			skillRanges = v;
			return this;
		}

		@Override
		public NormalSpecies build(final String id) {
			if (skillRanges == null) {
				skillRanges = new EnumMap<>(SkillType.class);
			}
			final Set<SkillType> missings = EnumSet.allOf(SkillType.class);
			missings.removeAll(skillRanges.keySet());
			for (final SkillType type : missings) {
				skillRanges.put(type, new IntRange(0, 0));
			}
			if (relationships == null) {
				relationships = new SpeciesRelationships();
			}
			if (motion == null) {
				motion = MotionType.NEUTRAL;
			}
			return new NormalSpecies(id, this);
		}

	}

	protected final Map<SkillType, IntRange> skillRanges;

	protected NormalSpecies(final String id, final Builder builder) {
		super(id, builder);
		skillRanges = new EnumMap<>(builder.skillRanges);
	}

	@XmlType(name = "skillRange")
	@XmlAccessorType(XmlAccessType.NONE)
	protected static class SkillRange {

		@XmlAttribute
		protected SkillType type;
		@XmlElement
		protected int min;
		@XmlElement
		protected int max;

		@SuppressWarnings("unused")
		private SkillRange() {
			// used by JAXB
		}

		protected SkillRange(final SkillType type, final IntRange range) {
			this.type = type;
			min = range.getMin();
			max = range.getMax();
		}

		protected SkillType getType() {
			return type;
		}

		protected IntRange getRange() {
			return new IntRange(min, max);
		}

	}

	@XmlRootElement(name = "normalSpecies")
	@XmlType(name = "normalSpecies")
	@XmlAccessorType(XmlAccessType.NONE)
	protected static class Adapted extends AbstractSpecies.Adapted {

		@XmlElement(name = "skillRange")
		protected List<SkillRange> skillRanges;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final NormalSpecies v) {
			super(v);
			skillRanges = new ArrayList<>();
			for (final Map.Entry<SkillType, IntRange> entry : v.skillRanges.entrySet()) {
				skillRanges.add(new SkillRange(entry.getKey(), entry.getValue()));
			}
		}

		@Override
		protected NormalSpecies unmarshal() {
			final Builder builder = new Builder();
			builder.name(name);
			builder.shapes(shape);
			builder.abilities(abilities);
			builder.powers(powers);
			builder.relationships(relationships);
			builder.motion(motion);
			builder.visualType(visualType);

			final Map<SkillType, IntRange> map = new EnumMap<>(SkillType.class);
			for (final SkillRange range : skillRanges) {
				map.put(range.getType(), range.getRange());
			}
			builder.skillRanges(map);
			return builder.build(id);
		}

	}

	@Override
	protected Adapted marshal() {
		return new Adapted(this);
	}

	@Override
	public IntRange getSkillRange(final SkillType type) {
		return skillRanges.get(type);
	}

}
