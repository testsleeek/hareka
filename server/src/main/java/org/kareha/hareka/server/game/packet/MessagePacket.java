package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;

public final class MessagePacket extends GamePacket {

	public MessagePacket(final String message) {
		out.writeString(message);
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.MESSAGE;
	}

}
