package org.kareha.hareka.server.game.packet;

import java.util.Collection;

import org.kareha.hareka.field.Region;
import org.kareha.hareka.field.Transformation;
import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;

public final class RegionListPacket extends GamePacket {

	public RegionListPacket(final Collection<Region> regions, final Transformation transformation) {
		out.writeCompactUInt(regions.size());
		for (final Region region : regions) {
			out.write(region.transform(transformation));
		}
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.REGION_LIST;
	}

}
