package org.kareha.hareka.server.resource.ipacket;

import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.ResourceInternalClientPacketType;
import org.kareha.hareka.server.resource.ResourceInternalPacket;

public final class ExitPacket extends ResourceInternalPacket {

	public ExitPacket(final String message) {
		out.writeString(message);
	}

	@Override
	protected PacketType getType() {
		return ResourceInternalClientPacketType.EXIT;
	}

}
