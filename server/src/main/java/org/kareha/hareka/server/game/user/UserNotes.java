package org.kareha.hareka.server.game.user;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class UserNotes {

	private static final String FILENAME = "UserNotes.xml";

	private final File dataDirectory;
	@GuardedBy("this")
	@Private
	final Set<UserNote> set = new HashSet<>();

	public UserNotes(final File dataDirectory) {
		this.dataDirectory = dataDirectory;

		final Adapted adapted = load();
		if (adapted != null) {
			if (adapted.list != null) {
				set.addAll(adapted.list);
			}
		}
	}

	@XmlRootElement(name = "userNotes")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement(name = "userNote")
		@Private
		List<UserNote> list;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final UserNotes v) {
			synchronized (v) {
				list = new ArrayList<>(v.set);
			}
		}

	}

	private File getFile() {
		return new File(dataDirectory, FILENAME);
	}

	private Adapted load() {
		final File file = getFile();
		if (!file.isFile()) {
			return null;
		}
		final JAXBContext context;
		try {
			context = JAXBContext.newInstance(Adapted.class);
		} catch (final JAXBException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		try {
			return JaxbUtil.unmarshal(file, context);
		} catch (final JAXBException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public void save() throws JAXBException {
		final JAXBContext context = JAXBContext.newInstance(Adapted.class);
		final Adapted adapted = new Adapted(this);
		final File file = getFile();
		JaxbUtil.marshal(adapted, file, context);
	}

	public synchronized Collection<UserNote> getUserNotes() {
		return new ArrayList<>(set);
	}

}
