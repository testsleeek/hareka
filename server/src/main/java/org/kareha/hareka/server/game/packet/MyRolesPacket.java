package org.kareha.hareka.server.game.packet;

import java.util.Collection;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.user.AccessController;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Role;

public final class MyRolesPacket extends GamePacket {

	public MyRolesPacket(final User user, final AccessController accessController) {
		final Collection<Role> roles = accessController.getRoleSet(user).getRoles();
		out.writeCompactUInt(roles.size());
		for (final Role role : roles) {
			out.writeString(role.getId());
		}
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.MY_ROLES;
	}

}
