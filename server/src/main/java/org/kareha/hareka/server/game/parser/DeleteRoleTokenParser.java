package org.kareha.hareka.server.game.parser;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.packet.RoleTokenListPacket;
import org.kareha.hareka.server.game.user.RoleToken;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.Role;
import org.kareha.hareka.user.RoleSet;

public final class DeleteRoleTokenParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final long id = in.readCompactULong();

		final User user = session.getUser();
		if (user == null) {
			return;
		}
		final RoleToken token = session.getContext().getRoleTokens().get(id);
		if (token == null) {
			return;
		}
		final RoleSet roleSet = session.getContext().getAccessController().getRoleSet(user);
		final boolean accepted;
		if (token.getIssuer().equals(user.getId())) {
			accepted = true;
		} else {
			final int rank;
			final Role mrole = roleSet.getHighestRole(Permission.MANAGE_ROLE_TOKENS);
			if (mrole == null) {
				rank = 0;
			} else {
				rank = mrole.getRank();
			}
			final Role role = session.getContext().getRoles().get(token.getRoleId());
			if (role != null && role.getRank() <= rank) {
				accepted = true;
			} else {
				accepted = false;
			}
		}
		if (!accepted) {
			return;
		}

		if (session.getContext().getRoleTokens().remove(token.getId())) {
			session.write(new RoleTokenListPacket(session.getContext().getRoleTokens(), user,
					session.getContext().getAccessController(), session.getContext().getRoles(),
					session.getContext().getWorkspace().getUserIdCipher()));
		}
	}

}
