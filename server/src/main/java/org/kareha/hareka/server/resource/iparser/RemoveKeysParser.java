package org.kareha.hareka.server.resource.iparser;

import java.util.logging.Logger;

import org.kareha.hareka.LocalSessionId;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.resource.ResourceInternalSession;

public class RemoveKeysParser implements Parser<ResourceInternalSession> {

	private static final Logger logger = Logger.getLogger(RemoveKeysParser.class.getName());

	@Override
	public void handle(final PacketInput in, final ResourceInternalSession session) {
		final LocalSessionId sessionKey = LocalSessionId.readFrom(in);

		session.getContext().getSessionKeyTable().remove(sessionKey);

		logger.info("Keys removed");
	}

}
