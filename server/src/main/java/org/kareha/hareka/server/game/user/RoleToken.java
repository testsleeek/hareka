package org.kareha.hareka.server.game.user;

import java.security.SecureRandom;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.key.KeyId;

@ThreadSafe
@XmlJavaTypeAdapter(RoleToken.Adapter.class)
public class RoleToken {

	@Private
	final long id;
	@Private
	final long issuedDate;
	@Private
	final KeyId issuer;
	@Private
	final String roleId;
	@Private
	final byte[] value;

	public RoleToken(final long id, final KeyId issuer, final String roleId) {
		this.id = id;
		issuedDate = System.currentTimeMillis();
		this.issuer = issuer;
		this.roleId = roleId;
		value = new byte[32];
		new SecureRandom().nextBytes(value);
	}

	public RoleToken(final long id, final long issuedDate, final KeyId issuer, final String roleId,
			final byte[] value) {
		this.id = id;
		this.issuedDate = issuedDate;
		this.issuer = issuer;
		this.roleId = roleId;
		this.value = value.clone();
	}

	@XmlType(name = "roleToken")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		private String id;
		@XmlElement
		private Date issuedDate;
		@XmlElement
		private KeyId issuer;
		@XmlElement(name = "role")
		private String roleId;
		@XmlElement
		private byte[] value;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final RoleToken v) {
			id = Long.toHexString(v.id);
			issuedDate = new Date(v.issuedDate);
			issuer = v.issuer;
			roleId = v.roleId;
			value = v.value.clone();
		}

		@Private
		RoleToken unmarshal() {
			return new RoleToken(Long.parseLong(id, 16), issuedDate.getTime(), issuer, roleId, value);
		}

	}

	@Private
	Adapted marshal() {
		return new Adapted(this);
	}

	static class Adapter extends XmlAdapter<Adapted, RoleToken> {

		@Override
		public Adapted marshal(final RoleToken v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public RoleToken unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public long getId() {
		return id;
	}

	public long getIssuedDate() {
		return issuedDate;
	}

	public KeyId getIssuer() {
		return issuer;
	}

	public String getRoleId() {
		return roleId;
	}

	public byte[] getValue() {
		return value.clone();
	}

}
