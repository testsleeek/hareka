package org.kareha.hareka.server.game.ipacket;

import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.ResourceInternalServerPacketType;
import org.kareha.hareka.server.game.GameInternalPacket;
import org.kareha.hareka.server.game.Session;

public final class AddKeysPacket extends GameInternalPacket {

	public AddKeysPacket(final Session session) {
		out.write(session.getContext().getSessionStatic().toResourceSessionKey(session.getId()));
		out.writeByteArray(session.getInetAddress().getAddress());
	}

	@Override
	protected PacketType getType() {
		return ResourceInternalServerPacketType.ADD_KEYS;
	}

}
