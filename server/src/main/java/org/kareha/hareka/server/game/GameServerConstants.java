package org.kareha.hareka.server.game;

public final class GameServerConstants {

	private GameServerConstants() {
		throw new AssertionError();
	}

	public static final String KEY_STORE_FILENAME = "KeyStore";
	// Using keytool, you must specify minimum 6 character password
	public static final String KEY_STORE_PASSWORD = "foobar";
	public static final String KEY_PASSWORD = "foobar";
	public static final int CONNECTION_SIZE = 100;
	public static final int MAX_CHARACTERS_PER_USER = 4;
	public static final String DICE_ROLL_HASH_ALGORITHM = "SHA-256";
	public static final int MAX_DICE_ROLL_TOKEN_LENGTH = 256;
	public static final int MAX_DICE_ROLL_CHOICE_LENGTH = 1024;
	public static final int POW_GRABBER_CAPACITY = 4;
	public static final int DRAWING_POW_BASE_SCALE = 19;

}
