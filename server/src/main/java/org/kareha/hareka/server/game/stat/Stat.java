package org.kareha.hareka.server.game.stat;

public interface Stat {

	String getShape();

	StatPoints getHealthPoints();

	StatPoints getMagicPoints();

	long getCount();

	boolean isExclusive();

	int getJumpDown();

	int getJumpUp();

}
