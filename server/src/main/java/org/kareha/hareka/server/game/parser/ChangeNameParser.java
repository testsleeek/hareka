package org.kareha.hareka.server.game.parser;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.Constants;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.packet.SelfNamePacket;
import org.kareha.hareka.util.Name;
import org.kareha.hareka.util.Utf8Util;

public final class ChangeNameParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(ChangeNameParser.class.getName());

	private enum BundleKey {
		LoginCharacterFirst, InvalidLanguage, TooShort, TooLong,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(ChangeNameParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final String language = in.readString();
		final String translation = in.readString();

		// check state
		final Player player = session.getPlayer();
		if (player == null) {
			inform(session, BundleKey.LoginCharacterFirst.name());
			logger.fine(session.getStamp() + "No player");
			return;
		}

		// validate input
		final List<String> languages = Arrays.asList(Locale.getISOLanguages());
		if (!languages.contains(language)) {
			inform(session, BundleKey.InvalidLanguage.name());
			return;
		}
		final int length = Utf8Util.length(translation);
		if (length < Constants.MIN_NAME_LENGTH) {
			inform(session, BundleKey.TooShort.name());
			return;
		}
		if (length > Constants.MAX_NAME_LENGTH) {
			inform(session, BundleKey.TooLong.name());
			return;
		}

		// process
		final CharacterEntity entity = player.getEntity();
		final Name name = entity.getName();
		name.put(language, translation);
		entity.setName(name);
		session.write(new SelfNamePacket(entity, session.getUser().getChatEntityIdCipher()));
	}

}
