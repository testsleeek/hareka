package org.kareha.hareka.server.game.parser;

import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.field.ServerFieldObject;

public final class AutopilotParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(AutopilotParser.class.getName());

	private enum BundleKey {
		LoginCharacterFirst,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(AutopilotParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final boolean flag = in.readBoolean();

		// check state
		final Player player = session.getPlayer();
		if (player == null) {
			inform(session, BundleKey.LoginCharacterFirst.name());
			logger.fine(session.getStamp() + "Character not logged in");
			return;
		}
		final ServerFieldObject fo = player.getEntity().getFieldObject();
		if (fo == null) {
			logger.warning(session.getStamp() + "FieldObject not found");
			return;
		}

		if (flag) {
			player.getEntity().startMotion();
		} else {
			player.getEntity().stopMotion();
		}
	}

}
