package org.kareha.hareka.server.game.ability;

import java.util.concurrent.ThreadLocalRandom;

import org.kareha.hareka.field.Direction;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.game.AbilityType;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.field.FieldPosition;
import org.kareha.hareka.server.game.field.ServerField;
import org.kareha.hareka.server.game.field.ServerFieldObject;
import org.kareha.hareka.server.game.stat.StatResult;
import org.kareha.hareka.wait.WaitType;

public class TeleportRandomlyAbility implements Ability {

	@Override
	public AbilityType getType() {
		return AbilityType.TELEPORT_RANDOMLY;
	}

	@Override
	public int getReach() {
		return 1;
	}

	@Override
	public WaitType getWaitType() {
		return WaitType.MOTION;
	}

	@Override
	public int getWait(final CharacterEntity entity, final TileType tileType) {
		return entity.getStat().getMotionWait(tileType);
	}

	@Override
	public void use(final CharacterEntity entity) {
		if (entity.isStuck()) {
			return;
		}
		final ServerFieldObject fo = entity.getFieldObject();
		if (fo == null) {
			return;
		}
		final ServerField field = fo.getField();

		final StatResult result = entity.getStat().addMagicPoints(-32);
		if (result == null) {
			return;
		}
		entity.sendMagicBar(result.getPoints());
		entity.startMagicRegeneration();
		if (result.isTooSmall()) {
			return;
		}

		Vector position = null;
		for (int i = 0; i < 16; i++) {
			final Direction direction = Direction.valueOf(ThreadLocalRandom.current().nextInt(6));
			final int distance = ThreadLocalRandom.current().nextInt(field.getViewSize() * 2);
			final Vector p = direction.vector().multiply(distance);
			final Tile tile = field.getTile(p);
			if (tile.type().isWalkable()) {
				position = p;
				break;
			}
		}
		if (position == null) {
			return;
		}
		entity.teleport(FieldPosition.valueOf(field.getId(), position));
	}

}
