package org.kareha.hareka.server.game.parser;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.kareha.hareka.field.Direction;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.field.FieldId;
import org.kareha.hareka.server.game.field.Gate;
import org.kareha.hareka.server.game.field.ServerField;
import org.kareha.hareka.server.game.field.ServerFieldObject;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;

public final class RemoveSpecialTileParser implements Parser<Session> {

	private enum BundleKey {
		ThisCharacterCannotEditTileFields, ThisIsNotSpecialTile, YouCannotEditStairs, YouCannotEditGates, StairsHaveBeenRemoved, GatePairHasBeenRemoved,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(RemoveSpecialTileParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		final User user = session.getUser();
		if (user == null) {
			return;
		}
		final RoleSet roles = session.getContext().getAccessController().getRoleSet(user);
		final boolean force = roles.isAbleTo(Permission.FORCE_EDIT_TILE_FIELDS);

		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}
		final CharacterEntity entity = player.getEntity();
		if (!force && !entity.isAbleToEditTileFields()) {
			inform(session, BundleKey.ThisCharacterCannotEditTileFields.name());
			return;
		}
		final ServerFieldObject fo = entity.getFieldObject();
		if (fo == null) {
			return;
		}
		final ServerField field = fo.getField();
		final Placement placement = fo.getPlacement();
		final Tile originalTile = field.getTile(placement.getPosition());
		if (!originalTile.type().isSpecial()) {
			inform(session, BundleKey.ThisIsNotSpecialTile.name());
			return;
		}
		switch (originalTile.type()) {
		default:
			return;
		case DOWNSTAIRS: {
			if (!roles.isAbleTo(Permission.EDIT_STAIRS)) {
				inform(session, BundleKey.YouCannotEditStairs.name());
				return;
			}
			if (!force && !field.isMutable(placement.getPosition())) {
				return;
			}
			final FieldId targetId = field.getDownstairs();
			final ServerField targetField = targetId == null ? null
					: session.getContext().getFields().getField(targetId);
			if (targetField == null) {
				return;
			}
			if (!force && !targetField.isMutable(placement.getPosition())) {
				return;
			}
			final Tile it1 = getInterpolated(field, placement.getPosition());
			field.setAndSyncTile(placement.getPosition(), it1, force);
			final Tile targetTile = targetField.getTile(placement.getPosition());
			if (targetTile.type() == TileType.UPSTAIRS) {
				final Tile it2 = getInterpolated(targetField, placement.getPosition());
				targetField.setAndSyncTile(placement.getPosition(), it2, force);
			}
			inform(session, BundleKey.StairsHaveBeenRemoved.name());
		}
			break;
		case GATE: {
			if (!roles.isAbleTo(Permission.EDIT_GATES)) {
				inform(session, BundleKey.YouCannotEditGates.name());
				return;
			}
			if (!force && !field.isMutable(placement.getPosition())) {
				return;
			}
			final Gate gate = field.getGate(placement.getPosition());
			if (gate == null) {
				return;
			}
			final ServerField targetField = session.getContext().getFields()
					.getField(gate.getDestination().getFieldId());
			if (targetField == null) {
				return;
			}
			if (!force && !targetField.isMutable(gate.getDestination().getPosition())) {
				return;
			}
			final Tile it1 = getInterpolated(field, placement.getPosition());
			field.setAndSyncTile(placement.getPosition(), it1, force);
			field.removeGate(gate);
			final Tile targetTile = targetField.getTile(gate.getDestination().getPosition());
			if (targetTile.type() != TileType.GATE) {
				return;
			}
			final Tile it2 = getInterpolated(targetField, gate.getDestination().getPosition());
			targetField.setAndSyncTile(gate.getDestination().getPosition(), it2, force);
			final Gate targetGate = targetField.getGate(gate.getDestination().getPosition());
			if (targetGate != null) {
				targetField.removeGate(targetGate);
			}
			inform(session, BundleKey.GatePairHasBeenRemoved.name());
		}
			break;
		case UPSTAIRS: {
			if (!roles.isAbleTo(Permission.EDIT_STAIRS)) {
				inform(session, BundleKey.YouCannotEditStairs.name());
				return;
			}
			if (!force && !field.isMutable(placement.getPosition())) {
				return;
			}
			final FieldId targetId = field.getUpstairs();
			final ServerField targetField = targetId == null ? null
					: session.getContext().getFields().getField(targetId);
			if (targetField == null) {
				return;
			}
			if (!force && !targetField.isMutable(placement.getPosition())) {
				return;
			}
			final Tile it1 = getInterpolated(field, placement.getPosition());
			field.setAndSyncTile(placement.getPosition(), it1, force);
			final Tile targetTile = targetField.getTile(placement.getPosition());
			if (targetTile.type() == TileType.DOWNSTAIRS) {
				final Tile it2 = getInterpolated(targetField, placement.getPosition());
				targetField.setAndSyncTile(placement.getPosition(), it2, force);
			}
			inform(session, BundleKey.StairsHaveBeenRemoved.name());
		}
			break;
		}
	}

	private static Tile getInterpolated(final ServerField field, final Vector position) {
		final Map<TileType, Integer> counts = new HashMap<>();
		for (final Direction d : EnumSet.allOf(Direction.class)) {
			if (d == Direction.NULL) {
				continue;
			}
			final Tile t = field.getTile(position.neighbor(d));
			if (t.type().isSpecial()) {
				continue;
			}
			if (!t.type().isWalkable()) {
				continue;
			}
			Integer count = counts.get(t.type());
			if (count == null) {
				count = 0;
			}
			count++;
			counts.put(t.type(), count);
		}
		if (counts.size() < 1) {
			return Tile.valueOf(TileType.GRASS, field.getTile(position).elevation());
		}
		int max = 0;
		TileType maxTileType = null;
		for (final Map.Entry<TileType, Integer> entry : counts.entrySet()) {
			if (entry.getValue() > max) {
				max = entry.getValue();
				maxTileType = entry.getKey();
			}
		}
		return Tile.valueOf(maxTileType, field.getTile(position).elevation());
	}

}
