package org.kareha.hareka.server.game.parser;

import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.entity.Entity;
import org.kareha.hareka.server.game.entity.EntityId;
import org.kareha.hareka.server.game.packet.CharactersPacket;
import org.kareha.hareka.server.game.user.User;

public final class DeleteCharacterParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(DeleteCharacterParser.class.getName());

	private enum BundleKey {
		LoginUserFirst, TheCharacterDeleted, CannotDeleteTheCharacter,
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final int requestId = in.readCompactUInt();
		final LocalEntityId localId = LocalEntityId.readFrom(in);

		final ResourceBundle bundle = session.getBundle(DeleteCharacterParser.class.getName());

		// check state
		final User user = session.getUser();
		if (user == null) {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.LoginUserFirst.name()));
			logger.fine(session.getStamp() + "No user");
			return;
		}

		// delete character
		final EntityId entityId = user.getChatEntityIdCipher().decrypt(localId);
		if (user.removeCharacterEntry(entityId)) {
			final Entity entity = session.getContext().getEntities().get(entityId);
			if (entity instanceof CharacterEntity) {
				final CharacterEntity characterEntity = (CharacterEntity) entity;
				characterEntity.removeOwner(user.getId());
			}
		} else {
			session.writeRejectPacket(requestId, bundle.getString(BundleKey.CannotDeleteTheCharacter.name()));
			logger.fine(session.getStamp() + "Failed to delete id=" + entityId);
			return;
		}
		session.writeAcceptPacket(requestId, bundle.getString(BundleKey.TheCharacterDeleted.name()));
		logger.info(session.getStamp() + "Deleted id=" + entityId);

		session.write(new CharactersPacket(user.getCharacterEntries(), session.getContext().getEntities(),
				user.getChatEntityIdCipher(), session.getLocale().getLanguage()));
	}

}
