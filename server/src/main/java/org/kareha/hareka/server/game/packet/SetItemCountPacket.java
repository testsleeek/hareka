package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.entity.ItemEntity;
import org.kareha.hareka.server.game.user.IdCipher;

public final class SetItemCountPacket extends GamePacket {

	public SetItemCountPacket(final ItemEntity itemEntity, final IdCipher itemEntityIdCipher) {
		out.write(itemEntityIdCipher.encrypt(itemEntity.getId()));
		out.writeCompactULong(itemEntity.getStat().getCount());
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.SET_ITEM_COUNT;
	}

}
