package org.kareha.hareka.server.game.parser;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.Entity;
import org.kareha.hareka.server.game.entity.EntityId;
import org.kareha.hareka.server.game.entity.FieldEntity;
import org.kareha.hareka.server.game.entity.ItemEntity;
import org.kareha.hareka.server.game.field.ServerFieldObject;
import org.kareha.hareka.server.game.item.Item;
import org.kareha.hareka.server.game.packet.WaitPacket;
import org.kareha.hareka.wait.WaitResult;

public final class UseItemParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final long waitId = in.readCompactULong();
		final LocalEntityId itemLocalId = LocalEntityId.readFrom(in);
		final LocalEntityId targetLocalId = LocalEntityId.readFrom(in);

		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}
		final EntityId itemId = session.getUser().getItemEntityIdCipher().decrypt(itemLocalId);
		final ItemEntity itemEntity = player.getEntity().getInventory().getItem(itemId);
		if (itemEntity == null) {
			return;
		}
		final Item item = itemEntity.getStat().getBrand();

		final ServerFieldObject fo = player.getEntity().getFieldObject();
		final TileType tileType = fo.getField().getTile(fo.getPlacement().getPosition()).type();
		final WaitResult result = player.getEntity().getWaitGenerator().next(waitId, item.getWaitType(),
				item.getWait(player.getEntity(), tileType), player.getWaitPenalty());
		if (!result.isSuccess()) {
			session.write(new WaitPacket(result.getWait()));
			return;
		}
		player.getEntity().sendWait(result.getWait());

		final EntityId targetEntityId = session.getUser().getFieldEntityIdCipher().decrypt(targetLocalId);
		final Entity targetEntity = session.getContext().getEntities().get(targetEntityId);
		if (!(targetEntity instanceof FieldEntity)) {
			return;
		}
		final FieldEntity targetFieldEntity = (FieldEntity) targetEntity;
		final ServerFieldObject targetFo = targetFieldEntity.getFieldObject();
		if (targetFo == null) {
			return;
		}

		if (fo.getField() != targetFo.getField()) {
			return;
		}
		final Placement placement = fo.getPlacement();
		final Placement targetPlacement = targetFo.getPlacement();
		if (fo.getField().getBoundary().distance(placement.getPosition(), targetPlacement.getPosition()) > item
				.getReach()) {
			return;
		}

		if (item.isConsumable()) {
			if (!player.getEntity().getInventory().remove(itemId, 1)) {
				return;
			}
		}
		item.use(player.getEntity(), itemEntity, targetFieldEntity);
	}

}
