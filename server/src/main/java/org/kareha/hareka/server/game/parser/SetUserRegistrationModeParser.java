package org.kareha.hareka.server.game.parser;

import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;
import org.kareha.hareka.user.UserRegistrationMode;

public final class SetUserRegistrationModeParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(SetUserRegistrationModeParser.class.getName());

	private enum BundleKey {
		LoginUserFirst, YouCannotUseThisFunction, SettingChanged,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(SetUserRegistrationModeParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		final UserRegistrationMode mode = UserRegistrationMode.readFrom(in);
		if (mode == null) {
			inform(session, BundleKey.LoginUserFirst.name());
			logger.fine(session.getStamp() + "User not logged in");
			return;
		}

		final User user = session.getUser();
		if (user == null) {
			return;
		}
		final RoleSet roles = session.getContext().getAccessController().getRoleSet(user);
		if (!roles.isAbleTo(Permission.MANAGE_SETTINGS)) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot use set user registration mode function");
			return;
		}

		session.getContext().getGameServerSettings().setUserRegistrationMode(mode);
		inform(session, BundleKey.SettingChanged.name());
		logger.fine(session.getStamp() + "User registration mode changed to " + mode);
	}

}
