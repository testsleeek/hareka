package org.kareha.hareka.server.game.parser;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Role;

public final class ConsumeRoleTokenParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(ConsumeRoleTokenParser.class.getName());

	private enum BundleKey {
		LoginUserFirst, InvalidToken, RoleAdded,
	}

	private static void inform(final Session session, final String key, final Object... args) {
		final ResourceBundle bundle = session.getBundle(ConsumeRoleTokenParser.class.getName());
		session.writeInformPacket(MessageFormat.format(bundle.getString(key), args));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final byte[] token = in.readByteArray();

		// check state
		final User user = session.getUser();
		if (user == null) {
			inform(session, BundleKey.LoginUserFirst.name());
			logger.fine(session.getStamp() + "User not logged in");
			return;
		}

		// process
		final Role role = session.getContext().getRoleTokens().consume(user, token);
		if (role == null) {
			inform(session, BundleKey.InvalidToken.name());
			logger.fine(session.getStamp() + "Invalid role token");
			return;
		}
		inform(session, BundleKey.RoleAdded.name(), role.getId());
		logger.info(session.getStamp() + "Role " + role.getId() + " added");
	}

}
