package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.entity.ChatEntity;
import org.kareha.hareka.server.game.user.IdCipher;

public final class SelfNamePacket extends GamePacket {

	public SelfNamePacket(final ChatEntity chatEntity, final IdCipher chatEntityIdCipher) {
		out.write(chatEntityIdCipher.encrypt(chatEntity.getId()));
		out.write(chatEntity.getName());
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.SELF_NAME;
	}

}
