package org.kareha.hareka.server.game.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.field.Direction;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.field.Vector;

@Immutable
@XmlJavaTypeAdapter(Boundary.Adapter.class)
public abstract class Boundary {

	@XmlType(name = "boundary")
	@XmlSeeAlso({ NullBoundary.Adapted.class, PeriodicBoundary.Adapted.class })
	@XmlAccessorType(XmlAccessType.NONE)
	protected static abstract class Adapted {

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final Boundary v) {
			// main constructor
		}

		protected abstract Boundary unmarshal();

	}

	protected abstract Adapted marshal();

	static class Adapter extends XmlAdapter<Adapted, Boundary> {

		@Override
		public Adapted marshal(final Boundary v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.marshal();
		}

		@Override
		public Boundary unmarshal(final Adapted v) throws Exception {
			if (v == null) {
				return null;
			}
			return v.unmarshal();
		}

	}

	public abstract Vector confine(Vector position);

	public Vector confine(final Vector position, final Vector center) {
		return confine(position.subtract(center)).add(center);
	}

	public Vector subtract(final Vector a, final Vector b) {
		return confine(b.subtract(a));
	}

	public int distance(final Vector a, final Vector b) {
		return confine(b.subtract(a)).distance();
	}

	public Direction direction(final Vector a, final Vector b) {
		return confine(b.subtract(a)).direction();
	}

	public Placement confine(final Placement placement) {
		return Placement.valueOf(confine(placement.getPosition()), placement.getDirection());
	}

	public Placement confine(final Placement placement, final Vector center) {
		return Placement.valueOf(confine(placement.getPosition(), center), placement.getDirection());
	}

	public TilePiece confine(final TilePiece tilePiece, final Vector center) {
		return TilePiece.valueOf(confine(tilePiece.position(), center), tilePiece.tile());
	}

}
