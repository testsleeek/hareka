package org.kareha.hareka.server.game.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.field.Region;
import org.kareha.hareka.field.SimpleRegion;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.field.ServerField;
import org.kareha.hareka.server.game.field.ServerFieldObject;
import org.kareha.hareka.server.game.field.ServerSimpleRegion;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;

public final class SetRegionListParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(SetRegionListParser.class.getName());

	private enum BundleKey {
		YouCannotUseThisFunction,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(SetRegionListParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		final int size = in.readCompactUInt();
		final Region[] regions = new Region[size];
		for (int i = 0; i < size; i++) {
			regions[i] = Region.readFrom(in);
		}

		final User user = session.getUser();
		if (user == null) {
			return;
		}
		final RoleSet roles = session.getContext().getAccessController().getRoleSet(user);
		if (!roles.isAbleTo(Permission.EDIT_REGIONS)) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot edit tile fields");
			return;
		}

		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}
		final CharacterEntity entity = player.getEntity();
		final ServerFieldObject fo = entity.getFieldObject();
		if (fo == null) {
			return;
		}
		final ServerField field = fo.getField();

		final List<Region> realRegions = new ArrayList<>();
		for (final Region region : regions) {
			Region realRegion = region.invert(player.getTransformation());
			if (realRegion instanceof SimpleRegion) {
				realRegion = new ServerSimpleRegion((SimpleRegion) realRegion, field);
			}
			realRegions.add(realRegion);
		}
		field.setRegions(realRegions);

		for (final Session s : session.getContext().getSessions().getSessions()) {
			final Player p = s.getPlayer();
			if (p == null) {
				continue;
			}
			final ServerFieldObject o = p.getEntity().getFieldObject();
			if (o == null) {
				continue;
			}
			if (o.getField() != field) {
				continue;
			}
			p.synchronize();
		}
	}

}
