package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.DiceRoll;
import org.kareha.hareka.server.game.GamePacket;

public final class DiceResultPacket extends GamePacket {

	public DiceResultPacket(final DiceRoll diceRoll) {
		out.writeCompactUInt(diceRoll.getId());
		out.writeCompactUInt(diceRoll.getValue());
		out.writeByteArray(diceRoll.getSecret());
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.DICE_RESULT;
	}

}
