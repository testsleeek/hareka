package org.kareha.hareka.server;

import java.io.IOException;

public interface Server {

	void start(int port) throws IOException;

	boolean stop(long millis) throws IOException, InterruptedException;

}
