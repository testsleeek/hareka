package org.kareha.hareka.server;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.annotation.ConfinedTo;
import org.kareha.hareka.server.game.GameServerParameters;
import org.kareha.hareka.server.game.Starter;
import org.kareha.hareka.server.resource.ResourceServerParameters;
import org.kareha.hareka.server.resource.ResourceStarter;
import org.kareha.hareka.ui.swing.LogPanel;
import org.kareha.hareka.ui.swing.PasswordReader;

@ConfinedTo("Swing")
@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	private enum BundleKey {

		Title,

		GameServerPort, ResourceServerInternalPort,

		StartGameServer, StartResourceServer, StopServers,

		InvalidPortNumberFormat,

	}

	private static final Logger logger = Logger.getLogger(MainFrame.class.getName());

	private final GameServerParameters gameParameters;
	private final ResourceServerParameters resourceParameters;
	private final JTextField gamePortField;
	private final JTextField resourceInternalPortField;
	private final JButton startGameServerButton;
	private final JButton startResourceServerButton;
	private final JButton stopServersButton;

	public MainFrame(final ServerParameters parameters) {
		gameParameters = new GameServerParameters(parameters);
		resourceParameters = new ResourceServerParameters(parameters);

		final ResourceBundle bundle = ResourceBundle.getBundle(MainFrame.class.getName());

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setTitle(bundle.getString(BundleKey.Title.name()));

		final JPanel northPanel = new JPanel();
		northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.Y_AXIS));

		final JPanel gamePortPanel = new JPanel();
		gamePortPanel.add(new JLabel(bundle.getString(BundleKey.GameServerPort.name())));
		gamePortField = new JTextField(Integer.toString(gameParameters.getPort()));
		gamePortPanel.add(gamePortField);
		northPanel.add(gamePortPanel);

		final JPanel resourceInternalPortPanel = new JPanel();
		resourceInternalPortPanel.add(new JLabel(bundle.getString(BundleKey.ResourceServerInternalPort.name())));
		resourceInternalPortField = new JTextField(Integer.toString(resourceParameters.getPort()));
		resourceInternalPortPanel.add(resourceInternalPortField);
		northPanel.add(resourceInternalPortPanel);

		final JPanel buttonPanel = new JPanel();
		startGameServerButton = new JButton(bundle.getString(BundleKey.StartGameServer.name()));
		startGameServerButton.addActionListener(e -> doStartGameServer());
		buttonPanel.add(startGameServerButton);
		startResourceServerButton = new JButton(bundle.getString(BundleKey.StartResourceServer.name()));
		startResourceServerButton.addActionListener(e -> doStartResourceServer());
		buttonPanel.add(startResourceServerButton);
		stopServersButton = new JButton(bundle.getString(BundleKey.StopServers.name()));
		stopServersButton.setEnabled(false);
		stopServersButton.addActionListener(e -> doStopServers());
		buttonPanel.add(stopServersButton);
		northPanel.add(buttonPanel);

		add(northPanel, BorderLayout.NORTH);
		final LogPanel logPanel = LogPanel.newInstance();
		logPanel.setPreferredSize(new Dimension(384, 384));
		add(logPanel, BorderLayout.CENTER);

		pack();
		setLocationRelativeTo(null);

		startResourceServerButton.requestFocus();
	}

	private void doStartGameServer() {
		final int port;
		try {
			port = Integer.parseInt(gamePortField.getText());
		} catch (final NumberFormatException e) {
			final ResourceBundle bundle = ResourceBundle.getBundle(MainFrame.class.getName());
			JOptionPane.showMessageDialog(this, bundle.getString(BundleKey.InvalidPortNumberFormat.name()));
			return;
		}
		gamePortField.setEnabled(false);
		startGameServerButton.setEnabled(false);
		try {
			Starter.start(gameParameters.getDataDirectory(), port, gameParameters.isNoTls(), new PasswordReader(this));
		} catch (final ServerException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		} catch (final IOException | JAXBException e) {
			logger.log(Level.SEVERE, "", e);
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		stopServersButton.setEnabled(true);
	}

	private void doStartResourceServer() {
		final int port;
		try {
			port = Integer.parseInt(resourceInternalPortField.getText());
		} catch (final NumberFormatException e) {
			final ResourceBundle bundle = ResourceBundle.getBundle(MainFrame.class.getName());
			JOptionPane.showMessageDialog(this, bundle.getString(BundleKey.InvalidPortNumberFormat.name()));
			return;
		}
		resourceInternalPortField.setEnabled(false);
		startResourceServerButton.setEnabled(false);
		try {
			ResourceStarter.start(resourceParameters.getDataDirectory(), port, resourceParameters.isNoTls(),
					new PasswordReader(this));
		} catch (final ServerException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		} catch (final IOException | JAXBException e) {
			logger.log(Level.SEVERE, "", e);
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		stopServersButton.setEnabled(true);
	}

	private void doStopServers() {
		// TODO use Rebooter.shutdown
		System.exit(0);
	}

}
