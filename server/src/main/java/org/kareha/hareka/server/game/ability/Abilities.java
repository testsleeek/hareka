package org.kareha.hareka.server.game.ability;

import java.util.EnumMap;
import java.util.Map;

import org.kareha.hareka.game.AbilityType;
import org.kareha.hareka.util.CamelCase;

public class Abilities {

	private final Map<AbilityType, Ability> map;

	public Abilities() {
		final String base = "org.kareha.hareka.server.game.ability";
		final String postfix = "Ability";
		final Map<AbilityType, Ability> m = new EnumMap<>(AbilityType.class);
		for (final AbilityType type : AbilityType.values()) {
			final String camelCase = CamelCase.snakeToCamel(type.name());
			if (camelCase == null) {
				throw new RuntimeException("The name must be underscore: " + type.name());
			}
			final StringBuilder sb = new StringBuilder(base).append(".").append(camelCase).append(postfix);
			Class<?> clazz;
			try {
				clazz = Class.forName(sb.toString());
			} catch (final ClassNotFoundException e) {
				throw new IllegalArgumentException(e.getMessage(), e);
			}
			final Object o;
			try {
				o = clazz.newInstance();
			} catch (InstantiationException e) {
				throw new IllegalArgumentException(e.getMessage(), e);
			} catch (IllegalAccessException e) {
				throw new IllegalArgumentException(e.getMessage(), e);
			}
			if (!(o instanceof Ability)) {
				throw new IllegalArgumentException(o.getClass().getName() + " is not instance of Ability");
			}
			final Ability t = (Ability) o;
			m.put(type, t);
		}
		map = m;
	}

	public Ability get(final AbilityType type) {
		return map.get(type);
	}

}
