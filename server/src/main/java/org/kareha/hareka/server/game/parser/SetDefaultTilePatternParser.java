package org.kareha.hareka.server.game.parser;

import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.field.OneUniformTilePattern;
import org.kareha.hareka.field.SolidTilePattern;
import org.kareha.hareka.field.TilePattern;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.field.ServerField;
import org.kareha.hareka.server.game.field.ServerFieldObject;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;

public final class SetDefaultTilePatternParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(SetDefaultTilePatternParser.class.getName());

	private enum BundleKey {
		YouCannotUseThisFunction, SpecialTileCannotBeUsed, DefaultTilePatternChanged,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(SetDefaultTilePatternParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		final TilePattern tilePattern = TilePattern.readFrom(in);

		final User user = session.getUser();
		if (user == null) {
			return;
		}
		final RoleSet roles = session.getContext().getAccessController().getRoleSet(user);
		if (!roles.isAbleTo(Permission.EDIT_REGIONS)) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot edit tile fields");
			return;
		}

		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}
		final CharacterEntity entity = player.getEntity();
		final ServerFieldObject fo = entity.getFieldObject();
		if (fo == null) {
			return;
		}

		if (tilePattern instanceof SolidTilePattern) {
			final SolidTilePattern tp = (SolidTilePattern) tilePattern;
			if (tp.getValue().type().isSpecial()) {
				inform(session, BundleKey.SpecialTileCannotBeUsed.name());
				return;
			}
		} else if (tilePattern instanceof OneUniformTilePattern) {
			final OneUniformTilePattern tp = (OneUniformTilePattern) tilePattern;
			if (tp.getA().type().isSpecial() || tp.getB().type().isSpecial() || tp.getC().type().isSpecial()) {
				inform(session, BundleKey.SpecialTileCannotBeUsed.name());
				return;
			}
		} else {
			session.writeInformPacket("Checks for this tile pattern type is not implemented yet");
			return;
		}

		final ServerField field = fo.getField();
		field.setDefaultTilePattern(tilePattern);
		for (final Session s : session.getContext().getSessions().getSessions()) {
			final Player p = s.getPlayer();
			if (p == null) {
				continue;
			}
			final ServerFieldObject o = p.getEntity().getFieldObject();
			if (o == null) {
				continue;
			}
			if (o.getField() != field) {
				continue;
			}
			p.synchronize();
		}
		inform(session, BundleKey.DefaultTilePatternChanged.name());
	}

}
