package org.kareha.hareka.server.game.field;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.text.MessageFormat;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.field.SolidTilePattern;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.util.FileUtil;

public class Fields {

	private static final Logger logger = Logger.getLogger(Fields.class.getName());
	private static final String RESOURCE_PATH = "org/kareha/hareka/server/game/resource/field/";

	private final File directory;
	private final FieldStatic idTable;
	private final boolean directoryCreated;
	private final Map<FieldId, ServerField> map = new ConcurrentHashMap<>();
	@GuardedBy("this")
	private ServerField rootField;

	public Fields(final File directory, final FieldStatic idTable) throws IOException {
		this.directory = directory;
		this.idTable = idTable;

		directoryCreated = FileUtil.ensureDirectoryExists(directory);
	}

	private File getFile(final FieldId id) {
		return new File(directory, id + ".xml");
	}

	public void load() throws IOException, JAXBException {
		map.clear();
		if (!directory.isDirectory()) {
			logger.severe("Cannot find directory: " + directory);
			return;
		}

		if (directoryCreated) {
			final long count = idTable.getCount();
			for (int i = 0; i < count; i++) {
				final String name = RESOURCE_PATH + i + ".xml";
				try (final InputStream in = getClass().getClassLoader().getResourceAsStream(name)) {
					if (in == null) {
						continue;
					}
					final ServerField field = ServerField.load(in);
					map.put(field.getId(), field);
				} catch (final IOException e) {
					continue;
				}
			}
		} else {
			final File[] list = directory.listFiles();
			if (list == null) {
				logger.severe("Cannot list directory: " + directory);
				return;
			}
			for (final File file : list) {
				if (!file.isFile()) {
					continue;
				}
				final ServerField field = ServerField.load(file);
				map.put(field.getId(), field);
			}
		}

		if (map.size() < 1) {
			// create default root field
			final ServerField.Builder builder = new ServerField.Builder();
			builder.defaultTilePattern(new SolidTilePattern(Tile.valueOf(TileType.GRASS, 0)));
			builder.boundary(new PeriodicBoundary(35, new SecureRandom().nextBoolean()));
			createField(builder);
		}

		// find root field
		FieldId min = null;
		for (final ServerField field : map.values()) {
			if (min == null) {
				min = field.getId();
			} else {
				if (field.getId().value() < min.value()) {
					min = field.getId();
				}
			}
		}
		synchronized (this) {
			rootField = map.get(min);
		}
	}

	public void save() throws JAXBException {
		for (final ServerField field : map.values()) {
			final File file = getFile(field.getId());
			field.save(file);
		}
	}

	private FieldId nextId() {
		FieldId id = idTable.next();
		while (map.containsKey(id)) {
			logger.severe(MessageFormat.format("ID {0} already exists", id));
			id = idTable.next();
		}
		return id;
	}

	public ServerField getField(final FieldId id) {
		return map.get(id);
	}

	public synchronized ServerField getRootField() {
		return rootField;
	}

	public ServerField createField(final ServerField.Builder builder) {
		final ServerField field = builder.build(nextId());
		map.put(field.getId(), field);
		return field;
	}

}
