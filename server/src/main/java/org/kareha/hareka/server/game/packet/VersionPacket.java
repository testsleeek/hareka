package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.Version;
import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;

public class VersionPacket extends GamePacket {

	public VersionPacket(final Version version) {
		out.write(version);
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.VERSION;
	}

}
