package org.kareha.hareka.server;

import java.io.IOException;
import java.net.ServerSocket;

public abstract class PlainServer extends AbstractServer {

	@Override
	protected ServerSocket createServerSocket(final int port) throws IOException {
		return new ServerSocket(port);
	}

}
