package org.kareha.hareka.server.game.parser;

import java.util.IllformedLocaleException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;

public final class LocaleParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(LocaleParser.class.getName());

	private enum BundleKey {
		BadLocaleFormat,
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		// check format
		final Locale locale;
		try {
			locale = in.readLocale();
		} catch (final IllformedLocaleException e) {
			final ResourceBundle bundle = session.getBundle(LocaleParser.class.getName());
			session.writeInformPacket(bundle.getString(BundleKey.BadLocaleFormat.name()));
			logger.fine(session.getStamp() + "Illformed");
			return;
		}

		// process
		session.setLocale(locale);
	}

}
