package org.kareha.hareka.server.game.packet;

import java.util.Collection;

import org.kareha.hareka.key.KeyId;
import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.entity.Entity;
import org.kareha.hareka.server.game.user.IdCipher;

public final class InspectionPacket extends GamePacket {

	public InspectionPacket(final Entity entity, final IdCipher chatEntityIdCipher, final IdCipher fieldEntityIdCipher,
			final Collection<KeyId> userIds, final IdCipher userIdCipher) {
		out.write(chatEntityIdCipher.encrypt(entity.getId()));
		out.write(fieldEntityIdCipher.encrypt(entity.getId()));
		out.writeCompactUInt(userIds.size());
		for (final KeyId i : userIds) {
			out.write(userIdCipher.encrypt(i));
		}
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.INSPECTION;
	}

}
