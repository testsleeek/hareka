package org.kareha.hareka.server.game.parser;

import java.util.Collection;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.key.KeyId;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.entity.Entity;
import org.kareha.hareka.server.game.entity.EntityId;
import org.kareha.hareka.server.game.packet.InspectionPacket;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Permission;

public final class InspectChatEntityParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(InspectChatEntityParser.class.getName());

	private enum BundleKey {
		LoginUserFirst, YouCannotUseThisFunction,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(InspectChatEntityParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final LocalEntityId localId = LocalEntityId.readFrom(in);

		// check state
		final User user = session.getUser();
		if (user == null) {
			inform(session, BundleKey.LoginUserFirst.name());
			logger.fine(session.getStamp() + "User not logged in");
			return;
		}
		if (!session.getContext().getAccessController().getRoleSet(user).isAbleTo(Permission.INSPECT_ENTITIES)) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot use inspect entities function");
			return;
		}

		final EntityId targetId = user.getChatEntityIdCipher().decrypt(localId);
		final Entity targetEntity = session.getContext().getEntities().get(targetId);
		if (targetEntity == null) {
			return;
		}

		final Collection<KeyId> userIds;
		if (targetEntity instanceof CharacterEntity) {
			final CharacterEntity characterEntity = (CharacterEntity) targetEntity;
			userIds = characterEntity.getOwners();
		} else {
			userIds = Collections.emptyList();
		}
		session.write(new InspectionPacket(targetEntity, user.getChatEntityIdCipher(), user.getFieldEntityIdCipher(),
				userIds, session.getContext().getWorkspace().getUserIdCipher()));
	}

}
