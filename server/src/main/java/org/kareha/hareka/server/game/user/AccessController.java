package org.kareha.hareka.server.game.user;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.key.KeyId;
import org.kareha.hareka.user.Role;
import org.kareha.hareka.user.RoleSet;
import org.kareha.hareka.user.SingleRoleSet;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class AccessController {

	private static final String DEFAULT_ROLE_ID = "Default";

	private final File file;
	private final Roles roles;
	@GuardedBy("this")
	@Private
	final Map<KeyId, CustomRoleSet> map = new HashMap<>();
	private volatile RoleSet defaultRoleSet;

	public AccessController(final File file, final Roles roles) throws JAXBException {
		this.file = file;
		this.roles = roles;

		final Adapted adapted = load();
		if (adapted != null) {
			if (adapted.list != null) {
				for (final Entry entry : adapted.list) {
					map.put(entry.user, createRoleSet(entry.list));
				}
			}
		}

		helperUpdateDefaultRoleSet();
	}

	private void helperUpdateDefaultRoleSet() {
		final Role defaultRole = roles.get(DEFAULT_ROLE_ID);
		if (defaultRole != null) {
			defaultRoleSet = new SingleRoleSet(defaultRole);
		} else {
			defaultRoleSet = RoleSet.EMPTY;
		}
	}

	private CustomRoleSet createRoleSet(final List<String> ids) {
		final CustomRoleSet roleSet = new CustomRoleSet();
		for (final String id : ids) {
			final Role role = roles.get(id);
			if (role != null) {
				roleSet.add(role);
			}
		}
		return roleSet;
	}

	@XmlType(name = "accessControlEntry")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Entry {

		@XmlElement
		@Private
		KeyId user;
		@XmlElement(name = "role")
		@Private
		List<String> list;

		@SuppressWarnings("unused")
		private Entry() {
			// used by JAXB
		}

		@Private
		Entry(final KeyId user, final List<String> list) {
			this.user = user;
			this.list = new ArrayList<>(list);
		}

	}

	@XmlRootElement(name = "accessControl")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement(name = "entry")
		List<Entry> list;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final AccessController v) {
			list = new ArrayList<>();
			synchronized (v) {
				for (final Map.Entry<KeyId, CustomRoleSet> entry : v.map.entrySet()) {
					final List<String> ids = new ArrayList<>();
					for (final Role role : entry.getValue().getRoles()) {
						ids.add(role.getId());
					}
					list.add(new Entry(entry.getKey(), ids));
				}
			}
		}

	}

	private Adapted load() throws JAXBException {
		if (!file.isFile()) {
			return null;
		}
		return JaxbUtil.unmarshal(file, Adapted.class);
	}

	public void save() throws JAXBException {
		JaxbUtil.marshal(new Adapted(this), file);
	}

	public void updateDefaultRoleSet() {
		helperUpdateDefaultRoleSet();
	}

	public synchronized RoleSet getRoleSet(final User user) {
		final RoleSet roleSet = map.get(user.getId());
		if (roleSet == null) {
			return defaultRoleSet;
		}
		return roleSet;
	}

	public synchronized Role addRole(final User user, final String roleId) {
		final Role role = roles.get(roleId);
		if (role == null) {
			return null;
		}
		final CustomRoleSet roleSet = map.get(user.getId());
		if (roleSet == null) {
			final CustomRoleSet newRoleSet = new CustomRoleSet(defaultRoleSet);
			newRoleSet.add(role);
			map.put(user.getId(), newRoleSet);
			return role;
		}
		if (!roleSet.add(role)) {
			return null;
		}
		return role;
	}

	public synchronized Role removeRole(final User user, final String roleId) {
		final Role role = roles.get(roleId);
		if (role == null) {
			return null;
		}
		final CustomRoleSet roleSet = map.get(user.getId());
		if (roleSet == null || !roleSet.remove(role)) {
			return null;
		}
		if (roleSet.isEmpty()) {
			map.remove(user.getId());
		}
		return role;
	}

	public synchronized void setRoleSet(final User user, final CustomRoleSet roleSet) {
		if (roleSet.isEmpty()) {
			map.remove(user.getId());
			return;
		}
		map.put(user.getId(), roleSet);
	}

}
