package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.field.TileType;
import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;
import org.kareha.hareka.server.game.ability.Ability;
import org.kareha.hareka.server.game.entity.CharacterEntity;

public final class AddAbilityPacket extends GamePacket {

	public AddAbilityPacket(final Ability ability, final CharacterEntity entity) {
		out.write(ability.getType());
		out.writeCompactUInt(ability.getReach());
		out.write(ability.getWaitType());
		out.writeCompactUInt(ability.getWait(entity, TileType.NULL));
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.ADD_ABILITY;
	}

}
