package org.kareha.hareka.server.game.ability;

import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.game.AbilityType;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.field.ServerField;
import org.kareha.hareka.server.game.field.ServerFieldObject;
import org.kareha.hareka.wait.WaitType;

public class BreakWallAbility implements Ability {

	@Override
	public AbilityType getType() {
		return AbilityType.BREAK_WALL;
	}

	@Override
	public int getReach() {
		return 1;
	}

	@Override
	public WaitType getWaitType() {
		return WaitType.ATTACK;
	}

	@Override
	public int getWait(final CharacterEntity entity, final TileType tileType) {
		return entity.getStat().getAttackWait(tileType);
	}

	@Override
	public void use(final CharacterEntity entity, final Vector target) {
		if (entity.isStuck()) {
			return;
		}
		final ServerFieldObject fo = entity.getFieldObject();
		if (fo == null) {
			return;
		}
		final ServerField field = fo.getField();
		final Tile tile = field.getTile(target);
		final TileType toTileType;
		switch (tile.type()) {
		default:
			return;
		case EARTH:
			toTileType = TileType.SAND;
			break;
		case WALL:
			toTileType = TileType.FLOOR;
			break;
		case WATER:
			toTileType = TileType.GRASS;
			break;
		}
		final Tile toTile = Tile.valueOf(toTileType, tile.elevation());
		field.setAndSyncTile(target, toTile, false);
	}

}
