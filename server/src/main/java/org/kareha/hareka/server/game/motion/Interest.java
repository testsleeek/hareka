package org.kareha.hareka.server.game.motion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.server.game.entity.CharacterEntity;

public class Interest {

	private static class Entry {

		final CharacterEntity entity;
		@GuardedBy("Interest.this")
		private long value;

		Entry(final CharacterEntity entity, final int initialValue) {
			this.entity = entity;
			value = initialValue;
		}

		long getValue() {
			return value;
		}

		void addValue(final int v) {
			value += v;
		}

	}

	private static final Comparator<Entry> entryComparator = new Comparator<Entry>() {
		@Override
		public int compare(final Entry o1, final Entry o2) {
			return Long.compare(o1.getValue(), o2.getValue());
		}
	};

	@GuardedBy("this")
	private List<Entry> list;

	public synchronized boolean isEmpty() {
		return list == null;
	}

	@GuardedBy("this")
	private Entry getEntry(final CharacterEntity entity) {
		if (list == null) {
			return null;
		}
		for (final Entry entry : list) {
			if (entry.entity == entity) {
				return entry;
			}
		}
		return null;
	}

	public synchronized void addValue(final CharacterEntity entity, final int value) {
		final Entry entry = getEntry(entity);
		if (entry != null) {
			entry.addValue(value);
		} else {
			final Entry newEntry = new Entry(entity, value);
			if (list == null) {
				list = new ArrayList<>();
			}
			list.add(newEntry);
		}
	}

	public synchronized CharacterEntity decideTarget() {
		if (list == null || list.isEmpty()) {
			return null;
		}
		Collections.sort(list, entryComparator);
		return list.get(list.size() - 1).entity;
	}

	public synchronized void removeEntity(final CharacterEntity entity) {
		if (list == null) {
			return;
		}
		for (final Iterator<Entry> i = list.iterator(); i.hasNext();) {
			final Entry entry = i.next();
			if (entry.entity == entity) {
				i.remove();
				break;
			}
		}
		if (list.isEmpty()) {
			list = null;
		}
	}

}
