package org.kareha.hareka.server.game.item;

import org.kareha.hareka.field.TileType;
import org.kareha.hareka.game.ItemType;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.entity.FieldEntity;
import org.kareha.hareka.server.game.entity.ItemEntity;
import org.kareha.hareka.util.Name;
import org.kareha.hareka.wait.WaitType;

public class CoinItem implements Item {

	private final Name name;

	public CoinItem() {
		name = new Name("Coin");
		name.put("ja", "コイン");
	}

	@Override
	public ItemType getType() {
		return ItemType.COIN;
	}

	@Override
	public Name getName() {
		return name;
	}

	@Override
	public String getShape() {
		return "Coin";
	}

	@Override
	public boolean isExclusive() {
		return false;
	}

	@Override
	public boolean isStackable() {
		return true;
	}

	@Override
	public boolean isConsumable() {
		return false;
	}

	@Override
	public int getWeight() {
		return 16;
	}

	@Override
	public int getReach() {
		return 1;
	}

	@Override
	public WaitType getWaitType() {
		return WaitType.ATTACK;
	}

	@Override
	public int getWait(final CharacterEntity entity, final TileType tileType) {
		return entity.getStat().getAttackWait(tileType);
	}

	@Override
	public void use(final CharacterEntity characterEntity, final ItemEntity itemEntity, final FieldEntity target) {
		// TODO item function
		characterEntity.sendMessage("not implemented");
	}

}
