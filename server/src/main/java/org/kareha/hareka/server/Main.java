package org.kareha.hareka.server;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.server.game.GameServerConstants;
import org.kareha.hareka.server.game.GameServerParameters;
import org.kareha.hareka.server.game.Starter;
import org.kareha.hareka.server.resource.ResourceServerConstants;
import org.kareha.hareka.server.resource.ResourceServerParameters;
import org.kareha.hareka.server.resource.ResourceStarter;
import org.kareha.hareka.ui.console.ConsolePasswordReader;
import org.kareha.hareka.util.FileLogger;
import org.kareha.hareka.util.FileUtil;
import org.kareha.hareka.util.KeyStoreUtil;
import org.kareha.hareka.util.LogUtil;
import org.kareha.hareka.util.SingleLaunch;

final class Main {

	private static final Logger logger = Logger.getLogger(Main.class.getName());
	private static Logger appRootLogger;

	private Main() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		appRootLogger = Logger.getLogger("org.kareha.hareka");
		LogUtil.loadLogLevel(appRootLogger);
		final ServerParameters parameters = new ServerParameters(args);
		final GameServerParameters gameParameters = new GameServerParameters(parameters);
		final ResourceServerParameters resourceParameters = new ResourceServerParameters(parameters);

		FileUtil.ensureDirectoryExistsOrExit(gameParameters.getDataDirectory(), gameParameters.getUiType());
		FileUtil.ensureDirectoryExistsOrExit(resourceParameters.getDataDirectory(), resourceParameters.getUiType());

		SingleLaunch.INSTANCE.ensure(parameters.getDataDirectory(), parameters.getUiType());
		FileLogger.connect(appRootLogger, parameters.getDataDirectory(), parameters.getUiType());
		Thread.setDefaultUncaughtExceptionHandler((t, e) -> logger.log(Level.SEVERE, t.getName(), e));

		if (gameParameters.isEnabled()) {
			final File gameKeyStoreFile = new File(gameParameters.getDataDirectory(),
					GameServerConstants.KEY_STORE_FILENAME);
			if (!gameKeyStoreFile.exists()) {
				KeyStoreUtil.generate(gameParameters.getDataDirectory(), GameServerConstants.KEY_STORE_PASSWORD,
						GameServerConstants.KEY_PASSWORD, "localhost");
			}
		}

		if (resourceParameters.isEnabled()) {
			final File resourceKeyStoreFile = new File(resourceParameters.getDataDirectory(),
					ResourceServerConstants.KEY_STORE_FILENAME);
			if (!resourceKeyStoreFile.exists()) {
				KeyStoreUtil.generate(resourceParameters.getDataDirectory(), ResourceServerConstants.KEY_STORE_PASSWORD,
						ResourceServerConstants.KEY_PASSWORD, "localhost");
			}
		}

		switch (parameters.getUiType()) {
		default:
			throw new AssertionError();
		case SWING:
			SwingUtilities.invokeLater(() -> new MainFrame(parameters).setVisible(true));
			break;
		case CONSOLE:
			if (resourceParameters.isEnabled()) {
				try {
					ResourceStarter.start(resourceParameters.getDataDirectory(), resourceParameters.getPort(),
							resourceParameters.isNoTls(), new ConsolePasswordReader());
				} catch (final ServerException e) {
					System.err.println(e.getMessage());
					return;
				} catch (final IOException | JAXBException e) {
					logger.log(Level.SEVERE, "", e);
					return;
				}
			}
			if (gameParameters.isEnabled()) {
				try {
					Starter.start(gameParameters.getDataDirectory(), gameParameters.getPort(), gameParameters.isNoTls(),
							new ConsolePasswordReader());
				} catch (final ServerException e) {
					System.err.println(e.getMessage());
					return;
				} catch (final IOException | JAXBException e) {
					logger.log(Level.SEVERE, "", e);
					return;
				}
			}
			break;
		}
	}

}
