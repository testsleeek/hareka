package org.kareha.hareka.server.game.field;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.kareha.hareka.field.ArrayListHashMap;
import org.kareha.hareka.field.FieldObject;
import org.kareha.hareka.field.ObjectField;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.field.Vectors;

public class ServerObjectField implements ObjectField {

	private final ServerField field;
	private final int viewSize;
	private final ArrayListHashMap<Vector, FieldObject> objectMap = new ArrayListHashMap<>();

	public ServerObjectField(final ServerField field, final int viewSize) {
		this.field = field;
		this.viewSize = viewSize;
	}

	@Override
	public int getViewSize() {
		return viewSize;
	}

	@Override
	public boolean addObject(final FieldObject object) {
		final Vector position = field.getBoundary().confine(object.getPlacement().getPosition());
		if (!objectMap.put(position, object)) {
			return false;
		}
		final List<FieldObject> list = getInViewObjects(position);
		Collections.shuffle(list);
		for (final FieldObject o : list) {
			o.addInViewObject(object);
			if (o != object) {
				object.addInViewObject(o);
			}
		}
		return true;
	}

	@Override
	public boolean removeObject(final FieldObject object) {
		if (!objectMap.remove(object)) {
			return false;
		}
		final List<FieldObject> list = getInViewObjects(object.getPlacement().getPosition());
		Collections.shuffle(list);
		for (final FieldObject o : list) {
			o.removeInViewObject(object);
			if (o != object) {
				object.removeInViewObject(o);
			}
		}
		return true;
	}

	@Override
	public boolean moveObject(final FieldObject object) {
		return objectMap.move(field.getBoundary().confine(object.getPlacement().getPosition()), object);
	}

	public Collection<FieldObject> getObjects() {
		return objectMap.values();
	}

	@Override
	public List<FieldObject> getObjects(final Vector position) {
		return objectMap.get(field.getBoundary().confine(position));
	}

	public List<FieldObject> getObjects(final Vector center, final int size) {
		final List<FieldObject> list = new ArrayList<>();
		for (final Vector p : Vectors.range(center, size)) {
			list.addAll(getObjects(p));
		}
		if (list.isEmpty()) {
			return Collections.emptyList();
		}
		return list;
	}

	@Override
	public List<FieldObject> getInViewObjects(final Vector center) {
		return getObjects(center, viewSize);
	}

	@Override
	public boolean isExclusive(final Vector position) {
		for (final FieldObject fo : getObjects(position)) {
			if (fo.isExclusive()) {
				return true;
			}
		}
		return false;
	}

}
