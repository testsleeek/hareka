package org.kareha.hareka.server.game.entity;

import java.util.Collection;

import org.kareha.hareka.util.Name;

public interface ChatEntity extends Entity {

	String getName(String language);

	Name getName();

	void setName(Name name);

	void sendLocalChat(String content);

	void receiveLocalChat(ChatEntity speaker, String content);

	void sendPrivateChat(ChatEntity peer, String content);

	void receivePrivateChat(ChatEntity speaker, String content);

	void addChatDriver(ChatDriver driver);

	boolean removeChatDriver(ChatDriver driver);

	Collection<ChatDriver> getChatDrivers();

}
