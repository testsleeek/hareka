package org.kareha.hareka.server.resource;

import java.io.File;

import org.kareha.hareka.server.ServerParameters;
import org.kareha.hareka.ui.UiType;

public class ResourceServerParameters {

	private final UiType uiType;
	private final File dataDirectory;
	private final int port;
	private final boolean noTls;
	private final boolean enabled;

	public ResourceServerParameters(final ServerParameters parameters) {
		uiType = parameters.getUiType();
		dataDirectory = new File(parameters.getDataDirectory(), "resource");
		port = parameters.getResourceInternalPort();
		noTls = parameters.isNoTls();
		enabled = !parameters.isGameServerOnly();
	}

	public UiType getUiType() {
		return uiType;
	}

	public File getDataDirectory() {
		return dataDirectory;
	}

	public int getPort() {
		return port;
	}

	public boolean isNoTls() {
		return noTls;
	}

	public boolean isEnabled() {
		return enabled;
	}

}
