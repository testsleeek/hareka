package org.kareha.hareka.server.game.entity;

import java.io.File;
import java.util.concurrent.atomic.AtomicLong;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class EntityStatic {

	private final File file;
	@Private
	final AtomicLong count;

	public EntityStatic(final File file) throws JAXBException {
		this.file = file;
		final Long v = load();
		if (v == null) {
			count = new AtomicLong();
		} else {
			count = new AtomicLong(v);
		}
	}

	@XmlRootElement(name = "entityStatic")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		@Private
		String count;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final EntityStatic v) {
			count = Long.toHexString(v.count.get());
		}

	}

	private Long load() throws JAXBException {
		if (!file.isFile()) {
			return null;
		}
		final Adapted adapted = JaxbUtil.unmarshal(file, Adapted.class);
		return Long.parseLong(adapted.count, 16);
	}

	public void save() throws JAXBException {
		JaxbUtil.marshal(new Adapted(this), file);
	}

	public EntityId next() {
		return EntityId.valueOf(count.getAndIncrement());
	}

}
