package org.kareha.hareka.server.game.parser;

import java.util.ResourceBundle;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;

public final class RemoveOrphansParser implements Parser<Session> {

	private enum BundleKey {
		YouCannotUseThisFunction,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(RemoveOrphansParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		final User user = session.getUser();
		if (user == null) {
			return;
		}
		final RoleSet roles = session.getContext().getAccessController().getRoleSet(user);
		if (!roles.isAbleTo(Permission.DELETE_ENTITIES)) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			return;
		}

		session.getContext().getEntities().removeOrphans();
	}

}
