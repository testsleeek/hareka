package org.kareha.hareka.server.game.ability;

import org.kareha.hareka.field.TileType;
import org.kareha.hareka.game.AbilityType;
import org.kareha.hareka.game.Power;
import org.kareha.hareka.server.game.Global;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.entity.FieldEntity;
import org.kareha.hareka.wait.WaitType;

public class ReviveAbility implements Ability {

	@Override
	public AbilityType getType() {
		return AbilityType.REVIVE;
	}

	@Override
	public int getReach() {
		return 1;
	}

	@Override
	public WaitType getWaitType() {
		return WaitType.ATTACK;
	}

	@Override
	public int getWait(final CharacterEntity entity, final TileType tileType) {
		return entity.getStat().getAttackWait(tileType);
	}

	@Override
	public void use(final CharacterEntity entity, final FieldEntity target) {
		if (target == entity) {
			return;
		}
		if (!(target instanceof CharacterEntity)) {
			return;
		}
		final CharacterEntity characterEntity = (CharacterEntity) target;
		if (characterEntity.getStat().getHealthPoints().isAlive()) {
			return;
		}
		if (characterEntity.revive()) {
			if (entity.getStat().hasPower(Power.VOLATILE)) {
				entity.died();
				entity.unmount();
				Global.INSTANCE.context().getEntities().destroyEntity(entity);
			}
		}
	}

}
