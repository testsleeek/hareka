package org.kareha.hareka.server.game.parser;

import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.game.Power;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.CharacterEntity;

public final class LocalChatParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(LocalChatParser.class.getName());

	private enum BundleKey {
		LoginCharacterFirst, ThisCharacterCannotSpeak,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(LocalChatParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final String content = in.readString();

		// check state
		final Player player = session.getPlayer();
		if (player == null) {
			inform(session, BundleKey.LoginCharacterFirst.name());
			logger.fine(session.getStamp() + "No player");
			return;
		}
		final CharacterEntity entity = player.getEntity();
		if (!entity.getStat().hasPower(Power.ABLE_TO_SPEAK)) {
			inform(session, BundleKey.ThisCharacterCannotSpeak.name());
			return;
		}
		entity.sendLocalChat(content);
	}

}
