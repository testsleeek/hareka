package org.kareha.hareka.server.game.parser;

import java.io.IOException;
import java.util.Collection;
import java.util.EnumSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.packet.RoleListPacket;
import org.kareha.hareka.server.game.user.CustomRole;
import org.kareha.hareka.server.game.user.RolesLogRecord;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.Role;
import org.kareha.hareka.user.RoleSet;

public final class DefineRoleParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(DefineRoleParser.class.getName());

	private enum BundleKey {
		PermissionNotFound,

		LoginUserFirst, YouCannotUseThisFunction, InsufficientRank,

		PermissionCannotBeIncluded, PermissionCannotBeRemoved,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(DefineRoleParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		// read arguments
		final CustomRole role = CustomRole.readFrom(in);

		// check state
		final User user = session.getUser();
		if (user == null) {
			inform(session, BundleKey.LoginUserFirst.name());
			return;
		}
		final RoleSet roleSet = session.getContext().getAccessController().getRoleSet(user);
		final Role manageRole = roleSet.getHighestRole(Permission.MANAGE_ROLES);
		if (manageRole == null) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			return;
		}
		if (manageRole.getRank() <= role.getRank()) {
			inform(session, BundleKey.InsufficientRank.name());
			return;
		}
		final Collection<Permission> permissions = role.getPermissions();
		for (final Permission permission : permissions) {
			final Role myRole = roleSet.getHighestRole(permission);
			if (myRole == null) {
				inform(session, BundleKey.PermissionCannotBeIncluded.name());
				return;
			}
			if (myRole.getRank() <= role.getRank()) {
				inform(session, BundleKey.PermissionCannotBeIncluded.name());
				return;
			}
		}
		final Role oldRole = session.getContext().getRoles().get(role.getId());
		if (oldRole != null) {
			final Set<Permission> oldPermissions = EnumSet.copyOf(oldRole.getPermissions());
			for (final Permission permission : oldPermissions) {
				if (!permissions.contains(permission)) {
					final Role myRole = roleSet.getHighestRole(permission);
					if (myRole == null) {
						inform(session, BundleKey.PermissionCannotBeRemoved.name());
						return;
					}
					if (myRole.getRank() <= oldRole.getRank()) {
						inform(session, BundleKey.PermissionCannotBeRemoved.name());
						return;
					}
				}
			}
		}

		// process
		if (session.getContext().getRoles().add(role)) {
			session.getContext().getAccessController().updateDefaultRoleSet();
			try {
				session.getContext().getRolesLogger().add(new RolesLogRecord(user.getId(), role));
			} catch (final IOException | JAXBException e) {
				logger.log(Level.SEVERE, "", e);
			}
			session.write(new RoleListPacket(session.getContext().getRoles(), user,
					session.getContext().getAccessController()));
		}
	}

}
