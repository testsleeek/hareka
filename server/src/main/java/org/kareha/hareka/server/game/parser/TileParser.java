package org.kareha.hareka.server.game.parser;

import java.math.BigInteger;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.kareha.hareka.Constants;
import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.pow.PowGrabber;
import org.kareha.hareka.server.game.GameServerConstants;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.field.ServerField;
import org.kareha.hareka.server.game.field.ServerFieldObject;
import org.kareha.hareka.server.game.packet.RequestPowPacket;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.user.Permission;
import org.kareha.hareka.user.RoleSet;

public final class TileParser implements Parser<Session> {

	private static final Logger logger = Logger.getLogger(TileParser.class.getName());

	private enum BundleKey {
		YouCannotUseThisFunction, LoginCharacterFirst, ThisCharacterCannotEditTileFields, PowTitle,
	}

	private static void inform(final Session session, final String key) {
		final ResourceBundle bundle = session.getBundle(TileParser.class.getName());
		session.writeInformPacket(bundle.getString(key));
	}

	@Override
	public void handle(final PacketInput in, final Session session) {
		final TilePiece tilePiece = TilePiece.readFrom(in);

		final User user = session.getUser();
		if (user == null) {
			return;
		}
		final RoleSet roles = session.getContext().getAccessController().getRoleSet(user);
		if (!roles.isAbleTo(Permission.EDIT_TILE_FIELDS)) {
			inform(session, BundleKey.YouCannotUseThisFunction.name());
			logger.fine(session.getStamp() + "User cannot edit tile fields");
			return;
		}
		final boolean force = roles.isAbleTo(Permission.FORCE_EDIT_TILE_FIELDS);

		final Player player = session.getPlayer();
		if (player == null) {
			inform(session, BundleKey.LoginCharacterFirst.name());
			logger.fine(session.getStamp() + "Character not logged in");
			return;
		}
		final CharacterEntity entity = player.getEntity();
		if (!entity.isAbleToEditTileFields()) {
			inform(session, BundleKey.ThisCharacterCannotEditTileFields.name());
			return;
		}
		final TilePiece realTilePiece = tilePiece.invert(player.getTransformation());
		final ServerFieldObject fo = entity.getFieldObject();
		if (fo == null) {
			return;
		}
		final ServerField field = fo.getField();
		if (field.getBoundary().distance(fo.getPlacement().getPosition(), realTilePiece.position()) >= field
				.getViewSize()) {
			return;
		}
		if (field.getTile(realTilePiece.position()).type().isSpecial()) {
			return;
		}
		if (realTilePiece.tile().type().isSpecial()) {
			return;
		}

		final PowGrabber.Handler powHandler = new PowGrabber.Handler() {
			@Override
			public void handle(boolean result) {
				if (!result) {
					return;
				}
				field.setAndSyncTile(realTilePiece.position(), realTilePiece.tile(), force);
			}

			@Override
			public void canceled() {
				// nothing to do
			}
		};
		if (roles.isAbleTo(Permission.EDIT_TILE_FIELDS_WITHOUT_POW)) {
			powHandler.handle(true);
			return;
		}
		final BigInteger target = BigInteger.ONE.shiftLeft(256 - GameServerConstants.DRAWING_POW_BASE_SCALE);
		final PowGrabber.Entry powEntry = session.getPowGrabber().add(powHandler, target);
		if (powEntry == null) {
			logger.severe(session.getStamp() + "PowGrabber overflow");
			return;
		}
		final byte[] data = powEntry.newData(Constants.POW_DATA_BYTE_LENGTH);
		final ResourceBundle bundle = session.getBundle(TileParser.class.getName());
		session.write(
				new RequestPowPacket(powEntry.getId(), target, data, bundle.getString(BundleKey.PowTitle.name())));
	}

}
