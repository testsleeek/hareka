package org.kareha.hareka.server.game.parser;

import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.entity.CharacterEntity;
import org.kareha.hareka.server.game.field.ServerFieldObject;
import org.kareha.hareka.server.game.packet.PositionMemoryPacket;
import org.kareha.hareka.server.game.user.User;

public final class GetPositionMemoryParser implements Parser<Session> {

	@Override
	public void handle(final PacketInput in, final Session session) {
		final User user = session.getUser();
		if (user == null) {
			return;
		}

		final Player player = session.getPlayer();
		if (player == null) {
			return;
		}
		final CharacterEntity entity = player.getEntity();
		final ServerFieldObject fo = entity.getFieldObject();
		if (fo == null) {
			return;
		}
		session.write(new PositionMemoryPacket(entity, fo.getField(), fo.getPlacement().getPosition(),
				user.getStorageCipher()));
	}

}
