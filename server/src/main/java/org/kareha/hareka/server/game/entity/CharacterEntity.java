package org.kareha.hareka.server.game.entity;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.field.Direction;
import org.kareha.hareka.field.Field;
import org.kareha.hareka.field.FieldObject;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.game.Power;
import org.kareha.hareka.key.KeyId;
import org.kareha.hareka.server.game.Global;
import org.kareha.hareka.server.game.Inventory;
import org.kareha.hareka.server.game.Player;
import org.kareha.hareka.server.game.Session;
import org.kareha.hareka.server.game.field.FieldId;
import org.kareha.hareka.server.game.field.ServerField;
import org.kareha.hareka.server.game.field.ServerFieldObject;
import org.kareha.hareka.server.game.motion.Motion;
import org.kareha.hareka.server.game.motion.NormalMotion;
import org.kareha.hareka.server.game.packet.MessagePacket;
import org.kareha.hareka.server.game.packet.WaitPacket;
import org.kareha.hareka.server.game.stat.AbstractCharacterStat;
import org.kareha.hareka.server.game.stat.CharacterStat;
import org.kareha.hareka.server.game.stat.LongStatPoints;
import org.kareha.hareka.server.game.stat.SkillType;
import org.kareha.hareka.server.game.stat.SkillVariation;
import org.kareha.hareka.server.game.stat.StatPoints;
import org.kareha.hareka.server.game.stat.StatResult;
import org.kareha.hareka.server.game.stat.VisualType;
import org.kareha.hareka.server.game.user.User;
import org.kareha.hareka.util.Name;
import org.kareha.hareka.wait.Wait;
import org.kareha.hareka.wait.WaitGenerator;

@ThreadSafe
public class CharacterEntity extends AbstractEntity implements ChatEntity {

	private static final int ACTION_DURATION = 2000;
	private static final int ATTACK_VISIBLE_DURATION = 2000;
	private static final Random random = new SecureRandom();

	public static class Builder extends AbstractEntity.Builder {

		protected Name name;
		protected CharacterStat stat;
		protected List<KeyId> owners;
		protected Inventory inventory;

		public Builder name(final Name v) {
			if (v == null) {
				name = null;
			} else {
				name = new Name(v);
			}
			return this;
		}

		public Builder stat(final CharacterStat v) {
			stat = v;
			return this;
		}

		public Builder owners(final Collection<KeyId> v) {
			if (v == null) {
				owners = null;
			} else {
				owners = new ArrayList<>(v);
			}
			return this;
		}

		public Builder inventory(final Inventory v) {
			inventory = v;
			return this;
		}

		@Override
		public CharacterEntity build(final EntityId id) {
			return new CharacterEntity(id, this);
		}

	}

	@GuardedBy("this")
	protected Name name;
	protected final List<ChatDriver> chatDrivers = new CopyOnWriteArrayList<>();
	protected final CharacterStat stat;
	@GuardedBy("this")
	protected List<KeyId> owners;
	@GuardedBy("this")
	protected Inventory inventory;
	@GuardedBy("this")
	protected Motion motion;
	@GuardedBy("this")
	protected HealthRegenerator healthRegenerator;
	@GuardedBy("this")
	protected MagicRegenerator magicRegenerator;
	@GuardedBy("this")
	protected WaitGenerator waitGenerator;
	protected volatile long lastActionTime;

	protected CharacterEntity(final EntityId id, final Builder builder) {
		super(id, builder);
		name = builder.name;
		stat = builder.stat;
		owners = builder.owners;
		inventory = builder.inventory;
	}

	@XmlRootElement(name = "characterEntity")
	@XmlType(name = "characterEntity")
	@XmlAccessorType(XmlAccessType.NONE)
	protected static class Adapted extends AbstractEntity.Adapted {

		@XmlElement
		protected Name name;
		@XmlElement
		protected AbstractCharacterStat stat;
		@XmlElement(name = "owner")
		protected List<KeyId> owners;
		@XmlElement
		protected Inventory inventory;

		protected Adapted() {
			// used by JAXB
		}

		protected Adapted(final CharacterEntity v) {
			super(v);
			synchronized (v) {
				if (v.name == null) {
					name = null;
				} else {
					name = new Name(v.name);
				}
				if (v.owners != null) {
					owners = new ArrayList<>(v.owners);
				}
				if (v.inventory != null) {
					synchronized (v.inventory) {
						if (!v.inventory.isEmpty()) {
							inventory = new Inventory(v.inventory);
						}
					}
				}
			}
			if (v.stat instanceof AbstractCharacterStat) {
				stat = (AbstractCharacterStat) v.stat;
			}
		}

		@Override
		protected CharacterEntity unmarshal() {
			final Builder builder = new Builder();
			builder.name(name);
			builder.shape(shape);
			if (stat != null) {
				builder.stat(stat);
			} else {
				// TODO set default CharacterStat
			}
			builder.owners(owners);
			builder.inventory(inventory);
			builder.fieldId(fieldId);
			builder.placement(placement);
			builder.transformation(transformation);
			builder.mounted(mounted);
			return builder.build(id);
		}

	}

	@Override
	public Adapted marshal() {
		return new Adapted(this);
	}

	@Override
	public synchronized String getName(final String language) {
		if (name == null) {
			return "";
		}
		return name.get(language);
	}

	@Override
	public synchronized Name getName() {
		if (name == null) {
			return new Name("");
		}
		return name;
	}

	@Override
	public synchronized void setName(final Name name) {
		if (name == null) {
			this.name = null;
		} else {
			if (name.isEmpty()) {
				this.name = null;
			} else {
				this.name = name;
			}
		}
	}

	@Override
	public void sendLocalChat(final String content) {
		final ServerFieldObject fo = getFieldObject();
		if (fo == null) {
			return;
		}
		final List<FieldObject> list = fo.getField().getInViewObjects(fo.getPlacement().getPosition());
		Collections.shuffle(list);
		for (final FieldObject o : list) {
			final Entity entity = ((ServerFieldObject) o).getFieldEntity();
			if (entity instanceof ChatEntity) {
				final ChatEntity chatEntity = (ChatEntity) entity;
				chatEntity.receiveLocalChat(this, content);
			}
		}
	}

	@Override
	public void receiveLocalChat(final ChatEntity speaker, final String content) {
		for (final ChatDriver driver : chatDrivers) {
			driver.handleReceiveLocalChat(speaker, content);
		}
	}

	@Override
	public void sendPrivateChat(final ChatEntity peer, final String content) {
		for (final ChatDriver driver : chatDrivers) {
			driver.handleSendPrivateChatEcho(peer, content);
		}
		peer.receivePrivateChat(this, content);
	}

	@Override
	public void receivePrivateChat(final ChatEntity peer, final String content) {
		for (final ChatDriver driver : chatDrivers) {
			driver.handleReceivePrivateChat(peer, content);
		}
	}

	@Override
	public void addChatDriver(final ChatDriver driver) {
		chatDrivers.add(driver);
	}

	@Override
	public boolean removeChatDriver(final ChatDriver driver) {
		return chatDrivers.remove(driver);
	}

	@Override
	public Collection<ChatDriver> getChatDrivers() {
		return new ArrayList<>(chatDrivers);
	}

	@Override
	public int getMotionWait(final TileType tileType) {
		return stat.getMotionWait(tileType);
	}

	@Override
	public CharacterStat getStat() {
		return stat;
	}

	protected Motion createMotion() {
		return new NormalMotion(this);
	}

	@Override
	public synchronized void startMotion() {
		if (motion != null) {
			motion.stopMotion();
		}
		motion = createMotion();
		if (motion != null) {
			motion.startMotion();
		}
	}

	@Override
	public synchronized void stopMotion() {
		if (motion != null) {
			motion.stopMotion();
			motion = null;
		}
	}

	public void updateLastActionTime() {
		lastActionTime = System.currentTimeMillis();
	}

	public boolean isActionExpired(final int duration) {
		return System.currentTimeMillis() >= lastActionTime + duration;
	}

	public boolean regenerateHealth() {
		final StatResult result;
		synchronized (stat) {
			if (!stat.getHealthPoints().isAlive()) {
				return false;
			}
			if (!isActionExpired(ACTION_DURATION)) {
				return true;
			}
			result = stat.addHealthPoints(stat.getSkill(SkillType.RECOVERY));
		}
		if (result == null) {
			return false;
		}
		stat.getSkills().get(SkillType.RECOVERY).addValue(SkillVariation.RECOVERY_INCREMENT);
		sendHealthBar(result.getPoints());
		return true;
	}

	public boolean regenerateMagic() {
		final StatResult result;
		synchronized (stat) {
			if (!stat.getHealthPoints().isAlive()) {
				return false;
			}
			if (!isActionExpired(ACTION_DURATION)) {
				return true;
			}
			result = stat.addMagicPoints(stat.getSkill(SkillType.MAGIC_RECOVERY));
		}
		if (result == null) {
			return false;
		}
		stat.getSkills().get(SkillType.MAGIC_RECOVERY).addValue(SkillVariation.MAGIC_RECOVERY_INCREMENT);
		sendMagicBar(result.getPoints());
		return true;
	}

	private class HealthRegenerator implements Runnable {

		private static final int REGENERATION_WAIT = 9000;

		@GuardedBy("this")
		private ScheduledFuture<?> future;

		@Private
		HealthRegenerator() {

		}

		public synchronized void startRegeneration() {
			if (future == null) {
				final StatPoints hp = stat.getHealthPoints();
				if (hp.isAlive() && hp.isDamaged()) {
					future = Global.INSTANCE.scheduledExecutor().scheduleWithFixedDelay(this, REGENERATION_WAIT,
							REGENERATION_WAIT, TimeUnit.MILLISECONDS);
				}
			}
		}

		public synchronized void stopRegeneration() {
			if (future != null) {
				future.cancel(false);
				future = null;
			}
		}

		@Override
		public void run() {
			if (!regenerateHealth()) {
				synchronized (CharacterEntity.this) {
					if (healthRegenerator != null) {
						healthRegenerator.stopRegeneration();
						healthRegenerator = null;
					}
				}
			}
		}

	}

	private class MagicRegenerator implements Runnable {

		private static final int REGENERATION_WAIT = 9000;

		@GuardedBy("this")
		private ScheduledFuture<?> future;

		@Private
		MagicRegenerator() {

		}

		public synchronized void startRegeneration() {
			if (future == null) {
				final StatPoints hp = stat.getHealthPoints();
				final StatPoints mp = stat.getMagicPoints();
				if (hp.isAlive() && mp.isDamaged()) {
					future = Global.INSTANCE.scheduledExecutor().scheduleWithFixedDelay(this, REGENERATION_WAIT,
							REGENERATION_WAIT, TimeUnit.MILLISECONDS);
				}
			}
		}

		public synchronized void stopRegeneration() {
			if (future != null) {
				future.cancel(false);
				future = null;
			}
		}

		@Override
		public void run() {
			if (!regenerateMagic()) {
				synchronized (CharacterEntity.this) {
					if (magicRegenerator != null) {
						magicRegenerator.stopRegeneration();
						magicRegenerator = null;
					}
				}
			}
		}

	}

	@Override
	public synchronized void startHealthRegeneration() {
		if (healthRegenerator == null) {
			final StatPoints hp = stat.getHealthPoints();
			if (hp.isAlive() && hp.isDamaged()) {
				healthRegenerator = new HealthRegenerator();
				healthRegenerator.startRegeneration();
			}
		}
	}

	@Override
	public synchronized void stopHealthRegeneration() {
		if (healthRegenerator != null) {
			healthRegenerator.stopRegeneration();
			healthRegenerator = null;
		}
	}

	@Override
	public synchronized void startMagicRegeneration() {
		if (magicRegenerator == null) {
			final StatPoints hp = stat.getHealthPoints();
			final StatPoints mp = stat.getMagicPoints();
			if (hp.isAlive() && mp.isDamaged()) {
				magicRegenerator = new MagicRegenerator();
				magicRegenerator.startRegeneration();
			}
		}
	}

	@Override
	public synchronized void stopMagicRegeneration() {
		if (magicRegenerator != null) {
			magicRegenerator.stopRegeneration();
			magicRegenerator = null;
		}
	}

	@Override
	public int getJumpDown() {
		return stat.getJumpDown();
	}

	@Override
	public int getJumpUp() {
		return stat.getJumpUp();
	}

	@Override
	public VisualType getVisualType() {
		return stat.getVisualType();
	}

	@Override
	public boolean isVisible(final VisualType visualType) {
		return stat.isVisible(visualType);
	}

	public synchronized boolean addOwner(final KeyId v) {
		if (owners != null) {
			if (owners.contains(v)) {
				return false;
			}
		}
		if (owners == null) {
			owners = new ArrayList<>();
		}
		owners.add(v);
		return true;
	}

	public synchronized boolean removeOwner(final KeyId v) {
		if (owners == null) {
			return false;
		}
		final boolean result = owners.remove(v);
		if (owners.isEmpty()) {
			owners = null;
		}
		return result;
	}

	public synchronized Collection<KeyId> getOwners() {
		if (owners == null) {
			return Collections.emptyList();
		}
		return owners;
	}

	public synchronized Inventory getInventory() {
		if (inventory == null) {
			inventory = new Inventory(id);
		}
		return inventory;
	}

	public LongStatPoints getWeightPoints() {
		return new LongStatPoints(stat.getWeightCapacity(), getInventory().getWeight());
	}

	public boolean isStuck() {
		return getInventory().getWeight() >= stat.getWeightCapacity();
	}

	public synchronized WaitGenerator getWaitGenerator() {
		if (waitGenerator == null) {
			waitGenerator = new WaitGenerator();
		}
		return waitGenerator;
	}

	public void sendWait(final Wait wait) {
		for (final FieldDriver driver : getFieldDrivers()) {
			if (!(driver instanceof Player)) {
				continue;
			}
			final Player player = (Player) driver;
			player.getSession().write(new WaitPacket(wait.penalize(player.getWaitPenalty().get(wait.getType()))));
		}
	}

	public void sendMessage(final String message) {
		for (final ChatDriver driver : getChatDrivers()) {
			if (!(driver instanceof Player)) {
				continue;
			}
			final Player player = (Player) driver;
			player.getSession().write(new MessagePacket(message));
		}
	}

	public boolean isAbleToEditTileFields() {
		return !getStat().hasPower(Power.IMMUNE_TO_ATTACK);
	}

	public void handleSetTile(final TilePiece tilePiece) {
		for (final FieldDriver driver : fieldDrivers) {
			driver.handleSetTile(tilePiece);
		}
	}

	public void sendHealthBar(final StatPoints points) {
		final ServerFieldObject fo = getFieldObject();
		final ServerField field = fo.getField();
		final Placement placement = fo.getPlacement();
		for (final FieldObject o : field.getInViewObjects(placement.getPosition())) {
			final FieldEntity entity = ((ServerFieldObject) o).getFieldEntity();
			for (final FieldDriver driver : entity.getFieldDrivers()) {
				driver.handleHealthPointsChanged(fo, points);
			}
		}
	}

	public void sendMagicBar(final StatPoints points) {
		final ServerFieldObject fo = getFieldObject();
		final Field field = fo.getField();
		final Placement placement = fo.getPlacement();
		if (field instanceof ServerField) {
			for (final FieldObject o : field.getInViewObjects(placement.getPosition())) {
				final FieldEntity entity = ((ServerFieldObject) o).getFieldEntity();
				for (final FieldDriver driver : entity.getFieldDrivers()) {
					driver.handleMagicPointsChanged(fo, points);
				}
			}
		}
	}

	public void attack(final CharacterEntity target) {
		final ServerFieldObject fo = getFieldObject();
		final ServerFieldObject targetFo = target.getFieldObject();
		final ServerField field = fo.getField();
		final ServerField targetField = targetFo.getField();
		if (field != targetField) {
			return;
		}
		final Placement placement = fo.getPlacement();
		final Placement targetPlacement = targetFo.getPlacement();
		final int distance = field.getBoundary().distance(placement.getPosition(), targetPlacement.getPosition());
		if (distance > 1) {
			return;
		}
		updateLastActionTime();
		stat.getSkills().get(SkillType.STRENGTH).addValue(SkillVariation.STRENGTH_INCREMENT);
		stat.getSkills().get(SkillType.AGILITY).addValue(SkillVariation.AGILITY_INCREMENT);
		stat.getSkills().get(SkillType.MIND).addValue(SkillVariation.MIND_DECREMENT);
		if (target.beAttacked(this)) {
			if (stat.getHealthPoints().getBar() < 64) {
				stat.getSkills().get(SkillType.HEALTH).addValue(SkillVariation.HEALTH_INCREMENT);
			}
			stat.getSkills().get(SkillType.FANG).addValue(SkillVariation.FANG_INCREMENT);

			targetFo.addVisibleObject(fo, ATTACK_VISIBLE_DURATION);
		}
		final List<FieldObject> objects = field.getInViewObjects(placement.getPosition());
		final List<FieldObject> targetObjects = targetField.getInViewObjects(targetPlacement.getPosition());
		final Set<FieldObject> set = new HashSet<>();
		set.addAll(objects);
		set.addAll(targetObjects);
		for (final FieldObject o : set) {
			final FieldEntity entity = ((ServerFieldObject) o).getFieldEntity();
			for (final FieldDriver driver : entity.getFieldDrivers()) {
				driver.handleAddEffect("Attack", placement.getPosition(), targetPlacement.getPosition());
			}
		}
	}

	protected boolean beAttacked(final CharacterEntity attacker) {
		if (stat.hasPower(Power.IMMUNE_TO_ATTACK)) {
			return false;
		}
		final Motion m;
		synchronized (this) {
			m = motion;
		}
		final int accuracy = attacker.getStat().getAccuracy(stat.getSkill(SkillType.DODGE));
		if (random.nextInt(128) >= accuracy) {
			if (m != null) {
				if (stat.getHealthPoints().isAlive()) {
					m.addHate(attacker, 1);
				}
			}
			attacker.stat.getSkills().get(SkillType.DEXTERITY).addValue(SkillVariation.DEXTERITY_DECREMENT);
			stat.getSkills().get(SkillType.DODGE).addValue(SkillVariation.DODGE_INCREMENT);
			return false;
		}
		attacker.stat.getSkills().get(SkillType.DEXTERITY).addValue(SkillVariation.DEXTERITY_INCREMENT);
		stat.getSkills().get(SkillType.DODGE).addValue(SkillVariation.DODGE_DECREMENT);
		final StatResult result;
		final int attackPoints;
		synchronized (stat) {
			if (!stat.getHealthPoints().isAlive()) {
				return false;
			}
			attackPoints = attacker.getStat().getAttackPoints(stat.getArmorClass());
			result = stat.addHealthPoints(-attackPoints);
		}
		if (result == null) {
			return false;
		}
		if (m != null) {
			m.addHate(attacker, attackPoints);
		}
		stat.getRelationships().addHate(attacker.getId(), attackPoints);
		stat.getSkills().get(SkillType.LOVE).addValue(-attackPoints);
		stat.getSkills().get(SkillType.HATE).addValue(attackPoints * 4);
		stat.getSpecies().getRelationships().addHate(attacker.getStat().getSpecies().getId(), attackPoints);
		if (attacker.stat.getSpecies().getRelationships().getLove(stat.getSpecies().getId())
				+ attacker.stat.getRelationships().getLove(id) > attacker.stat.getSpecies().getRelationships()
						.getHate(stat.getSpecies().getId()) + attacker.stat.getRelationships().getHate(id)) {
			attacker.stat.getSkills().get(SkillType.UNSOUNDNESS).addValue(attackPoints * 4);
		} else {
			attacker.stat.getSkills().get(SkillType.UNSOUNDNESS).addValue(-attackPoints);
		}
		sendHealthBar(result.getPoints());
		if (result.getPoints().isAlive()) {
			startHealthRegeneration();
		} else {
			stopHealthRegeneration();
			stopMotion();
			died();
		}
		stat.getSkills().get(SkillType.SKIN).addValue(SkillVariation.SKIN_INCREMENT);
		return true;
	}

	public void pull(final FieldEntity target) {
		final ServerFieldObject fo = getFieldObject();
		final FieldObject targetFo = target.getFieldObject();
		final ServerField field = fo.getField();
		final Field targetField = targetFo.getField();
		if (field != targetField) {
			return;
		}
		final Placement placement = fo.getPlacement();
		final Placement targetPlacement = targetFo.getPlacement();
		final int distance = field.getBoundary().distance(placement.getPosition(), targetPlacement.getPosition());
		if (distance > 1) {
			return;
		}
		stat.getSkills().get(SkillType.STRENGTH).addValue(SkillVariation.STRENGTH_INCREMENT);
		stat.getSkills().get(SkillType.AGILITY).addValue(SkillVariation.AGILITY_INCREMENT);
		stat.getSkills().get(SkillType.MIND).addValue(SkillVariation.MIND_DECREMENT);
		final Direction d = field.getBoundary().direction(targetPlacement.getPosition(), placement.getPosition());
		final Placement toPlacement = Placement.valueOf(placement.getPosition().neighbor(d), d.opposite());
		final Placement placement2 = fo.move(toPlacement);
		if (placement2 != null) {
			targetFo.move(Placement.valueOf(placement.getPosition(), targetPlacement.getDirection()));
		}
	}

	public void push(final FieldEntity target) {
		final ServerFieldObject fo = getFieldObject();
		final FieldObject targetFo = target.getFieldObject();
		final ServerField field = fo.getField();
		final Field targetField = targetFo.getField();
		if (field != targetField) {
			return;
		}
		final Placement placement = fo.getPlacement();
		final Placement targetPlacement = targetFo.getPlacement();
		final int distance = field.getBoundary().distance(placement.getPosition(), targetPlacement.getPosition());
		if (distance > 1) {
			return;
		}
		stat.getSkills().get(SkillType.STRENGTH).addValue(SkillVariation.STRENGTH_INCREMENT);
		stat.getSkills().get(SkillType.AGILITY).addValue(SkillVariation.AGILITY_INCREMENT);
		stat.getSkills().get(SkillType.MIND).addValue(SkillVariation.MIND_DECREMENT);
		final Direction d = field.getBoundary().direction(placement.getPosition(), targetPlacement.getPosition());
		final Placement toPlacement = Placement.valueOf(targetPlacement.getPosition().neighbor(d),
				targetPlacement.getDirection());
		final Placement placement2 = targetFo.move(toPlacement);
		if (placement2 != null) {
			fo.move(Placement.valueOf(targetPlacement.getPosition(), d));
		}
	}

	public void died() {
		final ServerFieldObject fo = getFieldObject();
		if (fo == null) {
			return;
		}
		for (final FieldDriver driver : getFieldDrivers()) {
			if (!(driver instanceof Player)) {
				continue;
			}
			final Player player = (Player) driver;
			final Session session = player.getSession();
			final User user = player.getUser();

			final FieldId fieldId = fo.getField().getId();
			final Placement placement = fo.getPlacement();
			final CharacterEntity newEntity = session.getContext().getEntities().createSoulEntity(fieldId, placement);

			session.logoutCharacter();
			newEntity.mount(session.getContext().getFields());
			final Player newPlayer = new Player(newEntity, session, user);
			if (!session.setPlayer(newPlayer)) {
				continue;
			}
			if (user.removeCharacterEntry(getId())) {
				removeOwner(user.getId());
				if (user.addCharacterEntry(newEntity.getId())) {
					newEntity.addOwner(user.getId());
				}
			}
			newPlayer.synchronize();
		}
	}

	public boolean revive() {
		if (!stat.revive()) {
			return false;
		}
		startHealthRegeneration();
		startMagicRegeneration();
		startMotion();
		sendHealthBar(stat.getHealthPoints());
		return true;
	}

}
