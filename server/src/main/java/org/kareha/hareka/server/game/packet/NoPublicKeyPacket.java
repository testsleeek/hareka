package org.kareha.hareka.server.game.packet;

import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.PacketType;
import org.kareha.hareka.server.game.GamePacket;

public final class NoPublicKeyPacket extends GamePacket {

	public NoPublicKeyPacket(final int handlerId, final int version) {
		out.writeCompactUInt(handlerId);
		out.writeCompactUInt(version);
	}

	@Override
	protected PacketType getType() {
		return ClientPacketType.NO_PUBLIC_KEY;
	}

}
