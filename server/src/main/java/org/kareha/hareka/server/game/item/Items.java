package org.kareha.hareka.server.game.item;

import java.util.EnumMap;
import java.util.Map;

import org.kareha.hareka.game.ItemType;
import org.kareha.hareka.util.CamelCase;

public class Items {

	private final Map<ItemType, Item> map;

	public Items() {
		final String base = "org.kareha.hareka.server.game.item";
		final String postfix = "Item";
		final Map<ItemType, Item> m = new EnumMap<>(ItemType.class);
		for (final ItemType type : ItemType.values()) {
			final String camelCase = CamelCase.snakeToCamel(type.name());
			if (camelCase == null) {
				throw new RuntimeException("The name must be underscore: " + type.name());
			}
			final StringBuilder sb = new StringBuilder(base).append(".").append(camelCase).append(postfix);
			Class<?> clazz;
			try {
				clazz = Class.forName(sb.toString());
			} catch (final ClassNotFoundException e) {
				throw new IllegalArgumentException(e.getMessage(), e);
			}
			final Object o;
			try {
				o = clazz.newInstance();
			} catch (InstantiationException e) {
				throw new IllegalArgumentException(e.getMessage(), e);
			} catch (IllegalAccessException e) {
				throw new IllegalArgumentException(e.getMessage(), e);
			}
			if (!(o instanceof Item)) {
				throw new IllegalArgumentException(o.getClass().getName() + " is not instance of Item");
			}
			final Item t = (Item) o;
			m.put(type, t);
		}
		map = m;
	}

	public Item get(final ItemType type) {
		return map.get(type);
	}

}
