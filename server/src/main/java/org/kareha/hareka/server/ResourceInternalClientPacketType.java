package org.kareha.hareka.server;

import org.kareha.hareka.packet.PacketType;

public enum ResourceInternalClientPacketType implements PacketType {

	VERSION, EXIT,

	;

	@Override
	public int typeValue() {
		return ordinal();
	}

}
