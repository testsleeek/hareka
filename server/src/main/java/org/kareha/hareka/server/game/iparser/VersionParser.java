package org.kareha.hareka.server.game.iparser;

import org.kareha.hareka.Version;
import org.kareha.hareka.packet.PacketInput;
import org.kareha.hareka.packet.Parser;
import org.kareha.hareka.server.game.InternalSession;

public class VersionParser implements Parser<InternalSession> {

	@Override
	public void handle(final PacketInput in, final InternalSession session) {
		final Version serverVersion = Version.readFrom(in);

		final Version clientVersion = session.getContext().getVersion();
		final boolean matched = serverVersion.equals(clientVersion);

		// XXX debug print
		if (matched) {
			System.out.println("version matched");
		} else {
			System.out.println("version mismatched");
		}
	}

}
