package org.kareha.hareka.swingclient;

import java.io.IOException;

import javax.swing.SwingUtilities;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.client.Context;
import org.kareha.hareka.sound.SoundContext;
import org.kareha.hareka.swingclient.gui.Gui;

final class MainTest {

	private MainTest() {
		throw new AssertionError();
	}

	public static void main(final String[] args) throws IOException, JAXBException {
		final Context context = new Context(null, null);
		final SoundContext soundContext = new SoundContext(null);
		SwingUtilities.invokeLater(() -> {
			@SuppressWarnings("resource")
			final Strap strap = new Strap(null, context, soundContext);
			final Gui gui = strap.getGui();
			gui.getMainFrame().setVisible(true);
			gui.newConnectionFrame();
		});
	}

}
