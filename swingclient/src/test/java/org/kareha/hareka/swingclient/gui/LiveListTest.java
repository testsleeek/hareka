package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import org.kareha.hareka.graphics.GraphicsLoader;
import org.kareha.hareka.ui.swing.LiveList;

final class LiveListTest {

	private LiveListTest() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		final LiveList<ModelItem> liveList = new LiveList<>(LiveList.Axis.Y);
		final GraphicsLoader loader = new GraphicsLoader();
		final ModelItem elf = new ModelItem(loader, "Elf");
		liveList.addItem(elf);
		final ModelItem fox = new ModelItem(loader, "Fox");
		liveList.addItem(fox);
		final ModelItem bubble = new ModelItem(loader, "Bubble");
		liveList.addItem(bubble);
		final JScrollPane scrollPane = new JScrollPane(liveList);
		final JPanel panel = new JPanel(new BorderLayout());
		panel.add(scrollPane, BorderLayout.CENTER);

		liveList.setPreferredSize(new Dimension(256, 256));

		final JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.add(panel, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
	}

	@SuppressWarnings("serial")
	private static class ModelItem extends JComponent implements LiveList.Selectable {

		private final ModelPanel modelPanel;

		ModelItem(final GraphicsLoader loader, final String shape) {
			setFocusable(true);
			modelPanel = new ModelPanel(loader, shape);
			setLayout(new BorderLayout());
			add(modelPanel, BorderLayout.CENTER);
		}

		@Override
		public void setSelected(final boolean v) {
			if (v) {
				setBackground(Color.orange);
				modelPanel.setWalking(true);
			} else {
				setBackground(Color.black);
				modelPanel.setWalking(false);
			}
		}

		@Override
		protected void paintComponent(final Graphics g) {
			final Dimension size = getSize();
			g.setColor(getBackground());
			g.fillRect(0, 0, size.width, size.height);
		}

	}

}
