package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.kareha.hareka.graphics.GraphicsLoader;

final class ModelPanelTest {

	private ModelPanelTest() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		final JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		final GraphicsLoader loader = new GraphicsLoader();
		final ModelPanel panel = new ModelPanel(loader, "Elf");
		panel.setWalking(true);
		frame.add(panel, BorderLayout.CENTER);
		frame.setVisible(true);
	}

}
