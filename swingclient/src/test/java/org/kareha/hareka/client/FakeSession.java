package org.kareha.hareka.client;

import java.net.Socket;

import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.packet.ClientPacketType;
import org.kareha.hareka.packet.Packet;
import org.kareha.hareka.packet.PacketSocket;
import org.kareha.hareka.packet.ParserTable;
import org.kareha.hareka.swingclient.Strap;

public class FakeSession extends Session {

	@SuppressWarnings("unused")
	private final Strap strap;

	FakeSession(final Strap strap) {
		super(strap.getContext());
		this.strap = strap;
	}

	@Override
	PacketSocket<ClientPacketType, Session> newPacketSocket(final Socket socket,
			final ParserTable<ClientPacketType, Session> parserTable) {
		return null;
	}

	public static Session newInstance(final Strap strap) {
		final Session session = new FakeSession(strap);
		session.initialize(null, null);
		return session;
	}

	@Override
	public void write(final Packet packet) {
		// nothing to do
	}

	@Override
	public void writeLocalChat(final String content) {
		final ChatEntity self = getMirrors().getSelfMirror().getChatEntity();
		getMirrors().getChatMirror().getLocalChatSession().receiveChat(self, content);
	}

	@Override
	public void writePrivateChat(final ChatEntity peer, final String content) {
		final ChatEntity self = getMirrors().getSelfMirror().getChatEntity();
		getMirrors().getChatMirror().getPrivateChatSession(peer).receiveChat(self, content);
	}

}
