package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.client.ResponseHandler;
import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class LoginFrame extends JInternalFrame {

	private enum BundleKey {
		Login, Disconnect, WaitingForKeyExchange,
	}

	private final Strap strap;

	public LoginFrame(final Strap strap) {
		this.strap = strap;
		final Gui gui = strap.getGui();
		final ResourceBundle bundle = ResourceBundle.getBundle(LoginFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Login.name()));

		final JButton loginButton = new JButton(bundle.getString(BundleKey.Login.name()));
		loginButton.addActionListener(e -> doLogin());
		loginButton.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_SPACE) {
					doLogin();
				}
			}
		});
		final JButton disconnectButton = new JButton(bundle.getString(BundleKey.Disconnect.name()));
		disconnectButton.addActionListener(e -> doDisconnect());

		final JPanel buttonPanel = new JPanel();
		buttonPanel.add(loginButton);
		buttonPanel.add(disconnectButton);

		add(buttonPanel, BorderLayout.CENTER);

		pack();

		gui.getViewPane().add(this);
		gui.center(this);
		setVisible(true);
	}

	@Private
	void doLogin() {
		final Gui gui = strap.getGui();
		if (strap.getSession().getPeerKey() == null) {
			final ResourceBundle bundle = ResourceBundle.getBundle(LoginFrame.class.getName());
			JOptionPane.showInternalMessageDialog(gui.getViewPane(),
					bundle.getString(BundleKey.WaitingForKeyExchange.name()));
			return;
		}
		final ResponseHandler responseHandler = new ResponseHandler() {
			@Override
			public void rejected(final String message) {
				SwingUtilities.invokeLater(() -> JOptionPane.showInternalMessageDialog(LoginFrame.this, message));
			}

			@Override
			public void accepted(final String message) {
				SwingUtilities.invokeLater(() -> {
					gui.getViewPane().remove(LoginFrame.this);
					dispose();

					final CharactersFrame frame = gui.getCharactersFrame();
					gui.center(frame);
					frame.setVisible(true);
				});
			}
		};
		strap.getSession().writeLoginUser(responseHandler);
	}

	private void doDisconnect() {
		final Gui gui = strap.getGui();
		strap.getSession().close();
		gui.getViewPane().remove(this);
		dispose();
		gui.newConnectionFrame();
	}

}
