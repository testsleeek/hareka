package org.kareha.hareka.swingclient;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.annotation.ConfinedTo;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.client.Context;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.graphics.GraphicsLoader;
import org.kareha.hareka.sound.SoundContext;
import org.kareha.hareka.swingclient.gui.AbilityButton;
import org.kareha.hareka.swingclient.gui.Gui;
import org.kareha.hareka.swingclient.gui.ItemButton;
import org.kareha.hareka.swingclient.gui.ShortcutButton;

@ConfinedTo("Swing")
public class Strap implements AutoCloseable {

	private static final Logger logger = Logger.getLogger(Strap.class.getName());

	private final SwingClientParameters parameters;
	private final Context context;
	private final SoundContext soundContext;
	private final SwingClientSettings swingClientSettings;
	private final GraphicsLoader loader;
	@Private
	final Gui gui;
	private final PersistentEntities persistentEntities;
	private final Shortcuts shortcuts;
	private final ScheduledThreadPoolExecutor scheduledExecutor;

	@GuardedBy("this")
	private Session session;

	private boolean defaultCharacterSelected;

	public Strap(final SwingClientParameters parameters, final Context context, final SoundContext soundContext) {
		this.parameters = parameters;
		this.context = context;
		this.soundContext = soundContext;
		swingClientSettings = new SwingClientSettings(context.getDataDirectory());
		loader = new GraphicsLoader();
		gui = new Gui(this);
		persistentEntities = new PersistentEntities(this);
		shortcuts = new Shortcuts(this);
		shortcuts.addListener(new Shortcuts.Listener() {
			@Override
			public void shortcutsCleared() {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						gui.getShortcutsFrame().getPanel().clear();
					}
				});
			}

			@Override
			public void shortcutsSwapped(final int indexA, final int indexB) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						gui.getShortcutsFrame().getPanel().swapShortcuts(indexA, indexB);
					}
				});
			}

			@Override
			public void shortcutAdded(final int index, final Shortcuts.Entry entry) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						final ShortcutButton c = gui.getShortcutsFrame().getPanel().getButton(index);
						if (entry instanceof Shortcuts.BlankEntry) {
							c.setValue(null);
							c.setRepeating(entry.isRepeating());
							c.setSelfTargetting(entry.isSelfTargetting());
							c.setKeyCode(entry.getKeyCode());
							c.repaint();
						} else if (entry instanceof Shortcuts.AbilityEntry) {
							final Shortcuts.AbilityEntry abilityEntry = (Shortcuts.AbilityEntry) entry;
							final AbilityButton button = gui.getAbilitiesFrame().getPanel()
									.getButton(abilityEntry.getType());
							if (button != null) {
								c.setValue(button.getValue());
							} else {
								c.setValue(null);
							}
							c.setRepeating(entry.isRepeating());
							c.setSelfTargetting(entry.isSelfTargetting());
							c.setKeyCode(entry.getKeyCode());
							c.repaint();
						} else if (entry instanceof Shortcuts.ItemEntry) {
							final Shortcuts.ItemEntry itemEntry = (Shortcuts.ItemEntry) entry;
							final ItemButton button = gui.getInventoryFrame().getPanel().getButton(itemEntry.getId());
							if (button != null) {
								c.setValue(button.getValue());
							} else {
								c.setValue(null);
							}
							c.setRepeating(entry.isRepeating());
							c.setSelfTargetting(entry.isSelfTargetting());
							c.setKeyCode(entry.getKeyCode());
							c.repaint();
						} else {
							throw new AssertionError("Unknown Shortcuts.Entry type: " + entry.getClass().getName());
						}
					}
				});
			}
		});

		scheduledExecutor = new ScheduledThreadPoolExecutor(2);
	}

	public SwingClientParameters getParameters() {
		return parameters;
	}

	public Context getContext() {
		return context;
	}

	public SoundContext getSoundContext() {
		return soundContext;
	}

	public SwingClientSettings getSwingClientSettings() {
		return swingClientSettings;
	}

	public GraphicsLoader getLoader() {
		return loader;
	}

	public Gui getGui() {
		return gui;
	}

	public PersistentEntities getPersistentEntities() {
		return persistentEntities;
	}

	public Shortcuts getShortcuts() {
		return shortcuts;
	}

	public synchronized Session getSession() {
		return session;
	}

	public synchronized void setSession(final Session session) {
		this.session = session;
	}

	public void disconnect() {
		if (session == null) {
			throw new IllegalStateException();
		}
		session.close();
		session = null;

		gui.disconnect();

		defaultCharacterSelected = false;
	}

	@Override
	public void close() {
		gui.close();
		if (session != null) {
			session.close();
		}
		scheduledExecutor.shutdownNow();
		if (soundContext != null) {
			soundContext.close();
		}
	}

	public void exit() {
		close();

		shortcuts.setOwner(null); // trigger save

		swingClientSettings.save();

		gui.exit();

		try {
			context.save();
		} catch (final JAXBException e) {
			logger.log(Level.SEVERE, "", e);
		}
	}

	boolean isDefaultCharacterSelected() {
		return defaultCharacterSelected;
	}

	void setDefaultCharacterSelected(final boolean v) {
		defaultCharacterSelected = v;
	}

	public ScheduledThreadPoolExecutor getScheduledExecutor() {
		return scheduledExecutor;
	}

}
