package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ResourceBundle;
import java.util.function.Supplier;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.packet.DrawLinePacket;
import org.kareha.hareka.field.OneUniformTilePattern;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.SolidTilePattern;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TilePattern;
import org.kareha.hareka.field.TilePatternType;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class DrawLineFrame extends JInternalFrame {

	private static final int FRAME_WIDTH = 384;
	private static final int FRAME_HEIGHT = 384;

	private enum BundleKey {
		Title, NotConnected,

		CurrentPosition, Type,

		Draw, ForceDraw,
	}

	public DrawLineFrame(final Strap strap) {
		super("", true, true, true, true);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		final ResourceBundle bundle = ResourceBundle.getBundle(DrawLineFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		final JPanel northPanel = new JPanel();
		northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.Y_AXIS));

		final JPanel positionPanelA = new JPanel();
		positionPanelA.add(new JLabel("AX"));
		final JTextField xFieldA = new JTextField("0", 6);
		positionPanelA.add(xFieldA);
		positionPanelA.add(new JLabel("AY"));
		final JTextField yFieldA = new JTextField("0", 6);
		positionPanelA.add(yFieldA);
		final Runnable currentPositionRunnableA = () -> {
			final Session session = strap.getSession();
			if (session == null) {
				return;
			}
			final FieldEntity entity = session.getMirrors().getSelfMirror().getFieldEntity();
			if (entity == null) {
				return;
			}
			final Placement placement = entity.getPlacement();
			xFieldA.setText(Integer.toString(placement.getPosition().x()));
			yFieldA.setText(Integer.toString(placement.getPosition().y()));
		};
		currentPositionRunnableA.run();
		final JButton currentPositionButtonA = new JButton(bundle.getString(BundleKey.CurrentPosition.name()));
		currentPositionButtonA.addActionListener(e -> currentPositionRunnableA.run());
		positionPanelA.add(currentPositionButtonA);
		northPanel.add(positionPanelA);

		final JPanel positionPanelB = new JPanel();
		positionPanelB.add(new JLabel("BX"));
		final JTextField xFieldB = new JTextField("0", 6);
		positionPanelB.add(xFieldB);
		positionPanelB.add(new JLabel("BY"));
		final JTextField yFieldB = new JTextField("0", 6);
		positionPanelB.add(yFieldB);
		final Runnable currentPositionRunnableB = () -> {
			final Session session = strap.getSession();
			if (session == null) {
				return;
			}
			final FieldEntity entity = session.getMirrors().getSelfMirror().getFieldEntity();
			if (entity == null) {
				return;
			}
			final Placement placement = entity.getPlacement();
			xFieldB.setText(Integer.toString(placement.getPosition().x()));
			yFieldB.setText(Integer.toString(placement.getPosition().y()));
		};
		currentPositionRunnableB.run();
		final JButton currentPositionButtonB = new JButton(bundle.getString(BundleKey.CurrentPosition.name()));
		currentPositionButtonB.addActionListener(e -> currentPositionRunnableB.run());
		positionPanelB.add(currentPositionButtonB);
		northPanel.add(positionPanelB);

		final JPanel typePanel = new JPanel();
		typePanel.add(new JLabel(bundle.getString(BundleKey.Type.name())));
		final JComboBox<TilePatternType> typeComboBox = new JComboBox<>(TilePatternType.values());
		typePanel.add(typeComboBox);

		final JPanel tilePanel = new JPanel();
		tilePanel.setLayout(new BoxLayout(tilePanel, BoxLayout.Y_AXIS));
		final JPanel tilePanelA = new JPanel();
		final TileSelector tileSelectorA = new TileSelector(strap);
		tilePanelA.add(tileSelectorA);
		final JTextField elevationFieldA = new JTextField("0", 6);
		tilePanelA.add(elevationFieldA);
		tilePanel.add(tilePanelA);
		final JPanel tilePanelB = new JPanel();
		final TileSelector tileSelectorB = new TileSelector(strap);
		tilePanelB.add(tileSelectorB);
		final JTextField elevationFieldB = new JTextField("0", 6);
		tilePanelB.add(elevationFieldB);
		tilePanel.add(tilePanelB);
		final JPanel tilePanelC = new JPanel();
		final TileSelector tileSelectorC = new TileSelector(strap);
		tilePanelC.add(tileSelectorC);
		final JTextField elevationFieldC = new JTextField("0", 6);
		tilePanelC.add(elevationFieldC);
		tilePanel.add(tilePanelC);

		tilePanelB.setVisible(false);
		tilePanelC.setVisible(false);

		typeComboBox.addActionListener(e -> {
			switch (typeComboBox.getItemAt(typeComboBox.getSelectedIndex())) {
			case SOLID:
				tilePanelA.setVisible(true);
				tilePanelB.setVisible(false);
				tilePanelC.setVisible(false);
				break;
			case ONE_UNIFORM:
				tilePanelA.setVisible(true);
				tilePanelB.setVisible(true);
				tilePanelC.setVisible(true);
				break;
			}
		});

		final JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		centerPanel.add(typePanel);
		centerPanel.add(tilePanel);

		final Supplier<TilePattern> tilePatternSupplier = () -> {
			final TilePatternType type = typeComboBox.getItemAt(typeComboBox.getSelectedIndex());
			final int elevationA;
			final int elevationB;
			final int elevationC;
			try {
				elevationA = Integer.parseInt(elevationFieldA.getText());
				elevationB = Integer.parseInt(elevationFieldB.getText());
				elevationC = Integer.parseInt(elevationFieldC.getText());
			} catch (final NumberFormatException ex) {
				JOptionPane.showInternalMessageDialog(this, ex.getMessage());
				return null;
			}
			final Tile tileA = Tile.valueOf(tileSelectorA.getTile(), elevationA);
			final Tile tileB = Tile.valueOf(tileSelectorB.getTile(), elevationB);
			final Tile tileC = Tile.valueOf(tileSelectorC.getTile(), elevationC);
			switch (type) {
			default:
				return null;
			case SOLID:
				return new SolidTilePattern(tileA);
			case ONE_UNIFORM:
				return new OneUniformTilePattern(tileA, tileB, tileC);
			}
		};

		final JPanel buttonPanel = new JPanel();
		final JButton drawButton = new JButton(bundle.getString(BundleKey.Draw.name()));
		drawButton.addActionListener(e -> {
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(this, bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			final Vector a;
			final Vector b;
			try {
				a = Vector.valueOf(Integer.parseInt(xFieldA.getText()), Integer.parseInt(yFieldA.getText()));
				b = Vector.valueOf(Integer.parseInt(xFieldB.getText()), Integer.parseInt(yFieldB.getText()));
			} catch (final NumberFormatException ex) {
				JOptionPane.showInternalMessageDialog(this, ex.getMessage());
				return;
			}
			final TilePattern tilePattern = tilePatternSupplier.get();
			if (tilePattern == null) {
				return;
			}
			session.write(new DrawLinePacket(a, b, tilePattern, false));
		});
		buttonPanel.add(drawButton);
		final JButton forceDrawButton = new JButton(bundle.getString(BundleKey.ForceDraw.name()));
		forceDrawButton.addActionListener(e -> {
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(this, bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			final Vector a;
			final Vector b;
			try {
				a = Vector.valueOf(Integer.parseInt(xFieldA.getText()), Integer.parseInt(yFieldA.getText()));
				b = Vector.valueOf(Integer.parseInt(xFieldB.getText()), Integer.parseInt(yFieldB.getText()));
			} catch (final NumberFormatException ex) {
				JOptionPane.showInternalMessageDialog(this, ex.getMessage());
				return;
			}
			final TilePattern tilePattern = tilePatternSupplier.get();
			if (tilePattern == null) {
				return;
			}
			session.write(new DrawLinePacket(a, b, tilePattern, true));
		});
		buttonPanel.add(forceDrawButton);

		add(northPanel, BorderLayout.NORTH);
		add(centerPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		pack();
	}

}
