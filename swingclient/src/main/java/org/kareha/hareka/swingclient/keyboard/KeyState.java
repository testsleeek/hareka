package org.kareha.hareka.swingclient.keyboard;

public enum KeyState {

	STOP,

	RIGHT, LOWER_RIGHT, LOWER_LEFT, LEFT, UPPER_LEFT, UPPER_RIGHT,

	ATTACK,

	;

	public KeyCommand toKeyCommand() {
		switch (this) {
		default:
			throw new AssertionError(this);
		case RIGHT:
			return KeyCommand.RIGHT;
		case LOWER_RIGHT:
			return KeyCommand.LOWER_RIGHT;
		case LOWER_LEFT:
			return KeyCommand.LOWER_LEFT;
		case LEFT:
			return KeyCommand.LEFT;
		case UPPER_LEFT:
			return KeyCommand.UPPER_LEFT;
		case UPPER_RIGHT:
			return KeyCommand.UPPER_RIGHT;
		case STOP:
			return KeyCommand.STOP;
		case ATTACK:
			return KeyCommand.ATTACK;
		}
	}

}
