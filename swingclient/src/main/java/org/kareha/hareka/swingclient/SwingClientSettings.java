package org.kareha.hareka.swingclient;

import java.io.File;

import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.annotation.ConfinedTo;
import org.kareha.hareka.annotation.Private;

@ConfinedTo("Swing")
public class SwingClientSettings {

	private static final String filename = "SwingClientSettings.xml";

	private final File file;

	@Private
	boolean showFrameRate;
	@Private
	int targetFrameRate;
	@Private
	int chatDisplayDuration;
	@Private
	boolean enableClip;
	@Private
	boolean floatDeadBodies;

	public SwingClientSettings(final File dataDirectory) {
		file = new File(dataDirectory, filename);
		if (!load()) {
			setupDefault();
			helperSave();
		}
	}

	private void setupDefault() {
		showFrameRate = isDefaultShowFrameRate();
		targetFrameRate = getDefaultTargetFrameRate();
		chatDisplayDuration = getDefaultChatDisplayDuration();
		enableClip = isDefaultEnableClip();
		floatDeadBodies = isDefaultFloatDeadBodies();
	}

	@XmlRootElement(name = "swingClientSettings")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement
		@Private
		Boolean showFrameRate;
		@XmlElement
		@Private
		Integer targetFrameRate;
		@XmlElement
		@Private
		Integer chatDisplayDuration;
		@XmlElement
		@Private
		Boolean enableClip;
		@XmlElement
		@Private
		Boolean floatDeadBodies;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final SwingClientSettings v) {
			showFrameRate = v.showFrameRate;
			targetFrameRate = v.targetFrameRate;
			chatDisplayDuration = v.chatDisplayDuration;
			enableClip = v.enableClip;
			floatDeadBodies = v.floatDeadBodies;
		}

	}

	private boolean load() {
		if (!file.isFile()) {
			return false;
		}
		final Adapted adapted = JAXB.unmarshal(file, Adapted.class);
		showFrameRate = adapted.showFrameRate != null ? adapted.showFrameRate : isDefaultShowFrameRate();
		targetFrameRate = adapted.targetFrameRate != null ? adapted.targetFrameRate : getDefaultTargetFrameRate();
		chatDisplayDuration = adapted.chatDisplayDuration != null ? adapted.chatDisplayDuration
				: getDefaultChatDisplayDuration();
		enableClip = adapted.enableClip != null ? adapted.enableClip : isDefaultEnableClip();
		floatDeadBodies = adapted.floatDeadBodies != null ? adapted.floatDeadBodies : isDefaultFloatDeadBodies();
		return true;
	}

	private void helperSave() {
		synchronized (this) {
			JAXB.marshal(new Adapted(this), file);
		}
	}

	public void save() {
		helperSave();
	}

	private static boolean isDefaultShowFrameRate() {
		return false;
	}

	public boolean isShowFrameRate() {
		return showFrameRate;
	}

	public void setShowFrameRate(final boolean showFrameRate) {
		this.showFrameRate = showFrameRate;
	}

	private static int getDefaultTargetFrameRate() {
		return 15;
	}

	public int getTargetFrameRate() {
		return targetFrameRate;
	}

	public void setTargetFrameRate(final int targetFrameRate) {
		this.targetFrameRate = targetFrameRate;
	}

	private static int getDefaultChatDisplayDuration() {
		return 4000;
	}

	public int getChatDisplayDuration() {
		return chatDisplayDuration;
	}

	public void setChatDisplayDuration(final int chatDisplayDuration) {
		this.chatDisplayDuration = chatDisplayDuration;
	}

	private static boolean isDefaultEnableClip() {
		return true;
	}

	public boolean isEnableClip() {
		return enableClip;
	}

	public void setEnableClip(final boolean enableClip) {
		this.enableClip = enableClip;
	}

	private static boolean isDefaultFloatDeadBodies() {
		return false;
	}

	public boolean isFloatDeadBodies() {
		return floatDeadBodies;
	}

	public void setFloatDeadBodies(final boolean floatDeadBodies) {
		this.floatDeadBodies = floatDeadBodies;
	}

}
