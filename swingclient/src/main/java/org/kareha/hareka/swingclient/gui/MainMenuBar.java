package org.kareha.hareka.swingclient.gui;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.security.KeyPair;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.Constants;
import org.kareha.hareka.client.ClientInfo;
import org.kareha.hareka.client.ResponseHandler;
import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.packet.AddDownstairsPacket;
import org.kareha.hareka.client.packet.AddGatePairPacket;
import org.kareha.hareka.client.packet.AddUpstairsPacket;
import org.kareha.hareka.client.packet.ChangeNamePacket;
import org.kareha.hareka.client.packet.ConsumeRoleTokenPacket;
import org.kareha.hareka.client.packet.IssueInvitationTokenPacket;
import org.kareha.hareka.client.packet.NewFieldPacket;
import org.kareha.hareka.client.packet.RebootPacket;
import org.kareha.hareka.client.packet.ReduceToBackgroundTilesPacket;
import org.kareha.hareka.client.packet.RemoveOrphansPacket;
import org.kareha.hareka.client.packet.RemoveSpecialTilePacket;
import org.kareha.hareka.client.packet.RequestMyRolesPacket;
import org.kareha.hareka.client.packet.RequestRoleListPacket;
import org.kareha.hareka.client.packet.RequestRoleTokenListPacket;
import org.kareha.hareka.client.packet.RequestSettingsPacket;
import org.kareha.hareka.client.packet.SetMarkPacket;
import org.kareha.hareka.client.packet.ShutdownPacket;
import org.kareha.hareka.key.KeyXml;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.swingclient.SwingClientUtil;
import org.kareha.hareka.ui.swing.PasswordChangerDialog;
import org.kareha.hareka.util.FileUtil;
import org.kareha.hareka.util.JaxbUtil;
import org.kareha.hareka.util.KeyPairUtil;
import org.kareha.hareka.util.Name;
import org.kareha.hareka.util.Utf8Util;

@SuppressWarnings("serial")
public class MainMenuBar extends JMenuBar {

	private static final int DEFAULT_FIELD_BOUNDARY_SIZE = 128;

	private enum BundleKey {

		Main, ChangeMyName, InvalidLanguage, LoginCharacterFirst, TheNameIsTooShort, TheNameIsTooLong, SetNickname, SetTargetFirst, SelectCharacter, NotConnected, ChangeServer, SoundSettings, Exit,

		Key, ChangePassword, ExportPublicKey, GenerateNewKey, ConfirmGenerateNewKey, ExportKeyPair, ExportedFilesPermissionIsNotSecure, ImportKeyPair, CannotImportKeyPair, ConfirmReplaceKeyPair, KeyPairImported,

		View, Abilities, Inventory, PositionMemories, ShowFrameRate, ChangeTargetFrameRate, ChangeChatDisplayDuration, FloatDeadBodies, Command,

		Edit, DrawLine, FillRange, SetDefaultTilePattern, Regions, ReduceToBackgroundTiles, AddDownstairs, ConfirmAddDownstairs, AddUpstairs, ConfirmAddUpstairs, SetMark, AddGatePair, ConfirmAddGatePair, CreateNewField, EnterFieldBoundarySize, ConfirmCreateNewField, RemoveSpecialTile, ConfirmRemoveSpecialTile,

		Admin, ConsumeRoleToken, EnterRoleToken, InvalidToken, ServerSettings, IssueInvitationToken, IssueRoleToken, RoleTokenList, EditRoles, Shutdown, ConfirmShutdown, Reboot, ConfirmReboot,

		Develop, ChangeShape, ShapeToChange, ReloadGraphics, EnableClip, PlayMidiFile, PlayMidiFileLoop, StopMidi, RemoveOrphans, ConfirmRemoveOrphans,

		Help, BrowseSite, Documents, SourceCode, About,

	}

	private JFileChooser midiFileChooser;

	public MainMenuBar(final Strap strap) {
		final Gui gui = strap.getGui();
		final ResourceBundle bundle = ResourceBundle.getBundle(MainMenuBar.class.getName());

		final JMenu mainMenu = new JMenu(bundle.getString(BundleKey.Main.name()));

		// should this menu item be somewhere else than main menu?
		final JMenuItem changeMyNameItem = new JMenuItem(bundle.getString(BundleKey.ChangeMyName.name()));
		changeMyNameItem.addActionListener(e -> {
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			final String language = session.getLocale().getLanguage();
			final List<String> languages = Arrays.asList(Locale.getISOLanguages());
			if (!languages.contains(language)) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.InvalidLanguage.name()));
				return;
			}
			final Name currentName = session.getMirrors().getSelfMirror().getName();
			if (currentName == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.LoginCharacterFirst.name()));
				return;
			}
			final String defaultName = currentName.get(language);
			final String input = (String) JOptionPane.showInternalInputDialog(gui.getViewPane(),
					bundle.getString(BundleKey.ChangeMyName.name()), bundle.getString(BundleKey.ChangeMyName.name()),
					JOptionPane.QUESTION_MESSAGE, null, null, defaultName);
			if (input == null) {
				// canceled
				return;
			}
			if (input.equals(defaultName)) {
				// no effect
				return;
			}
			if (!input.isEmpty()) {
				// length is counted in UTF-8 bytes
				final int length = Utf8Util.length(input);
				// if actual length is included in the message, the message will
				// be complicated
				if (length < Constants.MIN_NAME_LENGTH) {
					JOptionPane.showInternalMessageDialog(gui.getViewPane(),
							bundle.getString(BundleKey.TheNameIsTooShort.name()));
					return;
				}
				if (length > Constants.MAX_NAME_LENGTH) {
					JOptionPane.showInternalMessageDialog(gui.getViewPane(),
							bundle.getString(BundleKey.TheNameIsTooLong.name()));
					return;
				}
			}
			session.write(new ChangeNamePacket(language, input));
		});
		mainMenu.add(changeMyNameItem);

		final JMenuItem addNicknameItem = new JMenuItem(bundle.getString(BundleKey.SetNickname.name()));
		addNicknameItem.addActionListener(e -> {
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			final FieldEntity target = gui.getViewPane().getTagetEntity();
			if (target == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.SetTargetFirst.name()));
				return;
			}
			final String defaultName = session.getServer().getFieldNickname(target.getLocalId());
			final String input = (String) JOptionPane.showInternalInputDialog(gui.getViewPane(),
					bundle.getString(BundleKey.SetNickname.name()), bundle.getString(BundleKey.SetNickname.name()),
					JOptionPane.QUESTION_MESSAGE, null, null, defaultName != null ? defaultName : "");
			if (input == null) {
				// canceled
				return;
			}
			if (input.isEmpty()) {
				session.getServer().removeFieldNickname(target.getLocalId());
			} else {
				session.getServer().addFieldNickname(target.getLocalId(), input);
			}
			gui.getViewPane().getView().updateEntityName(target);
		});
		mainMenu.add(addNicknameItem);

		mainMenu.addSeparator();

		final JMenuItem selectCharacterItem = new JMenuItem(bundle.getString(BundleKey.SelectCharacter.name()));
		selectCharacterItem.addActionListener(e -> {
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.writeLogoutCharacter(new ResponseHandler() {
				@Override
				public void rejected(final String message) {
					SwingUtilities.invokeLater(() -> JOptionPane.showInternalMessageDialog(gui.getViewPane(), message));
				}

				@Override
				public void accepted(final String message) {
					SwingUtilities.invokeLater(() -> {
						gui.getChatFrame().setVisible(false);
						gui.getCharactersFrame().setVisible(true);
					});
				}
			});
		});
		mainMenu.add(selectCharacterItem);

		final JMenuItem changeServerItem = new JMenuItem(bundle.getString(BundleKey.ChangeServer.name()));
		changeServerItem.addActionListener(e -> {
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			strap.disconnect();
			gui.newConnectionFrame();
		});
		mainMenu.add(changeServerItem);

		mainMenu.addSeparator();

		final JMenuItem soundSettingsItem = new JMenuItem(bundle.getString(BundleKey.SoundSettings.name()));
		soundSettingsItem.addActionListener(e -> {
			new SoundSettingsDialog(gui.getMainFrame(), strap.getSoundContext());
		});
		mainMenu.add(soundSettingsItem);

		mainMenu.addSeparator();

		final JMenuItem exitItem = new JMenuItem(bundle.getString(BundleKey.Exit.name()));
		exitItem.addActionListener(e -> strap.exit());
		mainMenu.add(exitItem);

		add(mainMenu);

		final JMenu keyMenu = new JMenu(bundle.getString(BundleKey.Key.name()));

		final JMenuItem changePasswordItem = new JMenuItem(bundle.getString(BundleKey.ChangePassword.name()));
		changePasswordItem.addActionListener(e -> {
			try {
				new PasswordChangerDialog(gui.getMainFrame(), strap.getContext().getKeyStack()).setVisible(true);
			} catch (final JAXBException ex) {
				JOptionPane.showMessageDialog(gui.getMainFrame(), ex.getMessage());
				return;
			}
		});
		keyMenu.add(changePasswordItem);

		final JMenuItem exportPublicKeyItem = new JMenuItem(bundle.getString(BundleKey.ExportPublicKey.name()));
		exportPublicKeyItem.addActionListener(e -> {
			final JFileChooser fc = new JFileChooser();
			fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
			fc.setSelectedFile(new File("PublicKey.xml"));
			final int result = fc.showSaveDialog(gui.getMainFrame());
			if (result != JFileChooser.APPROVE_OPTION) {
				return;
			}
			final File file = fc.getSelectedFile();
			try {
				final KeyXml xml = new KeyXml(SwingClientUtil.getKeyPair(strap, 0).getPublic());
				JaxbUtil.marshal(xml, file);
			} catch (final JAXBException ex) {
				JOptionPane.showMessageDialog(gui.getMainFrame(), ex.getMessage());
				return;
			}
		});
		keyMenu.add(exportPublicKeyItem);

		final JMenuItem generateNewKeyItem = new JMenuItem(bundle.getString(BundleKey.GenerateNewKey.name()));
		generateNewKeyItem.addActionListener(e -> {
			final int result = JOptionPane.showConfirmDialog(gui.getMainFrame(),
					bundle.getString(BundleKey.ConfirmGenerateNewKey.name()));
			if (result != JOptionPane.YES_OPTION) {
				return;
			}
			try {
				strap.getContext().getKeyStack().newKeyPair();
			} catch (final JAXBException ex) {
				JOptionPane.showMessageDialog(gui.getMainFrame(), ex.getMessage());
				return;
			}
		});
		keyMenu.add(generateNewKeyItem);

		keyMenu.addSeparator();

		final JMenuItem exportKeyPairItem = new JMenuItem(bundle.getString(BundleKey.ExportKeyPair.name()));
		exportKeyPairItem.addActionListener(e -> {
			final JFileChooser fc = new JFileChooser();
			fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
			fc.setSelectedFile(new File("hareka.keypair"));
			final int result = fc.showSaveDialog(gui.getMainFrame());
			if (result != JFileChooser.APPROVE_OPTION) {
				return;
			}
			final File file = fc.getSelectedFile();
			final KeyPair keyPair;
			try {
				keyPair = SwingClientUtil.getKeyPair(strap, 0);
			} catch (final JAXBException ex) {
				JOptionPane.showMessageDialog(gui.getMainFrame(), ex.getMessage());
				return;
			}
			if (keyPair == null) {
				return;
			}
			final String exported = KeyPairUtil.exportKeyPair(keyPair);
			try {
				if (!FileUtil.setPermissionReadableAndWritableOnlyByOwner(file)) {
					JOptionPane.showMessageDialog(gui.getMainFrame(),
							bundle.getString(BundleKey.ExportedFilesPermissionIsNotSecure.name()));
				}
			} catch (final IOException ex) {
				JOptionPane.showMessageDialog(gui.getMainFrame(), ex.getMessage());
				return;
			}
			try (final OutputStream out = new FileOutputStream(file)) {
				out.write(exported.getBytes("ASCII"));
			} catch (final IOException ex) {
				JOptionPane.showMessageDialog(gui.getMainFrame(), ex.getMessage());
				return;
			}
		});
		keyMenu.add(exportKeyPairItem);

		final JMenuItem importKeyPairItem = new JMenuItem(bundle.getString(BundleKey.ImportKeyPair.name()));
		importKeyPairItem.addActionListener(e -> {
			final JFileChooser fc = new JFileChooser();
			fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
			final int result = fc.showOpenDialog(gui.getMainFrame());
			if (result != JFileChooser.APPROVE_OPTION) {
				return;
			}
			final File file = fc.getSelectedFile();
			final String content;
			try {
				content = new String(Files.readAllBytes(file.toPath()));
			} catch (final Exception ex) {
				JOptionPane.showMessageDialog(gui.getMainFrame(), ex.getMessage());
				return;
			}
			final KeyPair keyPair = KeyPairUtil.importKeyPair(content);
			if (keyPair == null) {
				JOptionPane.showMessageDialog(gui.getMainFrame(),
						bundle.getString(BundleKey.CannotImportKeyPair.name()));
				return;
			}
			//
			final int confirmation = JOptionPane.showConfirmDialog(gui.getMainFrame(),
					bundle.getString(BundleKey.ConfirmReplaceKeyPair.name()));
			if (confirmation != JOptionPane.YES_OPTION) {
				return;
			}
			try {
				strap.getContext().getKeyStack().replaceKeyPair(keyPair);
			} catch (final JAXBException ex) {
				JOptionPane.showMessageDialog(gui.getMainFrame(), ex.getMessage());
				return;
			}
			JOptionPane.showMessageDialog(gui.getMainFrame(), bundle.getString(BundleKey.KeyPairImported.name()));
		});
		keyMenu.add(importKeyPairItem);

		add(keyMenu);

		final JMenu viewMenu = new JMenu(bundle.getString(BundleKey.View.name()));

		final JMenuItem abilitiesItem = new JMenuItem(bundle.getString(BundleKey.Abilities.name()));
		abilitiesItem.addActionListener(e -> {
			final AbilitiesFrame frame = gui.getAbilitiesFrame();
			frame.setVisible(true);
			frame.toFront();
		});
		viewMenu.add(abilitiesItem);

		final JMenuItem inventoryItem = new JMenuItem(bundle.getString(BundleKey.Inventory.name()));
		inventoryItem.addActionListener(e -> {
			final InventoryFrame frame = gui.getInventoryFrame();
			frame.setVisible(true);
			frame.toFront();
		});
		viewMenu.add(inventoryItem);

		final JMenuItem positionMemoriesItem = new JMenuItem(bundle.getString(BundleKey.PositionMemories.name()));
		positionMemoriesItem.addActionListener(e -> {
			final PositionMemoryFrame frame = gui.getPositionMemoryFrame();
			frame.setVisible(true);
			frame.toFront();
		});
		viewMenu.add(positionMemoriesItem);

		viewMenu.addSeparator();

		final JCheckBoxMenuItem showFrameRateItem = new JCheckBoxMenuItem(
				bundle.getString(BundleKey.ShowFrameRate.name()));
		showFrameRateItem.setSelected(strap.getSwingClientSettings().isShowFrameRate());
		showFrameRateItem.addActionListener(
				e -> strap.getSwingClientSettings().setShowFrameRate(showFrameRateItem.isSelected()));
		viewMenu.add(showFrameRateItem);

		final JMenuItem changeTargetFrameRateItem = new JMenuItem(
				bundle.getString(BundleKey.ChangeTargetFrameRate.name()));
		changeTargetFrameRateItem.addActionListener(e -> {
			final String result = JOptionPane.showInputDialog(bundle.getString(BundleKey.ChangeTargetFrameRate.name()),
					strap.getSwingClientSettings().getTargetFrameRate());
			if (result == null) {
				return;
			}
			try {
				final int rate = Integer.parseInt(result);
				strap.getSwingClientSettings().setTargetFrameRate(rate);
				gui.getViewPane().setTargetFrameRate(rate);
			} catch (final NumberFormatException ex) {
				JOptionPane.showMessageDialog(gui.getMainFrame(), ex.getMessage());
				return;
			}
		});
		viewMenu.add(changeTargetFrameRateItem);

		final JMenuItem changeChatDisplayDurationItem = new JMenuItem(
				bundle.getString(BundleKey.ChangeChatDisplayDuration.name()));
		changeChatDisplayDurationItem.addActionListener(e -> {
			final String result = JOptionPane.showInputDialog(
					bundle.getString(BundleKey.ChangeChatDisplayDuration.name()),
					strap.getSwingClientSettings().getChatDisplayDuration());
			if (result == null) {
				return;
			}
			try {
				final int duration = Integer.parseInt(result);
				strap.getSwingClientSettings().setChatDisplayDuration(duration);
				final View view = gui.getViewPane().getView();
				if (view != null) {
					view.setChatDisplayDuration(duration);
				}
			} catch (final NumberFormatException ex) {
				JOptionPane.showMessageDialog(gui.getMainFrame(), ex.getMessage());
				return;
			}
		});
		viewMenu.add(changeChatDisplayDurationItem);

		final JCheckBoxMenuItem floatDeadBodiesItem = new JCheckBoxMenuItem(
				bundle.getString(BundleKey.FloatDeadBodies.name()));
		floatDeadBodiesItem.setSelected(strap.getSwingClientSettings().isFloatDeadBodies());
		floatDeadBodiesItem.addActionListener(e -> {
			strap.getSwingClientSettings().setFloatDeadBodies(floatDeadBodiesItem.isSelected());
			final View view = gui.getViewPane().getView();
			if (view != null) {
				view.reloadEntities();
			}
		});
		viewMenu.add(floatDeadBodiesItem);

		viewMenu.addSeparator();

		final JMenuItem commandItem = new JMenuItem(bundle.getString(BundleKey.Command.name()));
		commandItem.addActionListener(e -> gui.getCommandFrame().setVisible(true));
		viewMenu.add(commandItem);

		add(viewMenu);

		final JMenu editMenu = new JMenu(bundle.getString(BundleKey.Edit.name()));

		final JMenuItem drawLineItem = new JMenuItem(bundle.getString(BundleKey.DrawLine.name()));
		drawLineItem.addActionListener(e -> gui.getDrawLineFrame().setVisible(true));
		editMenu.add(drawLineItem);

		final JMenuItem fillRangeItem = new JMenuItem(bundle.getString(BundleKey.FillRange.name()));
		fillRangeItem.addActionListener(e -> gui.getFillRangeFrame().setVisible(true));
		editMenu.add(fillRangeItem);

		final JMenuItem setDefaultTilePatternItem = new JMenuItem(
				bundle.getString(BundleKey.SetDefaultTilePattern.name()));
		setDefaultTilePatternItem.addActionListener(e -> gui.getDefaultTilePatternFrame().setVisible(true));
		editMenu.add(setDefaultTilePatternItem);

		final JMenuItem regionsItem = new JMenuItem(bundle.getString(BundleKey.Regions.name()));
		regionsItem.addActionListener(e -> gui.getRegionsFrame().setVisible(true));
		editMenu.add(regionsItem);

		final JMenuItem reduceToBackgroundTilesItem = new JMenuItem(
				bundle.getString(BundleKey.ReduceToBackgroundTiles.name()));
		reduceToBackgroundTilesItem.addActionListener(e -> {
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new ReduceToBackgroundTilesPacket());
		});
		editMenu.add(reduceToBackgroundTilesItem);

		final JMenuItem addDownstairsItem = new JMenuItem(bundle.getString(BundleKey.AddDownstairs.name()));
		addDownstairsItem.addActionListener(e -> {
			final int result = JOptionPane.showConfirmDialog(gui.getMainFrame(),
					bundle.getString(BundleKey.ConfirmAddDownstairs.name()));
			if (result != JOptionPane.YES_OPTION) {
				return;
			}
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new AddDownstairsPacket());
		});
		editMenu.add(addDownstairsItem);

		final JMenuItem addUpstairsItem = new JMenuItem(bundle.getString(BundleKey.AddUpstairs.name()));
		addUpstairsItem.addActionListener(e -> {
			final int result = JOptionPane.showConfirmDialog(gui.getMainFrame(),
					bundle.getString(BundleKey.ConfirmAddUpstairs.name()));
			if (result != JOptionPane.YES_OPTION) {
				return;
			}
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new AddUpstairsPacket());
		});
		editMenu.add(addUpstairsItem);

		final JMenuItem setMarkItem = new JMenuItem(bundle.getString(BundleKey.SetMark.name()));
		setMarkItem.addActionListener(e -> {
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new SetMarkPacket());
		});
		editMenu.add(setMarkItem);

		final JMenuItem addGatePairItem = new JMenuItem(bundle.getString(BundleKey.AddGatePair.name()));
		addGatePairItem.addActionListener(e -> {
			final int result = JOptionPane.showConfirmDialog(gui.getMainFrame(),
					bundle.getString(BundleKey.ConfirmAddGatePair.name()));
			if (result != JOptionPane.YES_OPTION) {
				return;
			}
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new AddGatePairPacket());
		});
		editMenu.add(addGatePairItem);

		final JMenuItem createNewFieldItem = new JMenuItem(bundle.getString(BundleKey.CreateNewField.name()));
		createNewFieldItem.addActionListener(e -> {
			final String input = (String) JOptionPane.showInternalInputDialog(gui.getViewPane(),
					bundle.getString(BundleKey.CreateNewField.name()),
					bundle.getString(BundleKey.EnterFieldBoundarySize.name()), JOptionPane.QUESTION_MESSAGE, null, null,
					DEFAULT_FIELD_BOUNDARY_SIZE);
			if (input == null) {
				// canceled
				return;
			}
			final int size;
			try {
				size = Integer.parseInt(input);
			} catch (final NumberFormatException ex) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(), ex.getMessage());
				return;
			}
			final int result = JOptionPane.showConfirmDialog(gui.getMainFrame(),
					bundle.getString(BundleKey.ConfirmCreateNewField.name()));
			if (result != JOptionPane.YES_OPTION) {
				return;
			}
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new NewFieldPacket(size));
		});
		editMenu.add(createNewFieldItem);

		final JMenuItem removeSpecialTileItem = new JMenuItem(bundle.getString(BundleKey.RemoveSpecialTile.name()));
		removeSpecialTileItem.addActionListener(e -> {
			final int result = JOptionPane.showConfirmDialog(gui.getMainFrame(),
					bundle.getString(BundleKey.ConfirmRemoveSpecialTile.name()));
			if (result != JOptionPane.YES_OPTION) {
				return;
			}
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new RemoveSpecialTilePacket());
		});
		editMenu.add(removeSpecialTileItem);

		add(editMenu);

		final JMenu adminMenu = new JMenu(bundle.getString(BundleKey.Admin.name()));

		final JMenuItem consumeRoleTokenItem = new JMenuItem(bundle.getString(BundleKey.ConsumeRoleToken.name()));
		consumeRoleTokenItem.addActionListener(e -> {
			final String input = JOptionPane.showInternalInputDialog(gui.getViewPane(),
					bundle.getString(BundleKey.EnterRoleToken.name()));
			if (input == null || input.isEmpty()) {
				return;
			}
			final byte[] token = Base64.getDecoder().decode(input.trim());
			if (token == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.InvalidToken.name()));
				return;
			}
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new ConsumeRoleTokenPacket(token));
		});
		adminMenu.add(consumeRoleTokenItem);

		final JMenuItem serverSettingsItem = new JMenuItem(bundle.getString(BundleKey.ServerSettings.name()));
		serverSettingsItem.addActionListener(e -> {
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new RequestSettingsPacket());
			gui.getServerSettingsFrame().setVisible(true);
		});
		adminMenu.add(serverSettingsItem);

		final JMenuItem issueInvitationTokenItem = new JMenuItem(
				bundle.getString(BundleKey.IssueInvitationToken.name()));
		issueInvitationTokenItem.addActionListener(e -> {
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new IssueInvitationTokenPacket());
		});
		adminMenu.add(issueInvitationTokenItem);

		final JMenuItem issueRoleTokenItem = new JMenuItem(bundle.getString(BundleKey.IssueRoleToken.name()));
		issueRoleTokenItem.addActionListener(e -> {
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new RequestRoleListPacket());
			session.write(new RequestMyRolesPacket());
			gui.getRoleTokenIssueFrame().setVisible(true);
		});
		adminMenu.add(issueRoleTokenItem);

		final JMenuItem roleTokenListItem = new JMenuItem(bundle.getString(BundleKey.RoleTokenList.name()));
		roleTokenListItem.addActionListener(e -> {
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new RequestRoleTokenListPacket());
			gui.getRoleTokenListFrame().setVisible(true);
		});
		adminMenu.add(roleTokenListItem);

		final JMenuItem editRolesItem = new JMenuItem(bundle.getString(BundleKey.EditRoles.name()));
		editRolesItem.addActionListener(e -> {
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new RequestRoleListPacket());
			session.write(new RequestMyRolesPacket());
			gui.getRoleEditorFrame().setVisible(true);
		});
		adminMenu.add(editRolesItem);

		adminMenu.addSeparator();

		final JMenuItem shutdownItem = new JMenuItem(bundle.getString(BundleKey.Shutdown.name()));
		shutdownItem.addActionListener(e -> {
			final int result = JOptionPane.showConfirmDialog(gui.getMainFrame(),
					bundle.getString(BundleKey.ConfirmShutdown.name()));
			if (result != JOptionPane.YES_OPTION) {
				return;
			}
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new ShutdownPacket());
		});
		adminMenu.add(shutdownItem);

		final JMenuItem rebootItem = new JMenuItem(bundle.getString(BundleKey.Reboot.name()));
		rebootItem.addActionListener(e -> {
			final int result = JOptionPane.showConfirmDialog(gui.getMainFrame(),
					bundle.getString(BundleKey.ConfirmReboot.name()));
			if (result != JOptionPane.YES_OPTION) {
				return;
			}
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new RebootPacket());
		});
		adminMenu.add(rebootItem);

		add(adminMenu);

		final JMenu developMenu = new JMenu(bundle.getString(BundleKey.Develop.name()));

		final JMenuItem changeShapeItem = new JMenuItem(bundle.getString(BundleKey.ChangeShape.name()));
		changeShapeItem.addActionListener(e -> {
			final String shape = JOptionPane.showInternalInputDialog(gui.getViewPane(),
					bundle.getString(BundleKey.ShapeToChange.name()));
			if (shape == null || shape.isEmpty()) {
				return;
			}
			final FieldEntity entity = strap.getSession().getMirrors().getSelfMirror().getFieldEntity();
			if (entity == null) {
				return;
			}
			entity.setShape(shape);
		});
		developMenu.add(changeShapeItem);

		final JMenuItem reloadGraphicsItem = new JMenuItem(bundle.getString(BundleKey.ReloadGraphics.name()));
		reloadGraphicsItem.addActionListener(e -> {
			strap.getLoader().reload();
			final View view = gui.getViewPane().getView();
			if (view != null) {
				view.reloadTiles();
				view.reloadEntities();
			}
		});
		developMenu.add(reloadGraphicsItem);

		final JCheckBoxMenuItem enableClipItem = new JCheckBoxMenuItem(bundle.getString(BundleKey.EnableClip.name()));
		enableClipItem.setSelected(strap.getSwingClientSettings().isEnableClip());
		enableClipItem
				.addActionListener(e -> strap.getSwingClientSettings().setEnableClip(enableClipItem.isSelected()));
		developMenu.add(enableClipItem);

		final JMenuItem playMidiFileItem = new JMenuItem(bundle.getString(BundleKey.PlayMidiFile.name()));
		playMidiFileItem.addActionListener(e -> {
			if (midiFileChooser == null) {
				midiFileChooser = new JFileChooser();
				midiFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			}
			final int result = midiFileChooser.showOpenDialog(gui.getMainFrame());
			if (result != JFileChooser.APPROVE_OPTION) {
				return;
			}
			final File file = midiFileChooser.getSelectedFile();
			strap.getSoundContext().getMusicPlayer().play(file, false);
		});
		developMenu.add(playMidiFileItem);

		final JMenuItem playMidiFileLoopItem = new JMenuItem(bundle.getString(BundleKey.PlayMidiFileLoop.name()));
		playMidiFileLoopItem.addActionListener(e -> {
			if (midiFileChooser == null) {
				midiFileChooser = new JFileChooser();
				midiFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			}
			final int result = midiFileChooser.showOpenDialog(gui.getMainFrame());
			if (result != JFileChooser.APPROVE_OPTION) {
				return;
			}
			final File file = midiFileChooser.getSelectedFile();
			strap.getSoundContext().getMusicPlayer().play(file, true);
		});
		developMenu.add(playMidiFileLoopItem);

		final JMenuItem stopMidiItem = new JMenuItem(bundle.getString(BundleKey.StopMidi.name()));
		stopMidiItem.addActionListener(e -> {
			strap.getSoundContext().getMusicPlayer().stop();
		});
		developMenu.add(stopMidiItem);

		final JMenuItem removeOrphansItem = new JMenuItem(bundle.getString(BundleKey.RemoveOrphans.name()));
		removeOrphansItem.addActionListener(e -> {
			final int result = JOptionPane.showConfirmDialog(gui.getMainFrame(),
					bundle.getString(BundleKey.ConfirmRemoveOrphans.name()));
			if (result != JOptionPane.YES_OPTION) {
				return;
			}
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new RemoveOrphansPacket());
		});
		developMenu.add(removeOrphansItem);

		add(developMenu);

		final JMenu helpMenu = new JMenu(bundle.getString(BundleKey.Help.name()));

		final JMenuItem browseSiteItem = new JMenuItem(bundle.getString(BundleKey.BrowseSite.name()));
		browseSiteItem.addActionListener(e -> {
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(),
						bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			try {
				Desktop.getDesktop().browse(new URI(session.getConnection().getSite()));
			} catch (final IOException | URISyntaxException ex) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(), ex.getMessage());
			}
		});
		helpMenu.add(browseSiteItem);

		final JMenuItem documentsItem = new JMenuItem(bundle.getString(BundleKey.Documents.name()));
		documentsItem.addActionListener(e -> {
			try {
				Desktop.getDesktop().browse(new URI(ClientInfo.INSTANCE.getDocuments()));
			} catch (final IOException | URISyntaxException ex) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(), ex.getMessage());
			}
		});
		helpMenu.add(documentsItem);

		final JMenuItem sourceCodeItem = new JMenuItem(bundle.getString(BundleKey.SourceCode.name()));
		sourceCodeItem.addActionListener(e -> {
			try {
				Desktop.getDesktop().browse(new URI(ClientInfo.INSTANCE.getSourceCode()));
			} catch (final IOException | URISyntaxException ex) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(), ex.getMessage());
			}
		});
		helpMenu.add(sourceCodeItem);

		helpMenu.addSeparator();

		final JMenuItem aboutItem = new JMenuItem(bundle.getString(BundleKey.About.name()));
		aboutItem.addActionListener(e -> {
			new AboutDialog(strap);
		});
		helpMenu.add(aboutItem);

		add(helpMenu);
	}

}
