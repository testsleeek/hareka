package org.kareha.hareka.swingclient;

import java.io.File;
import java.util.List;

import org.kareha.hareka.client.ClientConstants;
import org.kareha.hareka.util.Parameters;

public class SwingClientParameters {

	private final File dataDirectory;
	private final boolean noTls;
	private final List<String> raw;

	public SwingClientParameters(final String[] args) {
		final Parameters parameters = new Parameters(args);

		final String datadirOption = parameters.getNamed().get("datadir");
		if (datadirOption != null) {
			dataDirectory = new File(datadirOption);
		} else {
			dataDirectory = new File(ClientConstants.DATA_DIRECTORY_PATH);
		}

		noTls = parameters.getUnnamed().contains("--notls");

		raw = parameters.getRaw();
	}

	public File getDataDirectory() {
		return dataDirectory;
	}

	public boolean isNoTls() {
		return noTls;
	}

	public List<String> getRaw() {
		return raw;
	}

}
