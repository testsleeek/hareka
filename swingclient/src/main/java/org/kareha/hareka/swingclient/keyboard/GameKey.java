package org.kareha.hareka.swingclient.keyboard;

public enum GameKey {

	NONE,

	RIGHT, DOWN, LOWER_LEFT, LEFT, UP, UPPER_RIGHT, ROTATE_LEFT, ROTATE_RIGHT, MIRROR,

	ATTACK,

}
