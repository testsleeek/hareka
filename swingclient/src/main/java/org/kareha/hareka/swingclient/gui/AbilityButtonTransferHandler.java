package org.kareha.hareka.swingclient.gui;

import java.awt.datatransfer.Transferable;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

@SuppressWarnings("serial")
public class AbilityButtonTransferHandler extends TransferHandler {

	@Override
	public int getSourceActions(final JComponent c) {
		return MOVE;
	}

	@Override
	protected Transferable createTransferable(final JComponent c) {
		final AbilityButton button = (AbilityButton) c;
		return new AbilityButtonValue(false, button.getValue().getEntry());
	}

	@Override
	protected void exportDone(final JComponent source, final Transferable data, final int action) {
		if (action == MOVE) {
			final AbilityButton button = (AbilityButton) source;
			button.getPanel().swap();
		}
	}

	@Override
	public boolean canImport(TransferHandler.TransferSupport support) {
		if (!support.isDrop()) {
			return false;
		}
		return support.isDataFlavorSupported(AbilityButtonValue.abilityFlavor);
	}

	@Override
	public boolean importData(TransferHandler.TransferSupport support) {
		if (!canImport(support)) {
			return false;
		}
		final AbilityButton button = (AbilityButton) support.getComponent();
		button.getPanel().setSwapButton(button);
		return true;
	}

}
