package org.kareha.hareka.swingclient.gui;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.util.ResourceBundle;

import org.kareha.hareka.client.mirror.AbilityMirror;
import org.kareha.hareka.game.AbilityType;

public class AbilityButtonValue extends ShortcutButtonValue {

	public static final DataFlavor abilityFlavor = new DataFlavor(AbilityButtonValue.class, "Ability");
	private static final ResourceBundle abilityTypeBundle = ResourceBundle.getBundle(AbilityType.class.getName());

	protected final AbilityMirror.Entry entry;

	public AbilityButtonValue(final AbilityMirror.Entry entry) {
		this(false, entry);
	}

	public AbilityButtonValue(final boolean shortcut, final AbilityMirror.Entry entry) {
		super(shortcut);
		this.entry = entry;
	}

	public AbilityMirror.Entry getEntry() {
		return entry;
	}

	@Override
	public String getIconName() {
		return entry.getType().name();
	}

	@Override
	public String getName() {
		return abilityTypeBundle.getString(entry.getType().name());
	}

	@Override
	public Object getTransferData(final DataFlavor flavor) throws UnsupportedFlavorException {
		if (flavor.equals(shortcutFlavor)) {
			return this;
		} else if (flavor.equals(abilityFlavor)) {
			return this;
		} else {
			throw new UnsupportedFlavorException(flavor);
		}
	}

	@Override
	public DataFlavor[] getTransferDataFlavors() {
		final DataFlavor[] array = { shortcutFlavor, abilityFlavor, };
		return array;
	}

	@Override
	public boolean isDataFlavorSupported(final DataFlavor flavor) {
		if (flavor.equals(shortcutFlavor)) {
			return true;
		} else if (flavor.equals(abilityFlavor)) {
			return true;
		} else {
			return false;
		}
	}

}