package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Supplier;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.packet.RequestRegionListPacket;
import org.kareha.hareka.client.packet.SetRegionListPacket;
import org.kareha.hareka.field.OneUniformTilePattern;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.field.Region;
import org.kareha.hareka.field.SimpleRegion;
import org.kareha.hareka.field.SolidTilePattern;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TilePattern;
import org.kareha.hareka.field.TilePatternType;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class RegionsFrame extends JInternalFrame {

	private static final int FRAME_WIDTH = 768;
	private static final int FRAME_HEIGHT = 384;

	private enum BundleKey {
		Title, NotConnected,

		Center, CurrentPosition, Size, Mutable, Type,

		Load, New, Delete, Save, LoadFirst,
	}

	private final JList<Region> list;
	private boolean regionsLoaded;

	public RegionsFrame(final Strap strap) {
		super("", true, true, true, true);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		final ResourceBundle bundle = ResourceBundle.getBundle(RegionsFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		final JPanel northPanel = new JPanel();
		northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.Y_AXIS));
		final JPanel positionPanel = new JPanel();
		positionPanel.add(new JLabel(bundle.getString(BundleKey.Center.name())));
		positionPanel.add(new JLabel("X"));
		final JTextField xField = new JTextField("0", 6);
		positionPanel.add(xField);
		positionPanel.add(new JLabel("Y"));
		final JTextField yField = new JTextField("0", 6);
		positionPanel.add(yField);
		final Runnable currentPositionRunnable = () -> {
			final Session session = strap.getSession();
			if (session == null) {
				return;
			}
			final FieldEntity entity = session.getMirrors().getSelfMirror().getFieldEntity();
			if (entity == null) {
				return;
			}
			final Placement placement = entity.getPlacement();
			xField.setText(Integer.toString(placement.getPosition().x()));
			yField.setText(Integer.toString(placement.getPosition().y()));
		};
		currentPositionRunnable.run();
		final JButton currentPositionButton = new JButton(bundle.getString(BundleKey.CurrentPosition.name()));
		currentPositionButton.addActionListener(e -> currentPositionRunnable.run());
		positionPanel.add(currentPositionButton);
		northPanel.add(positionPanel);
		final JPanel sizePanel = new JPanel();
		sizePanel.add(new JLabel(bundle.getString(BundleKey.Size.name())));
		final JTextField sizeField = new JTextField("4", 6);
		sizePanel.add(sizeField);
		northPanel.add(sizePanel);
		final JPanel mutablePanel = new JPanel();
		mutablePanel.add(new JLabel(bundle.getString(BundleKey.Mutable.name())));
		final JCheckBox mutableCheckBox = new JCheckBox();
		mutablePanel.add(mutableCheckBox);
		northPanel.add(mutablePanel);

		final JPanel typePanel = new JPanel();
		typePanel.add(new JLabel(bundle.getString(BundleKey.Type.name())));
		final JComboBox<TilePatternType> typeComboBox = new JComboBox<>(TilePatternType.values());
		typePanel.add(typeComboBox);

		final JPanel tilePanel = new JPanel();
		tilePanel.setLayout(new BoxLayout(tilePanel, BoxLayout.Y_AXIS));
		final JPanel tilePanelA = new JPanel();
		final TileSelector tileSelectorA = new TileSelector(strap);
		tilePanelA.add(tileSelectorA);
		final JTextField elevationFieldA = new JTextField("0", 6);
		tilePanelA.add(elevationFieldA);
		tilePanel.add(tilePanelA);
		final JPanel tilePanelB = new JPanel();
		final TileSelector tileSelectorB = new TileSelector(strap);
		tilePanelB.add(tileSelectorB);
		final JTextField elevationFieldB = new JTextField("0", 6);
		tilePanelB.add(elevationFieldB);
		tilePanel.add(tilePanelB);
		final JPanel tilePanelC = new JPanel();
		final TileSelector tileSelectorC = new TileSelector(strap);
		tilePanelC.add(tileSelectorC);
		final JTextField elevationFieldC = new JTextField("0", 6);
		tilePanelC.add(elevationFieldC);
		tilePanel.add(tilePanelC);

		tilePanelB.setVisible(false);
		tilePanelC.setVisible(false);

		typeComboBox.addActionListener(e -> {
			switch (typeComboBox.getItemAt(typeComboBox.getSelectedIndex())) {
			case SOLID:
				tilePanelA.setVisible(true);
				tilePanelB.setVisible(false);
				tilePanelC.setVisible(false);
				break;
			case ONE_UNIFORM:
				tilePanelA.setVisible(true);
				tilePanelB.setVisible(true);
				tilePanelC.setVisible(true);
				break;
			}
		});

		final JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		centerPanel.add(northPanel);
		centerPanel.add(typePanel);
		centerPanel.add(tilePanel);

		list = new JList<>(new DefaultListModel<Region>());
		list.addListSelectionListener(e -> {
			final int index = e.getFirstIndex();
			if (index == -1) {
				return;
			}
			final DefaultListModel<Region> model = (DefaultListModel<Region>) list.getModel();
			if (index >= model.getSize()) {
				return;
			}
			final Region region = model.getElementAt(index);
			if (!(region instanceof SimpleRegion)) {
				return;
			}
			final SimpleRegion r = (SimpleRegion) region;
			xField.setText(Integer.toString(r.getCenter().x()));
			yField.setText(Integer.toString(r.getCenter().y()));
			sizeField.setText(Integer.toString(r.getSize()));
			mutableCheckBox.setSelected(r.isMutable());
			final TilePattern tilePattern = r.getTilePattern();
			if (tilePattern instanceof SolidTilePattern) {
				typeComboBox.setSelectedItem(TilePatternType.SOLID);
				final SolidTilePattern tp = (SolidTilePattern) tilePattern;
				tileSelectorA.setTile(tp.getValue().type());
			} else if (tilePattern instanceof OneUniformTilePattern) {
				final OneUniformTilePattern tp = (OneUniformTilePattern) tilePattern;
				tileSelectorA.setTile(tp.getA().type());
				tileSelectorB.setTile(tp.getB().type());
				tileSelectorC.setTile(tp.getC().type());
			} else {
				return;
			}
		});
		final JScrollPane scrollPane = new JScrollPane(list, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setPreferredSize(new Dimension(256, 384));

		final Supplier<TilePattern> tilePatternSupplier = () -> {
			final TilePatternType type = typeComboBox.getItemAt(typeComboBox.getSelectedIndex());
			final int elevationA;
			final int elevationB;
			final int elevationC;
			try {
				elevationA = Integer.parseInt(elevationFieldA.getText());
				elevationB = Integer.parseInt(elevationFieldB.getText());
				elevationC = Integer.parseInt(elevationFieldC.getText());
			} catch (final NumberFormatException ex) {
				JOptionPane.showInternalMessageDialog(this, ex.getMessage());
				return null;
			}
			final Tile tileA = Tile.valueOf(tileSelectorA.getTile(), elevationA);
			final Tile tileB = Tile.valueOf(tileSelectorB.getTile(), elevationB);
			final Tile tileC = Tile.valueOf(tileSelectorC.getTile(), elevationC);
			switch (type) {
			default:
				return null;
			case SOLID:
				return new SolidTilePattern(tileA);
			case ONE_UNIFORM:
				return new OneUniformTilePattern(tileA, tileB, tileC);
			}
		};

		final JPanel buttonPanel = new JPanel();
		final JButton loadButton = new JButton(bundle.getString(BundleKey.Load.name()));
		loadButton.addActionListener(e -> {
			final Session session = strap.getSession();
			if (session == null) {
				return;
			}
			session.write(new RequestRegionListPacket());
		});
		buttonPanel.add(loadButton);
		final JButton newButton = new JButton(bundle.getString(BundleKey.New.name()));
		newButton.addActionListener(e -> {
			final Vector center;
			final int size;
			try {
				center = Vector.valueOf(Integer.parseInt(xField.getText()), Integer.parseInt(yField.getText()));
				size = Integer.parseInt(sizeField.getText());
			} catch (final NumberFormatException ex) {
				JOptionPane.showInternalMessageDialog(this, ex.getMessage());
				return;
			}
			final Boolean mutable = mutableCheckBox.isSelected();
			final TilePattern tilePattern = tilePatternSupplier.get();
			if (tilePattern == null) {
				return;
			}
			final Region region = new SimpleRegion(tilePattern, mutable, center, size);
			final DefaultListModel<Region> model = (DefaultListModel<Region>) list.getModel();
			final int index;
			if (list.getSelectedIndex() == -1) {
				index = model.size();
			} else {
				index = list.getSelectedIndex();
			}
			model.add(index, region);
		});
		buttonPanel.add(newButton);
		final JButton deleteButton = new JButton(bundle.getString(BundleKey.Delete.name()));
		deleteButton.addActionListener(e -> {
			final int index = list.getSelectedIndex();
			if (index == -1) {
				return;
			}
			final DefaultListModel<Region> model = (DefaultListModel<Region>) list.getModel();
			model.remove(index);
		});
		buttonPanel.add(deleteButton);
		final JButton saveButton = new JButton(bundle.getString(BundleKey.Save.name()));
		saveButton.addActionListener(e -> {
			if (!regionsLoaded) {
				JOptionPane.showInternalMessageDialog(this, bundle.getString(BundleKey.LoadFirst.name()));
				return;
			}
			final List<Region> regions = new ArrayList<>();
			final DefaultListModel<Region> model = (DefaultListModel<Region>) list.getModel();
			for (final Enumeration<Region> en = model.elements(); en.hasMoreElements();) {
				regions.add(en.nextElement());
			}
			final Session session = strap.getSession();
			if (session == null) {
				return;
			}
			session.write(new SetRegionListPacket(regions));
		});
		buttonPanel.add(saveButton);

		add(centerPanel, BorderLayout.CENTER);
		add(scrollPane, BorderLayout.WEST);
		add(buttonPanel, BorderLayout.SOUTH);
		setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		pack();
	}

	public void clearRegions() {
		final DefaultListModel<Region> model = (DefaultListModel<Region>) list.getModel();
		model.clear();
		regionsLoaded = false;
	}

	public void setRegions(final Collection<Region> regions) {
		final DefaultListModel<Region> model = (DefaultListModel<Region>) list.getModel();
		model.clear();
		for (final Region region : regions) {
			model.addElement(region);
		}
		if (model.getSize() > 0) {
			list.setSelectedIndex(0);
		}
		regionsLoaded = true;
	}

}
