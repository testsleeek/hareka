package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.util.Base64;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.LocalKeyId;
import org.kareha.hareka.annotation.ConfinedTo;
import org.kareha.hareka.client.packet.CancelTokenPacket;
import org.kareha.hareka.client.packet.TokenPacket;
import org.kareha.hareka.swingclient.Strap;

@ConfinedTo("Swing")
public class Gui implements AutoCloseable {

	private enum BundleKey {
		InvalidToken,
	}

	private final Strap strap;

	private MainFrame mainFrame;
	private ViewPane viewPane;

	private CharactersFrame charactersFrame;
	private ChatFrame chatFrame;
	private TileFrame tileFrame;
	private ShortcutsFrame shortcutsFrame;
	private AbilitiesFrame abilitiesFrame;
	private InventoryFrame inventoryFrame;

	private PositionMemoryFrame positionMemoryFrame;

	// Edit
	private DrawLineFrame drawLineFrame;
	private FillRangeFrame fillRangeFrame;
	private DefaultTilePatternFrame defaultTilePatternFrame;
	private RegionsFrame regionsFrame;

	// Admin
	private CommandFrame commandFrame;
	private RoleTokenIssueFrame roleTokenIssueFrame;
	private RoleTokenListFrame roleTokenListFrame;
	private RoleEditorFrame roleEditorFrame;
	private ServerSettingsFrame serverSettingsFrame;

	public Gui(final Strap strap) {
		this.strap = strap;
	}

	public void disconnect() {
		if (viewPane != null) {
			viewPane.stop();
		}
		if (charactersFrame != null) {
			charactersFrame.setVisible(false);
		}
		if (chatFrame != null) {
			chatFrame.setVisible(false);
		}
		if (tileFrame != null) {
			tileFrame.setVisible(false);
		}
		if (shortcutsFrame != null) {
			shortcutsFrame.setVisible(false);
		}
		if (abilitiesFrame != null) {
			abilitiesFrame.setVisible(false);
		}
		if (inventoryFrame != null) {
			inventoryFrame.setVisible(false);
		}
	}

	@Override
	public void close() {
		if (viewPane != null) {
			viewPane.close();
		}
	}

	public void exit() {
		close();

		if (charactersFrame != null) {
			// CharactersFrame uses Swing Timers
			charactersFrame.dispose();
		}
		if (chatFrame != null) {
			chatFrame.dispose();
		}
		if (tileFrame != null) {
			tileFrame.dispose();
		}
		if (shortcutsFrame != null) {
			shortcutsFrame.dispose();
		}
		if (abilitiesFrame != null) {
			abilitiesFrame.dispose();
		}
		if (inventoryFrame != null) {
			inventoryFrame.dispose();
		}
		if (mainFrame != null) {
			mainFrame.dispose();
		}
	}

	public MainFrame getMainFrame() {
		if (mainFrame == null) {
			mainFrame = new MainFrame(strap);
			mainFrame.setLocationRelativeTo(null);
		}
		if (viewPane == null) {
			viewPane = ViewPane.newInstance(strap);
			mainFrame.add(viewPane, BorderLayout.CENTER);
		}
		return mainFrame;
	}

	public CharactersFrame getCharactersFrame() {
		if (charactersFrame == null) {
			charactersFrame = new CharactersFrame(strap);
			getViewPane().add(charactersFrame);
			SwingUtilities.invokeLater(() -> {
				center(charactersFrame);
			});
		}
		return charactersFrame;
	}

	public ViewPane getViewPane() {
		return viewPane;
	}

	public void showViewPane() {
		getViewPane().start();

		getTileFrame().setVisible(true);
		getChatFrame().setVisible(true);
		getShortcutsFrame().setVisible(true);
		getAbilitiesFrame().setVisible(true);
		getInventoryFrame().setVisible(true);

		getChatFrame().getChatPanel().setAlternateComponent(getViewPane());

		getViewPane().requestFocus();
	}

	public void hideViewPane() {
		if (chatFrame != null) {
			chatFrame.setVisible(false);
		}
		if (tileFrame != null) {
			tileFrame.setVisible(false);
		}
		if (shortcutsFrame != null) {
			shortcutsFrame.setVisible(false);
		}
		if (abilitiesFrame != null) {
			abilitiesFrame.setVisible(false);
		}
		if (inventoryFrame != null) {
			inventoryFrame.setVisible(false);
		}
	}

	public void center(final JInternalFrame frame) {
		final Dimension paneSize = getViewPane().getSize();
		final Dimension frameSize = frame.getSize();
		final int x = (paneSize.width - frameSize.width) / 2;
		final int y = (paneSize.height - frameSize.height) / 2;
		// XXX workaround for initial launch
		if (x < 0 || y < 0) {
			return;
		}
		frame.setLocation(x, y);
	}

	public ChatFrame getChatFrame() {
		if (chatFrame == null) {
			chatFrame = new ChatFrame(strap, new ChatPanel(strap));
			getViewPane().add(chatFrame);
			final Dimension size = getViewPane().getSize();
			chatFrame.setLocation(0, size.height - chatFrame.getSize().height);
		}
		return chatFrame;
	}

	public TileFrame getTileFrame() {
		if (tileFrame == null) {
			tileFrame = new TileFrame(strap);
			getViewPane().add(tileFrame);
			final Point point = getShortcutsFrame().getLocation();
			final Dimension size = getShortcutsFrame().getSize();
			tileFrame.setLocation(point.x + size.width, point.y);
		}
		return tileFrame;
	}

	public ShortcutsFrame getShortcutsFrame() {
		if (shortcutsFrame == null) {
			shortcutsFrame = new ShortcutsFrame(strap);
			getViewPane().add(shortcutsFrame);
			final Point point = getChatFrame().getLocation();
			final Dimension size = getChatFrame().getSize();
			shortcutsFrame.setLocation(point.x + size.width, point.y);
		}
		return shortcutsFrame;
	}

	public AbilitiesFrame getAbilitiesFrame() {
		if (abilitiesFrame == null) {
			abilitiesFrame = new AbilitiesFrame(strap);
			getViewPane().add(abilitiesFrame);
			final Point point = getShortcutsFrame().getLocation();
			final Dimension size = getShortcutsFrame().getSize();
			abilitiesFrame.setLocation(point.x, point.y + size.height);
		}
		return abilitiesFrame;
	}

	public InventoryFrame getInventoryFrame() {
		if (inventoryFrame == null) {
			inventoryFrame = new InventoryFrame(strap);
			getViewPane().add(inventoryFrame);
			final Dimension size = getViewPane().getSize();
			inventoryFrame.setLocation(size.width - inventoryFrame.getSize().width,
					size.height - inventoryFrame.getSize().height);
		}
		return inventoryFrame;
	}

	public void showInformation(final String message) {
		JOptionPane.showInternalMessageDialog(getViewPane(), message);
	}

	public void showText(final String text) {
		final TextFrame frame = new TextFrame(strap);
		getViewPane().add(frame);
		center(frame);
		frame.setText(text);
		frame.setVisible(true);
	}

	public void showInspection(final LocalEntityId chatLocalId, final LocalEntityId fieldLocalId, final List<LocalKeyId> userIds) {
		for (final Component c : getViewPane().getComponents()) {
			if (!(c instanceof InspectionFrame)) {
				continue;
			}
			final InspectionFrame f = (InspectionFrame) c;
			if (f.getChatLocalId().equals(chatLocalId)) {
				f.setUserIds(userIds);
				f.toFront();
				center(f);
				f.setVisible(true);
				return;
			}
		}
		final InspectionFrame f = new InspectionFrame(strap, chatLocalId, fieldLocalId, userIds);
		getViewPane().add(f);
		center(f);
		f.setVisible(true);
	}

	public PositionMemoryFrame getPositionMemoryFrame() {
		if (positionMemoryFrame == null) {
			positionMemoryFrame = new PositionMemoryFrame(strap);
			getViewPane().add(positionMemoryFrame);
			center(positionMemoryFrame);
		}
		return positionMemoryFrame;
	}

	public DrawLineFrame getDrawLineFrame() {
		if (drawLineFrame == null) {
			drawLineFrame = new DrawLineFrame(strap);
			getViewPane().add(drawLineFrame);
			center(drawLineFrame);
		}
		return drawLineFrame;
	}

	public FillRangeFrame getFillRangeFrame() {
		if (fillRangeFrame == null) {
			fillRangeFrame = new FillRangeFrame(strap);
			getViewPane().add(fillRangeFrame);
			center(fillRangeFrame);
		}
		return fillRangeFrame;
	}

	public DefaultTilePatternFrame getDefaultTilePatternFrame() {
		if (defaultTilePatternFrame == null) {
			defaultTilePatternFrame = new DefaultTilePatternFrame(strap);
			getViewPane().add(defaultTilePatternFrame);
			center(defaultTilePatternFrame);
		}
		return defaultTilePatternFrame;
	}

	public RegionsFrame getRegionsFrame() {
		if (regionsFrame == null) {
			regionsFrame = new RegionsFrame(strap);
			getViewPane().add(regionsFrame);
			center(regionsFrame);
		}
		return regionsFrame;
	}

	public CommandFrame getCommandFrame() {
		if (commandFrame == null) {
			commandFrame = new CommandFrame(strap);
			getViewPane().add(commandFrame);
			center(commandFrame);
		}
		return commandFrame;
	}

	public RoleTokenIssueFrame getRoleTokenIssueFrame() {
		if (roleTokenIssueFrame == null) {
			roleTokenIssueFrame = new RoleTokenIssueFrame(strap);
			getViewPane().add(roleTokenIssueFrame);
			center(roleTokenIssueFrame);
		}
		return roleTokenIssueFrame;
	}

	public RoleTokenListFrame getRoleTokenListFrame() {
		if (roleTokenListFrame == null) {
			roleTokenListFrame = new RoleTokenListFrame(strap);
			getViewPane().add(roleTokenListFrame);
			center(roleTokenListFrame);
		}
		return roleTokenListFrame;
	}

	public RoleEditorFrame getRoleEditorFrame() {
		if (roleEditorFrame == null) {
			roleEditorFrame = new RoleEditorFrame(strap);
			getViewPane().add(roleEditorFrame);
			center(roleEditorFrame);
		}
		return roleEditorFrame;
	}

	public void requestToken(final int handlerId, final String message) {
		byte[] token = null;
		while (true) {
			final String input = JOptionPane.showInternalInputDialog(getViewPane(), message);
			if (input == null || input.isEmpty()) {
				strap.getSession().write(new CancelTokenPacket(handlerId));
				return;
			}
			try {
				token = Base64.getDecoder().decode(input.trim());
			} catch (final IllegalArgumentException e) {
				final ResourceBundle bundle = ResourceBundle.getBundle(Gui.class.getName());
				JOptionPane.showInternalMessageDialog(getViewPane(), bundle.getString(BundleKey.InvalidToken.name()));
			}
			if (token != null) {
				break;
			}
		}
		strap.getSession().write(new TokenPacket(handlerId, token));
	}

	public ServerSettingsFrame getServerSettingsFrame() {
		if (serverSettingsFrame == null) {
			serverSettingsFrame = new ServerSettingsFrame(strap);
			getViewPane().add(serverSettingsFrame);
			center(serverSettingsFrame);
		}
		return serverSettingsFrame;
	}

	public void addMessage(final String message) {
		getChatFrame().getChatPanel().addMessage(message);
	}

	public void newConnectionFrame() {
		final ConnectionFrame frame = new ConnectionFrame(strap);
		getViewPane().add(frame);
		center(frame);
		frame.setVisible(true);
	}

}
