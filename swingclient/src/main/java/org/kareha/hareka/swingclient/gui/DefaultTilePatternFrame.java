package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ResourceBundle;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.packet.SetDefaultTilePatternPacket;
import org.kareha.hareka.field.OneUniformTilePattern;
import org.kareha.hareka.field.SolidTilePattern;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TilePattern;
import org.kareha.hareka.field.TilePatternType;
import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class DefaultTilePatternFrame extends JInternalFrame {

	private static final int FRAME_WIDTH = 384;
	private static final int FRAME_HEIGHT = 384;

	private enum BundleKey {
		Title, NotConnected,

		Type,

		Ok,
	}

	public DefaultTilePatternFrame(final Strap strap) {
		super("", true, true, true, true);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		final ResourceBundle bundle = ResourceBundle.getBundle(DefaultTilePatternFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		final JPanel typePanel = new JPanel();
		typePanel.add(new JLabel(bundle.getString(BundleKey.Type.name())));
		final JComboBox<TilePatternType> typeComboBox = new JComboBox<>(TilePatternType.values());
		typePanel.add(typeComboBox);

		final JPanel tilePanel = new JPanel();
		tilePanel.setLayout(new BoxLayout(tilePanel, BoxLayout.Y_AXIS));
		final JPanel tilePanelA = new JPanel();
		final TileSelector tileSelectorA = new TileSelector(strap);
		tilePanelA.add(tileSelectorA);
		final JTextField elevationFieldA = new JTextField("0", 6);
		tilePanelA.add(elevationFieldA);
		tilePanel.add(tilePanelA);
		final JPanel tilePanelB = new JPanel();
		final TileSelector tileSelectorB = new TileSelector(strap);
		tilePanelB.add(tileSelectorB);
		final JTextField elevationFieldB = new JTextField("0", 6);
		tilePanelB.add(elevationFieldB);
		tilePanel.add(tilePanelB);
		final JPanel tilePanelC = new JPanel();
		final TileSelector tileSelectorC = new TileSelector(strap);
		tilePanelC.add(tileSelectorC);
		final JTextField elevationFieldC = new JTextField("0", 6);
		tilePanelC.add(elevationFieldC);
		tilePanel.add(tilePanelC);

		tilePanelB.setVisible(false);
		tilePanelC.setVisible(false);

		typeComboBox.addActionListener(e -> {
			switch (typeComboBox.getItemAt(typeComboBox.getSelectedIndex())) {
			case SOLID:
				tilePanelA.setVisible(true);
				tilePanelB.setVisible(false);
				tilePanelC.setVisible(false);
				break;
			case ONE_UNIFORM:
				tilePanelA.setVisible(true);
				tilePanelB.setVisible(true);
				tilePanelC.setVisible(true);
				break;
			}
		});

		final JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		centerPanel.add(typePanel);
		centerPanel.add(tilePanel);

		final JPanel buttonPanel = new JPanel();
		final JButton okButton = new JButton(bundle.getString(BundleKey.Ok.name()));
		okButton.addActionListener(e -> {
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(this, bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			final TilePatternType type = typeComboBox.getItemAt(typeComboBox.getSelectedIndex());
			final int elevationA;
			final int elevationB;
			final int elevationC;
			try {
				elevationA = Integer.parseInt(elevationFieldA.getText());
				elevationB = Integer.parseInt(elevationFieldB.getText());
				elevationC = Integer.parseInt(elevationFieldC.getText());
			} catch (final NumberFormatException ex) {
				JOptionPane.showInternalMessageDialog(this, ex.getMessage());
				return;
			}
			final Tile tileA = Tile.valueOf(tileSelectorA.getTile(), elevationA);
			final Tile tileB = Tile.valueOf(tileSelectorB.getTile(), elevationB);
			final Tile tileC = Tile.valueOf(tileSelectorC.getTile(), elevationC);
			final TilePattern tilePattern;
			switch (type) {
			default:
				return;
			case SOLID:
				tilePattern = new SolidTilePattern(tileA);
				break;
			case ONE_UNIFORM:
				tilePattern = new OneUniformTilePattern(tileA, tileB, tileC);
				break;
			}
			session.write(new SetDefaultTilePatternPacket(tilePattern));
			setVisible(false);
		});
		buttonPanel.add(okButton);

		add(centerPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		pack();
	}

}
