package org.kareha.hareka.swingclient;

import java.io.File;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.server.Server;

public class PersistentEntities {

	private static final String DIRECTORY_NAME = "entity";

	private final Strap strap;

	public PersistentEntities(final Strap strap) {
		this.strap = strap;
	}

	public File getDirectory(final FieldEntity entity) {
		if (entity == null) {
			return null;
		}
		final Session session = strap.getSession();
		if (session == null) {
			return null;
		}
		final Server server = session.getServer();
		if (server == null) {
			return null;
		}
		final File serverDirectory = strap.getContext().getServers().getDirectory(server.getId());
		return new File(serverDirectory, DIRECTORY_NAME + File.separator + entity.getLocalId());
	}

}
