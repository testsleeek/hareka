package org.kareha.hareka.swingclient.gui;

import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.logging.Logger;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

@SuppressWarnings("serial")
public class ShortcutButtonTransferHandler extends TransferHandler {

	private static final Logger logger = Logger.getLogger(ShortcutButtonTransferHandler.class.getName());

	@Override
	public int getSourceActions(final JComponent c) {
		return MOVE;
	}

	@Override
	protected Transferable createTransferable(final JComponent c) {
		final ShortcutButton button = (ShortcutButton) c;
		if (button.getValue() instanceof AbilityButtonValue) {
			final AbilityButtonValue value = (AbilityButtonValue) button.getValue();
			return new AbilityButtonValue(true, value.getEntry());
		} else if (button.getValue() instanceof ItemButtonValue) {
			final ItemButtonValue value = (ItemButtonValue) button.getValue();
			return new ItemButtonValue(true, value.getEntry());
		} else {
			logger.warning("Unknown button value type: " + button.getValue().getClass().getName());
			return null;
		}
	}

	@Override
	protected void exportDone(final JComponent source, final Transferable data, final int action) {
		if (action == MOVE) {
			final ShortcutButton button = (ShortcutButton) source;
			button.getPanel().swap();
		}
	}

	@Override
	public boolean canImport(TransferHandler.TransferSupport support) {
		if (!support.isDrop()) {
			return false;
		}
		return support.isDataFlavorSupported(ShortcutButtonValue.shortcutFlavor);
	}

	@Override
	public boolean importData(TransferHandler.TransferSupport support) {
		if (!canImport(support)) {
			return false;
		}
		final ShortcutButtonValue value;
		try {
			value = (ShortcutButtonValue) support.getTransferable().getTransferData(ShortcutButtonValue.shortcutFlavor);
		} catch (final UnsupportedFlavorException | IOException e) {
			return false;
		}
		final ShortcutButton button = (ShortcutButton) support.getComponent();
		if (value.isShortcut()) {
			button.getPanel().setSwapButton(button);
		} else {
			if (value instanceof AbilityButtonValue) {
				button.setRepeating(true);
				button.setSelfTargetting(false);
			} else if (value instanceof ItemButtonValue) {
				button.setRepeating(false);
				button.setSelfTargetting(true);
			}
			button.send(value);
		}
		return true;
	}

}