package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.swingclient.SwingConnector;

@SuppressWarnings("serial")
public class ConnectionFrame extends JInternalFrame {

	private enum BundleKey {
		Connection, Connect,
	}

	private final Strap strap;
	private final ConnectionPanel connectionPanel;
	private final JButton connectButton;

	public ConnectionFrame(final Strap strap) {
		this.strap = strap;
		final ResourceBundle bundle = ResourceBundle.getBundle(ConnectionFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Connection.name()));

		connectionPanel = new ConnectionPanel(strap);

		connectButton = new JButton(bundle.getString(BundleKey.Connect.name()));
		connectButton.addActionListener(e -> doConnect());
		connectButton.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					doConnect();
				}
			}
		});

		add(connectionPanel, BorderLayout.CENTER);
		final JPanel buttonPanel = new JPanel();
		buttonPanel.add(connectButton);
		add(buttonPanel, BorderLayout.SOUTH);
		pack();
	}

	@Private
	void doConnect() {
		connectionPanel.save();
		dispose();
		SwingConnector.connect(strap);
	}

}
