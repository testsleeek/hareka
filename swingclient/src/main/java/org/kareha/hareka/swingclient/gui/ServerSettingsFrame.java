package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ResourceBundle;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.packet.SetUserRegistrationModePacket;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.user.UserRegistrationMode;

@SuppressWarnings("serial")
public class ServerSettingsFrame extends JInternalFrame {

	private static final int FRAME_WIDTH = 512;
	private static final int FRAME_HEIGHT = 384;

	private enum BundleKey {
		Title, NotConnected,

		UserRegistrationMode,
	}

	private final JComboBox<UserRegistrationMode> modeComboBox;
	private boolean tempDisabled;

	public ServerSettingsFrame(final Strap strap) {
		super("", true, true, true, true);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		final ResourceBundle bundle = ResourceBundle.getBundle(ServerSettingsFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		final JPanel modePanel = new JPanel();
		modePanel.add(new JLabel(bundle.getString(BundleKey.UserRegistrationMode.name())));
		modeComboBox = new JComboBox<>(UserRegistrationMode.values());
		modeComboBox.addActionListener(e -> {
			if (tempDisabled) {
				return;
			}
			final int index = modeComboBox.getSelectedIndex();
			if (index == -1) {
				return;
			}
			final UserRegistrationMode mode = modeComboBox.getItemAt(index);
			final Session session = strap.getSession();
			if (session == null) {
				JOptionPane.showInternalMessageDialog(this, bundle.getString(BundleKey.NotConnected.name()));
				return;
			}
			session.write(new SetUserRegistrationModePacket(mode));
		});
		modePanel.add(modeComboBox);

		final JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		centerPanel.add(modePanel);

		add(centerPanel, BorderLayout.CENTER);
		setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		pack();
	}

	public void setSettings(final UserRegistrationMode mode) {
		tempDisabled = true;
		modeComboBox.setSelectedItem(mode);
		tempDisabled = false;
	}

}
