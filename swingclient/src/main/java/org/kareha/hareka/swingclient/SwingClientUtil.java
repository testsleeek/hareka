package org.kareha.hareka.swingclient;

import java.io.File;
import java.io.IOException;
import java.security.KeyPair;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.annotation.NotPure;
import org.kareha.hareka.key.KeyStack;
import org.kareha.hareka.swingclient.gui.Gui;
import org.kareha.hareka.ui.swing.PasswordReader;

public final class SwingClientUtil {

	private enum BundleKey {
		PasswordReaderTitle, PasswordReaderMessage,
	}

	private SwingClientUtil() {
		throw new AssertionError();
	}

	public static KeyPair getKeyPair(final Strap strap, final int version) throws JAXBException {
		final Gui gui = strap.getGui();
		final KeyStack stack = strap.getContext().getKeyStack();
		if (version > 0) {
			final ResourceBundle bundle = ResourceBundle.getBundle(SwingClientUtil.class.getName());
			final String title = bundle.getString(BundleKey.PasswordReaderTitle.name());
			final String message = MessageFormat.format(bundle.getString(BundleKey.PasswordReaderMessage.name()),
					version);
			return stack.get(version, new PasswordReader(gui.getMainFrame(), title, message));
		}
		return stack.get(version, new PasswordReader(gui.getMainFrame()));
	}

	@NotPure
	public static void runUpdater(final File dataDirectory, final String url, final List<String> clientArgs)
			throws IOException {
		final List<String> argList = new ArrayList<>();
		argList.add(System.getProperty("java.home") + File.separator + "bin" + File.separator + "java");
		argList.add("-jar");
		argList.add(SwingClientConstants.UPDATER_PATH);
		argList.add("--update");
		if (dataDirectory != null) {
			argList.add("--datadir=" + dataDirectory.getAbsolutePath());
		}
		if (url != null) {
			argList.add("--url=" + url);
		}
		if (clientArgs != null) {
			argList.add("--");
			argList.addAll(clientArgs);
		}
		new ProcessBuilder(argList).directory(new File(System.getProperty("user.dir"))).start();
	}

}
