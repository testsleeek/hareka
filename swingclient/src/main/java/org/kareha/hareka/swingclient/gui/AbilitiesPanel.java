package org.kareha.hareka.swingclient.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.logging.Logger;

import javax.swing.JComponent;

import org.kareha.hareka.game.AbilityType;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.swingclient.SwingClientConstants;

@SuppressWarnings("serial")
public class AbilitiesPanel extends JComponent {

	private static final Logger logger = Logger.getLogger(AbilitiesPanel.class.getName());

	private static final int COLUMNS = 4;
	private static final int ROWS = 2;

	private final Strap strap;
	private final GridLayout layout = new GridLayout(0, COLUMNS);
	private int abilitiesSize;
	private AbilityButton selectedButton;
	@SuppressWarnings("unused")
	private AbilityButton dragButton;
	private AbilityButton swapButton;
	private int count; // XXX temporal implementation

	public AbilitiesPanel(final Strap strap) {
		this.strap = strap;
		setLayout(layout);
		helperClear();
	}

	int getAbilitiesSize() {
		return abilitiesSize;
	}

	void setAbilitiesSize(final int v) {
		abilitiesSize = v;
		for (final Component c : getComponents()) {
			c.repaint();
		}
	}

	private int getAbilitiesRows() {
		return Math.max((abilitiesSize + COLUMNS - 1) / COLUMNS, ROWS);
	}

	private void extend(final int maxIndex) {
		final int requiredRows = maxIndex / COLUMNS + 1;
		final int currentRows = layout.getRows();
		if (requiredRows > currentRows) {
			layout.setRows(requiredRows);
			for (int j = currentRows; j < requiredRows; j++) {
				for (int i = 0; i < COLUMNS; i++) {
					final int index = COLUMNS * j + i;
					final AbilityButton button = new AbilityButton(strap, this, index);
					add(button);
				}
			}
			validate();
		}
	}

	private void helperClear() {
		removeAll();
		layout.setRows(0);
		extend(COLUMNS * getAbilitiesRows() - 1);
		count = 0;
	}

	public void clear() {
		helperClear();
	}

	public AbilityButton getButton(final int index) {
		extend(index);
		return (AbilityButton) getComponent(index);
	}

	public AbilityButton getButton(final AbilityType type) {
		for (int j = 0, r = layout.getRows(); j < r; j++) {
			for (int i = 0; i < COLUMNS; i++) {
				final int index = COLUMNS * j + i;
				final AbilityButton button = (AbilityButton) getComponent(index);
				final AbilityButtonValue value = button.getValue();
				if (value == null) {
					continue;
				}
				if (value.getEntry().getType().equals(type)) {
					return button;
				}
			}
		}
		return null;
	}

	public void setValue(final int index, final AbilityButtonValue value) {
		final AbilityButton button = getButton(index);
		button.setValue(value);
	}

	public void addValue(final AbilityButtonValue value) {
		// XXX temporal implementation
		setValue(count, value);
		count++;
	}

	@SuppressWarnings("unused")
	private void repaintButtons() {
		for (int j = 0, r = layout.getRows(); j < r; j++) {
			for (int i = 0; i < COLUMNS; i++) {
				final int index = COLUMNS * j + i;
				final AbilityButton button = (AbilityButton) getComponent(index);
				button.repaint();
			}
		}
	}

	public AbilityButton getSelectedButton() {
		return selectedButton;
	}

	void setSelectedButton(final AbilityButton v) {
		final AbilityButton b = selectedButton;
		selectedButton = v;
		if (b != null) {
			b.repaint();
		}
		if (v != null) {
			v.repaint();
		}
	}

	void setDragButton(final AbilityButton v) {
		dragButton = v;
	}

	void setSwapButton(final AbilityButton v) {
		swapButton = v;
	}

	void swap() {
		if (swapButton == null) {
			return;
		}
		// gui.handle().mirror().skills().swapSkills(dragButton.getIndex(),
		// swapButton.getIndex());
		// TODO implement ability swapping
		logger.info("Swapping ability buttons is not implemented");
		swapButton = null;
	}

	public void swapAbilities(final int indexA, final int indexB) {
		final AbilityButton buttonA = getButton(indexA);
		final AbilityButton buttonB = getButton(indexB);
		final AbilityButtonValue value = buttonA.getValue();
		buttonA.setValue(buttonB.getValue());
		buttonB.setValue(value);

		final ShortcutButton sa = strap.getGui().getShortcutsFrame().getPanel().getButton(buttonA.getValue());
		if (sa != null) {
			if (sa.getValue() != null) {
				// sa.getValue().setTableIndex(indexA);
				// sa.send();
			}
		}
		final ShortcutButton sb = strap.getGui().getShortcutsFrame().getPanel().getButton(buttonB.getValue());
		if (sb != null) {
			if (sb.getValue() != null) {
				// sb.getValue().setTableIndex(indexB);
				// sb.send();
			}
		}
	}

	public Dimension getViewportSize() {
		return new Dimension(SwingClientConstants.ICON_WIDTH * COLUMNS, SwingClientConstants.ICON_HEIGHT * ROWS);
	}

}