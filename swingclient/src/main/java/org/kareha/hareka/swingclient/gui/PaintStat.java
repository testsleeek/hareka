package org.kareha.hareka.swingclient.gui;

class PaintStat {

	private long startTime;

	private int count;
	private long frameCountStartTime = Long.MIN_VALUE;
	private float frameRate;
	private long paintTimeSum;
	private float paintLoad;

	void startPaint() {
		startTime = System.nanoTime();
	}

	void endPaint(final int targetFrameRate) {
		paintTimeSum += System.nanoTime() - startTime;
		if (count % targetFrameRate == 0) {
			if (frameCountStartTime != Long.MIN_VALUE) {
				frameRate = targetFrameRate * 1e9f / (startTime - frameCountStartTime);
			}
			frameCountStartTime = startTime;

			paintLoad = paintTimeSum / 1e9f;
			paintTimeSum = 0;
		}
		count++;
	}

	float getFrameRate() {
		return frameRate;
	}

	float getPaintLoad() {
		return paintLoad;
	}

}
