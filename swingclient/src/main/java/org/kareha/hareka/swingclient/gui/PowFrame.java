package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.math.BigInteger;
import java.util.ResourceBundle;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.FocusManager;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.client.packet.CancelPowPacket;
import org.kareha.hareka.client.packet.PowPacket;
import org.kareha.hareka.pow.ProofOfWork;
import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class PowFrame extends JInternalFrame {

	private enum BundleKey {
		ProofOfWork, Cancel,
	}

	private static final int PREF_WIDTH = 256;
	private static final int PREF_HEIGHT = 128;

	@Private
	final Strap strap;
	@Private
	final JProgressBar progressBar;

	private final PowWorker worker;

	public PowFrame(final Strap strap, final int handlerId, final BigInteger target, final byte[] data,
			final String message) {
		super("", false, false, false, false);
		this.strap = strap;
		final Gui gui = strap.getGui();
		final ResourceBundle bundle = ResourceBundle.getBundle(PowFrame.class.getName());

		final Component focusedComponent = FocusManager.getCurrentManager().getFocusOwner();
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosed(final InternalFrameEvent e) {
				if (focusedComponent != null) {
					focusedComponent.requestFocus();
				}
			}

			@Override
			public void internalFrameOpened(final InternalFrameEvent e) {
				SwingUtilities.invokeLater(() -> {
					if (focusedComponent != null) {
						focusedComponent.requestFocus();
					}
				});
			}
		});

		worker = new PowWorker(handlerId, target, data);

		final JLabel messageLabel = new JLabel(message);
		progressBar = new JProgressBar();

		final JButton cancelButton = new JButton(bundle.getString(BundleKey.Cancel.name()));
		cancelButton.addActionListener(e -> worker.cancel(true));

		add(messageLabel, BorderLayout.NORTH);
		final JPanel barPanel = new JPanel();
		barPanel.add(progressBar);
		add(barPanel, BorderLayout.CENTER);
		final JPanel buttonPanel = new JPanel();
		buttonPanel.add(cancelButton);
		add(buttonPanel, BorderLayout.SOUTH);

		setPreferredSize(new Dimension(PREF_WIDTH, PREF_HEIGHT));
		pack();

		gui.getViewPane().add(this);
		gui.center(this);
		setVisible(true);

		worker.execute();
	}

	private class PowWorker extends SwingWorker<byte[], Integer> {

		private final int handlerId;
		private final BigInteger target;
		private final byte[] data;

		PowWorker(final int handlerId, final BigInteger target, final byte[] data) {
			this.handlerId = handlerId;
			this.target = target;
			this.data = data;
		}

		@Override
		protected byte[] doInBackground() throws InterruptedException {
			final ProofOfWork pow = new ProofOfWork(target, data);
			return pow.generate((goal, current) -> {
				SwingUtilities.invokeLater(() -> progressBar.setValue(100 * current / goal));
			});
		}

		@Override
		protected void done() {
			final Gui gui = strap.getGui();
			try {
				strap.getSession().write(new PowPacket(handlerId, get()));
			} catch (final CancellationException e) {
				strap.getSession().write(new CancelPowPacket(handlerId));
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
				return;
			} catch (final ExecutionException e) {
				JOptionPane.showInternalMessageDialog(gui.getViewPane(), e.getMessage());
				return;
			} finally {
				gui.getViewPane().remove(PowFrame.this);
				dispose();
			}
		}
	}

}
