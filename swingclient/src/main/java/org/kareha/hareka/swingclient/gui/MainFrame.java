package org.kareha.hareka.swingclient.gui;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	private enum BundleKey {
		Title,
	}

	private static final int width = 800;
	private static final int height = 600;

	public MainFrame(final Strap strap) {
		final ResourceBundle bundle = ResourceBundle.getBundle(MainFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				strap.exit();
			}
		});
		setJMenuBar(new MainMenuBar(strap));
		setPreferredSize(new Dimension(width, height));
		pack();
	}

}
