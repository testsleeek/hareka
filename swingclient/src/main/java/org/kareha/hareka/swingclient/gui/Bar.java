package org.kareha.hareka.swingclient.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JComponent;

import org.kareha.hareka.ui.swing.SwingUtil;

@SuppressWarnings("serial")
public class Bar extends JComponent {

	private final Color bgColor;
	private final Color fgColor;
	private final Color maxColor;
	private int max;
	private int current;
	private final NumberFormat format = new DecimalFormat("##0.0");

	public Bar(final Color bgColor, final Color fgColor, final Color maxColor) {
		this.bgColor = bgColor;
		this.fgColor = fgColor;
		this.maxColor = maxColor;
	}

	public void setValue(final int max, final int current) {
		this.max = max;
		this.current = current;
		repaint();
	}

	@Override
	protected void paintComponent(final Graphics g) {
		final Dimension size = getSize();
		final float ratio;
		if (max == 0) {
			ratio = 1;
		} else {
			ratio = (float) current / max;
		}
		g.setColor(bgColor);
		g.fillRect(0, 0, size.width, size.height);
		if (current >= max) {
			g.setColor(maxColor);
		} else {
			g.setColor(fgColor);
		}
		g.fillRect(0, 0, (int) (size.width * ratio), size.height);
		final String str = format.format(100 * ratio) + "%";
		final FontMetrics fm = g.getFontMetrics();
		final int strWidth = fm.stringWidth(str);
		final int strHeight = fm.getAscent();
		SwingUtil.drawString(g, str, (size.width - strWidth) / 2, strHeight + (size.height - strHeight) / 2);
	}

}
