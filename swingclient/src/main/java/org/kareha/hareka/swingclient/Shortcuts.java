package org.kareha.hareka.swingclient;

import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.annotation.GuardedBy;
import org.kareha.hareka.annotation.Immutable;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.annotation.ThreadSafe;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.mirror.AbilityMirror;
import org.kareha.hareka.game.AbilityType;
import org.kareha.hareka.util.FileUtil;
import org.kareha.hareka.util.JaxbUtil;

@ThreadSafe
public class Shortcuts {

	private static final Logger logger = Logger.getLogger(Shortcuts.class.getName());
	private static final String FILENAME = "Shortcuts.xml";

	@Immutable
	@XmlJavaTypeAdapter(Shortcuts.Entry.Adapter.class)
	public static abstract class Entry {

		@Private
		final boolean repeating;
		@Private
		final boolean selfTargetting;
		@Private
		final int keyCode;

		public Entry(final boolean repeating, final boolean selfTargetting, final int keyCode) {
			this.repeating = repeating;
			this.selfTargetting = selfTargetting;
			this.keyCode = keyCode;
		}

		@XmlType(name = "entry")
		@XmlSeeAlso({ Shortcuts.BlankEntry.Adapted.class, Shortcuts.AbilityEntry.Adapted.class,
				Shortcuts.ItemEntry.Adapted.class })
		@XmlAccessorType(XmlAccessType.NONE)
		private static abstract class Adapted {

			@XmlElement
			protected boolean repeating;
			@XmlElement
			protected boolean selfTargetting;
			@XmlElement
			protected int keyCode;

			protected Adapted() {
				// used by JAXB
			}

			protected Adapted(final Entry v) {
				repeating = v.repeating;
				selfTargetting = v.selfTargetting;
				keyCode = v.keyCode;
			}

			protected abstract Entry unmarshal();

		}

		protected abstract Adapted marshal();

		static class Adapter extends XmlAdapter<Adapted, Entry> {

			@Override
			public Adapted marshal(final Entry v) throws Exception {
				if (v == null) {
					return null;
				}
				return v.marshal();
			}

			@Override
			public Entry unmarshal(final Adapted v) throws Exception {
				if (v == null) {
					return null;
				}
				return v.unmarshal();
			}

		}

		public boolean isRepeating() {
			return repeating;
		}

		public boolean isSelfTargetting() {
			return selfTargetting;
		}

		public int getKeyCode() {
			return keyCode;
		}

	}

	@Immutable
	public static class BlankEntry extends Entry {

		public BlankEntry(final boolean repeating, final boolean selfTargetting, final int keyCode) {
			super(repeating, selfTargetting, keyCode);
		}

		@XmlType(name = "blankEntry")
		@XmlAccessorType(XmlAccessType.NONE)
		private static class Adapted extends Shortcuts.Entry.Adapted {

			@SuppressWarnings("unused")
			protected Adapted() {
				// used by JAXB
			}

			protected Adapted(final BlankEntry v) {
				super(v);
			}

			@Override
			protected BlankEntry unmarshal() {
				return new BlankEntry(repeating, selfTargetting, keyCode);
			}

		}

		@Override
		protected Adapted marshal() {
			return new Adapted(this);
		}

	}

	@Immutable
	public static class AbilityEntry extends Entry {

		@Private
		final AbilityType type;

		public AbilityEntry(final boolean repeating, final boolean selfTargetting, final int keyCode,
				final AbilityType type) {
			super(repeating, selfTargetting, keyCode);
			this.type = type;
		}

		@XmlType(name = "abilityEntry")
		@XmlAccessorType(XmlAccessType.NONE)
		private static class Adapted extends Shortcuts.Entry.Adapted {

			@XmlElement
			protected AbilityType type;

			@SuppressWarnings("unused")
			protected Adapted() {
				// used by JAXB
			}

			protected Adapted(final AbilityEntry v) {
				super(v);
				type = v.type;
			}

			@Override
			protected AbilityEntry unmarshal() {
				return new AbilityEntry(repeating, selfTargetting, keyCode, type);
			}

		}

		@Override
		public Adapted marshal() {
			return new Adapted(this);
		}

		public AbilityType getType() {
			return type;
		}

	}

	@Immutable
	public static class ItemEntry extends Entry {

		@Private
		final LocalEntityId id;

		public ItemEntry(final boolean repeating, final boolean selfTargetting, final int keyCode, final LocalEntityId id) {
			super(repeating, selfTargetting, keyCode);
			this.id = id;
		}

		@XmlType(name = "itemEntry")
		@XmlAccessorType(XmlAccessType.NONE)
		private static class Adapted extends Shortcuts.Entry.Adapted {

			@XmlElement
			protected LocalEntityId id;

			@SuppressWarnings("unused")
			protected Adapted() {
				// used by JAXB
			}

			protected Adapted(final ItemEntry v) {
				super(v);
				id = v.id;
			}

			@Override
			protected ItemEntry unmarshal() {
				return new ItemEntry(repeating, selfTargetting, keyCode, id);
			}

		}

		@Override
		public Adapted marshal() {
			return new Adapted(this);
		}

		public LocalEntityId getId() {
			return id;
		}

	}

	public static interface Listener {

		void shortcutsCleared();

		void shortcutAdded(int index, Entry entry);

		void shortcutsSwapped(int indexA, int indexB);

	}

	private final List<Listener> listeners = new CopyOnWriteArrayList<>();
	private final Strap strap;
	@GuardedBy("this")
	private FieldEntity owner;
	@GuardedBy("this")
	@Private
	final Map<Integer, Entry> map = new HashMap<>();

	public Shortcuts(final Strap strap) {
		this.strap = strap;
	}

	@XmlType(name = "shortcutEntry")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class ShortcutEntry {

		@XmlAttribute
		@Private
		int index;
		@XmlElement
		@Private
		Entry entry;

		@SuppressWarnings("unused")
		private ShortcutEntry() {
			// used by JAXB
		}

		@Private
		ShortcutEntry(final int index, final Entry entry) {
			this.index = index;
			this.entry = entry;
		}

	}

	@XmlRootElement(name = "shortcuts")
	@XmlAccessorType(XmlAccessType.NONE)
	private static class Adapted {

		@XmlElement(name = "shortcut")
		@Private
		List<ShortcutEntry> list;

		@SuppressWarnings("unused")
		private Adapted() {
			// used by JAXB
		}

		@Private
		Adapted(final Shortcuts v) {
			list = new ArrayList<>();
			synchronized (v) {
				for (final Map.Entry<Integer, Entry> entry : v.map.entrySet()) {
					list.add(new ShortcutEntry(entry.getKey(), entry.getValue()));
				}
			}
		}
	}

	@GuardedBy("this")
	private File getFile() {
		final File entityDirectory = strap.getPersistentEntities().getDirectory(owner);
		if (entityDirectory == null) {
			return null;
		}
		return new File(entityDirectory, FILENAME);
	}

	@GuardedBy("this")
	private boolean load() throws IOException, JAXBException {
		final File file = getFile();
		if (file == null) {
			return false;
		}
		if (!file.isFile()) {
			return false;
		}
		final Adapted adapted = JaxbUtil.unmarshal(file, Adapted.class);
		for (final ShortcutEntry entry : adapted.list) {
			map.put(entry.index, entry.entry);
		}
		return true;
	}

	@GuardedBy("this")
	private void save() throws IOException, JAXBException {
		final File file = getFile();
		if (file == null) {
			return;
		}
		FileUtil.ensureDirectoryExists(file.getParentFile());
		JaxbUtil.marshal(new Adapted(this), file);
	}

	public void addListener(final Listener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(final Listener listener) {
		return listeners.remove(listener);
	}

	public Collection<Listener> getListeners() {
		return new ArrayList<>(listeners);
	}

	private int getNumberKeyCode(final int index) {
		switch (index) {
		default:
			return KeyEvent.VK_UNDEFINED;
		case 0:
			return KeyEvent.VK_1;
		case 1:
			return KeyEvent.VK_2;
		case 2:
			return KeyEvent.VK_3;
		case 3:
			return KeyEvent.VK_4;
		case 4:
			return KeyEvent.VK_5;
		case 5:
			return KeyEvent.VK_6;
		case 6:
			return KeyEvent.VK_7;
		case 7:
			return KeyEvent.VK_8;
		case 8:
			return KeyEvent.VK_9;
		case 9:
			return KeyEvent.VK_0;
		}
	}

	public void setOwner(final FieldEntity v) {
		synchronized (this) {
			if (owner != null) {
				try {
					save();
				} catch (final IOException | JAXBException e) {
					logger.log(Level.SEVERE, "", e);
				}
			}
			owner = v;
			map.clear();
		}
		for (final Listener listener : listeners) {
			listener.shortcutsCleared();
		}
		final Collection<Map.Entry<Integer, Entry>> entrySet;
		synchronized (this) {
			if (owner != null) {
				try {
					if (!load()) {
						final List<AbilityMirror.Entry> abilities = new ArrayList<>(
								strap.getSession().getMirrors().getAbilityMirror().getEntries());
						for (int i = 0; i <= 9; i++) {
							if (i < abilities.size()) {
								map.put(i,
										new AbilityEntry(true, false, getNumberKeyCode(i), abilities.get(i).getType()));
							} else {
								map.put(i, new BlankEntry(false, false, getNumberKeyCode(i)));
							}
						}
					}
				} catch (final IOException | JAXBException e) {
					logger.log(Level.SEVERE, "", e);
				}
			}
			entrySet = new ArrayList<>(map.entrySet());
		}
		for (final Map.Entry<Integer, Entry> entry : entrySet) {
			for (final Listener listener : listeners) {
				listener.shortcutAdded(entry.getKey(), entry.getValue());
			}
		}
	}

	public void addShortcut(final int index, final Entry entry) {
		synchronized (this) {
			map.put(index, entry);
		}
		for (final Listener listener : listeners) {
			listener.shortcutAdded(index, entry);
		}
	}

	public void swapShortcuts(final int indexA, final int indexB) {
		synchronized (this) {
			final Entry entryA = map.get(indexA);
			final Entry entryB = map.get(indexB);
			if (entryA == null) {
				map.remove(indexB);
			} else {
				map.put(indexB, entryA);
			}
			if (entryB == null) {
				map.remove(indexA);
			} else {
				map.put(indexA, entryB);
			}
		}
		for (final Listener listener : listeners) {
			listener.shortcutsSwapped(indexA, indexB);
		}
	}

}