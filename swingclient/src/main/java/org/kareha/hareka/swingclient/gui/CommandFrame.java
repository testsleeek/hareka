package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ResourceBundle;

import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.kareha.hareka.client.Session;
import org.kareha.hareka.client.packet.CommandPacket;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.util.CommandLineTokenizer;
import org.kareha.hareka.util.SimpleLogger;

@SuppressWarnings("serial")
public class CommandFrame extends JInternalFrame {

	private enum BundleKey {
		Command, NotConnected,
	}

	private final CommandPanel commandPanel;

	public CommandFrame(final Strap strap) {
		super("", true, true, true, true);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		final ResourceBundle bundle = ResourceBundle.getBundle(CommandFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Command.name()));
		commandPanel = new CommandPanel(strap);
		add(commandPanel, BorderLayout.CENTER);
		setPreferredSize(new Dimension(384, 192));
		pack();
	}

	public void addOut(final String result) {
		commandPanel.addOut(result);
	}

	private static class CommandPanel extends JComponent {

		private static final String lineSeparator = System.getProperty("line.separator");

		private final JTextPane pane;
		private final Style echoStyle;
		private final Style outStyle;
		private final JScrollPane scroll;
		private int maxLogSize = 4 * 1024 * 1024;

		CommandPanel(final Strap strap) {
			final Gui gui = strap.getGui();
			final ResourceBundle bundle = ResourceBundle.getBundle(CommandFrame.class.getName());

			pane = new JTextPane();
			pane.setEditable(false);
			echoStyle = pane.addStyle("echo", null);
			StyleConstants.setForeground(echoStyle, Color.blue);
			outStyle = pane.addStyle("out", null);
			StyleConstants.setForeground(outStyle, Color.black);

			scroll = new JScrollPane(pane, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
					ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

			final JTextField input = new JTextField();
			input.addActionListener(e -> {
				final Session session = strap.getSession();
				if (session == null) {
					JOptionPane.showInternalMessageDialog(gui.getViewPane(),
							bundle.getString(BundleKey.NotConnected.name()));
					return;
				}
				final String line = input.getText();
				input.setText("");
				addEcho(line);
				final CommandLineTokenizer t = new CommandLineTokenizer(line);
				final String[] args = t.getTokens();
				session.write(new CommandPacket(args));
			});

			setLayout(new BorderLayout());
			add(scroll, BorderLayout.CENTER);
			add(input, BorderLayout.SOUTH);
		}

		private void trimLogText() {
			final StyledDocument d = pane.getStyledDocument();
			if (d.getLength() >= maxLogSize) {
				final int p0 = d.getLength() - maxLogSize / 2;
				try {
					final int p;
					final String text = d.getText(p0, d.getLength() - p0);
					final String ls = System.getProperty("line.separator");
					final int next = text.indexOf(ls);
					if (next == -1) {
						p = p0;
					} else {
						p = p0 + next + ls.length();
					}
					d.remove(0, p);
				} catch (final BadLocationException e) {
					SimpleLogger.INSTANCE.log(e);
				}
			}
		}

		private void scrollBottom() {
			final StyledDocument d = pane.getStyledDocument();
			pane.setCaretPosition(d.getLength());
		}

		private boolean isBottom() {
			final JScrollBar bar = scroll.getVerticalScrollBar();
			// * 3 / 2 : workaround
			return bar.getValue() + bar.getSize().height * 3 / 2 >= bar.getMaximum();
		}

		// To prevent loop, don't use Logger here. Use SimpleLogger instead.
		private void addLine(final Style style, final String s) {
			trimLogText();
			final boolean bottom = isBottom();

			final StyledDocument d = pane.getStyledDocument();
			try {
				d.insertString(d.getLength(), s, style);
			} catch (final BadLocationException e) {
				SimpleLogger.INSTANCE.log(e);
			}

			if (bottom) {
				scrollBottom();
			}
		}

		void addEcho(final String line) {
			addLine(echoStyle, line + lineSeparator);
		}

		void addOut(final String result) {
			addLine(outStyle, result);
		}

	}

}
