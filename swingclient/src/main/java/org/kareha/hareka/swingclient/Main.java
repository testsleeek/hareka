package org.kareha.hareka.swingclient;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;
import javax.xml.bind.JAXBException;

import org.kareha.hareka.Constants;
import org.kareha.hareka.client.Context;
import org.kareha.hareka.sound.SoundContext;
import org.kareha.hareka.ui.UiType;
import org.kareha.hareka.util.FileLogger;
import org.kareha.hareka.util.FileUtil;
import org.kareha.hareka.util.LogUtil;
import org.kareha.hareka.util.SingleLaunch;

final class Main {

	private static final Logger logger = Logger.getLogger(Main.class.getName());
	private static Logger appRootLogger;

	private Main() {
		throw new AssertionError();
	}

	public static void main(final String[] args) {
		appRootLogger = Logger.getLogger("org.kareha.hareka");
		LogUtil.loadLogLevel(appRootLogger);
		final SwingClientParameters parameters = new SwingClientParameters(args);
		FileUtil.ensureDirectoryExistsOrExit(parameters.getDataDirectory(), UiType.SWING);
		SingleLaunch.INSTANCE.ensure(parameters.getDataDirectory(), UiType.SWING);
		FileLogger.connect(appRootLogger, parameters.getDataDirectory(), UiType.SWING);
		Thread.setDefaultUncaughtExceptionHandler((t, e) -> logger.log(Level.SEVERE, t.getName(), e));

		SwingUtilities.invokeLater(() -> {
			try {
				// final Context context = new Context(new Version("test"),
				// parameters.getDataDirectory());
				final Context context = new Context(Constants.VERSION, parameters.getDataDirectory());
				final SoundContext soundContext = new SoundContext(parameters.getDataDirectory());
				final Strap strap = new Strap(parameters, context, soundContext);

				strap.getGui().getMainFrame().setVisible(true);

				SwingConnector.connect(strap);
			} catch (final IOException | JAXBException e) {
				logger.log(Level.SEVERE, "", e);
			}
		});
	}

}
