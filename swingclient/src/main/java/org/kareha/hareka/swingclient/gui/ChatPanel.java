package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.client.chat.ChatSession;
import org.kareha.hareka.client.chat.LocalChatSession;
import org.kareha.hareka.client.chat.PrivateChatSession;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.packet.InspectChatEntityPacket;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.util.HtmlUtil;

@SuppressWarnings("serial")
public class ChatPanel extends JComponent {

	private static final Logger logger = Logger.getLogger(ChatPanel.class.getName());
	private static final Color MESSAGE_COLOR = new Color(0xff, 0x80, 0x40);

	@Private
	final Strap strap;
	@Private
	final JEditorPane pane;
	private final JScrollPane scroll;
	private final JTextField input;
	@Private
	final JComboBox<ChannelItem> channelItems;
	@Private
	final Map<LocalEntityId, ChatEntity> entityMap = new HashMap<>();
	private JComponent alternateComponent;

	public ChatPanel(final Strap strap) {
		this.strap = strap;
		pane = new JEditorPane("text/html", "");
		pane.setEditable(false);
		pane.addHyperlinkListener(new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(final HyperlinkEvent e) {
				if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
					if (e.getURL() == null) {
						return;
					}
					final Map<String, String> map = parseQueryString(e.getURL().getPath());
					final String localIdString = map.get("localId");
					if (localIdString == null) {
						return;
					}
					final ChatEntity entity = entityMap.get(LocalEntityId.valueOf(localIdString));
					// if (entity == null
					// ||
					// entity.equals(gui.getSession().getMirrors().getSelfMirror().getChatEntity()))
					// {
					// return;
					// }
					if (entity == null) {
						return;
					}
					strap.getSession().write(new InspectChatEntityPacket(entity));
					// for (int i = 0; i < channelItems.getItemCount(); i++) {
					// final ChannelItem channelItem =
					// channelItems.getItemAt(i);
					// if (!(channelItem instanceof PrivateChannelItem)) {
					// continue;
					// }
					// final PrivateChannelItem privateChannelItem =
					// (PrivateChannelItem) channelItem;
					// if (privateChannelItem.getPeer().equals(entity)) {
					// channelItems.setSelectedItem(channelItem);
					// input.requestFocus();
					// return;
					// }
					// }
					// final PrivateChatSession chatChannel =
					// gui.getSession().getMirrors().getChatMirror()
					// .getPrivateChatSession(entity);
					// final ChannelItem channelItem =
					// getChatPanelChannel(chatChannel);
					// channelItems.setSelectedItem(channelItem);
					// input.requestFocus();
				}
			}
		});
		pane.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(final MouseEvent e) {
				if (!e.isPopupTrigger()) {
					return;
				}
				final JPopupMenu popup = new JPopupMenu();
				// final JMenuItem item = new JMenuItem("Test");
				// popup.add(item);
				popup.show(pane, e.getX(), e.getY());
			}
		});

		scroll = new JScrollPane(pane, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		channelItems = new JComboBox<>();

		input = new JTextField();
		input.addActionListener(e -> {
			final String content = input.getText();
			if (content.isEmpty()) {
				if (alternateComponent != null) {
					alternateComponent.requestFocus();
				}
			} else {
				input.setText("");
				final ChannelItem channelItem = this.channelItems.getItemAt(this.channelItems.getSelectedIndex());
				channelItem.chatEntered(content);
			}
		});
		input.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent e) {
				if (!e.isControlDown()) {
					return;
				}
				for (int i = 0; i < channelItems.getItemCount(); i++) {
					final ChannelItem channelItem = channelItems.getItemAt(i);
					if (e.getKeyCode() == channelItem.getShortcut()) {
						channelItems.setSelectedIndex(i);
						break;
					}
				}
			}
		});

		channelItems.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(final MouseEvent e) {
				if (!e.isPopupTrigger()) {
					return;
				}
				final JPopupMenu popup = new JPopupMenu();
				final ChannelItem channelItem = channelItems.getItemAt(channelItems.getSelectedIndex());
				if (channelItem.isRemovable()) {
					final JMenuItem item = new JMenuItem("Remove " + channelItem);
					item.addActionListener(ev -> {
						if (!(channelItem instanceof PrivateChannelItem)) {
							return;
						}
						final PrivateChannelItem privateChannelItem = (PrivateChannelItem) channelItem;
						strap.getSession().getMirrors().getChatMirror()
								.removePrivateChatSession(privateChannelItem.getPeer());
					});
					popup.add(item);
				}
				popup.show(channelItems, e.getX(), e.getY());
			}
		});

		setLayout(new BorderLayout());
		add(scroll, BorderLayout.CENTER);
		final JPanel inputPanel = new JPanel(new BorderLayout());
		inputPanel.add(channelItems, BorderLayout.WEST);
		inputPanel.add(input, BorderLayout.CENTER);
		add(inputPanel, BorderLayout.SOUTH);
	}

	public ChannelItem getChatPanelChannel(final ChatSession session) {
		for (int i = 0; i < channelItems.getItemCount(); i++) {
			final ChannelItem channelItem = channelItems.getItemAt(i);
			if (channelItem.getSession().equals(session)) {
				return channelItem;
			}
		}
		final ChannelItem channelItem;
		if (session instanceof LocalChatSession) {
			channelItem = new LocalChannelItem((LocalChatSession) session);
		} else if (session instanceof PrivateChatSession) {
			final PrivateChatSession privateSession = (PrivateChatSession) session;
			channelItem = new PrivateChannelItem(privateSession);
			final ChatEntity peer = privateSession.getPeer();
			entityMap.put(peer.getLocalId(), peer);
		} else {
			return null;
		}
		channelItems.addItem(channelItem);
		return channelItem;
	}

	public boolean removeChatPanelChannel(final ChatSession session) {
		for (int i = 0; i < channelItems.getItemCount(); i++) {
			final ChannelItem channelItem = channelItems.getItemAt(i);
			if (channelItem.getSession().equals(session)) {
				channelItems.removeItem(channelItem);
				return true;
			}
		}
		return false;
	}

	public void setAlternateComponent(final JComponent component) {
		alternateComponent = component;
	}

	public void requestFocusToInput() {
		input.requestFocus();
	}

	private void addHtmlElement(final String element) {
		final JScrollBar bar = scroll.getVerticalScrollBar();
		// * 3 / 2 : workaround
		final boolean bottom = bar.getValue() + bar.getSize().height * 3 / 2 >= bar.getMaximum();
		final HTMLDocument d = (HTMLDocument) pane.getDocument();
		try {
			d.insertBeforeEnd(d.getDefaultRootElement(), element);
		} catch (final BadLocationException | IOException e) {
			logger.log(Level.SEVERE, "", e);
		}
		if (bottom) {
			pane.setCaretPosition(d.getLength());
		}
	}

	@Private
	void addLine(final ChannelItem channelItem, final String format, final ChatEntity entity, final String content,
			final Color color) {
		entityMap.put(entity.getLocalId(), entity);

		final FieldEntity fieldEntity = strap.getSession().getServer().getFieldEntity(entity);
		final String nickname;
		if (fieldEntity != null) {
			nickname = strap.getSession().getServer().getFieldNickname(fieldEntity.getLocalId());
		} else {
			nickname = null;
		}
		final String name;
		if (nickname != null) {
			if (nickname.equals(entity.getAlias())) {
				name = nickname;
			} else {
				name = nickname + " (" + entity.getAlias() + ")";
			}
		} else {
			name = entity.getAlias();
		}

		final String line = MessageFormat.format(format,
				"<a href=\"http:localId=" + entity.getLocalId() + "\">" + name + "</a>", content);
		final String colorString = String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue());
		final String element = "<div><font color=\"" + colorString + "\">" + line + "</font></div>";
		addHtmlElement(element);
	}

	public void addMessage(final String message) {
		final Color color = MESSAGE_COLOR;
		final String colorString = String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue());
		final String line = HtmlUtil.escape(message);
		final String element = "<div><font color=\"" + colorString + "\">" + line + "</font></div>";
		addHtmlElement(element);
	}

	public abstract class ChannelItem {

		protected ChatSession session;
		protected String format;
		protected Color color;

		public ChannelItem(final ChatSession session) {
			this.session = session;
		}

		public abstract ChatSession getSession();

		public void setFormat(final String pattern) {
			format = pattern;
		}

		public void setColor(final Color color) {
			this.color = color;
		}

		public abstract int getShortcut();

		public abstract boolean isRemovable();

		public abstract void addChat(ChatEntity speaker, String content);

		public void chatEntered(final String content) {
			session.sendChat(content);
		}

	}

	public class LocalChannelItem extends ChannelItem {

		LocalChannelItem(final LocalChatSession session) {
			super(session);
			format = "{0}: {1}";
			color = Color.black;
		}

		@Override
		public String toString() {
			return "Local";
		}

		@Override
		public LocalChatSession getSession() {
			return (LocalChatSession) session;
		}

		@Override
		public int getShortcut() {
			return KeyEvent.VK_L;
		}

		@Override
		public boolean isRemovable() {
			return false;
		}

		@Override
		public void addChat(final ChatEntity speaker, final String content) {
			final Gui gui = strap.getGui();
			addLine(this, format, speaker, content, color);
			final View view = gui.getViewPane().getView();
			if (view != null) {
				view.addChat(speaker, content);
			}
		}

	}

	public class PrivateChannelItem extends ChannelItem {

		private String echoFormat = "&gt;{0} {1}";

		PrivateChannelItem(final PrivateChatSession session) {
			super(session);
			format = "{0}&gt; {1}";
			color = Color.pink;
		}

		@Override
		public String toString() {
			return getSession().getPeer().getAlias();
		}

		@Override
		public PrivateChatSession getSession() {
			return (PrivateChatSession) session;
		}

		public ChatEntity getPeer() {
			return getSession().getPeer();
		}

		public void setEchoFormat(final String pattern) {
			echoFormat = pattern;
		}

		@Override
		public int getShortcut() {
			return KeyEvent.VK_UNDEFINED;
		}

		@Override
		public boolean isRemovable() {
			return true;
		}

		@Override
		public void addChat(final ChatEntity speaker, final String content) {
			final String f;
			if (speaker.equals(strap.getSession().getMirrors().getSelfMirror().getChatEntity())) {
				f = echoFormat;
				addLine(this, f, getSession().getPeer(), content, color);
			} else {
				f = format;
				addLine(this, f, speaker, content, color);
			}
		}

	}

	@Private
	static Map<String, String> parseQueryString(final String query) {
		final String[] args = query.split("&");
		final Map<String, String> map = new HashMap<>();
		for (final String arg : args) {
			final String[] elements = arg.split("=");
			if (elements.length < 1) {
				continue;
			} else if (elements.length == 1) {
				map.put(elements[0], null);
			} else {
				map.put(elements[0], elements[1]);
			}
		}
		return map;
	}

}
