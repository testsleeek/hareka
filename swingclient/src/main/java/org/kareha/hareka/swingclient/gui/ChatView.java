package org.kareha.hareka.swingclient.gui;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.Rectangle;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kareha.hareka.client.chat.ChatEntity;
import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.graphics.model.Model;
import org.kareha.hareka.graphics.sprite.ImageSpriteFrame;
import org.kareha.hareka.graphics.sprite.NormalSpriteTemplate;
import org.kareha.hareka.graphics.sprite.Screen;
import org.kareha.hareka.graphics.sprite.Sprite;
import org.kareha.hareka.graphics.sprite.SpriteFrame;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.ui.swing.SwingUtil;

class ChatView {

	private static class ChatElement {

		final long timestamp;
		final FieldEntity entity;
		final Sprite sprite;

		ChatElement(final GraphicsConfiguration gc, final FontMetrics fm, final Screen chatScreen,
				final FieldEntity entity, final String content) {
			timestamp = System.currentTimeMillis();
			this.entity = entity;

			final int width = fm.stringWidth(content);
			final int height = fm.getHeight();

			final BufferedImage image = gc.createCompatibleImage(width + 2, height + 2, Transparency.BITMASK);
			final Graphics g = image.getGraphics();
			SwingUtil.drawString(g, content, 1, height + 1);
			g.dispose();

			final SpriteFrame frame = new ImageSpriteFrame(image, image.getWidth() / 2, image.getHeight(), 0);
			final NormalSpriteTemplate template = new NormalSpriteTemplate();
			template.putFrame("", frame);
			sprite = new Sprite(template, null);
			chatScreen.add(sprite);
			sprite.setFrame("");
		}

	}

	private static final Comparator<ChatElement> TIMESTAMP_COMPARATOR = new Comparator<ChatElement>() {
		@Override
		public int compare(ChatElement o1, ChatElement o2) {
			if (o1.timestamp < o2.timestamp) {
				return -1;
			} else if (o1.timestamp == o2.timestamp) {
				return 0;
			} else {
				return 1;
			}
		}
	};

	private final View view;
	private final Strap strap;
	private final GraphicsConfiguration gc;
	private final Screen chatScreen;
	private final Map<FieldEntity, ChatElement> elements = new HashMap<>();
	int duration;
	private final FontMetrics fm;

	ChatView(final View view, final Strap strap, final GraphicsConfiguration gc) {
		this.view = view;
		this.strap = strap;
		this.gc = gc;
		chatScreen = new Screen();

		duration = strap.getSwingClientSettings().getChatDisplayDuration();

		final BufferedImage dummyImage = gc.createCompatibleImage(1, 1);
		final Graphics dummyG = dummyImage.getGraphics();
		fm = dummyG.getFontMetrics();
		dummyG.dispose();
	}

	void paint(final Graphics g) {
		chatScreen.paint(g);
	}

	void setViewport(final int x, final int y) {
		chatScreen.setViewport(x, y);
	}

	void add(final ChatEntity chatEntity, final String content) {
		final FieldEntity fieldEntity = strap.getSession().getServer().getFieldEntity(chatEntity);
		if (fieldEntity == null) {
			return;
		}
		final ChatElement old = elements.remove(fieldEntity);
		if (old != null) {
			chatScreen.remove(old.sprite);
		}
		final Model model = view.getModel(fieldEntity);
		if (model == null) {
			return;
		}
		final ChatElement element = new ChatElement(gc, fm, chatScreen, fieldEntity, content);
		elements.put(fieldEntity, element);
		final Rectangle bounds = model.getBounds();
		element.sprite.setPosition(bounds.x + bounds.width / 2, bounds.y, 0);
	}

	void update() {
		final long timestamp = System.currentTimeMillis();
		final List<ChatElement> sorted = new ArrayList<>(elements.values());
		Collections.sort(sorted, TIMESTAMP_COMPARATOR);
		int i = 0;
		for (final ChatElement element : sorted) {
			final Model model = view.getModel(element.entity);
			if (model == null || element.timestamp < timestamp - duration) {
				chatScreen.remove(element.sprite);
				elements.remove(element.entity);
				continue;
			}
			final Rectangle bounds = model.getBounds();
			element.sprite.setPosition(bounds.x + bounds.width / 2, bounds.y, i);
			i++;
		}
	}

}
