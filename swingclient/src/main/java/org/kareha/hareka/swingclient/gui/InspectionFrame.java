package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.AbstractListModel;
import javax.swing.JInternalFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;

import org.kareha.hareka.LocalEntityId;
import org.kareha.hareka.LocalKeyId;
import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.swingclient.Strap;

@SuppressWarnings("serial")
public class InspectionFrame extends JInternalFrame {

	private static final int FRAME_WIDTH = 384;
	private static final int FRAME_HEIGHT = 256;

	private enum BundleKey {
		Title,
	}

	private final LocalEntityId chatLocalId;
	private final LocalEntityId fieldLocalId;
	private final List<LocalKeyId> userIds = new ArrayList<>();

	private final UserListModel userListModel;
	private final JList<UserEntry> userList;

	public InspectionFrame(final Strap strap, final LocalEntityId chatLocalId, final LocalEntityId fieldLocalId,
			final List<LocalKeyId> userIds) {
		super("", true, true, true, false);
		final ResourceBundle bundle = ResourceBundle.getBundle(InspectionFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Title.name()));

		this.chatLocalId = chatLocalId;
		this.fieldLocalId = fieldLocalId;
		this.userIds.addAll(userIds);

		userListModel = new UserListModel();
		userList = new JList<>(userListModel);
		for (final LocalKeyId i : userIds) {
			final UserEntry entry = new UserEntry(i);
			userListModel.add(entry);
		}

		final JScrollPane userScrollPane = new JScrollPane(userList);
		userScrollPane.setPreferredSize(new Dimension(128, 96));

		add(userScrollPane, BorderLayout.CENTER);
		setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		pack();
	}

	public LocalEntityId getChatLocalId() {
		return chatLocalId;
	}

	public LocalEntityId getFieldLocalId() {
		return fieldLocalId;
	}

	public List<LocalKeyId> getUserIds() {
		return new ArrayList<>(userIds);
	}

	public void setUserIds(final List<LocalKeyId> userIds) {
		this.userIds.clear();
		this.userIds.addAll(userIds);

		userListModel.clear();
		for (final LocalKeyId i : userIds) {
			final UserEntry entry = new UserEntry(i);
			userListModel.add(entry);
		}
	}

	private static class UserEntry {

		private final LocalKeyId id;

		UserEntry(final LocalKeyId id) {
			this.id = id;
		}

		@Override
		public String toString() {
			return id.toString().substring(0, 8);
		}

	}

	private static class UserListModel extends AbstractListModel<UserEntry> {

		private final List<UserEntry> list = new ArrayList<>();

		@Private
		UserListModel() {

		}

		@Override
		public UserEntry getElementAt(final int index) {
			return list.get(index);
		}

		@Override
		public int getSize() {
			return list.size();
		}

		void add(final UserEntry v) {
			list.add(v);
			final int index = list.size() - 1;
			fireIntervalAdded(this, index, index);
		}

		void clear() {
			final int size = list.size();
			if (size < 1) {
				return;
			}
			list.clear();
			fireIntervalRemoved(this, 0, size - 1);
		}

	}

}
