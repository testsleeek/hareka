package org.kareha.hareka.swingclient.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.client.ResponseHandler;
import org.kareha.hareka.client.mirror.UserMirror;
import org.kareha.hareka.swingclient.Strap;
import org.kareha.hareka.ui.swing.LiveList;

@SuppressWarnings("serial")
public class CharactersFrame extends JInternalFrame {

	private enum BundleKey {
		Characters, Login, Create, Delete, Logout, SelectCharacter, ConfirmDelete,
	}

	@Private
	final Strap strap;
	private final LiveList<CharacterEntryItem> list;

	public CharactersFrame(final Strap strap) {
		this.strap = strap;
		final ResourceBundle bundle = ResourceBundle.getBundle(CharactersFrame.class.getName());
		setTitle(bundle.getString(BundleKey.Characters.name()));

		list = new LiveList<>(LiveList.Axis.Y);
		final JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.getVerticalScrollBar().setUnitIncrement(12);
		scrollPane.getVerticalScrollBar().setBlockIncrement(48);

		final JButton loginButton = new JButton(bundle.getString(BundleKey.Login.name()));
		loginButton.addActionListener(e -> doLogin());
		final JButton createButton = new JButton(bundle.getString(BundleKey.Create.name()));
		createButton.addActionListener(e -> {
			final ResponseHandler responseHandler = new ResponseHandler() {
				@Override
				public void rejected(final String message) {
					SwingUtilities
							.invokeLater(() -> JOptionPane.showInternalMessageDialog(CharactersFrame.this, message));
				}

				@Override
				public void accepted(final String message) {
					SwingUtilities.invokeLater(() -> {
						JOptionPane.showInternalMessageDialog(CharactersFrame.this, message);
					});
				}
			};
			strap.getSession().writeNewCharacter(responseHandler);
		});
		final JButton deleteButton = new JButton(bundle.getString(BundleKey.Delete.name()));
		deleteButton.addActionListener(e -> {
			final CharacterEntryItem item = list.getSelectedItem();
			if (item == null) {
				JOptionPane.showInternalMessageDialog(CharactersFrame.this,
						bundle.getString(BundleKey.SelectCharacter.name()));
				return;
			}
			final UserMirror.CharacterEntry entry = item.getEntry();
			final int result = JOptionPane.showInternalConfirmDialog(CharactersFrame.this,
					bundle.getString(BundleKey.ConfirmDelete.name()));
			if (result != JOptionPane.YES_OPTION) {
				return;
			}
			final ResponseHandler responseHandler = new ResponseHandler() {
				@Override
				public void rejected(final String message) {
					SwingUtilities
							.invokeLater(() -> JOptionPane.showInternalMessageDialog(CharactersFrame.this, message));
				}

				@Override
				public void accepted(final String message) {
					SwingUtilities
							.invokeLater(() -> JOptionPane.showInternalMessageDialog(CharactersFrame.this, message));
				}
			};
			strap.getSession().writeDeleteCharacter(entry.getId(), responseHandler);
		});
		final JButton logoutButton = new JButton(bundle.getString(BundleKey.Logout.name()));
		logoutButton.addActionListener(e -> {
			strap.getSession().writeLogoutUser(new ResponseHandler() {
				@Override
				public void rejected(final String message) {
					SwingUtilities
							.invokeLater(() -> JOptionPane.showInternalMessageDialog(CharactersFrame.this, message));
				}

				@Override
				public void accepted(final String message) {
					SwingUtilities.invokeLater(() -> setVisible(false));
				}
			});
		});

		final JPanel buttonPanel = new JPanel();
		buttonPanel.add(loginButton);
		buttonPanel.add(createButton);
		buttonPanel.add(deleteButton);
		buttonPanel.add(logoutButton);

		add(scrollPane, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		setPreferredSize(new Dimension(384, 384));
		pack();
	}

	private void startMotion() {
		if (list == null) {
			return;
		}
		final Component[] components = list.getComponents();
		if (components == null) {
			return;
		}
		for (final Component c : components) {
			if (c instanceof CharacterEntryItem) {
				final CharacterEntryItem i = (CharacterEntryItem) c;
				i.startMotion();
			}
		}
	}

	private void stopMotion() {
		if (list == null) {
			return;
		}
		final Component[] components = list.getComponents();
		if (components == null) {
			return;
		}
		for (final Component c : components) {
			if (c instanceof CharacterEntryItem) {
				final CharacterEntryItem i = (CharacterEntryItem) c;
				i.stopMotion();
			}
		}
	}

	@Override
	public void setVisible(final boolean b) {
		if (b) {
			startMotion();
		} else {
			stopMotion();
		}
		super.setVisible(b);
	}

	@Override
	public void dispose() {
		stopMotion();
		super.dispose();
	}

	public void loadCharacters(final Collection<UserMirror.CharacterEntry> characterEntries) {
		stopMotion();
		list.clearItems();
		for (final UserMirror.CharacterEntry entry : characterEntries) {
			list.addItem(new CharacterEntryItem(entry));
		}
		if (list.getItemCount() > 0) {
			list.setSelectedItem(0);
			list.getSelectedItem().requestFocus();
		}
		pack();
	}

	@Private
	void doLogin() {
		final Gui gui = strap.getGui();
		final CharacterEntryItem item = list.getSelectedItem();
		if (item == null) {
			final ResourceBundle bundle = ResourceBundle.getBundle(CharactersFrame.class.getName());
			JOptionPane.showInternalMessageDialog(CharactersFrame.this,
					bundle.getString(BundleKey.SelectCharacter.name()));
			return;
		}
		final UserMirror.CharacterEntry entry = item.getEntry();
		final ResponseHandler responseHandler = new ResponseHandler() {
			@Override
			public void rejected(final String message) {
				SwingUtilities.invokeLater(() -> JOptionPane.showInternalMessageDialog(CharactersFrame.this, message));
			}

			@Override
			public void accepted(final String message) {
				SwingUtilities.invokeLater(() -> {
					setVisible(false);
					// gui.getChatFrame().setVisible(true);
					// create local chat entry
					strap.getSession().getMirrors().getChatMirror().getLocalChatSession();

					gui.getViewPane().setEditMode(false);
					gui.getViewPane().setAutopilot(true);

					gui.getViewPane().requestFocus();
				});
			}
		};
		strap.getSession().writeLoginCharacter(entry.getId(), responseHandler);
	}

	private class CharacterEntryItem extends JComponent implements LiveList.Selectable {

		private final UserMirror.CharacterEntry entry;
		private final ModelPanel modelPanel;
		private final JLabel label;

		CharacterEntryItem(final UserMirror.CharacterEntry entry) {
			setFocusable(true);
			this.entry = entry;
			modelPanel = new ModelPanel(strap.getLoader(), entry.getShape());
			setLayout(new FlowLayout(FlowLayout.LEFT));
			add(modelPanel);
			label = new JLabel(entry.getName());
			add(label);
			addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(final MouseEvent e) {
					if (e.getButton() == MouseEvent.BUTTON1) {
						if (e.getClickCount() >= 2) {
							doLogin();
						}
					}
				}
			});
			addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(final KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						doLogin();
					}
				}
			});
		}

		UserMirror.CharacterEntry getEntry() {
			return entry;
		}

		@Override
		public void setSelected(final boolean v) {
			if (v) {
				setBackground(Color.orange);
				modelPanel.setWalking(true);
				label.setForeground(Color.black);
			} else {
				setBackground(Color.black);
				modelPanel.setWalking(false);
				label.setForeground(Color.white);
			}
		}

		@Override
		protected void paintComponent(final Graphics g) {
			final Dimension size = getSize();
			g.setColor(getBackground());
			g.fillRect(0, 0, size.width, size.height);
		}

		void startMotion() {
			modelPanel.startMotion();
		}

		void stopMotion() {
			modelPanel.stopMotion();
		}
	}

}
