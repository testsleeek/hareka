package org.kareha.hareka.graphics.model;

import org.kareha.hareka.graphics.GraphicsConstants;
import org.kareha.hareka.graphics.sprite.SpriteGroup;
import org.kareha.hareka.graphics.sprite.Screen;

public class Effect extends AbstractModel {

	private EffectTemplate template;

	private boolean finished;

	public Effect(final EffectTemplate template, final SpriteGroup spriteGroup) {
		super(spriteGroup);
		this.template = template;
	}

	@Override
	protected int getZOffset() {
		return GraphicsConstants.EFFECT_Z_OFFSET;
	}

	@Override
	protected String makeFrameId() {
		return new StringBuilder().append(getVisualDirection(template.getWayType()).name()).append(" ")
				.append(template.getFrameNumber(phase)).toString();
	}

	@Override
	public void animate(final int v) {
		if (finished) {
			return;
		}

		if (phase >= template.getMaxPhase()) {
			spriteGroup.setFrame(null);
			finished = true;
			return;
		}

		super.animate(v);
	}

	public boolean isFinished() {
		return finished;
	}

	public void addTo(final Screen screen) {
		spriteGroup.addTo(screen);
	}

	public void removeFrom(final Screen screen) {
		spriteGroup.removeFrom(screen);
	}

}
