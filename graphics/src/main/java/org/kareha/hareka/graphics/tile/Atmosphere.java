package org.kareha.hareka.graphics.tile;

public enum Atmosphere {

	NULL,

	GROUND,

	INDOOR,

	UNDERGROUND,

	SKY,

}
