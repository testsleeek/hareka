package org.kareha.hareka.graphics.tile;

import java.awt.GraphicsConfiguration;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.field.Vector;
import org.kareha.hareka.graphics.sprite.SpriteGroup;
import org.kareha.hareka.graphics.sprite.SpriteTemplate;
import org.kareha.hareka.graphics.sprite.SpriteTemplateLoader;

public final class TileLoader {

	private static final Logger logger = Logger.getLogger(TileLoader.class.getName());

	private final String directory;
	private final GraphicsConfiguration gc;
	private Map<ViewTileType, CacheEntry> cache = new HashMap<>();

	public TileLoader(final String directory, final GraphicsConfiguration gc) {
		this.directory = directory;
		this.gc = gc;
	}

	public TileSprite createTile(final ViewTile value, final Vector position, final int elevation,
			final TileSprite.Part part, final ViewDirection direction) {
		CacheEntry entry = cache.get(value.type());
		if (entry == null) {
			final String subName = value.type().name();
			final String tileFilename = directory + "/" + subName + "/" + "tile.xml";
			final TileTemplate tileTemplate;
			try (final InputStream in = getClass().getClassLoader().getResourceAsStream(tileFilename)) {
				tileTemplate = TileTemplate.load(in);
			} catch (final IOException e) {
				logger.log(Level.SEVERE, "", e);
				return null;
			} catch (final JAXBException e) {
				logger.log(Level.SEVERE, "", e);
				return null;
			}

			final Collection<SpriteTemplate> spriteTemplates = SpriteTemplateLoader
					.loadTemplates(directory + "/" + subName, "sprites.xml", gc);
			entry = new CacheEntry(tileTemplate, spriteTemplates);
			cache.put(value.type(), entry);
		}

		final ViewTilePiece ce = ViewTilePiece.valueOf(position, value);
		final SpriteGroup spriteGroup = new SpriteGroup(entry.spriteTemplates, ce);
		return new TileSprite(entry.tileTemplate, spriteGroup, position, elevation, part, direction);
	}

	private static final class CacheEntry {

		TileTemplate tileTemplate;
		Collection<SpriteTemplate> spriteTemplates;

		CacheEntry(final TileTemplate tileTemplate, final Collection<SpriteTemplate> spriteTemplates) {
			this.tileTemplate = tileTemplate;
			this.spriteTemplates = spriteTemplates;
		}

	}

}
