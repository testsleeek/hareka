package org.kareha.hareka.graphics.model;

import java.awt.GraphicsConfiguration;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.graphics.sprite.SpriteGroup;
import org.kareha.hareka.graphics.sprite.SpriteGroupLoader;
import org.kareha.hareka.graphics.sprite.SpriteTemplate;

public final class EffectLoader {

	private static final Logger logger = Logger.getLogger(EffectLoader.class.getName());

	private final String directory;
	private final SpriteGroupLoader spriteGroupLoader;

	private final Map<String, EffectTemplate> cache = new HashMap<>();

	public EffectLoader(final String directory, final GraphicsConfiguration gc) {
		this.directory = directory;
		spriteGroupLoader = new SpriteGroupLoader(directory, gc);
	}

	public Effect createEffect(final String id) {
		final EffectTemplate template = load(id);
		final Collection<SpriteTemplate> spriteTemplate = spriteGroupLoader.load(id);
		final SpriteGroup spriteGroup = new SpriteGroup(spriteTemplate, null);
		return new Effect(template, spriteGroup);
	}

	private EffectTemplate load(final String id) {
		EffectTemplate template = cache.get(id);
		if (template != null) {
			return template;
		}

		final String path = directory + "/" + id + "/" + "effect.xml";
		try (final InputStream in = this.getClass().getClassLoader().getResourceAsStream(path)) {
			template = EffectTemplate.load(in);
			cache.put(id, template);
		} catch (final JAXBException e) {
			logger.log(Level.SEVERE, "", e);
			return null;
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
			return null;
		}

		return template;
	}

}
