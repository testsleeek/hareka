package org.kareha.hareka.graphics;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

public final class ImageLoader {

	private final String directory;

	private Map<String, BufferedImage> cache = new HashMap<>();

	public ImageLoader(final String directory) {
		this.directory = directory;
	}

	public BufferedImage load(final String filename) throws IOException {
		BufferedImage image = cache.get(filename);
		if (image != null) {
			return image;
		}
		final String path = directory + "/" + filename;
		try (final InputStream in = ImageLoader.class.getClassLoader().getResourceAsStream(path)) {
			if (in == null) {
				throw new IOException("Resource not found");
			}
			image = ImageIO.read(in);
			cache.put(filename, image);
		}
		return image;
	}

}
