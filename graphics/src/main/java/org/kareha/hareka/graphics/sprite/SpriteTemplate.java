package org.kareha.hareka.graphics.sprite;

public interface SpriteTemplate {

	SpriteFrame getFrame(String id);

}
