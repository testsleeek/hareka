package org.kareha.hareka.graphics.sprite;

public final class NullSpriteTemplate implements SpriteTemplate {

	public static final SpriteTemplate INSTANCE = new NullSpriteTemplate();

	private NullSpriteTemplate() {
		// do nothing
	}

	@Override
	public SpriteFrame getFrame(final String id) {
		return NullSpriteFrame.INSTANCE;
	}

}
