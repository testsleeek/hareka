package org.kareha.hareka.graphics.tile;

import java.util.EnumMap;
import java.util.Map;

import org.kareha.hareka.field.TileType;

public enum ViewTileType {

	NULL(ViewTileTypeCategory.NULL),

	WATER(ViewTileTypeCategory.GROUND),

	SHALLOW_WATER(ViewTileTypeCategory.GROUND),

	SAND(ViewTileTypeCategory.UNDERGROUND),

	ROCK(ViewTileTypeCategory.UNDERGROUND),

	SNOW(ViewTileTypeCategory.GROUND),

	TREE_WITH_SNOW(ViewTileTypeCategory.GROUND),

	GRASS(ViewTileTypeCategory.GROUND),

	FLOWER(ViewTileTypeCategory.SKY),

	TREE(ViewTileTypeCategory.GROUND),

	WOODEN_FENCE(ViewTileTypeCategory.GROUND),

	GATE(ViewTileTypeCategory.SKY),

	EARTH(ViewTileTypeCategory.UNDERGROUND),

	AIR(ViewTileTypeCategory.SKY),

	BRIDGE(ViewTileTypeCategory.GROUND),

	FLOOR(ViewTileTypeCategory.INDOOR),

	DOWNSTAIRS(ViewTileTypeCategory.UNDERGROUND),

	UPSTAIRS(ViewTileTypeCategory.SKY),

	WALL(ViewTileTypeCategory.INDOOR),

	VOID(ViewTileTypeCategory.NULL),

	;

	private static final Map<TileType, ViewTileType> converter;

	static {
		converter = new EnumMap<>(TileType.class);
		for (final TileType tt : TileType.values()) {
			try {
				final ViewTileType vtt = valueOf(tt.name());
				converter.put(tt, vtt);
			} catch (IllegalArgumentException e) {
				converter.put(tt, ViewTileType.NULL);
			}
		}
	}

	private final ViewTileTypeCategory category;

	private ViewTileType(final ViewTileTypeCategory category) {
		this.category = category;
	}

	public int terrainOrder() {
		return ordinal();
	}

	public ViewTileTypeCategory category() {
		return category;
	}

	public static ViewTileType valueOf(final TileType v) {
		if (v == null) {
			throw new IllegalArgumentException("null");
		}
		return converter.get(v);
	}

}
