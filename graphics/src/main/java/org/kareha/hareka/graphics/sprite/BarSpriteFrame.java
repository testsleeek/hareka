package org.kareha.hareka.graphics.sprite;

import java.awt.Graphics;
import java.awt.Rectangle;

import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.client.field.StatBar;

public class BarSpriteFrame implements SpriteFrame {

	private static final java.awt.Color borderColor = java.awt.Color.WHITE;
	private static final java.awt.Color healthPointColor = java.awt.Color.RED;
	private static final java.awt.Color magicPointColor = java.awt.Color.BLUE;
	private static final int yOffset = -6;
	private static final int barWidth = 32;
	private static final int barHeight = 2;
	private static final int margin = 2;

	private final FieldEntity entity;
	private int height;

	public BarSpriteFrame(final FieldEntity entity) {
		this.entity = entity;
	}

	@Override
	public int getCenterX() {
		return 0;
	}

	@Override
	public int getCenterY() {
		return 0;
	}

	@Override
	public int getCenterZ() {
		return 0;
	}

	@Override
	public void paint(final Graphics g, final int x, final int y) {
		final StatBar healthBar = entity.getHealthBar();
		final StatBar magicBar = entity.getMagicBar();
		final boolean healthPointsVisible = healthBar.isAlive() && healthBar.isDamaged();
		final boolean magicPointsVisible = magicBar.isAlive() && magicBar.isDamaged();

		if (!healthPointsVisible && !magicPointsVisible) {
			height = 0;
		} else if (healthPointsVisible && magicPointsVisible) {
			g.setColor(borderColor);
			g.fillRect(x - barWidth / 2 - 1, y + yOffset - barHeight - 1, barWidth + 2, barHeight * 2 + 3);

			int hp = barWidth * healthBar.getBar() / Byte.MAX_VALUE;
			g.setColor(healthPointColor);
			g.fillRect(x - barWidth / 2, y + yOffset - barHeight, hp, barHeight);

			int mp = barWidth * magicBar.getBar() / Byte.MAX_VALUE;
			g.setColor(magicPointColor);
			g.fillRect(x - barWidth / 2, y + yOffset + 1, mp, barHeight);

			height = barHeight * 2 + 3;
		} else {
			g.setColor(borderColor);
			g.fillRect(x - barWidth / 2 - 1, y + yOffset, barWidth + 2, barHeight + 2);
			if (healthPointsVisible) {
				int hp = barWidth * healthBar.getBar() / Byte.MAX_VALUE;
				g.setColor(healthPointColor);
				g.fillRect(x - barWidth / 2, y + yOffset + 1, hp, barHeight);
			} else {
				int mp = barWidth * magicBar.getBar() / Byte.MAX_VALUE;
				g.setColor(magicPointColor);
				g.fillRect(x - barWidth / 2, y + yOffset + 1, mp, barHeight);
			}

			height = barHeight + 2;
		}
	}

	@Override
	public boolean contains(final int x, final int y) {
		if (height == 0) {
			return false;
		}
		return x >= -17 - margin && x < 17 + margin && y >= yOffset - height / 2 - margin
				&& y < yOffset + height / 2 + margin;
	}

	@Override
	public Rectangle getBounds() {
		if (height == 0) {
			return new Rectangle(0, 0, -1, -1);
		}
		return new Rectangle(-17, yOffset - height / 2, 34, height);
	}

}
