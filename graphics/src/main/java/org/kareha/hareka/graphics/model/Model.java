package org.kareha.hareka.graphics.model;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.field.Placement;
import org.kareha.hareka.graphics.GraphicsConstants;
import org.kareha.hareka.graphics.sprite.BarSpriteFrame;
import org.kareha.hareka.graphics.sprite.NameSpriteFrame;
import org.kareha.hareka.graphics.sprite.NormalSpriteTemplate;
import org.kareha.hareka.graphics.sprite.ObjectCursorSpriteFrame;
import org.kareha.hareka.graphics.sprite.Screen;
import org.kareha.hareka.graphics.sprite.Sprite;
import org.kareha.hareka.graphics.sprite.SpriteFrame;
import org.kareha.hareka.graphics.sprite.SpriteGroup;

public class Model extends AbstractModel {

	private ModelTemplate template;
	private List<Effect> effects;

	private boolean walking;
	private float lastWalkPhase;
	private float subPhase;

	private final Sprite barSprite;
	private final Sprite nameSprite;
	private Sprite cursorSprite;
	private final FieldEntity object;

	public Model(final ModelTemplate template, final SpriteGroup spriteGroup, final FieldEntity object) {
		super(spriteGroup);
		this.template = template;

		final NormalSpriteTemplate barSpriteTemplate = new NormalSpriteTemplate();
		final SpriteFrame barSpriteFrame = new BarSpriteFrame(object);
		barSpriteTemplate.putFrame("", barSpriteFrame);
		barSprite = new Sprite(barSpriteTemplate, object);
		barSprite.setFrame("");

		final NormalSpriteTemplate nameSpriteTemplate = new NormalSpriteTemplate();
		final SpriteFrame nameSpriteFrame = new NameSpriteFrame();
		nameSpriteTemplate.putFrame("", nameSpriteFrame);
		nameSprite = new Sprite(nameSpriteTemplate, object);
		nameSprite.setFrame("");

		this.object = object;
	}

	@Override
	protected int getZOffset() {
		return GraphicsConstants.MODEL_Z_OFFSET;
	}

	public void setCursor(final Screen screen, final boolean v) {
		if (v) {
			if (cursorSprite == null) {
				final NormalSpriteTemplate cursorSpriteTemplate = new NormalSpriteTemplate();
				final SpriteFrame cursorSpriteFrame = new ObjectCursorSpriteFrame();
				cursorSpriteTemplate.putFrame("", cursorSpriteFrame);
				cursorSprite = new Sprite(cursorSpriteTemplate, null);
				cursorSprite.setFrame("");
				screen.add(cursorSprite);
				updatePosition();
			}
		} else {
			if (cursorSprite != null) {
				screen.remove(cursorSprite);
				cursorSprite = null;
			}
		}
	}

	public Sprite getBarSprite() {
		return barSprite;
	}

	@Override
	public void setPlacement(final Placement p, final int h) {
		super.setPlacement(p, h);

		if (effects != null) {
			for (final Effect effect : effects) {
				effect.setPlacement(p, h);
			}
		}
	}

	@Override
	protected void updatePosition() {
		super.updatePosition();

		final Rectangle bounds = spriteGroup.getBounds();
		barSprite.setPosition(spriteGroup.getX(), bounds.y, GraphicsConstants.BAR_Z_OFFSET + spriteGroup.getZ());

		bounds.add(barSprite.getBounds());
		nameSprite.setPosition(spriteGroup.getX(), bounds.y, GraphicsConstants.BAR_Z_OFFSET + spriteGroup.getZ());

		if (cursorSprite != null) {
			bounds.add(nameSprite.getBounds());
			cursorSprite.setPosition(spriteGroup.getX(), bounds.y, GraphicsConstants.BAR_Z_OFFSET + spriteGroup.getZ());
		}
	}

	@Override
	public Rectangle getBounds() {
		final Rectangle bounds = super.getBounds();
		bounds.add(barSprite.getBounds());
		bounds.add(nameSprite.getBounds());
		return bounds;
	}

	@Override
	public void place(final Placement p, final int h, final int motionWait) {
		super.place(p, h, motionWait);

		if (!walking) {
			subPhase = 0;
			walking = true;
		}
		lastWalkPhase = phase;

		if (effects != null) {
			for (final Effect effect : effects) {
				effect.place(p, h, motionWait);
			}
		}
	}

	@Override
	protected String makeFrameId() {
		if (!object.isAlive()) {
			return new StringBuilder().append("DEAD ").append(getVisualDirection(template.getWayType()).name())
					.append(" ").append(template.getDeadFrameNumber(subPhase)).toString();
		}
		if (!walking) {
			return new StringBuilder().append("STAND ").append(getVisualDirection(template.getWayType()).name())
					.append(" ").append(template.getStandFrameNumber(subPhase)).toString();
		}
		return new StringBuilder().append("WALK ").append(getVisualDirection(template.getWayType()).name()).append(" ")
				.append(template.getWalkFrameNumber(subPhase)).toString();
	}

	@Override
	public void animate(final int v) {
		final int mw = Math.min(prevMotionWait, motionWait);
		if (walking) {
			if (phase >= lastWalkPhase + mw / 1000f * 1.5) {
				walking = false;
				subPhase = 0;
			} else {
				subPhase += v / 1000f;
			}
		} else {
			subPhase += v / 1000f;
		}

		super.animate(v);
	}

	public void addEffect(final Effect v) {
		if (effects == null) {
			effects = new ArrayList<>();
		}
		effects.add(v);
	}

	public void removeEffect(final Effect v) {
		if (effects == null) {
			return;
		}
		effects.remove(v);
	}

	public void addTo(final Screen screen) {
		spriteGroup.addTo(screen);
		screen.add(barSprite);
		screen.add(nameSprite);
		if (cursorSprite != null) {
			screen.add(cursorSprite);
		}
	}

	public void removeFrom(final Screen screen) {
		spriteGroup.removeFrom(screen);
		screen.remove(barSprite);
		screen.remove(nameSprite);
		if (cursorSprite != null) {
			screen.remove(cursorSprite);
		}
	}

	public void setName(final String name) {
		((NameSpriteFrame) nameSprite.getFrame()).setName(name);
	}

}
