package org.kareha.hareka.graphics.model;

import java.awt.GraphicsConfiguration;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import org.kareha.hareka.client.field.FieldEntity;
import org.kareha.hareka.graphics.sprite.SpriteGroup;
import org.kareha.hareka.graphics.sprite.SpriteGroupLoader;
import org.kareha.hareka.graphics.sprite.SpriteTemplate;

public class ModelLoader {

	private static final Logger logger = Logger.getLogger(ModelLoader.class.getName());

	private final String directory;
	private final SpriteGroupLoader spriteGroupLoader;
	private final Map<String, ModelTemplate> cache = new HashMap<>();

	public ModelLoader(final String directory, final GraphicsConfiguration gc) {
		this.directory = directory;
		spriteGroupLoader = new SpriteGroupLoader(directory, gc);
	}

	public Model createModel(final FieldEntity object) {
		final ModelTemplate template = load(object.getShape());
		final Collection<SpriteTemplate> spriteTemplate = spriteGroupLoader.load(object.getShape());
		final SpriteGroup spriteGroup = new SpriteGroup(spriteTemplate, object);
		return new Model(template, spriteGroup, object);
	}

	private ModelTemplate load(final String id) {
		ModelTemplate template = cache.get(id);
		if (template != null) {
			return template;
		}

		final String path = directory + "/" + id + "/" + "model.xml";
		try (final InputStream in = this.getClass().getClassLoader().getResourceAsStream(path)) {
			if (in == null) {
				if (id.equals("Undefined")) {
					throw new RuntimeException("Undefined is undefined");
				}
				template = load("Undefined");
				cache.put(id, template);
				return template;
			}
			try {
				template = ModelTemplate.load(in);
			} catch (final JAXBException e) {
				logger.log(Level.SEVERE, "", e);
				return null;
			}
			cache.put(id, template);
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
			return null;
		}

		return template;
	}

}
