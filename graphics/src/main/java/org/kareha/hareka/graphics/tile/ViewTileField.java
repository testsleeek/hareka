package org.kareha.hareka.graphics.tile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.kareha.hareka.annotation.Private;
import org.kareha.hareka.client.field.ClientField;
import org.kareha.hareka.field.Direction;
import org.kareha.hareka.field.Tile;
import org.kareha.hareka.field.TilePiece;
import org.kareha.hareka.field.TileType;
import org.kareha.hareka.field.Transformation;
import org.kareha.hareka.field.Vector;
import org.kareha.hareka.field.Vectors;
import org.kareha.hareka.graphics.GraphicsConstants;
import org.kareha.hareka.graphics.sprite.NormalSpriteTemplate;
import org.kareha.hareka.graphics.sprite.Screen;
import org.kareha.hareka.graphics.sprite.Sprite;
import org.kareha.hareka.graphics.sprite.SpriteFrame;
import org.kareha.hareka.graphics.sprite.SpriteGroup;
import org.kareha.hareka.graphics.sprite.TileCursorSpriteFrame;

public class ViewTileField {

	private static final int MAX_WALL_HEIGHT = 64;
	@Private
	static final Direction[] DIRECTIONS = Direction.values();

	@Private
	final ClientField field;
	@Private
	final Screen screen;
	private TileLoader loader;
	private final Map<Vector, SpriteEntry> entries = new HashMap<>();
	@Private
	Transformation transformation = Transformation.IDENTITY;
	private Vector position = Vector.ZERO;
	private int viewSize;
	private final BufferField bufferField = new BufferField();
	private Vector cursorPosition;
	private Sprite cursorSprite;

	public ViewTileField(final ClientField field, final Screen screen, final TileLoader loader) {
		this.field = field;
		this.screen = screen;
		this.loader = loader;
	}

	private void addSprite(final Vector position, final ViewDirection direction, final TileSprite sprite,
			final int elevation, final ViewTile tile) {
		SpriteEntry entry = entries.get(position);
		if (entry == null) {
			entry = new SpriteEntry();
			entries.put(position, entry);
		}

		entry.putTop(direction, sprite);
		entry.putWalls(position, direction, sprite, elevation, tile);

		if (direction == ViewDirection.NULL && position.equals(cursorPosition)) {
			setCursor(cursorPosition);
		}
	}

	private void removeSprite(final Vector position) {
		final SpriteEntry entry = entries.remove(position);
		if (entry != null) {
			entry.removeSprites();
		}

		if (position.equals(cursorPosition)) {
			setCursor(null);
		}
	}

	private class SpriteEntry {

		private final TileSprite[] tops = new TileSprite[7];
		@SuppressWarnings("unchecked")
		private final List<TileSprite>[] walls = new List[7];

		@Private
		SpriteEntry() {

		}

		void putTop(final ViewDirection direction, final TileSprite sprite) {
			final TileSprite oldSprite = tops[direction.ordinal()];
			if (oldSprite != null) {
				oldSprite.removeFrom(screen);
			}
			tops[direction.ordinal()] = sprite;
			sprite.addTo(screen);
		}

		void putWalls(final Vector position, final ViewDirection direction, final TileSprite sprite,
				final int elevation, final ViewTile tile) {
			List<TileSprite> list = walls[direction.ordinal()];
			if (list == null) {
				list = new ArrayList<>();
				walls[direction.ordinal()] = list;
			} else {
				for (final TileSprite oldSprite : list) {
					oldSprite.removeFrom(screen);
				}
				list.clear();
			}

			final int diff;
			switch (direction) {
			default:
				diff = 0;
				break;
			case NULL:
			case DOWN: {
				final Tile downLeftTile = field.getTile(position.neighbor(Direction.DOWN_LEFT.invert(transformation)));
				final Tile downRightTile = field
						.getTile(position.neighbor(Direction.DOWN_RIGHT.invert(transformation)));
				final int el = Math.min(downLeftTile.elevation(), downRightTile.elevation());
				diff = tile.elevation() - el;
			}
				break;
			case UP: {
				final Tile downLeftTile = field.getTile(position.neighbor(Direction.DOWN_LEFT.invert(transformation)));
				final Tile downRightTile = field
						.getTile(position.neighbor(Direction.DOWN_RIGHT.invert(transformation)));
				final Tile nullTile = field.getTile(position);
				final int el = Math.min(Math.min(downLeftTile.elevation(), downRightTile.elevation()),
						nullTile.elevation());
				diff = tile.elevation() - el;
			}
				break;
			case DOWN_LEFT: {
				final Tile downLeftTile = field.getTile(position.neighbor(Direction.DOWN_LEFT.invert(transformation)));
				diff = tile.elevation() - downLeftTile.elevation();
			}
				break;
			case UP_LEFT: {
				final Tile downLeftTile = field.getTile(position.neighbor(Direction.DOWN_LEFT.invert(transformation)));
				final Tile nullTile = field.getTile(position);
				final int el = Math.min(downLeftTile.elevation(), nullTile.elevation());
				diff = tile.elevation() - el;
			}
				break;
			case DOWN_RIGHT: {
				final Tile downRightTile = field
						.getTile(position.neighbor(Direction.DOWN_RIGHT.invert(transformation)));
				diff = tile.elevation() - downRightTile.elevation();
			}
				break;
			case UP_RIGHT: {
				final Tile downRightTile = field
						.getTile(position.neighbor(Direction.DOWN_RIGHT.invert(transformation)));
				final Tile nullTile = field.getTile(position);
				final int el = Math.min(downRightTile.elevation(), nullTile.elevation());
				diff = tile.elevation() - el;
			}
				break;
			}

			if (diff > 0) {
				final Tile tile0 = field.getTile(position);
				if (tile.elevation() == tile0.elevation()) {
					switch (direction) {
					default:
						break;
					case NULL: {
						final List<TileSprite> downWall = walls[ViewDirection.DOWN.ordinal()];
						if (downWall != null && !downWall.isEmpty()) {
							final List<TileSprite> downLeftWall = walls[ViewDirection.DOWN_LEFT.ordinal()];
							if (downLeftWall != null && !downLeftWall.isEmpty()) {
								final List<TileSprite> downRightWall = walls[ViewDirection.DOWN_RIGHT.ordinal()];
								if (downRightWall != null && !downRightWall.isEmpty()) {
									return;
								}
							}
						}
					}
						break;
					case UP_LEFT: {
						final List<TileSprite> downLeftWall = walls[ViewDirection.DOWN_LEFT.ordinal()];
						if (downLeftWall != null && !downLeftWall.isEmpty()) {
							final List<TileSprite> nullWall = walls[ViewDirection.NULL.ordinal()];
							if (nullWall != null && !nullWall.isEmpty()) {
								return;
							}
						}
					}
						break;
					case UP: {
						final List<TileSprite> nullWall = walls[ViewDirection.NULL.ordinal()];
						if (nullWall != null && !nullWall.isEmpty()) {
							final List<TileSprite> upLeftWall = walls[ViewDirection.UP_LEFT.ordinal()];
							if (upLeftWall != null && !upLeftWall.isEmpty()) {
								final List<TileSprite> upRightWall = walls[ViewDirection.UP_RIGHT.ordinal()];
								if (upRightWall != null && !upRightWall.isEmpty()) {
									return;
								}
							}
						}
					}
						break;
					case UP_RIGHT: {
						final List<TileSprite> downRightWall = walls[ViewDirection.DOWN_RIGHT.ordinal()];
						if (downRightWall != null && !downRightWall.isEmpty()) {
							final List<TileSprite> nullWall = walls[ViewDirection.NULL.ordinal()];
							if (nullWall != null && !nullWall.isEmpty()) {
								return;
							}
						}
					}
						break;
					}
				}

				int d = 0;
				while (d < diff && d <= MAX_WALL_HEIGHT) {
					final TileSprite wallSprite = sprite.createWall(elevation - d, elevation - d - 1);
					list.add(wallSprite);
					wallSprite.addTo(screen);
					d++;
				}
			}

		}

		void removeSprites() {
			for (final TileSprite sprite : tops) {
				if (sprite == null) {
					continue;
				}
				sprite.removeFrom(screen);
			}

			for (final List<TileSprite> list : walls) {
				if (list == null) {
					continue;
				}
				for (final TileSprite sprite : list) {
					sprite.removeFrom(screen);
				}
			}
		}

		void animate(int v) {
			for (final TileSprite sprite : tops) {
				if (sprite == null) {
					continue;
				}
				sprite.animate(v);
			}

			for (final List<TileSprite> list : walls) {
				if (list == null) {
					continue;
				}
				for (final TileSprite sprite : list) {
					sprite.animate(v);
				}
			}
		}

		TileSprite getTopCenter() {
			return tops[6];
		}

	}

	public void render(final Transformation transformation, final Vector position, final int viewSize) {
		this.transformation = transformation;
		this.position = position;
		this.viewSize = viewSize;

		final Vector backupCursorPosition = cursorPosition;

		bufferField.clear();
		for (final Vector p : new ArrayList<>(entries.keySet())) {
			removeSprite(p);
		}
		for (final Vector p : Vectors.range(position, viewSize)) {
			final Tile tile = field.getTile(p);
			bufferField.setTile(p, tile);
		}
		for (final Vector p : Vectors.range(position, viewSize)) {
			helperAddTile(p);
		}

		setCursor(backupCursorPosition);
	}

	public void animate(final int v) {
		for (final SpriteEntry entry : entries.values()) {
			entry.animate(v);
		}
	}

	public void addTiles(final Collection<TilePiece> tiles) {
		bufferField.setTiles(tiles);

		final Set<Vector> positions = new HashSet<>();
		for (final TilePiece tilePiece : tiles) {
			for (final Vector p : Vectors.range(tilePiece.position(), 2)) {
				positions.add(p);
			}
		}
		for (final Vector p : positions) {
			helperAddTile(p);
		}

	}

	private static final ViewDirection[] VIEW_DIRECTIONS = { ViewDirection.DOWN, ViewDirection.DOWN_LEFT,
			ViewDirection.DOWN_RIGHT, ViewDirection.NULL, ViewDirection.UP_LEFT, ViewDirection.UP_RIGHT,
			ViewDirection.UP };

	private void helperAddTile(final Vector position) {
		final ViewTile tile = bufferField.getTile(position);
		if (tile.type() == ViewTileType.NULL) {
			return;
		}

		for (final ViewDirection subDirection : VIEW_DIRECTIONS) {
			addSideTile(position, subDirection, tile);
		}
	}

	private void addSideTile(final Vector position, final ViewDirection subDirection, final ViewTile ta) {
		if (subDirection == ViewDirection.NULL) {
			final Vector rPosition = ViewUtil.toViewVector(position.transform(transformation));
			final int elevation = (ta.elevation() + transformation.elevation());
			final TileSprite sprite = loader.createTile(ta, rPosition, elevation, TileSprite.Part.TOP,
					ViewDirection.NULL);
			addSprite(position, ViewDirection.NULL, sprite, elevation, ta);
			return;
		}

		// final Vector pa = position;
		final Vector pb = position.neighbor(Direction.DOWN_RIGHT.rotate(subDirection.invert(transformation).ordinal()));
		final Vector pc = position.neighbor(Direction.DOWN_LEFT.rotate(subDirection.invert(transformation).ordinal()));

		// final ViewTile ta = bufferField.getTile(pa);
		final ViewTile tb = bufferField.getTile(pb);
		final ViewTile tc = bufferField.getTile(pc);

		final ViewTile tile;
		if (tb.type() == tc.type()) {
			if (ta.type() == tb.type()) {
				if (ta.elevation() < tb.elevation() && ta.elevation() < tc.elevation()) {
					final int el = (tb.elevation() + tc.elevation()) / 2;
					tile = ViewTile.valueOf(ta.type(), el);
				} else {
					tile = ta;
				}
			} else if (ta.type().terrainOrder() < tb.type().terrainOrder()) {
				final int el = (tb.elevation() + tc.elevation()) / 2;
				tile = ViewTile.valueOf(tb.type(), el);
			} else {
				tile = ta;
			}
		} else if (ta.type().terrainOrder() < tb.type().terrainOrder()
				&& ta.type().terrainOrder() < tc.type().terrainOrder()) {
			if (tb.type().terrainOrder() < tc.type().terrainOrder()) {
				tile = tb;
			} else if (tb.type().terrainOrder() > tc.type().terrainOrder()) {
				tile = tc;
			} else {
				tile = ta;
			}
		} else {
			tile = ta;
		}

		final Vector rPosition = ViewUtil.toViewVector(position.transform(transformation)).add(subDirection.vector());
		final int elevation = tile.elevation() + transformation.elevation();
		final TileSprite sprite = loader.createTile(tile, rPosition, elevation, TileSprite.Part.TOP, subDirection);

		addSprite(position, subDirection, sprite, elevation, tile);
	}

	public void walk(final Vector position) {
		final Set<Vector> prevSet = new HashSet<>(Arrays.asList(Vectors.range(this.position, viewSize)));
		final Set<Vector> nextSet = new HashSet<>(Arrays.asList(Vectors.range(position, viewSize)));
		final Set<Vector> prevSet2 = new HashSet<>(prevSet);
		prevSet.removeAll(nextSet);
		for (final Vector p : prevSet) {
			removeSprite(p);
			bufferField.removeTile(p);
		}
		nextSet.removeAll(prevSet2);

		this.position = position;
	}

	public void setLoader(final TileLoader loader) {
		this.loader = loader;
	}

	public Atmosphere getAtmosphere() {
		return bufferField.getAtmosphere();
	}

	private static class BufferField {

		private final Map<Vector, Tile> map0 = new HashMap<>();
		private final Map<Vector, ViewTile> map1 = new HashMap<>();
		private final int[] categorySums = new int[ViewTileTypeCategory.values().length];
		private Atmosphere atmosphere = Atmosphere.GROUND;

		@Private
		BufferField() {

		}

		void clear() {
			map0.clear();
			map1.clear();

			for (int i = 0; i < categorySums.length; i++) {
				categorySums[i] = 0;
			}
			updateCategoryMax();
		}

		void removeTile(final Vector position) {
			map0.remove(position);
			update(position);
			final ViewTile tile = map1.remove(position);
			if (tile != null) {
				categorySums[tile.type().category().ordinal()]--;
			}
			updateCategoryMax();
		}

		ViewTile getTile(final Vector position) {
			final ViewTile tile = map1.get(position);
			if (tile == null) {
				return ViewTile.NULL_0;
			}
			return tile;
		}

		@Private
		void setTile(final Vector position, final Tile tile) {
			map0.put(position, tile);
			update(position);
			updateCategoryMax();
		}

		void setTiles(final Collection<TilePiece> tiles) {
			final Set<Vector> positions = new HashSet<>();
			for (final TilePiece tilePiece : tiles) {
				map0.put(tilePiece.position(), tilePiece.tile());
				for (final Direction d : DIRECTIONS) {
					positions.add(tilePiece.position().neighbor(d));
				}
			}
			for (final Vector position : positions) {
				helperUpdate(position);
			}
			updateCategoryMax();
		}

		private void update(final Vector position) {
			for (final Direction d : DIRECTIONS) {
				helperUpdate(position.add(d.vector()));
			}
		}

		private Tile getTile0(final Vector position) {
			final Tile tile = map0.get(position);
			if (tile == null) {
				return Tile.NULL_0;
			}
			return tile;
		}

		private void putTile1(final Vector position, final ViewTile tile) {
			if (tile.type() == ViewTileType.NULL) {
				return;
			}
			final ViewTile prev = map1.put(position, tile);
			if (prev != null) {
				categorySums[prev.type().category().ordinal()]--;
			}
			categorySums[tile.type().category().ordinal()]++;
		}

		private void helperUpdate(final Vector position) {
			final Tile tile0 = getTile0(position);
			switch (tile0.type()) {
			default: {
				putTile1(position, ViewTile.valueOf(tile0));
			}
				break;
			case TREE: {
				boolean found = false;
				for (final Direction d : DIRECTIONS) {
					if (d == Direction.NULL) {
						continue;
					}
					if (getTile0(position.add(d.vector())).type() == TileType.SNOW) {
						putTile1(position, ViewTile.valueOf(ViewTileType.TREE_WITH_SNOW, tile0.elevation()));
						found = true;
						break;
					}
				}
				if (!found) {
					putTile1(position, ViewTile.valueOf(tile0));
				}
			}
				break;
			case WALL: {
				int count = 0;
				for (final Direction d : DIRECTIONS) {
					if (d == Direction.NULL) {
						continue;
					}
					if (getTile0(position.add(d.vector())).type() == TileType.GRASS) {
						count++;
					}
				}
				if (count > 1) {
					putTile1(position, ViewTile.valueOf(ViewTileType.WOODEN_FENCE, tile0.elevation()));
				} else {
					putTile1(position, ViewTile.valueOf(tile0));
				}
			}
				break;
			case WATER: {
				int count = 0;
				for (final Direction d : DIRECTIONS) {
					if (d == Direction.NULL) {
						continue;
					}
					final TileType type = getTile0(position.add(d.vector())).type();
					if (type == TileType.WATER || type == TileType.NULL) {
						count++;
					}
				}
				if (count < 5) {
					putTile1(position, ViewTile.valueOf(ViewTileType.SHALLOW_WATER, tile0.elevation()));
				} else {
					putTile1(position, ViewTile.valueOf(tile0));
				}
			}
				break;
			}
		}

		private void updateCategoryMax() {
			int max = -1;
			int index = -1;
			for (int i = 1; i < categorySums.length; i++) {
				if (categorySums[i] > max) {
					max = categorySums[i];
					index = i;
				}
			}
			final ViewTileTypeCategory categoryMax = ViewTileTypeCategory.valueOf(index);
			switch (categoryMax) {
			default:
				atmosphere = Atmosphere.GROUND;
				break;
			case GROUND:
				atmosphere = Atmosphere.GROUND;
				break;
			case INDOOR:
				atmosphere = Atmosphere.INDOOR;
				break;
			case UNDERGROUND:
				atmosphere = Atmosphere.UNDERGROUND;
				break;
			case SKY:
				atmosphere = Atmosphere.SKY;
				break;
			}

			// debug
			// for (int i = 0; i < categorySums.length; i++) {
			// System.out.print(" " + categorySums[i]);
			// }
			// System.out.println();
		}

		Atmosphere getAtmosphere() {
			return atmosphere;
		}

	}

	public Vector getCursor() {
		return cursorPosition;
	}

	public void setCursor(final Vector position) {
		if (cursorSprite != null) {
			screen.remove(cursorSprite);
			cursorSprite = null;
			cursorPosition = null;
		}
		if (position != null) {
			final SpriteEntry entry = entries.get(position);
			if (entry == null) {
				return;
			}
			final TileSprite tileSprite = entry.getTopCenter();
			if (tileSprite == null) {
				return;
			}

			final NormalSpriteTemplate cursorSpriteTemplate = new NormalSpriteTemplate();
			final SpriteFrame cursorSpriteFrame = new TileCursorSpriteFrame();
			cursorSpriteTemplate.putFrame("", cursorSpriteFrame);
			cursorSprite = new Sprite(cursorSpriteTemplate, null);
			cursorSprite.setFrame("");
			screen.add(cursorSprite);

			final SpriteGroup g = tileSprite.getSpriteGroup();
			cursorSprite.setPosition(g.getX(), g.getY(), GraphicsConstants.CURSOR_Z_OFFSET + g.getZ());

			cursorPosition = position;
		}
	}

}
