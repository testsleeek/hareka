package org.kareha.hareka.graphics;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

public final class IconPainter {

	private static final Logger logger = Logger.getLogger(IconPainter.class.getName());

	private final String path;

	private boolean loaded;
	private boolean loadError;
	private BufferedImage rawImage;

	IconPainter(final String path) {
		this.path = path;
	}

	public void reload() {
		loaded = false;
		loadError = false;
	}

	void loadImages() {
		try (final InputStream in = IconPainter.class.getClassLoader().getResourceAsStream(path)) {
			if (in == null) {
				logger.severe(path + " not found");
				loaded = true;
				loadError = true;
				return;
			}
			rawImage = ImageIO.read(in);
		} catch (final IOException e) {
			logger.log(Level.SEVERE, "", e);
			loaded = true;
			loadError = true;
			return;
		}

		loaded = true;
	}

	public void paint(final Graphics g, final int x, final int y) {
		if (!loaded) {
			loadImages();
		}
		if (loadError) {
			return;
		}

		g.drawImage(rawImage, x - rawImage.getWidth() / 2, y - rawImage.getHeight() / 2, null);
	}

}
