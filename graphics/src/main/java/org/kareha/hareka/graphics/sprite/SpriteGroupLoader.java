package org.kareha.hareka.graphics.sprite;

import java.awt.GraphicsConfiguration;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class SpriteGroupLoader {

	private final String directory;
	private final GraphicsConfiguration gc;
	private final Map<String, Collection<SpriteTemplate>> cache = new HashMap<>();

	public SpriteGroupLoader(final String directory, final GraphicsConfiguration gc) {
		this.directory = directory;
		this.gc = gc;
	}

	public Collection<SpriteTemplate> load(final String id) {
		Collection<SpriteTemplate> template = cache.get(id);
		if (template != null) {
			return template;
		}

		template = SpriteTemplateLoader.loadTemplates(directory + "/" + id, "sprites.xml", gc);
		if (template.isEmpty()) {
			if (!id.equals("Undefined")) {
				template = load("Undefined");
			}
		}

		cache.put(id, template);

		return template;
	}

}
