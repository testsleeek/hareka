package org.kareha.hareka.graphics.model;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.kareha.hareka.util.JaxbUtil;

@XmlRootElement(name = "model")
@XmlAccessorType(XmlAccessType.NONE)
public class ModelTemplate {

	private static final JAXBContext jaxb;

	static {
		try {
			jaxb = JAXBContext.newInstance(ModelTemplate.class);
		} catch (final JAXBException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	@XmlElement
	private WayType wayType;
	@XmlElement
	private int standFrames;
	@XmlElement
	private float standFrequency;
	@XmlElement
	private int walkFrames;
	@XmlElement
	private float walkFrequency;
	@XmlElement
	private int deadFrames;
	@XmlElement
	private float deadFrequency;

	public static ModelTemplate load(final InputStream in) throws JAXBException {
		return JaxbUtil.unmarshal(in, jaxb);
	}

	WayType getWayType() {
		return wayType;
	}

	int getStandFrameNumber(final float phase) {
		float p = phase * standFrequency;
		return (int) (standFrames * (p - (int) p));
	}

	int getWalkFrameNumber(final float phase) {
		float p = phase * walkFrequency;
		return (int) (walkFrames * (p - (int) p));
	}

	int getDeadFrameNumber(final float phase) {
		float p = phase * deadFrequency;
		return (int) (deadFrames * (p - (int) p));
	}

}
