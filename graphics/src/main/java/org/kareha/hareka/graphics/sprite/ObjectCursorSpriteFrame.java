package org.kareha.hareka.graphics.sprite;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.Rectangle;

public class ObjectCursorSpriteFrame implements SpriteFrame {

	@Override
	public int getCenterX() {
		return 0;
	}

	@Override
	public int getCenterY() {
		return 0;
	}

	@Override
	public int getCenterZ() {
		return 0;
	}

	@Override
	public void paint(final Graphics g, final int x, final int y) {
		final Polygon p = new Polygon();
		p.addPoint(x, y - 28);
		p.addPoint(x - 7, y - 21);
		p.addPoint(x, y);
		p.addPoint(x + 7, y - 21);
		g.setColor(Color.CYAN);
		g.fillPolygon(p);
		g.setColor(Color.WHITE);
		g.drawPolygon(p);
	}

	@Override
	public boolean contains(final int x, final int y) {
		return false;
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(-7, -28, 15, 28);
	}

}
